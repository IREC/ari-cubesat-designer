<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="50" unitdist="mil" unit="mil" style="lines" multiple="1" display="yes" altdistance="25" altunitdist="mil" altunit="mil"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="template">
<packages>
<package name="LQFP100">
<circle x="-6.25" y="6" radius="0.25" width="0.254" layer="21"/>
<wire x1="-6.274" y1="6.883" x2="6.883" y2="6.883" width="0.254" layer="21"/>
<wire x1="6.883" y1="6.883" x2="6.883" y2="-6.883" width="0.254" layer="21"/>
<wire x1="6.883" y1="-6.883" x2="-6.883" y2="-6.883" width="0.254" layer="21"/>
<wire x1="-6.883" y1="-6.883" x2="-6.883" y2="6.274" width="0.254" layer="21"/>
<wire x1="-6.883" y1="6.274" x2="-6.274" y2="6.883" width="0.254" layer="21"/>
<rectangle x1="-8.131" y1="5.8476" x2="-7.0896" y2="6.1524" layer="51"/>
<rectangle x1="-7.115" y1="5.8476" x2="-6.861" y2="6.1524" layer="21"/>
<rectangle x1="-8.131" y1="5.3476" x2="-7.0896" y2="5.6524" layer="51"/>
<rectangle x1="-7.115" y1="5.3476" x2="-6.861" y2="5.6524" layer="21"/>
<rectangle x1="-8.131" y1="4.8476" x2="-7.0896" y2="5.1524" layer="51"/>
<rectangle x1="-7.115" y1="4.8476" x2="-6.861" y2="5.1524" layer="21"/>
<rectangle x1="-8.131" y1="4.3476" x2="-7.0896" y2="4.6524" layer="51"/>
<rectangle x1="-7.115" y1="4.3476" x2="-6.861" y2="4.6524" layer="21"/>
<rectangle x1="-8.131" y1="3.8476" x2="-7.0896" y2="4.1524" layer="51"/>
<rectangle x1="-7.115" y1="3.8476" x2="-6.861" y2="4.1524" layer="21"/>
<rectangle x1="-8.131" y1="3.3476" x2="-7.0896" y2="3.6524" layer="51"/>
<rectangle x1="-7.115" y1="3.3476" x2="-6.861" y2="3.6524" layer="21"/>
<rectangle x1="-8.131" y1="2.8476" x2="-7.0896" y2="3.1524" layer="51"/>
<rectangle x1="-7.115" y1="2.8476" x2="-6.861" y2="3.1524" layer="21"/>
<rectangle x1="-8.131" y1="2.3476" x2="-7.0896" y2="2.6524" layer="51"/>
<rectangle x1="-7.115" y1="2.3476" x2="-6.861" y2="2.6524" layer="21"/>
<rectangle x1="-8.131" y1="1.8476" x2="-7.0896" y2="2.1524" layer="51"/>
<rectangle x1="-7.115" y1="1.8476" x2="-6.861" y2="2.1524" layer="21"/>
<rectangle x1="-8.131" y1="1.3476" x2="-7.0896" y2="1.6524" layer="51"/>
<rectangle x1="-7.115" y1="1.3476" x2="-6.861" y2="1.6524" layer="21"/>
<rectangle x1="-8.131" y1="0.8476" x2="-7.0896" y2="1.1524" layer="51"/>
<rectangle x1="-7.115" y1="0.8476" x2="-6.861" y2="1.1524" layer="21"/>
<rectangle x1="-8.131" y1="0.3476" x2="-7.0896" y2="0.6524" layer="51"/>
<rectangle x1="-7.115" y1="0.3476" x2="-6.861" y2="0.6524" layer="21"/>
<rectangle x1="-8.131" y1="-0.1524" x2="-7.0896" y2="0.1524" layer="51"/>
<rectangle x1="-7.115" y1="-0.1524" x2="-6.861" y2="0.1524" layer="21"/>
<rectangle x1="-8.131" y1="-0.6524" x2="-7.0896" y2="-0.3476" layer="51"/>
<rectangle x1="-7.115" y1="-0.6524" x2="-6.861" y2="-0.3476" layer="21"/>
<rectangle x1="-8.131" y1="-1.1524" x2="-7.0896" y2="-0.8476" layer="51"/>
<rectangle x1="-7.115" y1="-1.1524" x2="-6.861" y2="-0.8476" layer="21"/>
<rectangle x1="-8.131" y1="-1.6524" x2="-7.0896" y2="-1.3476" layer="51"/>
<rectangle x1="-7.115" y1="-1.6524" x2="-6.861" y2="-1.3476" layer="21"/>
<rectangle x1="-8.131" y1="-2.1524" x2="-7.0896" y2="-1.8476" layer="51"/>
<rectangle x1="-7.115" y1="-2.1524" x2="-6.861" y2="-1.8476" layer="21"/>
<rectangle x1="-8.131" y1="-2.6524" x2="-7.0896" y2="-2.3476" layer="51"/>
<rectangle x1="-7.115" y1="-2.6524" x2="-6.861" y2="-2.3476" layer="21"/>
<rectangle x1="-8.131" y1="-3.1524" x2="-7.0896" y2="-2.8476" layer="51"/>
<rectangle x1="-7.115" y1="-3.1524" x2="-6.861" y2="-2.8476" layer="21"/>
<rectangle x1="-8.131" y1="-3.6524" x2="-7.0896" y2="-3.3476" layer="51"/>
<rectangle x1="-7.115" y1="-3.6524" x2="-6.861" y2="-3.3476" layer="21"/>
<rectangle x1="-8.131" y1="-4.1524" x2="-7.0896" y2="-3.8476" layer="51"/>
<rectangle x1="-7.115" y1="-4.1524" x2="-6.861" y2="-3.8476" layer="21"/>
<rectangle x1="-8.131" y1="-4.6524" x2="-7.0896" y2="-4.3476" layer="51"/>
<rectangle x1="-7.115" y1="-4.6524" x2="-6.861" y2="-4.3476" layer="21"/>
<rectangle x1="-8.131" y1="-5.1524" x2="-7.0896" y2="-4.8476" layer="51"/>
<rectangle x1="-7.115" y1="-5.1524" x2="-6.861" y2="-4.8476" layer="21"/>
<rectangle x1="-8.131" y1="-5.6524" x2="-7.0896" y2="-5.3476" layer="51"/>
<rectangle x1="-7.115" y1="-5.6524" x2="-6.861" y2="-5.3476" layer="21"/>
<rectangle x1="-8.131" y1="-6.1524" x2="-7.0896" y2="-5.8476" layer="51"/>
<rectangle x1="-7.115" y1="-6.1524" x2="-6.861" y2="-5.8476" layer="21"/>
<rectangle x1="-6.1524" y1="-8.131" x2="-5.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-6.1524" y1="-7.115" x2="-5.8476" y2="-6.861" layer="21"/>
<rectangle x1="-5.6524" y1="-8.131" x2="-5.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-5.6524" y1="-7.115" x2="-5.3476" y2="-6.861" layer="21"/>
<rectangle x1="-5.1524" y1="-8.131" x2="-4.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-5.1524" y1="-7.115" x2="-4.8476" y2="-6.861" layer="21"/>
<rectangle x1="-4.6524" y1="-8.131" x2="-4.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-4.6524" y1="-7.115" x2="-4.3476" y2="-6.861" layer="21"/>
<rectangle x1="-4.1524" y1="-8.131" x2="-3.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-4.1524" y1="-7.115" x2="-3.8476" y2="-6.861" layer="21"/>
<rectangle x1="-3.6524" y1="-8.131" x2="-3.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-3.6524" y1="-7.115" x2="-3.3476" y2="-6.861" layer="21"/>
<rectangle x1="-3.1524" y1="-8.131" x2="-2.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-3.1524" y1="-7.115" x2="-2.8476" y2="-6.861" layer="21"/>
<rectangle x1="-2.6524" y1="-8.131" x2="-2.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-2.6524" y1="-7.115" x2="-2.3476" y2="-6.861" layer="21"/>
<rectangle x1="-2.1524" y1="-8.131" x2="-1.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-2.1524" y1="-7.115" x2="-1.8476" y2="-6.861" layer="21"/>
<rectangle x1="-1.6524" y1="-8.131" x2="-1.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-1.6524" y1="-7.115" x2="-1.3476" y2="-6.861" layer="21"/>
<rectangle x1="-1.1524" y1="-8.131" x2="-0.8476" y2="-7.0896" layer="51"/>
<rectangle x1="-1.1524" y1="-7.115" x2="-0.8476" y2="-6.861" layer="21"/>
<rectangle x1="-0.6524" y1="-8.131" x2="-0.3476" y2="-7.0896" layer="51"/>
<rectangle x1="-0.6524" y1="-7.115" x2="-0.3476" y2="-6.861" layer="21"/>
<rectangle x1="-0.1524" y1="-8.131" x2="0.1524" y2="-7.0896" layer="51"/>
<rectangle x1="-0.1524" y1="-7.115" x2="0.1524" y2="-6.861" layer="21"/>
<rectangle x1="0.3476" y1="-8.131" x2="0.6524" y2="-7.0896" layer="51"/>
<rectangle x1="0.3476" y1="-7.115" x2="0.6524" y2="-6.861" layer="21"/>
<rectangle x1="0.8476" y1="-8.131" x2="1.1524" y2="-7.0896" layer="51"/>
<rectangle x1="0.8476" y1="-7.115" x2="1.1524" y2="-6.861" layer="21"/>
<rectangle x1="1.3476" y1="-8.131" x2="1.6524" y2="-7.0896" layer="51"/>
<rectangle x1="1.3476" y1="-7.115" x2="1.6524" y2="-6.861" layer="21"/>
<rectangle x1="1.8476" y1="-8.131" x2="2.1524" y2="-7.0896" layer="51"/>
<rectangle x1="1.8476" y1="-7.115" x2="2.1524" y2="-6.861" layer="21"/>
<rectangle x1="2.3476" y1="-8.131" x2="2.6524" y2="-7.0896" layer="51"/>
<rectangle x1="2.3476" y1="-7.115" x2="2.6524" y2="-6.861" layer="21"/>
<rectangle x1="2.8476" y1="-8.131" x2="3.1524" y2="-7.0896" layer="51"/>
<rectangle x1="2.8476" y1="-7.115" x2="3.1524" y2="-6.861" layer="21"/>
<rectangle x1="3.3476" y1="-8.131" x2="3.6524" y2="-7.0896" layer="51"/>
<rectangle x1="3.3476" y1="-7.115" x2="3.6524" y2="-6.861" layer="21"/>
<rectangle x1="3.8476" y1="-8.131" x2="4.1524" y2="-7.0896" layer="51"/>
<rectangle x1="3.8476" y1="-7.115" x2="4.1524" y2="-6.861" layer="21"/>
<rectangle x1="4.3476" y1="-8.131" x2="4.6524" y2="-7.0896" layer="51"/>
<rectangle x1="4.3476" y1="-7.115" x2="4.6524" y2="-6.861" layer="21"/>
<rectangle x1="4.8476" y1="-8.131" x2="5.1524" y2="-7.0896" layer="51"/>
<rectangle x1="4.8476" y1="-7.115" x2="5.1524" y2="-6.861" layer="21"/>
<rectangle x1="5.3476" y1="-8.131" x2="5.6524" y2="-7.0896" layer="51"/>
<rectangle x1="5.3476" y1="-7.115" x2="5.6524" y2="-6.861" layer="21"/>
<rectangle x1="5.8476" y1="-8.131" x2="6.1524" y2="-7.0896" layer="51"/>
<rectangle x1="5.8476" y1="-7.115" x2="6.1524" y2="-6.861" layer="21"/>
<rectangle x1="7.0896" y1="-6.1524" x2="8.131" y2="-5.8476" layer="51"/>
<rectangle x1="6.861" y1="-6.1524" x2="7.115" y2="-5.8476" layer="21"/>
<rectangle x1="7.0896" y1="-5.6524" x2="8.131" y2="-5.3476" layer="51"/>
<rectangle x1="6.861" y1="-5.6524" x2="7.115" y2="-5.3476" layer="21"/>
<rectangle x1="7.0896" y1="-5.1524" x2="8.131" y2="-4.8476" layer="51"/>
<rectangle x1="6.861" y1="-5.1524" x2="7.115" y2="-4.8476" layer="21"/>
<rectangle x1="7.0896" y1="-4.6524" x2="8.131" y2="-4.3476" layer="51"/>
<rectangle x1="6.861" y1="-4.6524" x2="7.115" y2="-4.3476" layer="21"/>
<rectangle x1="7.0896" y1="-4.1524" x2="8.131" y2="-3.8476" layer="51"/>
<rectangle x1="6.861" y1="-4.1524" x2="7.115" y2="-3.8476" layer="21"/>
<rectangle x1="7.0896" y1="-3.6524" x2="8.131" y2="-3.3476" layer="51"/>
<rectangle x1="6.861" y1="-3.6524" x2="7.115" y2="-3.3476" layer="21"/>
<rectangle x1="7.0896" y1="-3.1524" x2="8.131" y2="-2.8476" layer="51"/>
<rectangle x1="6.861" y1="-3.1524" x2="7.115" y2="-2.8476" layer="21"/>
<rectangle x1="7.0896" y1="-2.6524" x2="8.131" y2="-2.3476" layer="51"/>
<rectangle x1="6.861" y1="-2.6524" x2="7.115" y2="-2.3476" layer="21"/>
<rectangle x1="7.0896" y1="-2.1524" x2="8.131" y2="-1.8476" layer="51"/>
<rectangle x1="6.861" y1="-2.1524" x2="7.115" y2="-1.8476" layer="21"/>
<rectangle x1="7.0896" y1="-1.6524" x2="8.131" y2="-1.3476" layer="51"/>
<rectangle x1="6.861" y1="-1.6524" x2="7.115" y2="-1.3476" layer="21"/>
<rectangle x1="7.0896" y1="-1.1524" x2="8.131" y2="-0.8476" layer="51"/>
<rectangle x1="6.861" y1="-1.1524" x2="7.115" y2="-0.8476" layer="21"/>
<rectangle x1="7.0896" y1="-0.6524" x2="8.131" y2="-0.3476" layer="51"/>
<rectangle x1="6.861" y1="-0.6524" x2="7.115" y2="-0.3476" layer="21"/>
<rectangle x1="7.0896" y1="-0.1524" x2="8.131" y2="0.1524" layer="51"/>
<rectangle x1="6.861" y1="-0.1524" x2="7.115" y2="0.1524" layer="21"/>
<rectangle x1="7.0896" y1="0.3476" x2="8.131" y2="0.6524" layer="51"/>
<rectangle x1="6.861" y1="0.3476" x2="7.115" y2="0.6524" layer="21"/>
<rectangle x1="7.0896" y1="0.8476" x2="8.131" y2="1.1524" layer="51"/>
<rectangle x1="6.861" y1="0.8476" x2="7.115" y2="1.1524" layer="21"/>
<rectangle x1="7.0896" y1="1.3476" x2="8.131" y2="1.6524" layer="51"/>
<rectangle x1="6.861" y1="1.3476" x2="7.115" y2="1.6524" layer="21"/>
<rectangle x1="7.0896" y1="1.8476" x2="8.131" y2="2.1524" layer="51"/>
<rectangle x1="6.861" y1="1.8476" x2="7.115" y2="2.1524" layer="21"/>
<rectangle x1="7.0896" y1="2.3476" x2="8.131" y2="2.6524" layer="51"/>
<rectangle x1="6.861" y1="2.3476" x2="7.115" y2="2.6524" layer="21"/>
<rectangle x1="7.0896" y1="2.8476" x2="8.131" y2="3.1524" layer="51"/>
<rectangle x1="6.861" y1="2.8476" x2="7.115" y2="3.1524" layer="21"/>
<rectangle x1="7.0896" y1="3.3476" x2="8.131" y2="3.6524" layer="51"/>
<rectangle x1="6.861" y1="3.3476" x2="7.115" y2="3.6524" layer="21"/>
<rectangle x1="7.0896" y1="3.8476" x2="8.131" y2="4.1524" layer="51"/>
<rectangle x1="6.861" y1="3.8476" x2="7.115" y2="4.1524" layer="21"/>
<rectangle x1="7.0896" y1="4.3476" x2="8.131" y2="4.6524" layer="51"/>
<rectangle x1="6.861" y1="4.3476" x2="7.115" y2="4.6524" layer="21"/>
<rectangle x1="7.0896" y1="4.8476" x2="8.131" y2="5.1524" layer="51"/>
<rectangle x1="6.861" y1="4.8476" x2="7.115" y2="5.1524" layer="21"/>
<rectangle x1="7.0896" y1="5.3476" x2="8.131" y2="5.6524" layer="51"/>
<rectangle x1="6.861" y1="5.3476" x2="7.115" y2="5.6524" layer="21"/>
<rectangle x1="7.0896" y1="5.8476" x2="8.131" y2="6.1524" layer="51"/>
<rectangle x1="6.861" y1="5.8476" x2="7.115" y2="6.1524" layer="21"/>
<rectangle x1="5.8476" y1="7.0896" x2="6.1524" y2="8.131" layer="51"/>
<rectangle x1="5.8476" y1="6.861" x2="6.1524" y2="7.115" layer="21"/>
<rectangle x1="5.3476" y1="7.0896" x2="5.6524" y2="8.131" layer="51"/>
<rectangle x1="5.3476" y1="6.861" x2="5.6524" y2="7.115" layer="21"/>
<rectangle x1="4.8476" y1="7.0896" x2="5.1524" y2="8.131" layer="51"/>
<rectangle x1="4.8476" y1="6.861" x2="5.1524" y2="7.115" layer="21"/>
<rectangle x1="4.3476" y1="7.0896" x2="4.6524" y2="8.131" layer="51"/>
<rectangle x1="4.3476" y1="6.861" x2="4.6524" y2="7.115" layer="21"/>
<rectangle x1="3.8476" y1="7.0896" x2="4.1524" y2="8.131" layer="51"/>
<rectangle x1="3.8476" y1="6.861" x2="4.1524" y2="7.115" layer="21"/>
<rectangle x1="3.3476" y1="7.0896" x2="3.6524" y2="8.131" layer="51"/>
<rectangle x1="3.3476" y1="6.861" x2="3.6524" y2="7.115" layer="21"/>
<rectangle x1="2.8476" y1="7.0896" x2="3.1524" y2="8.131" layer="51"/>
<rectangle x1="2.8476" y1="6.861" x2="3.1524" y2="7.115" layer="21"/>
<rectangle x1="2.3476" y1="7.0896" x2="2.6524" y2="8.131" layer="51"/>
<rectangle x1="2.3476" y1="6.861" x2="2.6524" y2="7.115" layer="21"/>
<rectangle x1="1.8476" y1="7.0896" x2="2.1524" y2="8.131" layer="51"/>
<rectangle x1="1.8476" y1="6.861" x2="2.1524" y2="7.115" layer="21"/>
<rectangle x1="1.3476" y1="7.0896" x2="1.6524" y2="8.131" layer="51"/>
<rectangle x1="1.3476" y1="6.861" x2="1.6524" y2="7.115" layer="21"/>
<rectangle x1="0.8476" y1="7.0896" x2="1.1524" y2="8.131" layer="51"/>
<rectangle x1="0.8476" y1="6.861" x2="1.1524" y2="7.115" layer="21"/>
<rectangle x1="0.3476" y1="7.0896" x2="0.6524" y2="8.131" layer="51"/>
<rectangle x1="0.3476" y1="6.861" x2="0.6524" y2="7.115" layer="21"/>
<rectangle x1="-0.1524" y1="7.0896" x2="0.1524" y2="8.131" layer="51"/>
<rectangle x1="-0.1524" y1="6.861" x2="0.1524" y2="7.115" layer="21"/>
<rectangle x1="-0.6524" y1="7.0896" x2="-0.3476" y2="8.131" layer="51"/>
<rectangle x1="-0.6524" y1="6.861" x2="-0.3476" y2="7.115" layer="21"/>
<rectangle x1="-1.1524" y1="7.0896" x2="-0.8476" y2="8.131" layer="51"/>
<rectangle x1="-1.1524" y1="6.861" x2="-0.8476" y2="7.115" layer="21"/>
<rectangle x1="-1.6524" y1="7.0896" x2="-1.3476" y2="8.131" layer="51"/>
<rectangle x1="-1.6524" y1="6.861" x2="-1.3476" y2="7.115" layer="21"/>
<rectangle x1="-2.1524" y1="7.0896" x2="-1.8476" y2="8.131" layer="51"/>
<rectangle x1="-2.1524" y1="6.861" x2="-1.8476" y2="7.115" layer="21"/>
<rectangle x1="-2.6524" y1="7.0896" x2="-2.3476" y2="8.131" layer="51"/>
<rectangle x1="-2.6524" y1="6.861" x2="-2.3476" y2="7.115" layer="21"/>
<rectangle x1="-3.1524" y1="7.0896" x2="-2.8476" y2="8.131" layer="51"/>
<rectangle x1="-3.1524" y1="6.861" x2="-2.8476" y2="7.115" layer="21"/>
<rectangle x1="-3.6524" y1="7.0896" x2="-3.3476" y2="8.131" layer="51"/>
<rectangle x1="-3.6524" y1="6.861" x2="-3.3476" y2="7.115" layer="21"/>
<rectangle x1="-4.1524" y1="7.0896" x2="-3.8476" y2="8.131" layer="51"/>
<rectangle x1="-4.1524" y1="6.861" x2="-3.8476" y2="7.115" layer="21"/>
<rectangle x1="-4.6524" y1="7.0896" x2="-4.3476" y2="8.131" layer="51"/>
<rectangle x1="-4.6524" y1="6.861" x2="-4.3476" y2="7.115" layer="21"/>
<rectangle x1="-5.1524" y1="7.0896" x2="-4.8476" y2="8.131" layer="51"/>
<rectangle x1="-5.1524" y1="6.861" x2="-4.8476" y2="7.115" layer="21"/>
<rectangle x1="-5.6524" y1="7.0896" x2="-5.3476" y2="8.131" layer="51"/>
<rectangle x1="-5.6524" y1="6.861" x2="-5.3476" y2="7.115" layer="21"/>
<rectangle x1="-6.1524" y1="7.0896" x2="-5.8476" y2="8.131" layer="51"/>
<rectangle x1="-6.1524" y1="6.861" x2="-5.8476" y2="7.115" layer="21"/>
<smd name="1" x="-7.75" y="6" dx="1.2" dy="0.32" layer="1"/>
<smd name="2" x="-7.75" y="5.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="3" x="-7.75" y="5" dx="1.2" dy="0.32" layer="1"/>
<smd name="4" x="-7.75" y="4.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="5" x="-7.75" y="4" dx="1.2" dy="0.32" layer="1"/>
<smd name="6" x="-7.75" y="3.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="7" x="-7.75" y="3" dx="1.2" dy="0.32" layer="1"/>
<smd name="8" x="-7.75" y="2.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="9" x="-7.75" y="2" dx="1.2" dy="0.32" layer="1"/>
<smd name="10" x="-7.75" y="1.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="11" x="-7.75" y="1" dx="1.2" dy="0.32" layer="1"/>
<smd name="12" x="-7.75" y="0.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="13" x="-7.75" y="0" dx="1.2" dy="0.32" layer="1"/>
<smd name="14" x="-7.75" y="-0.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="15" x="-7.75" y="-1" dx="1.2" dy="0.32" layer="1"/>
<smd name="16" x="-7.75" y="-1.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="17" x="-7.75" y="-2" dx="1.2" dy="0.32" layer="1"/>
<smd name="18" x="-7.75" y="-2.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="19" x="-7.75" y="-3" dx="1.2" dy="0.32" layer="1"/>
<smd name="20" x="-7.75" y="-3.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="21" x="-7.75" y="-4" dx="1.2" dy="0.32" layer="1"/>
<smd name="22" x="-7.75" y="-4.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="23" x="-7.75" y="-5" dx="1.2" dy="0.32" layer="1"/>
<smd name="24" x="-7.75" y="-5.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="25" x="-7.75" y="-6" dx="1.2" dy="0.32" layer="1"/>
<smd name="26" x="-6" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="27" x="-5.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="28" x="-5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="29" x="-4.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="30" x="-4" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="31" x="-3.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="32" x="-3" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="33" x="-2.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="34" x="-2" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="35" x="-1.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="36" x="-1" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="37" x="-0.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="38" x="0" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="39" x="0.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="40" x="1" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="41" x="1.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="42" x="2" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="43" x="2.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="44" x="3" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="45" x="3.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="46" x="4" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="47" x="4.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="48" x="5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="49" x="5.5" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="50" x="6" y="-7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="51" x="7.75" y="-6" dx="1.2" dy="0.32" layer="1"/>
<smd name="52" x="7.75" y="-5.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="53" x="7.75" y="-5" dx="1.2" dy="0.32" layer="1"/>
<smd name="54" x="7.75" y="-4.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="55" x="7.75" y="-4" dx="1.2" dy="0.32" layer="1"/>
<smd name="56" x="7.75" y="-3.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="57" x="7.75" y="-3" dx="1.2" dy="0.32" layer="1"/>
<smd name="58" x="7.75" y="-2.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="59" x="7.75" y="-2" dx="1.2" dy="0.32" layer="1"/>
<smd name="60" x="7.75" y="-1.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="61" x="7.75" y="-1" dx="1.2" dy="0.32" layer="1"/>
<smd name="62" x="7.75" y="-0.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="63" x="7.75" y="0" dx="1.2" dy="0.32" layer="1"/>
<smd name="64" x="7.75" y="0.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="65" x="7.75" y="1" dx="1.2" dy="0.32" layer="1"/>
<smd name="66" x="7.75" y="1.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="67" x="7.75" y="2" dx="1.2" dy="0.32" layer="1"/>
<smd name="68" x="7.75" y="2.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="69" x="7.75" y="3" dx="1.2" dy="0.32" layer="1"/>
<smd name="70" x="7.75" y="3.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="71" x="7.75" y="4" dx="1.2" dy="0.32" layer="1"/>
<smd name="72" x="7.75" y="4.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="73" x="7.75" y="5" dx="1.2" dy="0.32" layer="1"/>
<smd name="74" x="7.75" y="5.5" dx="1.2" dy="0.32" layer="1"/>
<smd name="75" x="7.75" y="6" dx="1.2" dy="0.32" layer="1"/>
<smd name="76" x="6" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="77" x="5.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="78" x="5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="79" x="4.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="80" x="4" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="81" x="3.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="82" x="3" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="83" x="2.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="84" x="2" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="85" x="1.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="86" x="1" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="87" x="0.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="88" x="0" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="89" x="-0.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="90" x="-1" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="91" x="-1.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="92" x="-2" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="93" x="-2.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="94" x="-3" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="95" x="-3.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="96" x="-4" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="97" x="-4.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="98" x="-5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="99" x="-5.5" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<smd name="100" x="-6" y="7.75" dx="0.32" dy="1.2" layer="1"/>
<text x="-1.905" y="8.89" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-0.635" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="24P-0.5-16.2X5.8X2.0H">
<smd name="1" x="-5.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-5.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="-4.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-4.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-3.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-3.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-2.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-2.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-1.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="-1.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-0.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-0.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="0.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="1.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="1.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="2.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="2.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="3.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="3.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="4.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="4.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="5.25" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="5.75" y="0" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<wire x1="-6" y1="-0.45" x2="-8.1" y2="-0.45" width="0.127" layer="21"/>
<wire x1="-8.1" y1="-0.45" x2="-8.1" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.1" y1="-4.85" x2="-7.75" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-7.75" y1="-4.85" x2="-7.75" y2="-5.0635" width="0.127" layer="21"/>
<wire x1="-7.75" y1="-5.0635" x2="-6.25" y2="-5.0635" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-5.35" x2="6.25" y2="-5.35" width="0.127" layer="21"/>
<wire x1="6.25" y1="-5.0635" x2="7.75" y2="-5.0635" width="0.127" layer="21"/>
<wire x1="7.75" y1="-5.0635" x2="7.75" y2="-4.85" width="0.127" layer="21"/>
<wire x1="7.75" y1="-4.85" x2="8.1" y2="-4.85" width="0.127" layer="21"/>
<wire x1="8.1" y1="-4.85" x2="8.1" y2="-0.45" width="0.127" layer="21"/>
<wire x1="8.1" y1="-0.45" x2="6" y2="-0.45" width="0.127" layer="21"/>
<smd name="P1" x="-7.6" y="-2.9" dx="2.2" dy="2.1" layer="1" rot="R90"/>
<smd name="P2" x="7.6" y="-2.9" dx="2.2" dy="2.1" layer="1" rot="R90"/>
<wire x1="-6.25" y1="-5.0635" x2="-6.25" y2="-5.35" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-5.35" x2="-6.25" y2="-6.8" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-6.8" x2="-3" y2="-6.8" width="0.127" layer="21"/>
<wire x1="-3" y1="-6.8" x2="-3" y2="-15.5" width="0.127" layer="21"/>
<wire x1="-3" y1="-15.5" x2="-4" y2="-15.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-15.5" x2="-4" y2="-23.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-23.5" x2="4" y2="-23.5" width="0.127" layer="21"/>
<wire x1="4" y1="-23.5" x2="4" y2="-15.5" width="0.127" layer="21"/>
<wire x1="4" y1="-15.5" x2="3" y2="-15.5" width="0.127" layer="21"/>
<wire x1="3" y1="-15.5" x2="3" y2="-6.8" width="0.127" layer="21"/>
<wire x1="3" y1="-6.8" x2="6.25" y2="-6.8" width="0.127" layer="21"/>
<wire x1="6.25" y1="-6.8" x2="6.25" y2="-5.35" width="0.127" layer="21"/>
<wire x1="6.25" y1="-5.35" x2="6.25" y2="-5.0635" width="0.127" layer="21"/>
<wire x1="-3" y1="-15.5" x2="3" y2="-15.5" width="0.127" layer="21"/>
<rectangle x1="-8.1" y1="-5.35" x2="8.1" y2="-0.5" layer="39"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="0.635" layer="25" ratio="10">&gt;VALUE</text>
<text x="-6.096" y="-1.905" size="0.889" layer="21" ratio="11">1</text>
<text x="4.699" y="-1.905" size="0.889" layer="21" ratio="11">24</text>
</package>
</packages>
<symbols>
<symbol name="VC0706">
<pin name="CS_SDA" x="-45.72" y="30.48" length="short"/>
<pin name="GPIO13" x="-45.72" y="27.94" length="short"/>
<pin name="CS_SCK" x="-45.72" y="25.4" length="short"/>
<pin name="GPIO12" x="-45.72" y="22.86" length="short"/>
<pin name="CS_HSYNC" x="-45.72" y="20.32" length="short"/>
<pin name="MCU_WEN" x="-45.72" y="17.78" length="short"/>
<pin name="CS_VSYNC" x="-45.72" y="15.24" length="short"/>
<pin name="MCU_A15" x="-45.72" y="12.7" length="short"/>
<pin name="MCU_A14" x="-45.72" y="10.16" length="short"/>
<pin name="CS_PCLK" x="-45.72" y="7.62" length="short"/>
<pin name="CS_CLK" x="-45.72" y="5.08" length="short"/>
<pin name="GPIO9" x="-45.72" y="2.54" length="short"/>
<pin name="CS_D0" x="-45.72" y="0" length="short"/>
<pin name="MCU_DCEN" x="-45.72" y="-2.54" length="short"/>
<pin name="CS_D1" x="-45.72" y="-5.08" length="short"/>
<pin name="CS_D2" x="-45.72" y="-7.62" length="short"/>
<pin name="MCU_A13" x="-45.72" y="-10.16" length="short"/>
<pin name="MCU_A12" x="-45.72" y="-12.7" length="short"/>
<pin name="CS_D3" x="-45.72" y="-15.24" length="short"/>
<pin name="CS_D4" x="-45.72" y="-17.78" length="short"/>
<pin name="MCU_A11" x="-45.72" y="-20.32" length="short"/>
<pin name="MCU_A10" x="-45.72" y="-22.86" length="short"/>
<pin name="CS_D5" x="-45.72" y="-25.4" length="short"/>
<pin name="CS_D6" x="-45.72" y="-27.94" length="short"/>
<pin name="MCU_A9" x="-45.72" y="-30.48" length="short"/>
<pin name="GPIO8" x="-30.48" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A8" x="-27.94" y="-45.72" length="short" rot="R90"/>
<pin name="CS_D7" x="-25.4" y="-45.72" length="short" rot="R90"/>
<pin name="CS_D8" x="-22.86" y="-45.72" length="short" rot="R90"/>
<pin name="CS_D9" x="-20.32" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A7" x="-17.78" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A6" x="-15.24" y="-45.72" length="short" rot="R90"/>
<pin name="CS_PWD" x="-12.7" y="-45.72" length="short" rot="R90"/>
<pin name="CS_RSTB" x="-10.16" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A5" x="-7.62" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A4" x="-5.08" y="-45.72" length="short" rot="R90"/>
<pin name="GPIO7" x="-2.54" y="-45.72" length="short" rot="R90"/>
<pin name="RSTN" x="0" y="-45.72" length="short" rot="R90"/>
<pin name="GPIO4" x="2.54" y="-45.72" length="short" rot="R90"/>
<pin name="GPIO11" x="5.08" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A3" x="7.62" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A2" x="10.16" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A1" x="12.7" y="-45.72" length="short" rot="R90"/>
<pin name="MCU_A0" x="15.24" y="-45.72" length="short" rot="R90"/>
<pin name="VDD_IO" x="17.78" y="-45.72" length="short" rot="R90"/>
<pin name="VSS" x="20.32" y="-45.72" length="short" rot="R90"/>
<pin name="VDD" x="22.86" y="-45.72" length="short" rot="R90"/>
<pin name="VSSA" x="25.4" y="-45.72" length="short" rot="R90"/>
<pin name="MONA" x="27.94" y="-45.72" length="short" rot="R90"/>
<pin name="VDDA" x="30.48" y="-45.72" length="short" rot="R90"/>
<pin name="VLX" x="45.72" y="-30.48" length="short" rot="R180"/>
<pin name="VSSA@1" x="45.72" y="-27.94" length="short" rot="R180"/>
<pin name="DAC_REXT" x="45.72" y="-25.4" length="short" rot="R180"/>
<pin name="DAC_COMP" x="45.72" y="-22.86" length="short" rot="R180"/>
<pin name="DAC_CVBS" x="45.72" y="-20.32" length="short" rot="R180"/>
<pin name="VSSA_DAC" x="45.72" y="-17.78" length="short" rot="R180"/>
<pin name="VDDA_DAC" x="45.72" y="-15.24" length="short" rot="R180"/>
<pin name="VSSA_PLL" x="45.72" y="-12.7" length="short" rot="R180"/>
<pin name="VDDA_PLL" x="45.72" y="-10.16" length="short" rot="R180"/>
<pin name="TV-D7" x="45.72" y="-7.62" length="short" rot="R180"/>
<pin name="TV_D6" x="45.72" y="-5.08" length="short" rot="R180"/>
<pin name="TV_D5" x="45.72" y="-2.54" length="short" rot="R180"/>
<pin name="TV_D4" x="45.72" y="0" length="short" rot="R180"/>
<pin name="GPIO6" x="45.72" y="2.54" length="short" rot="R180"/>
<pin name="TV_D3" x="45.72" y="5.08" length="short" rot="R180"/>
<pin name="TV_D2" x="45.72" y="7.62" length="short" rot="R180"/>
<pin name="GPIO5" x="45.72" y="10.16" length="short" rot="R180"/>
<pin name="TV_D1" x="45.72" y="12.7" length="short" rot="R180"/>
<pin name="TV_D0" x="45.72" y="15.24" length="short" rot="R180"/>
<pin name="CLK_IN" x="45.72" y="17.78" length="short" rot="R180"/>
<pin name="CLK_OUT" x="45.72" y="20.32" length="short" rot="R180"/>
<pin name="VSS@1" x="45.72" y="22.86" length="short" rot="R180"/>
<pin name="VDD_IO@1" x="45.72" y="25.4" length="short" rot="R180"/>
<pin name="TV_PCLK" x="45.72" y="27.94" length="short" rot="R180"/>
<pin name="TEST" x="45.72" y="30.48" length="short" rot="R180"/>
<pin name="GPIO0/TX" x="30.48" y="45.72" length="short" rot="R270"/>
<pin name="GPIO1/RX" x="27.94" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D0" x="25.4" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D1" x="22.86" y="45.72" length="short" rot="R270"/>
<pin name="UART_TXD" x="20.32" y="45.72" length="short" rot="R270"/>
<pin name="UART_RXD" x="17.78" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D2" x="15.24" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D3" x="12.7" y="45.72" length="short" rot="R270"/>
<pin name="GPIO2" x="10.16" y="45.72" length="short" rot="R270"/>
<pin name="GPIO3" x="7.62" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D4" x="5.08" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D5" x="2.54" y="45.72" length="short" rot="R270"/>
<pin name="GPIO10" x="0" y="45.72" length="short" rot="R270"/>
<pin name="SPI_SS" x="-2.54" y="45.72" length="short" rot="R270"/>
<pin name="SPI_SCK" x="-5.08" y="45.72" length="short" rot="R270"/>
<pin name="SPI_MOSI" x="-7.62" y="45.72" length="short" rot="R270"/>
<pin name="SPI_MISO" x="-10.16" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D6" x="-12.7" y="45.72" length="short" rot="R270"/>
<pin name="MCU_D7" x="-15.24" y="45.72" length="short" rot="R270"/>
<pin name="CS_ENB" x="-17.78" y="45.72" length="short" rot="R270"/>
<pin name="MCU_OEN" x="-20.32" y="45.72" length="short" rot="R270"/>
<pin name="MCU_PCEN" x="-22.86" y="45.72" length="short" rot="R270"/>
<pin name="VDD@1" x="-25.4" y="45.72" length="short" rot="R270"/>
<pin name="VSS@2" x="-27.94" y="45.72" length="short" rot="R270"/>
<pin name="VDD_IO@2" x="-30.48" y="45.72" length="short" rot="R270"/>
<text x="-40.64" y="43.18" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="33.02" y="43.18" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-41.91" y1="41.91" x2="-30.48" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="41.91" x2="-27.94" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="41.91" x2="-25.4" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="41.91" x2="-22.86" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="41.91" x2="-20.32" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="41.91" x2="-17.78" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="41.91" x2="-15.24" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="41.91" x2="-12.7" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="41.91" x2="-10.16" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="41.91" x2="-7.62" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="41.91" x2="-5.08" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="41.91" x2="-2.54" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="41.91" x2="0" y2="41.91" width="0.1524" layer="94"/>
<wire x1="0" y1="41.91" x2="2.54" y2="41.91" width="0.1524" layer="94"/>
<wire x1="2.54" y1="41.91" x2="5.08" y2="41.91" width="0.1524" layer="94"/>
<wire x1="5.08" y1="41.91" x2="7.62" y2="41.91" width="0.1524" layer="94"/>
<wire x1="7.62" y1="41.91" x2="10.16" y2="41.91" width="0.1524" layer="94"/>
<wire x1="10.16" y1="41.91" x2="12.7" y2="41.91" width="0.1524" layer="94"/>
<wire x1="12.7" y1="41.91" x2="15.24" y2="41.91" width="0.1524" layer="94"/>
<wire x1="15.24" y1="41.91" x2="17.78" y2="41.91" width="0.1524" layer="94"/>
<wire x1="17.78" y1="41.91" x2="20.32" y2="41.91" width="0.1524" layer="94"/>
<wire x1="20.32" y1="41.91" x2="22.86" y2="41.91" width="0.1524" layer="94"/>
<wire x1="22.86" y1="41.91" x2="25.4" y2="41.91" width="0.1524" layer="94"/>
<wire x1="25.4" y1="41.91" x2="27.94" y2="41.91" width="0.1524" layer="94"/>
<wire x1="27.94" y1="41.91" x2="30.48" y2="41.91" width="0.1524" layer="94"/>
<wire x1="30.48" y1="41.91" x2="41.91" y2="41.91" width="0.1524" layer="94"/>
<wire x1="41.91" y1="41.91" x2="41.91" y2="30.48" width="0.1524" layer="94"/>
<wire x1="41.91" y1="30.48" x2="41.91" y2="27.94" width="0.1524" layer="94"/>
<wire x1="41.91" y1="27.94" x2="41.91" y2="25.4" width="0.1524" layer="94"/>
<wire x1="41.91" y1="25.4" x2="41.91" y2="22.86" width="0.1524" layer="94"/>
<wire x1="41.91" y1="22.86" x2="41.91" y2="20.32" width="0.1524" layer="94"/>
<wire x1="41.91" y1="20.32" x2="41.91" y2="17.78" width="0.1524" layer="94"/>
<wire x1="41.91" y1="17.78" x2="41.91" y2="15.24" width="0.1524" layer="94"/>
<wire x1="41.91" y1="15.24" x2="41.91" y2="12.7" width="0.1524" layer="94"/>
<wire x1="41.91" y1="12.7" x2="41.91" y2="10.16" width="0.1524" layer="94"/>
<wire x1="41.91" y1="10.16" x2="41.91" y2="7.62" width="0.1524" layer="94"/>
<wire x1="41.91" y1="7.62" x2="41.91" y2="5.08" width="0.1524" layer="94"/>
<wire x1="41.91" y1="5.08" x2="41.91" y2="2.54" width="0.1524" layer="94"/>
<wire x1="41.91" y1="2.54" x2="41.91" y2="0" width="0.1524" layer="94"/>
<wire x1="41.91" y1="0" x2="41.91" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-2.54" x2="41.91" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-5.08" x2="41.91" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-7.62" x2="41.91" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-10.16" x2="41.91" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-12.7" x2="41.91" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-15.24" x2="41.91" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-17.78" x2="41.91" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-20.32" x2="41.91" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-22.86" x2="41.91" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-25.4" x2="41.91" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-27.94" x2="41.91" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-30.48" x2="41.91" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-41.91" x2="30.48" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="30.48" y1="-41.91" x2="27.94" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-41.91" x2="25.4" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-41.91" x2="22.86" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-41.91" x2="20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-41.91" x2="17.78" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-41.91" x2="15.24" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-41.91" x2="12.7" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-41.91" x2="10.16" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-41.91" x2="7.62" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-41.91" x2="5.08" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-41.91" x2="2.54" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-41.91" x2="0" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="0" y1="-41.91" x2="-2.54" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-41.91" x2="-5.08" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-41.91" x2="-7.62" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-41.91" x2="-10.16" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-41.91" x2="-12.7" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-41.91" x2="-15.24" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-41.91" x2="-17.78" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-41.91" x2="-20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-41.91" x2="-22.86" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="-41.91" x2="-25.4" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-41.91" x2="-27.94" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-41.91" x2="-30.48" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="-41.91" x2="-41.91" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-41.91" x2="-41.91" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-30.48" x2="-41.91" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-27.94" x2="-41.91" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-25.4" x2="-41.91" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-22.86" x2="-41.91" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-20.32" x2="-41.91" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-17.78" x2="-41.91" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-15.24" x2="-41.91" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-12.7" x2="-41.91" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-10.16" x2="-41.91" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-7.62" x2="-41.91" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-5.08" x2="-41.91" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="-2.54" x2="-41.91" y2="0" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="0" x2="-41.91" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="2.54" x2="-41.91" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="5.08" x2="-41.91" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="7.62" x2="-41.91" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="10.16" x2="-41.91" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="12.7" x2="-41.91" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="15.24" x2="-41.91" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="17.78" x2="-41.91" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="20.32" x2="-41.91" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="22.86" x2="-41.91" y2="25.4" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="25.4" x2="-41.91" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="27.94" x2="-41.91" y2="30.48" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="30.48" x2="-41.91" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="43.18" x2="-30.48" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="43.18" x2="-27.94" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="43.18" x2="-25.4" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="43.18" x2="-22.86" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="43.18" x2="-20.32" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="43.18" x2="-17.78" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="43.18" x2="-15.24" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="43.18" x2="-12.7" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="43.18" x2="-7.62" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="43.18" x2="-5.08" y2="41.91" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="43.18" x2="-2.54" y2="41.91" width="0.1524" layer="94"/>
<wire x1="0" y1="43.18" x2="0" y2="41.91" width="0.1524" layer="94"/>
<wire x1="2.54" y1="43.18" x2="2.54" y2="41.91" width="0.1524" layer="94"/>
<wire x1="5.08" y1="43.18" x2="5.08" y2="41.91" width="0.1524" layer="94"/>
<wire x1="7.62" y1="43.18" x2="7.62" y2="41.91" width="0.1524" layer="94"/>
<wire x1="10.16" y1="43.18" x2="10.16" y2="41.91" width="0.1524" layer="94"/>
<wire x1="12.7" y1="43.18" x2="12.7" y2="41.91" width="0.1524" layer="94"/>
<wire x1="15.24" y1="43.18" x2="15.24" y2="41.91" width="0.1524" layer="94"/>
<wire x1="17.78" y1="43.18" x2="17.78" y2="41.91" width="0.1524" layer="94"/>
<wire x1="20.32" y1="43.18" x2="20.32" y2="41.91" width="0.1524" layer="94"/>
<wire x1="22.86" y1="43.18" x2="22.86" y2="41.91" width="0.1524" layer="94"/>
<wire x1="25.4" y1="43.18" x2="25.4" y2="41.91" width="0.1524" layer="94"/>
<wire x1="27.94" y1="43.18" x2="27.94" y2="41.91" width="0.1524" layer="94"/>
<wire x1="30.48" y1="43.18" x2="30.48" y2="41.91" width="0.1524" layer="94"/>
<wire x1="43.18" y1="30.48" x2="41.91" y2="30.48" width="0.1524" layer="94"/>
<wire x1="43.18" y1="27.94" x2="41.91" y2="27.94" width="0.1524" layer="94"/>
<wire x1="43.18" y1="25.4" x2="41.91" y2="25.4" width="0.1524" layer="94"/>
<wire x1="43.18" y1="22.86" x2="41.91" y2="22.86" width="0.1524" layer="94"/>
<wire x1="43.18" y1="20.32" x2="41.91" y2="20.32" width="0.1524" layer="94"/>
<wire x1="43.18" y1="17.78" x2="41.91" y2="17.78" width="0.1524" layer="94"/>
<wire x1="43.18" y1="15.24" x2="41.91" y2="15.24" width="0.1524" layer="94"/>
<wire x1="43.18" y1="12.7" x2="41.91" y2="12.7" width="0.1524" layer="94"/>
<wire x1="43.18" y1="10.16" x2="41.91" y2="10.16" width="0.1524" layer="94"/>
<wire x1="43.18" y1="7.62" x2="41.91" y2="7.62" width="0.1524" layer="94"/>
<wire x1="43.18" y1="5.08" x2="41.91" y2="5.08" width="0.1524" layer="94"/>
<wire x1="43.18" y1="2.54" x2="41.91" y2="2.54" width="0.1524" layer="94"/>
<wire x1="43.18" y1="0" x2="41.91" y2="0" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-2.54" x2="41.91" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-5.08" x2="41.91" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-7.62" x2="41.91" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-10.16" x2="41.91" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="41.91" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-15.24" x2="41.91" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-17.78" x2="41.91" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-20.32" x2="41.91" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-22.86" x2="41.91" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-25.4" x2="41.91" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-27.94" x2="41.91" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="43.18" y1="-30.48" x2="41.91" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="30.48" y1="-43.18" x2="30.48" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-43.18" x2="27.94" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-43.18" x2="25.4" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-43.18" x2="22.86" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-43.18" x2="20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-43.18" x2="17.78" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-43.18" x2="15.24" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-43.18" x2="12.7" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-43.18" x2="10.16" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-43.18" x2="7.62" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-43.18" x2="5.08" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-43.18" x2="2.54" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="0" y1="-43.18" x2="0" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-43.18" x2="-2.54" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-43.18" x2="-5.08" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-43.18" x2="-7.62" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-43.18" x2="-10.16" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-43.18" x2="-12.7" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-43.18" x2="-15.24" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-43.18" x2="-17.78" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-43.18" x2="-20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="-43.18" x2="-22.86" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-43.18" x2="-25.4" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-43.18" x2="-27.94" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="-43.18" x2="-30.48" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="30.48" x2="-41.91" y2="30.48" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="27.94" x2="-41.91" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="25.4" x2="-41.91" y2="25.4" width="0.1524" layer="94"/>
<wire x1="-41.91" y1="22.86" x2="-43.18" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="20.32" x2="-41.91" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="17.78" x2="-41.91" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="15.24" x2="-41.91" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="12.7" x2="-41.91" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="10.16" x2="-41.91" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="7.62" x2="-41.91" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="5.08" x2="-41.91" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="2.54" x2="-41.91" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="0" x2="-41.91" y2="0" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-2.54" x2="-41.91" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-5.08" x2="-41.91" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-7.62" x2="-41.91" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-10.16" x2="-41.91" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-12.7" x2="-41.91" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-15.24" x2="-41.91" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-17.78" x2="-41.91" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-20.32" x2="-41.91" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-22.86" x2="-41.91" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-25.4" x2="-41.91" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-27.94" x2="-41.91" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-30.48" x2="-41.91" y2="-30.48" width="0.1524" layer="94"/>
</symbol>
<symbol name="CAMERA-OV7670">
<pin name="Y0" x="-8.89" y="29.21" length="short" direction="out"/>
<pin name="Y1" x="-8.89" y="26.67" length="short" direction="out"/>
<pin name="Y4" x="-8.89" y="24.13" length="short" direction="out"/>
<pin name="Y3" x="-8.89" y="21.59" length="short" direction="out"/>
<pin name="Y5" x="-8.89" y="19.05" length="short" direction="out"/>
<pin name="Y2" x="-8.89" y="16.51" length="short" direction="out"/>
<pin name="Y6" x="-8.89" y="13.97" length="short" direction="out"/>
<pin name="PCLK" x="-8.89" y="11.43" length="short" direction="out"/>
<pin name="Y7" x="-8.89" y="8.89" length="short" direction="out"/>
<pin name="DGND" x="-8.89" y="6.35" length="short" direction="sup"/>
<pin name="Y8" x="-8.89" y="3.81" length="short" direction="out"/>
<pin name="XCLK1" x="-8.89" y="1.27" length="short" direction="in"/>
<pin name="Y9" x="-8.89" y="-1.27" length="short" direction="out"/>
<pin name="DOVDD" x="-8.89" y="-3.81" length="short" direction="sup"/>
<pin name="DVDD" x="-8.89" y="-6.35" length="short" direction="sup"/>
<pin name="HREF" x="-8.89" y="-8.89" length="short" direction="out"/>
<pin name="PWDN" x="-8.89" y="-11.43" length="short"/>
<pin name="VSYNC" x="-8.89" y="-13.97" length="short" direction="out"/>
<pin name="RESET" x="-8.89" y="-16.51" length="short"/>
<pin name="SIO_C" x="-8.89" y="-19.05" length="short" direction="in"/>
<pin name="AVDD" x="-8.89" y="-21.59" length="short" direction="sup"/>
<pin name="SIO_D" x="-8.89" y="-24.13" length="short"/>
<pin name="AGND" x="-8.89" y="-26.67" length="short" direction="sup"/>
<pin name="NC" x="-8.89" y="-29.21" length="short" direction="nc"/>
<text x="-8.89" y="31.75" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="31.75" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="31.75" x2="5.08" y2="31.75" width="0.1524" layer="94"/>
<wire x1="5.08" y1="31.75" x2="5.08" y2="-31.75" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-31.75" x2="-5.08" y2="-31.75" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-31.75" x2="-5.08" y2="21.59" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="21.59" x2="-5.08" y2="24.13" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="24.13" x2="-5.08" y2="26.67" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="26.67" x2="-5.08" y2="29.21" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="29.21" x2="-5.08" y2="31.75" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="29.21" x2="-5.08" y2="29.21" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="26.67" x2="-5.08" y2="26.67" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="24.13" x2="-5.08" y2="24.13" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="21.59" x2="-5.08" y2="21.59" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="19.05" x2="-5.08" y2="19.05" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="16.51" x2="-5.08" y2="16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="13.97" x2="-5.08" y2="13.97" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="11.43" x2="-5.08" y2="11.43" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="8.89" x2="-5.08" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="6.35" x2="-5.08" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-5.08" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="1.27" x2="-5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-5.08" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-6.35" x2="-5.08" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-8.89" x2="-5.08" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-11.43" x2="-5.08" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-13.97" x2="-5.08" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-16.51" x2="-5.08" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-19.05" x2="-5.08" y2="-19.05" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-21.59" x2="-5.08" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-24.13" x2="-5.08" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-26.67" x2="-5.08" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-29.21" x2="-5.08" y2="-29.21" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VC0706" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="VC0706" x="0" y="0"/>
</gates>
<devices>
<device name="'PREA'" package="LQFP100">
<connects>
<connect gate="G$1" pin="CLK_IN" pad="70"/>
<connect gate="G$1" pin="CLK_OUT" pad="71"/>
<connect gate="G$1" pin="CS_CLK" pad="11"/>
<connect gate="G$1" pin="CS_D0" pad="13"/>
<connect gate="G$1" pin="CS_D1" pad="15"/>
<connect gate="G$1" pin="CS_D2" pad="16"/>
<connect gate="G$1" pin="CS_D3" pad="19"/>
<connect gate="G$1" pin="CS_D4" pad="20"/>
<connect gate="G$1" pin="CS_D5" pad="23"/>
<connect gate="G$1" pin="CS_D6" pad="24"/>
<connect gate="G$1" pin="CS_D7" pad="28"/>
<connect gate="G$1" pin="CS_D8" pad="29"/>
<connect gate="G$1" pin="CS_D9" pad="30"/>
<connect gate="G$1" pin="CS_ENB" pad="95"/>
<connect gate="G$1" pin="CS_HSYNC" pad="5"/>
<connect gate="G$1" pin="CS_PCLK" pad="10"/>
<connect gate="G$1" pin="CS_PWD" pad="33"/>
<connect gate="G$1" pin="CS_RSTB" pad="34"/>
<connect gate="G$1" pin="CS_SCK" pad="3"/>
<connect gate="G$1" pin="CS_SDA" pad="1"/>
<connect gate="G$1" pin="CS_VSYNC" pad="7"/>
<connect gate="G$1" pin="DAC_COMP" pad="54"/>
<connect gate="G$1" pin="DAC_CVBS" pad="55"/>
<connect gate="G$1" pin="DAC_REXT" pad="53"/>
<connect gate="G$1" pin="GPIO0/TX" pad="76"/>
<connect gate="G$1" pin="GPIO1/RX" pad="77"/>
<connect gate="G$1" pin="GPIO10" pad="88"/>
<connect gate="G$1" pin="GPIO11" pad="40"/>
<connect gate="G$1" pin="GPIO12" pad="4"/>
<connect gate="G$1" pin="GPIO13" pad="2"/>
<connect gate="G$1" pin="GPIO2" pad="84"/>
<connect gate="G$1" pin="GPIO3" pad="85"/>
<connect gate="G$1" pin="GPIO4" pad="39"/>
<connect gate="G$1" pin="GPIO5" pad="67"/>
<connect gate="G$1" pin="GPIO6" pad="64"/>
<connect gate="G$1" pin="GPIO7" pad="37"/>
<connect gate="G$1" pin="GPIO8" pad="26"/>
<connect gate="G$1" pin="GPIO9" pad="12"/>
<connect gate="G$1" pin="MCU_A0" pad="44"/>
<connect gate="G$1" pin="MCU_A1" pad="43"/>
<connect gate="G$1" pin="MCU_A10" pad="22"/>
<connect gate="G$1" pin="MCU_A11" pad="21"/>
<connect gate="G$1" pin="MCU_A12" pad="18"/>
<connect gate="G$1" pin="MCU_A13" pad="17"/>
<connect gate="G$1" pin="MCU_A14" pad="9"/>
<connect gate="G$1" pin="MCU_A15" pad="8"/>
<connect gate="G$1" pin="MCU_A2" pad="42"/>
<connect gate="G$1" pin="MCU_A3" pad="41"/>
<connect gate="G$1" pin="MCU_A4" pad="36"/>
<connect gate="G$1" pin="MCU_A5" pad="35"/>
<connect gate="G$1" pin="MCU_A6" pad="32"/>
<connect gate="G$1" pin="MCU_A7" pad="31"/>
<connect gate="G$1" pin="MCU_A8" pad="27"/>
<connect gate="G$1" pin="MCU_A9" pad="25"/>
<connect gate="G$1" pin="MCU_D0" pad="78"/>
<connect gate="G$1" pin="MCU_D1" pad="79"/>
<connect gate="G$1" pin="MCU_D2" pad="82"/>
<connect gate="G$1" pin="MCU_D3" pad="83"/>
<connect gate="G$1" pin="MCU_D4" pad="86"/>
<connect gate="G$1" pin="MCU_D5" pad="87"/>
<connect gate="G$1" pin="MCU_D6" pad="93"/>
<connect gate="G$1" pin="MCU_D7" pad="94"/>
<connect gate="G$1" pin="MCU_DCEN" pad="14"/>
<connect gate="G$1" pin="MCU_OEN" pad="96"/>
<connect gate="G$1" pin="MCU_PCEN" pad="97"/>
<connect gate="G$1" pin="MCU_WEN" pad="6"/>
<connect gate="G$1" pin="MONA" pad="49"/>
<connect gate="G$1" pin="RSTN" pad="38"/>
<connect gate="G$1" pin="SPI_MISO" pad="92"/>
<connect gate="G$1" pin="SPI_MOSI" pad="91"/>
<connect gate="G$1" pin="SPI_SCK" pad="90"/>
<connect gate="G$1" pin="SPI_SS" pad="89"/>
<connect gate="G$1" pin="TEST" pad="75"/>
<connect gate="G$1" pin="TV-D7" pad="60"/>
<connect gate="G$1" pin="TV_D0" pad="69"/>
<connect gate="G$1" pin="TV_D1" pad="68"/>
<connect gate="G$1" pin="TV_D2" pad="66"/>
<connect gate="G$1" pin="TV_D3" pad="65"/>
<connect gate="G$1" pin="TV_D4" pad="63"/>
<connect gate="G$1" pin="TV_D5" pad="62"/>
<connect gate="G$1" pin="TV_D6" pad="61"/>
<connect gate="G$1" pin="TV_PCLK" pad="74"/>
<connect gate="G$1" pin="UART_RXD" pad="81"/>
<connect gate="G$1" pin="UART_TXD" pad="80"/>
<connect gate="G$1" pin="VDD" pad="47"/>
<connect gate="G$1" pin="VDD@1" pad="98"/>
<connect gate="G$1" pin="VDDA" pad="50"/>
<connect gate="G$1" pin="VDDA_DAC" pad="57"/>
<connect gate="G$1" pin="VDDA_PLL" pad="59"/>
<connect gate="G$1" pin="VDD_IO" pad="45"/>
<connect gate="G$1" pin="VDD_IO@1" pad="73"/>
<connect gate="G$1" pin="VDD_IO@2" pad="100"/>
<connect gate="G$1" pin="VLX" pad="51"/>
<connect gate="G$1" pin="VSS" pad="46"/>
<connect gate="G$1" pin="VSS@1" pad="72"/>
<connect gate="G$1" pin="VSS@2" pad="99"/>
<connect gate="G$1" pin="VSSA" pad="48"/>
<connect gate="G$1" pin="VSSA@1" pad="52"/>
<connect gate="G$1" pin="VSSA_DAC" pad="56"/>
<connect gate="G$1" pin="VSSA_PLL" pad="58"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAMERA-OV7670" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAMERA-OV7670" x="0" y="0"/>
</gates>
<devices>
<device name="" package="24P-0.5-16.2X5.8X2.0H">
<connects>
<connect gate="G$1" pin="AGND" pad="23"/>
<connect gate="G$1" pin="AVDD" pad="21"/>
<connect gate="G$1" pin="DGND" pad="10"/>
<connect gate="G$1" pin="DOVDD" pad="14"/>
<connect gate="G$1" pin="DVDD" pad="15"/>
<connect gate="G$1" pin="HREF" pad="16"/>
<connect gate="G$1" pin="NC" pad="24"/>
<connect gate="G$1" pin="PCLK" pad="8"/>
<connect gate="G$1" pin="PWDN" pad="17"/>
<connect gate="G$1" pin="RESET" pad="19"/>
<connect gate="G$1" pin="SIO_C" pad="20"/>
<connect gate="G$1" pin="SIO_D" pad="22"/>
<connect gate="G$1" pin="VSYNC" pad="18"/>
<connect gate="G$1" pin="XCLK1" pad="12"/>
<connect gate="G$1" pin="Y0" pad="1"/>
<connect gate="G$1" pin="Y1" pad="2"/>
<connect gate="G$1" pin="Y2" pad="6"/>
<connect gate="G$1" pin="Y3" pad="4"/>
<connect gate="G$1" pin="Y4" pad="3"/>
<connect gate="G$1" pin="Y5" pad="5"/>
<connect gate="G$1" pin="Y6" pad="7"/>
<connect gate="G$1" pin="Y7" pad="9"/>
<connect gate="G$1" pin="Y8" pad="11"/>
<connect gate="G$1" pin="Y9" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power or GND ">
<packages>
</packages>
<symbols>
<symbol name="GND_POWER">
<wire x1="-1.27" y1="-0.635" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.3175" y1="-1.905" x2="0.3175" y2="-1.905" width="0.1524" layer="94"/>
<pin name="GND" x="0" y="0" visible="off" length="point" direction="sup" rot="R270"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<text x="-1.905" y="-3.175" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GND_SIGNAL">
<wire x1="-1.27" y1="-0.635" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<pin name="AGND" x="0" y="0" visible="off" length="point" direction="sup" rot="R270"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<text x="-1.905" y="-3.175" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND_POWER" uservalue="yes">
<gates>
<gate name="G$1" symbol="GND_POWER" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND_SIGNAL">
<gates>
<gate name="G$1" symbol="GND_SIGNAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC">
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.8" x2="0.1905" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.8" x2="0.6985" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.8" x2="-1.4605" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.8" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.06" dx="1.016" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="-1.06" dx="1.016" dy="1.2" layer="1"/>
<smd name="1" x="-0.95" y="-1.06" dx="1.016" dy="1.2" layer="1" rot="R180"/>
<text x="-1.778" y="1.905" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.778" y="-2.667" size="0.8128" layer="27" ratio="10">&gt;value</text>
<rectangle x1="-0.2286" y1="0.8382" x2="0.2286" y2="1.4224" layer="51"/>
<rectangle x1="0.7112" y1="-1.4224" x2="1.1684" y2="-0.8382" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4224" x2="-0.7112" y2="-0.8382" layer="51"/>
<rectangle x1="-1.45" y1="-0.8" x2="1.45" y2="0.8" layer="39"/>
</package>
<package name="SOT89">
<description>SOT98 Emitter Collector Basis</description>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="2.54" y2="1.905" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.905" width="0.127" layer="21"/>
<smd name="1" x="-1.524" y="-1.905" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.524" y="-1.905" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.651" dx="0.8" dy="1.9" layer="1" stop="no" cream="no"/>
<smd name="4" x="0" y="0.94" dx="2.032" dy="3.65" layer="1" roundness="75"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<rectangle x1="-0.4" y1="-2.68" x2="0.4" y2="-1.28" layer="31"/>
<rectangle x1="-0.5" y1="-2.78" x2="0.5" y2="-1.18" layer="29"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
</package>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="5.983" x2="3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-5.983" x2="-3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-5.983" x2="-3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="5.983" x2="3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="3" x="0" y="2.5" dx="5.3" dy="6.2" layer="1"/>
<smd name="1" x="-2.286" y="-4.826" dx="1.016" dy="1.651" layer="1"/>
<smd name="2" x="2.286" y="-4.826" dx="1.016" dy="1.651" layer="1"/>
<text x="-4.191" y="-3.683" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="6.223" y="-4.318" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
<package name="SOT-223">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
<wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.473" y1="4.483" x2="3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-4.483" x2="-3.473" y2="-4.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-4.483" x2="-3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="4.483" x2="3.473" y2="-4.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="2" x="0" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="3" x="2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="TER" x="0" y="3.175" dx="3.556" dy="2.159" layer="1"/>
<text x="-3.81" y="-3.81" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.7658" y="-4.445" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35"/>
</package>
<package name="TO-220">
<wire x1="-5.715" y1="1.905" x2="5.715" y2="1.905" width="0.254" layer="21"/>
<wire x1="5.715" y1="1.905" x2="5.715" y2="-6.985" width="0.254" layer="21"/>
<wire x1="5.715" y1="-6.985" x2="-5.715" y2="-6.985" width="0.254" layer="21"/>
<wire x1="-5.715" y1="-6.985" x2="-5.715" y2="1.905" width="0.254" layer="21"/>
<pad name="1" x="-2.54" y="-11.43" drill="1.27" diameter="2.032" shape="long" rot="R90"/>
<pad name="2" x="0" y="-11.43" drill="1.27" diameter="2.032" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-11.43" drill="1.27" diameter="2.032" shape="long" rot="R90"/>
<pad name="HEAT" x="0" y="6.35" drill="3.2004" diameter="7.62"/>
<text x="-4.445" y="-1.905" size="1.778" layer="25" ratio="10">&gt;name</text>
<text x="-4.445" y="-4.445" size="1.778" layer="27" ratio="10">&gt;value</text>
<rectangle x1="-5.715" y1="-13.335" x2="5.715" y2="10.795" layer="39"/>
</package>
<package name="SOIC8-6.0">
<wire x1="-2.5" y1="2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="-1.76890625" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.76890625" y1="-2" x2="-2.5" y2="-1.26890625" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.26890625" x2="-2.5" y2="2" width="0.127" layer="21"/>
<circle x="-2.794" y="-2.032" radius="0.254" width="0.127" layer="21"/>
<smd name="1" x="-1.905" y="-2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-0.635" y="-2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0.635" y="-2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1.905" y="-2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="1.905" y="2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="0.635" y="2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-0.635" y="2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.905" y="2.7" dx="1.4" dy="0.6" layer="1" rot="R90"/>
<text x="-3.175" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-2.032" x2="2.54" y2="2.032" layer="39"/>
</package>
<package name="TSSOP14">
<wire x1="-2.5" y1="2.2" x2="2.5" y2="2.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.2" x2="2.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.2" x2="-2.119" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.119" y1="-2.2" x2="-2.5" y2="-1.819" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.819" x2="-2.5" y2="2.2" width="0.127" layer="21"/>
<circle x="-2.794" y="-2.413" radius="0.3" width="0.1" layer="21"/>
<smd name="1" x="-2" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="2" x="-1.3" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="3" x="-0.65" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="4" x="0" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="5" x="0.65" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="6" x="1.3" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="7" x="2" y="-2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="8" x="2" y="2.925" dx="0.4" dy="1.2" layer="1" rot="R180"/>
<smd name="9" x="1.3" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="10" x="0.65" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="11" x="0" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="12" x="-0.65" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="13" x="-1.3" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<smd name="14" x="-2" y="2.925" dx="0.4" dy="1.2" layer="1"/>
<text x="-2.794" y="-1.397" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.556" y="-2.032" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-2.2" x2="2.54" y2="2.2" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="XC6206">
<wire x1="-8.89" y1="3.81" x2="8.89" y2="3.81" width="0.1524" layer="94"/>
<wire x1="8.89" y1="3.81" x2="8.89" y2="0" width="0.1524" layer="94"/>
<wire x1="8.89" y1="0" x2="8.89" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="-8.89" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-3.81" x2="-8.89" y2="0" width="0.1524" layer="94"/>
<pin name="VIN" x="-12.7" y="0" length="short" direction="in"/>
<pin name="VOUT" x="12.7" y="0" length="short" direction="out" rot="R180"/>
<pin name="VSS" x="0" y="-7.62" length="short" direction="sup" rot="R90"/>
<wire x1="-8.89" y1="0" x2="-8.89" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="0" x2="-8.89" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="10.16" y1="0" x2="8.89" y2="0" width="0.1524" layer="94"/>
<text x="0" y="3.81" size="1.778" layer="96" ratio="10">&gt;VALUE</text>
<text x="-8.89" y="3.81" size="1.778" layer="95" ratio="10">&gt;NAME</text>
</symbol>
<symbol name="LD1117DT">
<text x="-7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="-7.62" length="short" rot="R90"/>
<pin name="IN" x="-11.43" y="0" length="short"/>
<pin name="OUT" x="11.43" y="0" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="-7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="8.89" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="CAT24C256">
<pin name="A0" x="-10.16" y="3.81" length="short"/>
<pin name="A1" x="-10.16" y="1.27" length="short"/>
<pin name="A2" x="-10.16" y="-1.27" length="short"/>
<pin name="VSS" x="-10.16" y="-3.81" length="short"/>
<pin name="SDA" x="10.16" y="-3.81" length="short" rot="R180"/>
<pin name="SCL" x="10.16" y="-1.27" length="short" rot="R180"/>
<pin name="WP" x="10.16" y="1.27" length="short" rot="R180"/>
<pin name="VCC" x="10.16" y="3.81" length="short" rot="R180"/>
<text x="-6.35" y="6.35" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-6.35" y1="5.08" x2="6.35" y2="5.08" width="0.1524" layer="94"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="3.81" width="0.1524" layer="94"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="1.27" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="6.35" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="-6.35" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-6.35" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="-6.35" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="1.27" x2="-6.35" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="7.62" y1="1.27" x2="6.35" y2="1.27" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="6.35" y2="3.81" width="0.1524" layer="94"/>
</symbol>
<symbol name="BUFFER">
<pin name="A" x="-8.89" y="0" length="short"/>
<pin name="!OE" x="0" y="-8.89" length="middle" function="dot" rot="R90"/>
<pin name="Y" x="8.89" y="0" length="short" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<text x="0" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="7.62" y="3.81" size="1.27" layer="96" ratio="10">&gt;value</text>
</symbol>
<symbol name="VCC-GND">
<pin name="VCC" x="0" y="11.43" length="short" direction="sup" rot="R270"/>
<pin name="GND" x="0" y="-11.43" length="short" direction="sup" rot="R90"/>
<text x="-3.81" y="0" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="XC6206" prefix="U">
<description>LOW ESR Cap.Compatible Positive Voltage Regulators</description>
<gates>
<gate name="G$1" symbol="XC6206" x="0" y="0"/>
</gates>
<devices>
<device name="MR" package="SOT23">
<connects>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PR" package="SOT89">
<connects>
<connect gate="G$1" pin="VIN" pad="2 4"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LD1117" prefix="U">
<description>Low Drop Fixed And Adjustable Positive Voltage Regulators
http://www.datasheetcatalog.org/datasheet/SGSThomsonMicroelectronics/mXuqtqv.pdf</description>
<gates>
<gate name="G$1" symbol="LD1117DT" x="0" y="0"/>
</gates>
<devices>
<device name="DT" package="TO252">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S" package="SOT-223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 TER"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'SOT89'" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V" package="TO-220">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 HEAT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAT24C256" prefix="U" uservalue="yes">
<description>256 kb I2C CMOS Serial
EEPROM</description>
<gates>
<gate name="G$1" symbol="CAT24C256" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC8-6.0">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74V*125" prefix="U" uservalue="yes">
<description>Quad Buffer/Line Driver;3-state</description>
<gates>
<gate name="A" symbol="BUFFER" x="-12.7" y="10.16" swaplevel="4"/>
<gate name="PWR" symbol="VCC-GND" x="-27.94" y="0" addlevel="must"/>
<gate name="B" symbol="BUFFER" x="12.7" y="10.16" swaplevel="4"/>
<gate name="C" symbol="BUFFER" x="-12.7" y="-8.89" swaplevel="4"/>
<gate name="D" symbol="BUFFER" x="12.7" y="-8.89" swaplevel="4"/>
</gates>
<devices>
<device name="PW" package="TSSOP14">
<connects>
<connect gate="A" pin="!OE" pad="1"/>
<connect gate="A" pin="A" pad="2"/>
<connect gate="A" pin="Y" pad="3"/>
<connect gate="B" pin="!OE" pad="4"/>
<connect gate="B" pin="A" pad="5"/>
<connect gate="B" pin="Y" pad="6"/>
<connect gate="C" pin="!OE" pad="10"/>
<connect gate="C" pin="A" pad="9"/>
<connect gate="C" pin="Y" pad="8"/>
<connect gate="D" pin="!OE" pad="13"/>
<connect gate="D" pin="A" pad="12"/>
<connect gate="D" pin="Y" pad="11"/>
<connect gate="PWR" pin="GND" pad="7"/>
<connect gate="PWR" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name="HC"/>
<technology name="HCT"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Discrete">
<packages>
<package name="C0603">
<description>&lt;b&gt;0603&lt;b&gt;&lt;p&gt;</description>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.127" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<smd name="2" x="0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<text x="-1.397" y="0.762" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-1.27" y="-0.1905" size="0.381" layer="27" ratio="12">&gt;value</text>
<text x="-1.27" y="-0.254" size="0.635" layer="33" ratio="10">&gt;name</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39"/>
</package>
<package name="C0805">
<description>&lt;b&gt;0805&lt;b&gt;&lt;p&gt;</description>
<wire x1="1.651" y1="0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="-1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.889" x2="-1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<smd name="1" x="-0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<smd name="2" x="0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<text x="-1.778" y="1.016" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4605" y1="-0.762" x2="1.4605" y2="0.762" layer="39"/>
</package>
<package name="C0512/10.18">
<description>FILM CAP 0.01UF/275V</description>
<wire x1="-6" y1="2.45" x2="6" y2="2.45" width="0.127" layer="21"/>
<wire x1="6" y1="2.45" x2="6" y2="-2.45" width="0.127" layer="21"/>
<wire x1="6" y1="-2.45" x2="-6" y2="-2.45" width="0.127" layer="21"/>
<wire x1="-6" y1="-2.45" x2="-6" y2="2.45" width="0.127" layer="21"/>
<pad name="P$1" x="-5.09" y="0" drill="0.95" diameter="1.6"/>
<pad name="P$2" x="5.09" y="0" drill="0.95" diameter="1.6"/>
<text x="-3.81" y="2.921" size="1.778" layer="25" ratio="10">&gt;name</text>
<text x="-4.445" y="-1.27" size="1.778" layer="27" ratio="10">&gt;value</text>
<text x="-1.778" y="-0.381" size="0.8128" layer="33" ratio="10">&gt;name</text>
<rectangle x1="-5.969" y1="-2.413" x2="5.969" y2="2.413" layer="39"/>
</package>
<package name="CERAMIC-2.54">
<wire x1="-2.54" y1="1.143" x2="2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.143" x2="2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.143" x2="-2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.143" x2="-2.54" y2="1.143" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.635" diameter="1.143"/>
<pad name="2" x="1.27" y="0" drill="0.635" diameter="1.143"/>
<text x="-1.905" y="1.27" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-1.016" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-2.54" y1="-1.143" x2="2.54" y2="1.143" layer="39"/>
</package>
<package name="CERAMIC-5.08">
<wire x1="-3.175" y1="1.143" x2="3.175" y2="1.143" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.175" y2="-1.143" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.143" x2="-3.175" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.143" x2="-3.175" y2="1.143" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016"/>
<pad name="2" x="2.54" y="0" drill="1.016"/>
<text x="-1.905" y="1.27" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-0.635" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4064" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-3.175" y1="-1.143" x2="3.175" y2="1.143" layer="39"/>
</package>
<package name="C1206">
<wire x1="-2.4638" y1="1.2192" x2="2.4638" y2="1.2192" width="0.127" layer="21"/>
<wire x1="2.4638" y1="1.2192" x2="2.4638" y2="-1.2192" width="0.127" layer="21"/>
<wire x1="2.4638" y1="-1.2192" x2="-2.4638" y2="-1.2192" width="0.127" layer="21"/>
<wire x1="-2.4638" y1="-1.2192" x2="-2.4638" y2="1.2192" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="2.032" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="2.032" layer="1"/>
<text x="-1.778" y="1.397" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-0.508" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C1210">
<smd name="1" x="-1.5" y="0" dx="3.1" dy="1.55" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="3.1" dy="1.55" layer="1" rot="R90"/>
<wire x1="-2.55" y1="1.8" x2="2.55" y2="1.8" width="0.127" layer="21"/>
<wire x1="2.55" y1="1.8" x2="2.55" y2="-1.8" width="0.127" layer="21"/>
<wire x1="2.55" y1="-1.8" x2="-2.55" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-2.55" y1="-1.8" x2="-2.55" y2="1.8" width="0.127" layer="21"/>
<rectangle x1="-2.54" y1="-1.778" x2="2.54" y2="1.778" layer="39"/>
<text x="-3.81" y="1.905" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.635" size="0.889" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1813">
<description>&lt;b&gt;1813&lt;b&gt;&lt;p&gt;</description>
<wire x1="-2.25" y1="-0.5" x2="-1.75" y2="0" width="0.127" layer="51" curve="90"/>
<wire x1="-1.75" y1="0" x2="-2.25" y2="0.5" width="0.127" layer="51" curve="90"/>
<wire x1="-2.25" y1="0.5" x2="-0.5" y2="2" width="0.127" layer="51" curve="-90"/>
<wire x1="-0.5" y1="2" x2="0.5" y2="2" width="0.127" layer="51"/>
<wire x1="0.5" y1="2" x2="2.25" y2="0.5" width="0.127" layer="51" curve="-90"/>
<wire x1="2.25" y1="0.5" x2="1.75" y2="0" width="0.127" layer="51" curve="90"/>
<wire x1="1.75" y1="0" x2="2.25" y2="-0.5" width="0.127" layer="51" curve="90"/>
<wire x1="2.25" y1="-0.5" x2="0.5" y2="-2" width="0.127" layer="51" curve="-90"/>
<wire x1="0.5" y1="-2" x2="-0.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-2" x2="-2.25" y2="-0.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-2.794" y1="2.159" x2="2.794" y2="2.159" width="0.127" layer="21"/>
<wire x1="2.794" y1="2.159" x2="2.794" y2="-2.159" width="0.127" layer="21"/>
<wire x1="2.794" y1="-2.159" x2="-2.794" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-2.159" x2="-2.794" y2="2.159" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="3.81" dy="1.778" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="1.651" y="0" dx="3.81" dy="1.778" layer="1" roundness="50" rot="R90"/>
<text x="-2.032" y="2.286" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-0.889" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.667" y1="-2.032" x2="2.667" y2="2.032" layer="39"/>
</package>
<package name="1616BZ">
<wire x1="2.286" y1="1.524" x2="2.286" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.286" y1="-1.524" x2="1.778" y2="-2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="1.778" y1="-2.032" x2="-1.778" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-2.032" x2="-2.286" y2="-1.524" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.286" y1="-1.524" x2="-2.286" y2="1.524" width="0.127" layer="51"/>
<wire x1="-2.286" y1="1.524" x2="-1.778" y2="2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.778" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="2.286" y2="1.524" width="0.127" layer="21" curve="-90"/>
<smd name="1" x="-1.905" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="1.905" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
<text x="-1.778" y="2.159" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;value</text>
<text x="-1.27" y="-0.127" size="0.635" layer="33" ratio="10">&gt;name</text>
</package>
<package name="2020BZ">
<wire x1="2.794" y1="1.778" x2="2.794" y2="-1.778" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-1.778" x2="-2.794" y2="1.778" width="0.127" layer="51"/>
<wire x1="2.794" y1="-1.778" x2="2.794" y2="-2.032" width="0.127" layer="21"/>
<wire x1="2.794" y1="-2.032" x2="2.159" y2="-2.667" width="0.127" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.667" x2="-2.159" y2="-2.667" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-2.667" x2="-2.794" y2="-2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.794" y1="-2.032" x2="-2.794" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-2.794" y1="1.778" x2="-2.794" y2="2.032" width="0.127" layer="21"/>
<wire x1="-2.794" y1="2.032" x2="-2.159" y2="2.667" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.159" y1="2.667" x2="2.159" y2="2.667" width="0.127" layer="21"/>
<wire x1="2.159" y1="2.667" x2="2.794" y2="2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="2.794" y1="2.032" x2="2.794" y2="1.778" width="0.127" layer="21"/>
<smd name="1" x="-2.286" y="0" dx="3.048" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="2.286" y="0" dx="3.048" dy="2.032" layer="1" rot="R90"/>
<text x="-1.778" y="2.794" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;value</text>
<text x="-1.905" y="0" size="0.635" layer="33" ratio="10">&gt;name</text>
</package>
<package name="0806">
<description>&lt;b&gt;datasheet:&lt;br&gt;
&lt;u&gt;http://search.murata.co.jp/Ceramy/image/img/PDF/ENG/L0075S0103LQH2MC_02.pdf</description>
<wire x1="1.651" y1="1.016" x2="1.651" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.651" y1="-1.016" x2="-1.651" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-1.016" x2="-1.651" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.651" y1="1.016" x2="1.651" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.127" y1="-0.508" x2="0.127" y2="0" width="0.127" layer="21" curve="-180"/>
<wire x1="0.127" y1="0" x2="0.127" y2="0.508" width="0.127" layer="21" curve="-180"/>
<smd name="1" x="-0.889" y="0" dx="1.016" dy="1.524" layer="1" roundness="25"/>
<smd name="2" x="0.889" y="0" dx="1.016" dy="1.524" layer="1" roundness="25"/>
<text x="-1.905" y="1.143" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-0.3175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="-0.635" size="0.635" layer="33" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-1.4605" y1="-0.762" x2="1.4605" y2="0.762" layer="39"/>
</package>
<package name="2P-ROUND-11.4MM">
<circle x="0" y="0" radius="5.6796" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.6796" width="0.127" layer="39"/>
<pad name="1" x="-2.54" y="0" drill="1.27" diameter="1.905"/>
<pad name="2" x="2.54" y="0" drill="1.27" diameter="1.905"/>
<text x="-4.699" y="6.096" size="1.778" layer="25">&gt;name</text>
<text x="-4.064" y="-0.889" size="1.778" layer="25">&gt;value</text>
</package>
<package name="2218">
<description>http://www.vishay.com/docs/34096/idcp2218.pdf</description>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.905" y1="0" x2="-2.54" y2="-0.635" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0.635" x2="1.905" y2="0" width="0.127" layer="21" curve="90"/>
<wire x1="1.905" y1="0" x2="2.54" y2="-0.635" width="0.127" layer="21" curve="90"/>
<wire x1="-2.54" y1="0.635" x2="-0.635" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="2.54" y2="0.635" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="-0.635" x2="0.635" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-2.54" y2="-0.635" width="0.127" layer="21" curve="-90"/>
<smd name="P$1" x="-1.925" y="0" dx="5.5" dy="2.15" layer="1" roundness="50" rot="R90"/>
<smd name="P$2" x="1.925" y="0" dx="5.5" dy="2.15" layer="1" roundness="50" rot="R90"/>
<text x="-1.905" y="2.921" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.905" y="-3.683" size="0.8128" layer="27" ratio="10">&gt;value</text>
<text x="0.127" y="-0.508" size="0.254" layer="33" ratio="10" rot="R90">&gt;name</text>
</package>
<package name="5020">
<wire x1="-2" y1="4.7" x2="2" y2="4.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-4.7" x2="2" y2="-4.7" width="0.127" layer="21"/>
<wire x1="6.475" y1="1.64" x2="6.475" y2="-1.64" width="0.127" layer="21"/>
<wire x1="-6.475" y1="1.64" x2="-6.475" y2="-1.64" width="0.127" layer="21"/>
<wire x1="-2" y1="4.7" x2="-6.475" y2="1.64" width="0.127" layer="21"/>
<wire x1="2" y1="4.7" x2="6.475" y2="1.64" width="0.127" layer="21"/>
<wire x1="6.475" y1="-1.64" x2="2" y2="-4.7" width="0.127" layer="21"/>
<wire x1="-6.475" y1="-1.645" x2="-2" y2="-4.7" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.6" width="0.127" layer="21"/>
<smd name="1" x="-5.145" y="0" dx="2.92" dy="2.79" layer="1"/>
<smd name="2" x="5.145" y="0" dx="2.92" dy="2.79" layer="1"/>
<text x="-1.778" y="4.953" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.381" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.381" y="-0.127" size="0.254" layer="33" ratio="10">&gt;NAME</text>
<polygon width="0.127" layer="39">
<vertex x="-6.477" y="1.651"/>
<vertex x="-2.032" y="4.699"/>
<vertex x="1.905" y="4.699"/>
<vertex x="6.477" y="1.651"/>
<vertex x="6.477" y="-1.651"/>
<vertex x="2.032" y="-4.699"/>
<vertex x="-2.032" y="-4.699"/>
<vertex x="-6.477" y="-1.651"/>
</polygon>
</package>
<package name="PIO73">
<wire x1="-3.5" y1="2" x2="-3.5" y2="-2" width="0.127" layer="21"/>
<wire x1="3.5" y1="2" x2="3.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1" y1="3.9" x2="1" y2="3.9" width="0.127" layer="21" curve="180"/>
<wire x1="1" y1="-3.9" x2="-1" y2="-3.9" width="0.127" layer="21" curve="180"/>
<wire x1="-3.5" y1="2" x2="-1" y2="3.9" width="0.127" layer="21" curve="-89.995417"/>
<wire x1="1" y1="3.9" x2="3.5" y2="2" width="0.127" layer="21" curve="-89.995417"/>
<wire x1="-1" y1="-3.9" x2="-3.5" y2="-2" width="0.127" layer="21" curve="-89.990833"/>
<wire x1="3.5" y1="-2" x2="1" y2="-3.9" width="0.127" layer="21" curve="-89.995417"/>
<smd name="1" x="0" y="2.35" dx="3.3" dy="8" layer="1" rot="R270"/>
<smd name="2" x="0" y="-2.35" dx="3.3" dy="8" layer="1" rot="R270"/>
<text x="-0.635" y="0" size="0.254" layer="33" ratio="10">&gt;NAME</text>
<text x="-3.81" y="4.191" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-0.381" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="PIO105">
<wire x1="-4.5" y1="2.54" x2="-4.5" y2="-2.54" width="0.127" layer="21"/>
<wire x1="4.5" y1="2.54" x2="4.5" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="5" x2="1.27" y2="5" width="0.127" layer="21" curve="180"/>
<wire x1="1.27" y1="-5" x2="-1.27" y2="-5" width="0.127" layer="21" curve="180"/>
<wire x1="-4.5" y1="2.54" x2="-1.27" y2="5" width="0.127" layer="21" curve="-89.996452"/>
<wire x1="1.27" y1="5" x2="4.5" y2="2.54" width="0.127" layer="21" curve="-89.996452"/>
<wire x1="-1.27" y1="-5" x2="-4.5" y2="-2.54" width="0.127" layer="21" curve="-89.996452"/>
<wire x1="4.5" y1="-2.54" x2="1.27" y2="-5" width="0.127" layer="21" curve="-89.996452"/>
<smd name="1" x="0" y="3.1" dx="4.4" dy="10" layer="1" rot="R90"/>
<smd name="2" x="0" y="-3.1" dx="4.4" dy="10" layer="1" rot="R90"/>
<text x="-0.635" y="0" size="0.254" layer="33" ratio="10">&gt;NAME</text>
<text x="-3.81" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-0.381" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="2P-6*8H">
<circle x="1.75" y="0" radius="3.25" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="3.5" y="0" drill="0.8"/>
<text x="0" y="3.5" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-0.555" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<circle x="1.75" y="0" radius="3.25" width="0.127" layer="39"/>
</package>
<package name="2626">
<description>Low Profile ,High Current IHLP Inductor 6.47mm*6.47*2.4mm</description>
<smd name="1" x="-2.7686" y="0" dx="3.429" dy="1.8288" layer="1" rot="R90"/>
<smd name="2" x="2.7686" y="0" dx="3.429" dy="1.8288" layer="1" rot="R90"/>
<wire x1="1.9685" y1="-3.2385" x2="-1.9685" y2="-3.2385" width="0.127" layer="21"/>
<wire x1="-1.9685" y1="-3.2385" x2="-3.2385" y2="-1.9685" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.2385" y1="-1.9685" x2="-3.2385" y2="1.9685" width="0.127" layer="21"/>
<wire x1="-3.2385" y1="1.9685" x2="-1.9685" y2="3.2385" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.9685" y1="3.2385" x2="1.9685" y2="3.2385" width="0.127" layer="21"/>
<wire x1="1.9685" y1="3.2385" x2="3.2385" y2="1.9685" width="0.127" layer="21" curve="-90"/>
<wire x1="3.2385" y1="1.9685" x2="3.2385" y2="-1.9685" width="0.127" layer="21"/>
<wire x1="3.2385" y1="-1.9685" x2="1.9685" y2="-3.2385" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.9685" y1="3.2385" x2="1.9685" y2="3.2385" width="0.127" layer="39"/>
<wire x1="1.9685" y1="3.2385" x2="3.2385" y2="1.9685" width="0.127" layer="39" curve="-90"/>
<wire x1="3.2385" y1="1.9685" x2="3.2385" y2="-1.9685" width="0.127" layer="39"/>
<wire x1="3.2385" y1="-1.9685" x2="1.9685" y2="-3.2385" width="0.127" layer="39" curve="-90"/>
<wire x1="1.9685" y1="-3.2385" x2="-1.9685" y2="-3.2385" width="0.127" layer="39"/>
<wire x1="-1.9685" y1="-3.2385" x2="-3.2385" y2="-1.9685" width="0.127" layer="39" curve="-90"/>
<wire x1="-3.2385" y1="-1.9685" x2="-3.2385" y2="1.9685" width="0.127" layer="39"/>
<wire x1="-3.2385" y1="1.9685" x2="-1.9685" y2="3.2385" width="0.127" layer="39" curve="-90"/>
<text x="-3.81" y="3.81" size="1.778" layer="25">&gt;NAME</text>
<text x="-1.651" y="-0.254" size="0.635" layer="25">&gt;VALUE</text>
</package>
<package name="L0402">
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.762" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.762" y1="0.4445" x2="0.889" y2="0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.889" y1="0.3175" x2="0.889" y2="-0.3175" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.3175" x2="0.762" y2="-0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.762" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
<smd name="1" x="-0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<smd name="2" x="0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<text x="-1.27" y="0.635" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="L0805">
<wire x1="1.651" y1="0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="-1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.889" x2="-1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<smd name="1" x="-0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<smd name="2" x="0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<text x="-1.778" y="1.016" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4605" y1="-0.762" x2="1.4605" y2="0.762" layer="39"/>
</package>
<package name="L0603">
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.127" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<smd name="2" x="0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<text x="-1.397" y="0.762" size="0.635" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27" ratio="10">&gt;value</text>
</package>
<package name="L0630">
<description>06 for width 6.0mm ,30 for height 3.0mm</description>
<wire x1="3" y1="2.5" x2="3" y2="-2.5" width="0.127" layer="21"/>
<wire x1="3" y1="-2.5" x2="2.5" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="-3" y2="-2.5" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="-2.5" x2="-3" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="3" y2="2.5" width="0.127" layer="21" curve="-90"/>
<smd name="1" x="-2.85" y="0" dx="3.2" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.85" y="0" dx="3.2" dy="2.5" layer="1" rot="R90"/>
<text x="-3.81" y="3.175" size="1.778" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3" y1="-3" x2="3" y2="3" layer="39"/>
</package>
<package name="L0420">
<description>04 for width 4.0mm ,20 for height 2.0mm</description>
<wire x1="1.95" y1="-1.823" x2="-1.95" y2="-1.823" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-1.823" x2="-1.95" y2="2.077" width="0.127" layer="21"/>
<wire x1="-1.95" y1="2.077" x2="1.95" y2="2.077" width="0.127" layer="21"/>
<wire x1="1.95" y1="2.077" x2="1.95" y2="-1.823" width="0.127" layer="21"/>
<circle x="0" y="0.127" radius="1.655875" width="0.127" layer="21"/>
<text x="-1.905" y="2.667" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="0.254" y="-1.143" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<smd name="1" x="-1.6" y="0.127" dx="4.2" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="1.6" y="0.127" dx="4.2" dy="1.5" layer="1" rot="R90"/>
<rectangle x1="-1.905" y1="-1.778" x2="1.905" y2="2.032" layer="39"/>
</package>
<package name="L0402S">
<wire x1="-0.6985" y1="0.254" x2="0.6985" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="0.254" x2="0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="-0.254" x2="-0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.6985" y1="-0.254" x2="-0.6985" y2="0.254" width="0.0762" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<smd name="2" x="0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<text x="-0.635" y="0.381" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.254" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.0254" layer="51">
<vertex x="-0.508" y="0.254"/>
<vertex x="0.508" y="0.254"/>
<vertex x="0.508" y="-0.254"/>
<vertex x="-0.508" y="-0.254"/>
</polygon>
</package>
<package name="L0603S">
<wire x1="-1.016" y1="0.381" x2="1.016" y2="0.381" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.381" x2="1.016" y2="-0.381" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.381" x2="-1.016" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.381" x2="-1.016" y2="0.381" width="0.127" layer="21"/>
<smd name="1" x="-0.6985" y="0" dx="0.508" dy="0.762" layer="1" roundness="25"/>
<smd name="2" x="0.6985" y="0" dx="0.508" dy="0.762" layer="1" roundness="25"/>
<text x="-1.397" y="0.762" size="0.635" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27" ratio="10">&gt;value</text>
<polygon width="0.0254" layer="51">
<vertex x="-0.762" y="0.381"/>
<vertex x="0.762" y="0.381"/>
<vertex x="0.762" y="-0.381"/>
<vertex x="-0.762" y="-0.381"/>
</polygon>
</package>
<package name="R0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.0414" size="0.9906" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4064" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="R0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.1684" size="0.9906" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.8796" y="-0.3556" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="R0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.7686" y="0.9144" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="-3.2766" y="-2.3114" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="R0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.921" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.5146" y="-0.508" size="0.889" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="R0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.794" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-0.4318" size="0.889" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="R0207V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R0207/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.9" shape="octagon"/>
<text x="-3.302" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.683" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.794" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-0.5588" size="0.889" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="R0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.921" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="R0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.921" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="R0309/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 15mm</description>
<wire x1="-6.604" y1="0" x2="-7.62" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="7.62" y1="0" x2="6.477" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.921" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.477" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="6.477" y2="0.3175" layer="21"/>
</package>
<package name="R0309/20">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 20mm</description>
<wire x1="-9.144" y1="0" x2="-10.16" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="10.16" y1="0" x2="9.017" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="10.16" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.921" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-9.017" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="9.017" y2="0.3175" layer="21"/>
</package>
<package name="R0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R1206">
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-1.016" x2="-2.159" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<text x="-1.905" y="1.27" size="0.8128" layer="25" font="vector" ratio="10">&gt;name</text>
<text x="-2.032" y="-0.381" size="0.8128" layer="27" font="vector" ratio="10">&gt;value</text>
<rectangle x1="-2.032" y1="-0.889" x2="2.032" y2="0.889" layer="39"/>
</package>
<package name="R2512">
<description>2512</description>
<wire x1="-4.445" y1="1.905" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.905" x2="-4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.905" x2="-4.445" y2="1.905" width="0.127" layer="21"/>
<smd name="1" x="-3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<text x="-2.032" y="2.032" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.778" y="-0.254" size="0.8128" layer="33" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="R0821/27">
<wire x1="7.48840625" y1="-3.3" x2="-7.48840625" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-7.48840625" y1="-3.3" x2="-7.74075625" y2="-3.352296875" width="0.127" layer="21" curve="23.415886"/>
<wire x1="-7.74075625" y1="-3.352296875" x2="-8.76924375" y2="-3.797703125" width="0.127" layer="21"/>
<wire x1="-8.76924375" y1="-3.797703125" x2="-9.02159375" y2="-3.85" width="0.127" layer="21" curve="-23.415751"/>
<wire x1="-9.02159375" y1="-3.85" x2="-9.865" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-9.865" y1="-3.85" x2="-10.5" y2="-3.215" width="0.127" layer="21" curve="-90"/>
<wire x1="-10.5" y1="-3.215" x2="-10.5" y2="3.215" width="0.127" layer="21"/>
<wire x1="-10.5" y1="3.215" x2="-9.865" y2="3.85" width="0.127" layer="21" curve="-90"/>
<wire x1="-9.865" y1="3.85" x2="-9.02159375" y2="3.85" width="0.127" layer="21"/>
<wire x1="-9.02159375" y1="3.85" x2="-8.76924375" y2="3.797703125" width="0.127" layer="21" curve="-23.415886"/>
<wire x1="-8.76924375" y1="3.797703125" x2="-7.74075625" y2="3.352296875" width="0.127" layer="21"/>
<wire x1="-7.74075625" y1="3.352296875" x2="-7.48840625" y2="3.3" width="0.127" layer="21" curve="23.415751"/>
<wire x1="-7.48840625" y1="3.3" x2="7.48840625" y2="3.3" width="0.127" layer="21"/>
<wire x1="7.48840625" y1="3.3" x2="7.74075625" y2="3.352296875" width="0.127" layer="21" curve="23.415886"/>
<wire x1="7.74075625" y1="3.352296875" x2="8.76924375" y2="3.797703125" width="0.127" layer="21"/>
<wire x1="8.76924375" y1="3.797703125" x2="9.02159375" y2="3.85" width="0.127" layer="21" curve="-23.415751"/>
<wire x1="9.02159375" y1="3.85" x2="9.865" y2="3.85" width="0.127" layer="21"/>
<wire x1="9.865" y1="3.85" x2="10.5" y2="3.215" width="0.127" layer="21" curve="-90"/>
<wire x1="10.5" y1="3.215" x2="10.5" y2="-3.215" width="0.127" layer="21"/>
<wire x1="10.5" y1="-3.215" x2="9.865" y2="-3.85" width="0.127" layer="21" curve="-90"/>
<wire x1="9.865" y1="-3.85" x2="9.02159375" y2="-3.85" width="0.127" layer="21"/>
<wire x1="9.02159375" y1="-3.85" x2="8.76924375" y2="-3.797703125" width="0.127" layer="21" curve="-23.415886"/>
<wire x1="8.76924375" y1="-3.797703125" x2="7.74075625" y2="-3.352296875" width="0.127" layer="21"/>
<wire x1="7.74075625" y1="-3.352296875" x2="7.48840625" y2="-3.3" width="0.127" layer="21" curve="23.415751"/>
<wire x1="-12.065" y1="0" x2="-10.541" y2="0" width="0.127" layer="21"/>
<pad name="P$1" x="-13.5" y="0" drill="1.2" diameter="2.2"/>
<pad name="P$2" x="13.5" y="0" drill="1.2" diameter="2.2"/>
<text x="-5.08" y="3.683" size="1.778" layer="25" ratio="10">&gt;name</text>
<text x="-5.08" y="-0.762" size="1.778" layer="27" ratio="10">&gt;value</text>
<rectangle x1="10.541" y1="-0.254" x2="12.192" y2="0.254" layer="21"/>
<rectangle x1="-12.192" y1="-0.254" x2="-10.541" y2="0.254" layer="21"/>
</package>
<package name="R0204SMD">
<wire x1="-0.95" y1="0.7" x2="-0.9464" y2="0.6964" width="0.127" layer="21"/>
<wire x1="-0.9464" y1="0.6964" x2="-0.5929" y2="0.55" width="0.127" layer="21" curve="44.99167"/>
<wire x1="-0.5929" y1="0.55" x2="0.5929" y2="0.55" width="0.127" layer="21"/>
<wire x1="0.5929" y1="0.55" x2="0.9464" y2="0.6964" width="0.127" layer="21" curve="44.99167"/>
<wire x1="0.9464" y1="0.6964" x2="0.95" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.95" y1="-0.7" x2="0.9464" y2="-0.6964" width="0.127" layer="21"/>
<wire x1="0.9464" y1="-0.6964" x2="0.5929" y2="-0.55" width="0.127" layer="21" curve="44.99167"/>
<wire x1="0.5929" y1="-0.55" x2="-0.5929" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-0.5929" y1="-0.55" x2="-0.9464" y2="-0.6964" width="0.127" layer="21" curve="44.99167"/>
<wire x1="-0.9464" y1="-0.6964" x2="-0.95" y2="-0.7" width="0.127" layer="21"/>
<wire x1="0.95" y1="0.7" x2="1.75" y2="0.7" width="0.127" layer="51"/>
<wire x1="1.75" y1="0.7" x2="1.75" y2="-0.7" width="0.127" layer="51"/>
<wire x1="1.75" y1="-0.7" x2="0.95" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-0.95" y1="0.7" x2="-1.75" y2="0.7" width="0.127" layer="51"/>
<wire x1="-1.75" y1="0.7" x2="-1.75" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-0.7" x2="-0.95" y2="-0.7" width="0.127" layer="51"/>
<smd name="1" x="-1.524" y="0" dx="1.397" dy="0.889" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="1.524" y="0" dx="1.397" dy="0.889" layer="1" roundness="50" rot="R90"/>
<text x="-1.778" y="0.889" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;value</text>
<text x="-1.397" y="-0.254" size="0.635" layer="33" ratio="10">&gt;name</text>
<rectangle x1="-1.905" y1="-0.635" x2="1.905" y2="0.635" layer="39"/>
</package>
<package name="R0207SMD">
<wire x1="-1.6" y1="1.1" x2="-1.3804" y2="0.9431" width="0.127" layer="21"/>
<wire x1="-1.3804" y1="0.9431" x2="-1.0898" y2="0.85" width="0.127" layer="21" curve="35.541613"/>
<wire x1="-1.0898" y1="0.85" x2="1.0898" y2="0.85" width="0.127" layer="21"/>
<wire x1="1.0898" y1="0.85" x2="1.3804" y2="0.9431" width="0.127" layer="21" curve="35.534268"/>
<wire x1="1.3804" y1="0.9431" x2="1.6" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.1" x2="-1.3804" y2="-0.9431" width="0.127" layer="21"/>
<wire x1="-1.3804" y1="-0.9431" x2="-1.0898" y2="-0.85" width="0.127" layer="21" curve="-35.541613"/>
<wire x1="-1.0898" y1="-0.85" x2="1.0898" y2="-0.85" width="0.127" layer="21"/>
<wire x1="1.0898" y1="-0.85" x2="1.3804" y2="-0.9431" width="0.127" layer="21" curve="-35.534268"/>
<wire x1="1.3804" y1="-0.9431" x2="1.6" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-2.95" y2="1.1" width="0.127" layer="51"/>
<wire x1="-2.95" y1="1.1" x2="-2.95" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.95" y1="-1.1" x2="-1.6" y2="-1.1" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.1" x2="2.95" y2="1.1" width="0.127" layer="51"/>
<wire x1="2.95" y1="1.1" x2="2.95" y2="-1.1" width="0.127" layer="51"/>
<wire x1="2.95" y1="-1.1" x2="1.6" y2="-1.1" width="0.127" layer="51"/>
<smd name="1" x="-2.54" y="0" dx="2.159" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="2.54" y="0" dx="2.159" dy="1.524" layer="1" roundness="50" rot="R90"/>
<text x="-1.905" y="1.27" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.524" y="-0.381" size="0.635" layer="27" ratio="10">&gt;value</text>
<text x="-1.27" y="-0.254" size="0.635" layer="33" ratio="10">&gt;name</text>
</package>
<package name="R0515/20.4">
<wire x1="-5.08" y1="2.032" x2="5.08" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.032" x2="5.715" y2="2.413" width="0.127" layer="21"/>
<wire x1="5.715" y1="2.413" x2="7.239" y2="2.413" width="0.127" layer="21"/>
<wire x1="7.239" y1="2.413" x2="7.62" y2="2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="7.62" y1="2.032" x2="7.62" y2="-2.032" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.032" x2="7.239" y2="-2.413" width="0.127" layer="21" curve="-90"/>
<wire x1="7.239" y1="-2.413" x2="5.715" y2="-2.413" width="0.127" layer="21"/>
<wire x1="5.715" y1="-2.413" x2="5.08" y2="-2.032" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.032" x2="-5.08" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.032" x2="-5.715" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.413" x2="-7.239" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-7.239" y1="-2.413" x2="-7.62" y2="-2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="-7.62" y1="-2.032" x2="-7.62" y2="2.032" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.032" x2="-7.239" y2="2.413" width="0.127" layer="21" curve="-90"/>
<wire x1="-7.239" y1="2.413" x2="-5.715" y2="2.413" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.413" x2="-5.08" y2="2.032" width="0.127" layer="21"/>
<pad name="P$1" x="-10.2362" y="0" drill="1.0414" diameter="1.905"/>
<pad name="P$2" x="10.2362" y="0" drill="1.0414" diameter="1.905"/>
<text x="-3.81" y="2.54" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.905" y="-0.635" size="0.8128" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-9.144" y1="-0.3175" x2="-7.62" y2="0.3175" layer="21"/>
<rectangle x1="7.62" y1="-0.3175" x2="9.144" y2="0.3175" layer="21"/>
</package>
<package name="R0824/26">
<wire x1="6.508515625" y1="-3.6068" x2="-6.508515625" y2="-3.6068" width="0.127" layer="21"/>
<wire x1="-6.508515625" y1="-3.6068" x2="-6.6952625" y2="-3.63488125" width="0.127" layer="21" curve="17.102957"/>
<wire x1="-6.6952625" y1="-3.63488125" x2="-7.8335375" y2="-3.98511875" width="0.127" layer="21"/>
<wire x1="-7.8335375" y1="-3.98511875" x2="-8.020284375" y2="-4.0132" width="0.127" layer="21" curve="-17.102969"/>
<wire x1="-8.020284375" y1="-4.0132" x2="-11.3538" y2="-4.0132" width="0.127" layer="21"/>
<wire x1="-11.3538" y1="-4.0132" x2="-11.9888" y2="-3.3782" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.9888" y1="-3.3782" x2="-11.9888" y2="3.3782" width="0.127" layer="21"/>
<wire x1="-11.9888" y1="3.3782" x2="-11.3538" y2="4.0132" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.3538" y1="4.0132" x2="-8.020284375" y2="4.0132" width="0.127" layer="21"/>
<wire x1="-8.020284375" y1="4.0132" x2="-7.8335375" y2="3.98511875" width="0.127" layer="21" curve="-17.102957"/>
<wire x1="-7.8335375" y1="3.98511875" x2="-6.6952625" y2="3.63488125" width="0.127" layer="21"/>
<wire x1="-6.6952625" y1="3.63488125" x2="-6.508515625" y2="3.6068" width="0.127" layer="21" curve="17.102969"/>
<wire x1="-6.508515625" y1="3.6068" x2="6.508515625" y2="3.6068" width="0.127" layer="21"/>
<wire x1="6.508515625" y1="3.6068" x2="6.6952625" y2="3.63488125" width="0.127" layer="21" curve="17.102957"/>
<wire x1="6.6952625" y1="3.63488125" x2="7.8335375" y2="3.98511875" width="0.127" layer="21"/>
<wire x1="7.8335375" y1="3.98511875" x2="8.020284375" y2="4.0132" width="0.127" layer="21" curve="-17.102969"/>
<wire x1="8.020284375" y1="4.0132" x2="11.3538" y2="4.0132" width="0.127" layer="21"/>
<wire x1="11.3538" y1="4.0132" x2="11.9888" y2="3.3782" width="0.127" layer="21" curve="-90"/>
<wire x1="11.9888" y1="3.3782" x2="11.9888" y2="-3.3782" width="0.127" layer="21"/>
<wire x1="11.9888" y1="-3.3782" x2="11.3538" y2="-4.0132" width="0.127" layer="21" curve="-90"/>
<wire x1="11.3538" y1="-4.0132" x2="8.020284375" y2="-4.0132" width="0.127" layer="21"/>
<wire x1="8.020284375" y1="-4.0132" x2="7.8335375" y2="-3.98511875" width="0.127" layer="21" curve="-17.102957"/>
<wire x1="7.8335375" y1="-3.98511875" x2="6.6952625" y2="-3.63488125" width="0.127" layer="21"/>
<wire x1="6.6952625" y1="-3.63488125" x2="6.508515625" y2="-3.6068" width="0.127" layer="21" curve="17.102969"/>
<pad name="1" x="-13.335" y="0" drill="1.27" diameter="2.2098"/>
<pad name="2" x="13.335" y="0" drill="1.27" diameter="2.2098"/>
<text x="-4.445" y="3.683" size="1.778" layer="25" ratio="10">&gt;name</text>
<text x="-4.699" y="-0.254" size="1.778" layer="27" ratio="10">&gt;value</text>
</package>
<package name="R7519/60.4">
<wire x1="-38.1" y1="9.525" x2="38.1" y2="9.525" width="0.127" layer="21"/>
<wire x1="38.1" y1="9.525" x2="38.1" y2="-9.525" width="0.127" layer="21"/>
<wire x1="38.1" y1="-9.525" x2="-38.1" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-38.1" y1="-9.525" x2="-38.1" y2="9.525" width="0.127" layer="21"/>
<pad name="1" x="-30.226" y="2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="2" x="-30.226" y="-2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="3" x="30.226" y="2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="4" x="30.226" y="-2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<wire x1="-30.226" y1="1.524" x2="-30.226" y2="3.556" width="1.016" layer="46"/>
<wire x1="-30.226" y1="-3.556" x2="-30.226" y2="-1.524" width="1.016" layer="46"/>
<wire x1="30.226" y1="-3.556" x2="30.226" y2="-1.524" width="1.016" layer="46"/>
<wire x1="30.226" y1="1.524" x2="30.226" y2="3.556" width="1.016" layer="46"/>
<text x="-4.445" y="10.16" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-1.778" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-38.1" y1="-9.525" x2="38.1" y2="9.525" layer="39"/>
</package>
<package name="R0402">
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.762" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.762" y1="0.4445" x2="0.889" y2="0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.889" y1="0.3175" x2="0.889" y2="-0.3175" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.3175" x2="0.762" y2="-0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.762" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
<smd name="1" x="-0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<smd name="2" x="0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<text x="-1.27" y="0.635" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="R0603">
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.127" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<smd name="2" x="0.762" y="0" dx="0.889" dy="0.889" layer="1" roundness="25"/>
<text x="-1.397" y="0.762" size="0.635" layer="25" ratio="10">&gt;name</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27" ratio="10">&gt;value</text>
</package>
<package name="R0805">
<wire x1="1.651" y1="0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="-1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.889" x2="-1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<smd name="1" x="-0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<smd name="2" x="0.889" y="0" dx="1.016" dy="1.397" layer="1" roundness="25"/>
<text x="-1.778" y="1.016" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-2P-2.54-6MM">
<wire x1="1.9049" y1="1.9049" x2="1.9049" y2="-1.9049" width="0.4064" layer="21" curve="278.795288"/>
<wire x1="1.9049" y1="-1.9049" x2="1.9049" y2="1.9049" width="0.4064" layer="21"/>
<wire x1="2.032" y1="1.905" x2="2.032" y2="-1.905" width="0.127" layer="39" curve="281.928974"/>
<wire x1="2.032" y1="-1.905" x2="2.032" y2="1.905" width="0.127" layer="39"/>
<pad name="+" x="-1.2698" y="0" drill="0.889" diameter="1.651" shape="octagon"/>
<pad name="-" x="1.2698" y="0" drill="0.889" diameter="1.651"/>
<text x="2.54" y="0.508" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="1.016" size="1.016" layer="21" ratio="10">+</text>
<text x="0.889" y="1.016" size="1.016" layer="21" ratio="10">-</text>
<text x="2.54" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="0" y="-1.27" size="0.6096" layer="33" ratio="10" rot="R90">&gt;name</text>
</package>
<package name="LED-2P-2.54-8.5X5-90D">
<wire x1="-2.5" y1="0" x2="2.5" y2="0" width="0.127" layer="49"/>
<wire x1="2.5" y1="0" x2="2.5" y2="6" width="0.127" layer="49"/>
<wire x1="2.5" y1="6" x2="0" y2="8.5" width="0.127" layer="49" curve="90"/>
<wire x1="0" y1="8.5" x2="-2.5" y2="6" width="0.127" layer="49" curve="90"/>
<wire x1="-2.5" y1="6" x2="-2.5" y2="0" width="0.127" layer="49"/>
<wire x1="-2.413" y1="6.223" x2="2.54" y2="6.223" width="0.127" layer="39" curve="-171.202589"/>
<wire x1="-2.413" y1="6.223" x2="-2.413" y2="0" width="0.127" layer="39"/>
<wire x1="-2.413" y1="0" x2="2.54" y2="0" width="0.127" layer="39"/>
<wire x1="2.54" y1="0" x2="2.54" y2="6.223" width="0.127" layer="39"/>
<pad name="+" x="-1.27" y="0" drill="0.889" diameter="1.651" shape="square"/>
<pad name="-" x="1.27" y="0" drill="0.889" diameter="1.651"/>
<text x="-1.905" y="0.762" size="1.778" layer="49" ratio="10">+</text>
<text x="0.635" y="0.762" size="1.778" layer="49" ratio="10">-</text>
<text x="-3.175" y="10.795" size="1.27" layer="49" ratio="10">&gt;name</text>
<text x="-3.175" y="8.89" size="1.27" layer="49" ratio="10">&gt;value</text>
<text x="-0.508" y="4.191" size="0.254" layer="33" ratio="10">&gt;name</text>
<polygon width="0.127" layer="49">
<vertex x="2.54" y="0.635"/>
<vertex x="-3.175" y="0.635"/>
<vertex x="-3.175" y="0"/>
<vertex x="2.54" y="0"/>
</polygon>
</package>
<package name="LED-2P-2.54-3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="39" curve="282.680383"/>
<wire x1="1.524" y1="-1.27" x2="1.524" y2="1.27" width="0.127" layer="39"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.508" y="-0.127" size="0.254" layer="33" ratio="10">&gt;NAME</text>
</package>
<package name="LED-2P-2.54-4.8X2.54">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.286" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.286" y1="-1.27" x2="2.794" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="-1.27" x2="2.794" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="1.27" x2="2.286" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.286" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="-2.794" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="0.381" size="0.254" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.794" y2="1.27" layer="39"/>
</package>
<package name="LED-0603">
<wire x1="-1.3335" y1="0.635" x2="1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="0.635" x2="1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="-0.635" x2="-1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.3335" y1="-0.635" x2="-1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.381" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1905" x2="0.127" y2="0" width="0.127" layer="21"/>
<wire x1="0.127" y1="0" x2="0" y2="0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="-0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.0635" y1="0.254" x2="-0.0635" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<smd name="+" x="-0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<smd name="-" x="0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<text x="-1.778" y="0.762" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.905" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;value</text>
<rectangle x1="-1.27" y1="-0.5715" x2="1.27" y2="0.5715" layer="39"/>
</package>
<package name="LED-0805">
<wire x1="1.5875" y1="0.889" x2="1.5875" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-0.889" x2="-1.5875" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-0.889" x2="-1.5875" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="0.889" x2="1.5875" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.508" x2="-0.1905" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.254" x2="-0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0" x2="-0.1905" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="-0.254" x2="-0.1905" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="-0.508" x2="0" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="-0.1905" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.508" x2="0.127" y2="-0.0635" width="0.127" layer="21"/>
<wire x1="0.127" y1="-0.0635" x2="-0.1905" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.254" x2="0.0635" y2="-0.127" width="0.127" layer="21"/>
<wire x1="0.0635" y1="-0.127" x2="-0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0" x2="0" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="-0.254" x2="-0.1905" y2="-0.254" width="0.127" layer="21"/>
<smd name="+" x="-0.889" y="0" dx="0.889" dy="1.27" layer="1" roundness="25"/>
<smd name="-" x="0.889" y="0" dx="0.889" dy="1.27" layer="1" roundness="25"/>
<text x="-1.778" y="1.016" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-1.8415" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="-0.254" size="0.635" layer="33" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-1.5875" y1="-0.889" x2="1.5875" y2="0.889" layer="39"/>
</package>
<package name="LED-2P-2.54-5MM">
<pad name="+" x="-1.2698" y="0" drill="0.889" diameter="1.651" shape="square"/>
<pad name="-" x="1.2698" y="0" drill="0.889" diameter="1.651"/>
<text x="2.54" y="0.508" size="1.27" layer="27" ratio="10">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="2.5" width="0.127" layer="21"/>
</package>
<package name="LED-SIDE-VIEW-0603-LEFT">
<smd name="1" x="0" y="-0.95" dx="1" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="0.95" dx="1" dy="1" layer="1" rot="R90"/>
<polygon width="0.127" layer="21">
<vertex x="-0.381" y="-0.127"/>
<vertex x="0.381" y="-0.127"/>
<vertex x="0" y="0.254"/>
</polygon>
<wire x1="-0.7" y1="-1.65" x2="-0.7" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-0.7" y1="-0.6" x2="-0.7" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.6" x2="-0.7" y2="1.65" width="0.127" layer="21"/>
<wire x1="-0.7" y1="1.65" x2="0.7" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.7" y1="1.651" x2="0.7" y2="-1.65" width="0.127" layer="21"/>
<wire x1="0.7" y1="-1.65" x2="-0.7" y2="-1.65" width="0.127" layer="21"/>
<text x="-1.651" y="-1.778" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="1.778" y="-1.778" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.635" x2="1.651" y2="0.635" layer="39" rot="R90"/>
<wire x1="-0.7" y1="-0.6" x2="-0.9" y2="-0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.1" y1="-0.6" x2="-1.3" y2="-0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.3" y1="-0.6" x2="-1.3" y2="-0.4" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.3" y1="-0.1" x2="-1.3" y2="0" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.3" y1="0.6" x2="-1.1" y2="0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="-0.9" y1="0.6" x2="-0.7" y2="0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.3" y1="0.6" x2="-1.3" y2="0.4" width="0.127" layer="21" style="dashdot"/>
<wire x1="-1.3" y1="0" x2="-1.3" y2="0.1" width="0.127" layer="21" style="dashdot"/>
</package>
<package name="LED-SIDE-VIEW-0603-RIGHT">
<smd name="1" x="0" y="-0.95" dx="1" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="0.95" dx="1" dy="1" layer="1" rot="R90"/>
<polygon width="0.127" layer="21">
<vertex x="-0.381" y="-0.127"/>
<vertex x="0.381" y="-0.127"/>
<vertex x="0" y="0.254"/>
</polygon>
<wire x1="-0.7" y1="-1.65" x2="-0.7" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-0.7" y1="-0.6" x2="-0.7" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.6" x2="-0.7" y2="1.65" width="0.127" layer="21"/>
<wire x1="-0.7" y1="1.65" x2="0.7" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.7" y1="1.651" x2="0.7" y2="-1.65" width="0.127" layer="21"/>
<wire x1="0.7" y1="-1.65" x2="-0.7" y2="-1.65" width="0.127" layer="21"/>
<text x="-1.016" y="-1.778" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.413" y="-1.778" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.635" x2="1.651" y2="0.635" layer="39" rot="R90"/>
<wire x1="0.7" y1="0.6" x2="0.9" y2="0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.1" y1="0.6" x2="1.3" y2="0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.3" y1="0.6" x2="1.3" y2="0.4" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.3" y1="0.1" x2="1.3" y2="0" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.3" y1="-0.6" x2="1.1" y2="-0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="0.9" y1="-0.6" x2="0.7" y2="-0.6" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.3" y1="-0.6" x2="1.3" y2="-0.4" width="0.127" layer="21" style="dashdot"/>
<wire x1="1.3" y1="0" x2="1.3" y2="-0.1" width="0.127" layer="21" style="dashdot"/>
</package>
<package name="LED-1206">
<smd name="+" x="-1.6" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="-" x="1.6" y="0" dx="1.5" dy="1.5" layer="1"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.381" y="0.508"/>
<vertex x="-0.381" y="-0.508"/>
<vertex x="0.381" y="0"/>
</polygon>
<text x="-3.175" y="1.27" size="1.6764" layer="25" ratio="11">&gt;NAME</text>
<text x="-3.175" y="-2.54" size="1.6764" layer="27" ratio="11">&gt;VALUE</text>
</package>
<package name="LED-3528">
<wire x1="-1.75" y1="1.4" x2="1.75" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.75" y1="1.4" x2="1.75" y2="-0.72315625" width="0.127" layer="21"/>
<wire x1="1.75" y1="-0.72315625" x2="1.07315625" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.07315625" y1="-1.4" x2="-1.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-1.4" x2="-1.75" y2="1.4" width="0.127" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="2.6" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="2.6" dy="1.5" layer="1" rot="R90"/>
<text x="-3.81" y="1.905" size="1.778" layer="21" ratio="11">&gt;NAME</text>
<text x="-4.445" y="-3.81" size="1.778" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-1.75" y1="-1.4" x2="1.75" y2="1.4" layer="39"/>
<polygon width="0.127" layer="21">
<vertex x="-0.508" y="0.635"/>
<vertex x="0.508" y="0"/>
<vertex x="-0.508" y="-0.635"/>
</polygon>
</package>
<package name="LED-0402">
<smd name="+" x="-0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R90"/>
<smd name="-" x="0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R90"/>
<text x="-1.905" y="0.635" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-0.635" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.5715" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.5715" y1="0.4445" x2="0.889" y2="0.127" width="0.0762" layer="21"/>
<wire x1="0.889" y1="0.127" x2="0.889" y2="-0.127" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.127" x2="0.5715" y2="-0.4445" width="0.0762" layer="21"/>
<wire x1="0.5715" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
</package>
<package name="2P-SMD-D8.0MM">
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<smd name="+" x="-6.5" y="-0.75" dx="2.1" dy="1.8" layer="1"/>
<smd name="-" x="6.5" y="0.75" dx="2.1" dy="1.8" layer="1"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="1.27" width="0.127" layer="21"/>
<wire x1="-6.731" y1="1.27" x2="-6.731" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.731" y1="1.27" x2="-6.096" y2="1.27" width="0.127" layer="21"/>
<wire x1="-6.096" y1="1.905" x2="-6.096" y2="0.635" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4" width="0.127" layer="39"/>
<text x="-1.905" y="4.445" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-1.905" y="-0.635" size="0.635" layer="25" ratio="10">&gt;value</text>
</package>
<package name="C0402">
<description>&lt;b&gt;0402&lt;b&gt;&lt;p&gt;</description>
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.762" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.762" y1="0.4445" x2="0.889" y2="0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.889" y1="0.3175" x2="0.889" y2="-0.3175" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.3175" x2="0.762" y2="-0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.762" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
<smd name="1" x="-0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<smd name="2" x="0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<text x="-1.27" y="0.635" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.0254" layer="51">
<vertex x="-0.508" y="0.254"/>
<vertex x="0.508" y="0.254"/>
<vertex x="0.508" y="-0.254"/>
<vertex x="-0.508" y="-0.254"/>
</polygon>
</package>
<package name="C0402S">
<wire x1="-0.6985" y1="0.254" x2="0.6985" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="0.254" x2="0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="-0.254" x2="-0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.6985" y1="-0.254" x2="-0.6985" y2="0.254" width="0.0762" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<smd name="2" x="0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<text x="-0.635" y="0.381" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.254" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.0254" layer="51">
<vertex x="-0.508" y="0.254"/>
<vertex x="0.508" y="0.254"/>
<vertex x="0.508" y="-0.254"/>
<vertex x="-0.508" y="-0.254"/>
</polygon>
</package>
<package name="R0402S">
<wire x1="-0.6985" y1="0.254" x2="0.6985" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="0.254" x2="0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.6985" y1="-0.254" x2="-0.6985" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.6985" y1="-0.254" x2="-0.6985" y2="0.254" width="0.0762" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<smd name="2" x="0.508" y="0" dx="0.381" dy="0.508" layer="1" roundness="50"/>
<text x="-0.635" y="0.381" size="0.635" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.254" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.0254" layer="51">
<vertex x="-0.508" y="0.254"/>
<vertex x="0.508" y="0.254"/>
<vertex x="0.508" y="-0.254"/>
<vertex x="-0.508" y="-0.254"/>
</polygon>
</package>
<package name="3P-SMD-4.1X1.5">
<wire x1="-2.032" y1="0.762" x2="2.032" y2="0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="-2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-0.762" x2="-2.032" y2="0.762" width="0.127" layer="21"/>
<smd name="MM" x="0" y="0" dx="1.016" dy="1.016" layer="1" rot="R180"/>
<smd name="00" x="-1.27" y="0" dx="1.016" dy="1.016" layer="1" rot="R180"/>
<smd name="11" x="1.27" y="0" dx="1.016" dy="1.016" layer="1" rot="R180"/>
<text x="-1.778" y="0.889" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="0" size="0.3048" layer="33" ratio="10">&gt;name</text>
<text x="-2.159" y="-1.778" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="0.762" layer="39"/>
<rectangle x1="-1.905" y1="-0.635" x2="1.905" y2="0.635" layer="41"/>
</package>
<package name="NPN1.27">
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21" curve="-229.999205"/>
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<pad name="E" x="-1.27" y="0" drill="0.508" diameter="0.762" shape="square"/>
<pad name="B" x="0" y="0" drill="0.508" diameter="0.762"/>
<pad name="C" x="1.27" y="0" drill="0.508" diameter="0.762"/>
<text x="-2.794" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.508" y="0.254" size="0.254" layer="33">&gt;NAME</text>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.27" y="0.635" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="-1.27" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;value</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
<package name="TO-92-TRIANGLE">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-0.7863" y2="2.5485" width="0.1524" layer="21" curve="-111.098957"/>
<wire x1="0.7868" y1="2.5484" x2="2.095" y2="-1.651" width="0.1524" layer="21" curve="-111.09954"/>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.655" y1="-0.254" x2="-2.254" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="2.254" y1="-0.254" x2="2.655" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.1524" layer="51" curve="-34.293591"/>
<wire x1="-2.0955" y1="-1.651" x2="2.0955" y2="-1.651" width="0.1524" layer="39" curve="-256.46765"/>
<wire x1="-2.0955" y1="-1.651" x2="2.0955" y2="-1.651" width="0.1524" layer="39"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.524"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.524"/>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.524"/>
<text x="-1.778" y="-2.667" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.524" y="-0.127" size="0.635" layer="33" ratio="10">&gt;NAME</text>
</package>
<package name="SOT323">
<description>SC-70:3-lead</description>
<smd name="1" x="-0.635" y="-0.889" dx="0.762" dy="0.635" layer="1" rot="R90"/>
<smd name="2" x="0.635" y="-0.889" dx="0.762" dy="0.635" layer="1" rot="R90"/>
<smd name="3" x="0" y="0.889" dx="0.762" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.016" y1="0.635" x2="1.016" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="-1.016" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.635" x2="-1.016" y2="0.635" width="0.127" layer="21"/>
<rectangle x1="-1.016" y1="-0.635" x2="1.016" y2="0.635" layer="39"/>
<text x="-1.27" y="1.27" size="0.889" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-0.381" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="2P-SMD-1.78X1.27">
<wire x1="-0.635" y1="0.889" x2="0.635" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.889" x2="-0.635" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.889" x2="-0.635" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="0.508" y2="0.762" width="0" layer="39"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="-0.762" width="0" layer="39"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0" layer="39"/>
<wire x1="-0.508" y1="-0.762" x2="-0.508" y2="0.762" width="0" layer="39"/>
<smd name="1" x="0" y="0.381" dx="0.889" dy="0.6096" layer="1"/>
<smd name="2" x="0" y="-0.381" dx="0.889" dy="0.6096" layer="1"/>
<text x="-1.27" y="1.27" size="0.635" layer="25" ratio="10">&gt;name</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="41"/>
</package>
<package name="4P-SMD-7.0X3.5X3.5H-90D">
<wire x1="-3" y1="1.5" x2="3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3" y1="1.5" x2="3" y2="0.75" width="0.127" layer="21"/>
<wire x1="3" y1="0.75" x2="3.5" y2="0.25" width="0.127" layer="21"/>
<wire x1="3.5" y1="0.25" x2="3.5" y2="-1" width="0.127" layer="21"/>
<wire x1="3.5" y1="-1" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-1" x2="-3.5" y2="0.25" width="0.127" layer="21"/>
<wire x1="-3.5" y1="0.25" x2="-3" y2="0.75" width="0.127" layer="21"/>
<wire x1="-3" y1="0.75" x2="-3" y2="1.5" width="0.127" layer="21"/>
<smd name="1" x="-2.159" y="1.651" dx="1.778" dy="1.397" layer="1" rot="R90"/>
<smd name="2" x="2.159" y="1.651" dx="1.778" dy="1.397" layer="1" rot="R90"/>
<smd name="S1" x="-3.556" y="-0.508" dx="1.778" dy="1.016" layer="1" thermals="no"/>
<smd name="S2" x="3.556" y="-0.508" dx="1.778" dy="1.016" layer="1" thermals="no"/>
<hole x="-1.15" y="0" drill="0.95"/>
<hole x="1.15" y="0" drill="0.95"/>
<wire x1="-3.5" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="-1" y1="-1" x2="-0.75" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-0.75" y1="-1.25" x2="-0.75" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-0.75" y1="-1.75" x2="-0.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2" x2="0.5" y2="-2" width="0.127" layer="21"/>
<wire x1="0.5" y1="-2" x2="0.75" y2="-1.75" width="0.127" layer="21"/>
<wire x1="0.75" y1="-1.75" x2="0.75" y2="-1.25" width="0.127" layer="21"/>
<wire x1="0.75" y1="-1.25" x2="1" y2="-1" width="0.127" layer="21"/>
<text x="-1.905" y="2.54" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-0.635" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="4P-SMD-4.7X3.5X1.8">
<wire x1="-1" y1="1.05" x2="1" y2="1.05" width="0.127" layer="21"/>
<wire x1="1" y1="1.05" x2="1.5" y2="1.05" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.05" x2="2" y2="1.05" width="0.127" layer="21"/>
<wire x1="2" y1="1.05" x2="2.35" y2="1.05" width="0.127" layer="21"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="0" width="0.127" layer="21"/>
<wire x1="2.35" y1="0" x2="2.159" y2="-0.191" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.191" x2="2.159" y2="-1.2" width="0.127" layer="21"/>
<wire x1="2.159" y1="-1.2" x2="-2.159" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-1.2" x2="-2.159" y2="-0.191" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-0.191" x2="-2.35" y2="0" width="0.127" layer="21"/>
<wire x1="-2.35" y1="0" x2="-2.35" y2="1.05" width="0.127" layer="21"/>
<wire x1="-1" y1="1.05" x2="-1" y2="2.2" width="0.127" layer="21"/>
<wire x1="-1" y1="2.2" x2="-0.9" y2="2.3" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.9" y1="2.3" x2="0.9" y2="2.3" width="0.127" layer="21"/>
<wire x1="0.9" y1="2.3" x2="1" y2="2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="2.2" x2="1" y2="1.05" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.05" x2="1.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.2" x2="2" y2="1.2" width="0.127" layer="21"/>
<wire x1="2" y1="1.2" x2="2" y2="1.05" width="0.127" layer="21"/>
<wire x1="1.55" y1="1.15" x2="1.95" y2="1.15" width="0.127" layer="21"/>
<wire x1="-1" y1="1.05" x2="-1.5" y2="1.05" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-2" y2="1.05" width="0.127" layer="21"/>
<wire x1="-2" y1="1.05" x2="-2.35" y2="1.05" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.2" x2="-2" y2="1.2" width="0.127" layer="21"/>
<wire x1="-2" y1="1.2" x2="-2" y2="1.05" width="0.127" layer="21"/>
<wire x1="-1.55" y1="1.15" x2="-1.95" y2="1.15" width="0.127" layer="21"/>
<smd name="1" x="-1.675" y="-1.277" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="1.675" y="-1.277" dx="0.8" dy="0.4" layer="1" rot="R90"/>
<smd name="S1" x="-2.425" y="0.5" dx="1" dy="0.6" layer="1" rot="R270"/>
<smd name="S2" x="2.425" y="0.5" dx="1" dy="0.6" layer="1" rot="R270"/>
<text x="-1.905" y="-2.54" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-1.778" y="-0.381" size="0.635" layer="27" ratio="11">&gt;value</text>
</package>
<package name="3P-SMD-3.6X1.3">
<wire x1="1.7" y1="0.65" x2="1.8" y2="0.65" width="0.2032" layer="21"/>
<wire x1="1.8" y1="0.65" x2="1.8" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-0.65" x2="1.7" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-0.65" x2="-1.85" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="-0.65" x2="-1.85" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="0.65" x2="-1.7" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="2" x="0" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="3" x="1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<text x="-1.905" y="1.016" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-1.778" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="-0.127" size="0.254" layer="33">&gt;Name</text>
<rectangle x1="-1.905" y1="-0.889" x2="1.778" y2="0.889" layer="39"/>
</package>
<package name="3P-2.54-4.6X3.6">
<wire x1="-2.286" y1="1.778" x2="-2.286" y2="-1.778" width="0.127" layer="21" curve="162.403081"/>
<wire x1="2.286" y1="-1.778" x2="2.286" y2="1.778" width="0.127" layer="21" curve="162.403081"/>
<wire x1="-2.286" y1="-1.778" x2="2.286" y2="-1.778" width="0.127" layer="21" curve="12.71932"/>
<wire x1="2.286" y1="1.778" x2="-2.286" y2="1.778" width="0.127" layer="21" curve="12.758496"/>
<wire x1="-0.508" y1="-1.905" x2="0.508" y2="-1.905" width="0.127" layer="39"/>
<wire x1="0.508" y1="-1.905" x2="2.286" y2="-1.778" width="0.127" layer="39"/>
<wire x1="2.286" y1="-1.778" x2="2.159" y2="1.778" width="0.127" layer="39" curve="167.768993"/>
<wire x1="2.159" y1="1.778" x2="0.508" y2="1.905" width="0.127" layer="39"/>
<wire x1="0.508" y1="1.905" x2="-0.508" y2="1.905" width="0.127" layer="39"/>
<wire x1="-0.508" y1="1.905" x2="-2.286" y2="1.778" width="0.127" layer="39"/>
<wire x1="-0.508" y1="-1.905" x2="-2.286" y2="-1.778" width="0.127" layer="39"/>
<wire x1="-2.286" y1="-1.778" x2="-2.286" y2="1.778" width="0.127" layer="39" curve="-163.739795"/>
<pad name="2" x="0" y="0" drill="0.8"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="3" x="2.54" y="0" drill="0.8"/>
<text x="-2.54" y="2.032" size="1.016" layer="25" ratio="10">&gt;name</text>
<text x="-2.667" y="-3.048" size="1.016" layer="27" ratio="10">&gt;value</text>
<text x="-0.762" y="-0.127" size="0.3048" layer="33" ratio="10">&gt;name</text>
</package>
<package name="3P-SMD-4.5X2.0X1.2H">
<wire x1="-2.25" y1="1" x2="2.25" y2="1" width="0.127" layer="21"/>
<wire x1="2.25" y1="1" x2="2.25" y2="-1" width="0.127" layer="21"/>
<wire x1="2.25" y1="-1" x2="-2.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-1" x2="-2.25" y2="1" width="0.127" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="2.6" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="2.6" dy="0.8" layer="1" rot="R90"/>
<smd name="3" x="1.5" y="0" dx="2.6" dy="0.8" layer="1" rot="R90"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-0.635" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TP_3535">
<circle x="0" y="0" radius="0.635" width="0.127" layer="21"/>
<smd name="T" x="0" y="0" dx="0.889" dy="0.889" layer="1" roundness="100" thermals="no" cream="no"/>
<text x="-1.778" y="0.889" size="0.8128" layer="25" ratio="10">&gt;name</text>
</package>
<package name="TP_4545">
<circle x="0" y="0" radius="0.762" width="0.127" layer="21"/>
<smd name="T" x="0" y="0" dx="1.143" dy="1.143" layer="1" roundness="100" thermals="no" cream="no"/>
<text x="-1.651" y="0.889" size="0.8128" layer="25" ratio="10">&gt;name</text>
</package>
<package name="PAD_4080">
<smd name="1" x="0" y="0" dx="2.032" dy="1.016" layer="1" roundness="50" rot="R180"/>
<wire x1="-1.143" y1="0.635" x2="1.143" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.143" y1="-0.635" x2="-1.143" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.127" layer="21"/>
<text x="-1.27" y="0.889" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-1.27" y="-1.651" size="0.889" layer="27" ratio="11">&gt;value</text>
</package>
<package name="1P-D6MM">
<smd name="1" x="0" y="0" dx="7.5" dy="7.5" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="0" radius="3.81" width="0.127" layer="41"/>
</package>
<package name="ROUND-MARK-1.0">
<circle x="0" y="0" radius="1.27" width="0" layer="29"/>
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="0" radius="1.016" width="0.2032" layer="1"/>
</package>
<package name="ROUND-MARK-1.0/2.0">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100"/>
<circle x="0" y="0" radius="1.016" width="0" layer="41"/>
<circle x="0" y="0" radius="1.016" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="0" size="1.27" layer="95">&gt;name</text>
<text x="0" y="0" size="1.27" layer="96">&gt;value</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="L">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-6.35" y="0" size="1.27" layer="95">&gt;name</text>
<text x="2.54" y="0" size="1.27" layer="96">&gt;value</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R">
<wire x1="-1.27" y1="0.508" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<text x="-3.81" y="0" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="2.54" y="0" size="1.27" layer="96" ratio="10">&gt;value</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-3.81" y="0" size="1.27" layer="95">&gt;name</text>
<text x="0" y="0" size="1.27" layer="96">&gt;value</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="JUMPER">
<text x="2.54" y="-3.81" size="1.778" layer="95" ratio="10" rot="R90">&gt;name</text>
<pin name="L" x="-2.54" y="2.54" visible="off" length="short"/>
<pin name="C" x="-2.54" y="0" visible="off" length="short"/>
<pin name="R" x="-2.54" y="-2.54" visible="off" length="short"/>
<text x="-2.032" y="-0.508" size="1.016" layer="91" font="vector" ratio="10">C</text>
<rectangle x1="-1.016" y1="1.524" x2="1.016" y2="3.556" layer="94"/>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" layer="94"/>
<rectangle x1="-1.016" y1="-3.556" x2="1.016" y2="-1.524" layer="94"/>
</symbol>
<symbol name="NPN">
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.905" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="94"/>
<text x="-2.54" y="0" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="-2.54" y="-1.27" size="1.27" layer="96" ratio="10">&gt;value</text>
<text x="-0.635" y="0" size="0.635" layer="93" ratio="10">B</text>
<text x="1.905" y="2.54" size="0.635" layer="93" ratio="10">C</text>
<text x="1.905" y="-3.175" size="0.635" layer="93" ratio="10">E</text>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="JP_SW-1">
<text x="-2.54" y="2.794" size="1.27" layer="95" ratio="10">&gt;name</text>
<pin name="1" x="2.54" y="-1.27" visible="off" length="short" rot="R180"/>
<pin name="2" x="2.54" y="1.27" visible="off" length="short" rot="R180"/>
<rectangle x1="-1.016" y1="0.254" x2="1.016" y2="2.286" layer="94" rot="R90"/>
<rectangle x1="-1.016" y1="-2.286" x2="1.016" y2="-0.254" layer="94" rot="R90"/>
</symbol>
<symbol name="BUTTON-4P-SMD-1">
<pin name="S1" x="-6.35" y="1.27" visible="pad" length="short"/>
<pin name="S2" x="6.35" y="1.27" visible="pad" length="short" rot="R180"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.1524" layer="94"/>
<pin name="1" x="-6.35" y="-1.27" visible="pad" length="short"/>
<pin name="2" x="6.35" y="-1.27" visible="pad" length="short" rot="R180"/>
<wire x1="-3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<circle x="1.27" y="0" radius="0.254" width="0" layer="94"/>
<circle x="1.27" y="-1.27" radius="0.254" width="0" layer="94"/>
</symbol>
<symbol name="RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.032" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="1.778" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-3.048" x2="2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="1.778" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-3.048" x2="-2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.032" x2="-2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<text x="-3.81" y="3.556" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="-7.62" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="TEST_POINT">
<text x="-5.08" y="0" size="0.8128" layer="95" ratio="10">&gt;name</text>
<pin name="T" x="-2.54" y="0" visible="off" length="point"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="MARK">
<circle x="0" y="0" radius="0.635" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="0.8128" layer="95" ratio="10">&gt;name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C*" prefix="C" uservalue="yes">
<description>&lt;b&gt;Ceramic Capacitors&lt;b&gt;</description>
<gates>
<gate name="C" symbol="C" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="-0402" package="C0402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="C0603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="C0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0512" package="C0512/10.18">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'2.54'" package="CERAMIC-2.54">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'5.08'" package="CERAMIC-5.08">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1210" package="C1210">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-0402S'" package="C0402S">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L*" prefix="L" uservalue="yes">
<gates>
<gate name="L" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="'1813'" package="1813">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'1616BZ'" package="1616BZ">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'2020BZ'" package="2020BZ">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0806'" package="0806">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'11.4MM'" package="2P-ROUND-11.4MM">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'2218'" package="2218">
<connects>
<connect gate="L" pin="1" pad="P$1"/>
<connect gate="L" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'5020'" package="5020">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'PIO73'" package="PIO73">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'PIO105'" package="PIO105">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'6X8H'" package="2P-6*8H">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'IHLP-2525BD-01'" package="2626">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0402'" package="L0402">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0805'" package="L0805">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0603'" package="L0603">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0630'" package="L0630">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0420'" package="L0420">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-'0402S'" package="L0402S">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-0603S'" package="L0603S">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R*" prefix="R" uservalue="yes">
<gates>
<gate name="R" symbol="R" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="-0204/5" package="R0204/5">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0204/7" package="R0204/7">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0204V" package="R0204V">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/10" package="R0207/10">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/12" package="R0207/12">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/2V" package="R0207V">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/5V" package="R0207/5">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/7" package="R0207/7">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/10" package="R0309/10">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/12" package="R0309/12">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/15" package="R0309/15">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/20" package="R0309/20">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309V" package="R0309V">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2512" package="R2512">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0821/27-5W" package="R0821/27">
<connects>
<connect gate="R" pin="1" pad="P$1"/>
<connect gate="R" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0204SMD" package="R0204SMD">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207SMD" package="R0207SMD">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0515" package="R0515/20.4">
<connects>
<connect gate="R" pin="1" pad="P$1"/>
<connect gate="R" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0824/26-5W" package="R0824/26">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7519" package="R7519/60.4">
<connects>
<connect gate="R" pin="1" pad="1 2"/>
<connect gate="R" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R0402">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="R0603">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="R0805">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-0402S'" package="R0402S">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED*" prefix="D" uservalue="yes">
<gates>
<gate name="LED" symbol="LED" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="-6MM" package="LED-2P-2.54-6MM">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FLSH_LED" package="LED-2P-2.54-8.5X5-90D">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM" package="LED-2P-2.54-3MM">
<connects>
<connect gate="LED" pin="+" pad="A"/>
<connect gate="LED" pin="-" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SQUARE" package="LED-2P-2.54-4.8X2.54">
<connects>
<connect gate="LED" pin="+" pad="A"/>
<connect gate="LED" pin="-" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0603'" package="LED-0603">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0805'" package="LED-0805">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'TEST'" package="LED-2P-2.54-5MM">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'LEFT'" package="LED-SIDE-VIEW-0603-LEFT">
<connects>
<connect gate="LED" pin="+" pad="1"/>
<connect gate="LED" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'RIGHT'" package="LED-SIDE-VIEW-0603-RIGHT">
<connects>
<connect gate="LED" pin="+" pad="1"/>
<connect gate="LED" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'1206'" package="LED-1206">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'3528'" package="LED-3528">
<connects>
<connect gate="LED" pin="+" pad="1"/>
<connect gate="LED" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'0402'" package="LED-0402">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'8MM'" package="2P-SMD-D8.0MM">
<connects>
<connect gate="LED" pin="+" pad="+"/>
<connect gate="LED" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD-JUMPER-3P" prefix="P" uservalue="yes">
<gates>
<gate name="G$1" symbol="JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="-40*40" package="3P-SMD-4.1X1.5">
<connects>
<connect gate="G$1" pin="C" pad="MM"/>
<connect gate="G$1" pin="L" pad="00"/>
<connect gate="G$1" pin="R" pad="11"/>
</connects>
<technologies>
<technology name="">
<attribute name="PARTNO" value="DNP"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR-NPN*" prefix="Q" uservalue="yes">
<description>&lt;b&gt;NPN audion&lt;b&gt;&lt;p&gt;</description>
<gates>
<gate name="Q" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="-1.27" package="NPN1.27">
<connects>
<connect gate="Q" pin="B" pad="B"/>
<connect gate="Q" pin="C" pad="C"/>
<connect gate="Q" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-92" package="TO-92-TRIANGLE">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT323" package="SOT323">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD-JUMPER-2P" prefix="P" uservalue="yes">
<gates>
<gate name="G$1" symbol="JP_SW-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2P-SMD-1.78X1.27">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PARTNO" value="DNP"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUTTON-2P-REINFORCE" prefix="K">
<description>TS-1188E
&lt;br&gt;http://www.best-dz.com/?thread-374-2.html
&lt;BR&gt;SMD Button</description>
<gates>
<gate name="K" symbol="BUTTON-4P-SMD-1" x="0" y="0"/>
</gates>
<devices>
<device name="-3100060P1" package="4P-SMD-7.0X3.5X3.5H-90D">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="K" pin="S1" pad="S1"/>
<connect gate="K" pin="S2" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="4P-SMD-4.7X3.5X1.8">
<connects>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
<connect gate="K" pin="S1" pad="S1"/>
<connect gate="K" pin="S2" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-RESONATOR" prefix="X" uservalue="yes">
<description>&lt;b&gt;Resonator&lt;/b&gt;
Small SMD resonator. This is the itty bitty 10/20MHz resonators with built in caps. CSTCE10M0G55 and CSTCE20M0V53. Footprint has been reviewed closely but hasn't been tested yet.</description>
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="'SMD'" package="3P-SMD-3.6X1.3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'2.54'" package="3P-2.54-4.6X3.6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'CSTCR6M00G53-R0'" package="3P-SMD-4.5X2.0X1.2H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD-TEST-POINT" prefix="TP" uservalue="yes">
<gates>
<gate name="TP" symbol="TEST_POINT" x="0" y="0"/>
</gates>
<devices>
<device name="'3535'" package="TP_3535">
<connects>
<connect gate="TP" pin="T" pad="T"/>
</connects>
<technologies>
<technology name="">
<attribute name="PARTNO" value="DNP"/>
</technology>
</technologies>
</device>
<device name="'4545'" package="TP_4545">
<connects>
<connect gate="TP" pin="T" pad="T"/>
</connects>
<technologies>
<technology name="">
<attribute name="PARTNO" value="DNP"/>
</technology>
</technologies>
</device>
<device name="'4080'" package="PAD_4080">
<connects>
<connect gate="TP" pin="T" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1P-D6MM">
<connects>
<connect gate="TP" pin="T" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD-MARK" prefix="P" uservalue="yes">
<gates>
<gate name="G$1" symbol="MARK" x="0" y="0"/>
</gates>
<devices>
<device name="'ANNULAR'" package="ROUND-MARK-1.0">
<technologies>
<technology name="">
<attribute name="PARTNO" value="DNP"/>
</technology>
</technologies>
</device>
<device name="'SOLID'" package="ROUND-MARK-1.0/2.0">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="moudle">
<packages>
<package name="ARDUINO-38P">
<wire x1="22.606" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="49"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="49"/>
<wire x1="17.526" y1="52.07" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="26.67" y1="3.81" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="49"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="49"/>
<wire x1="27.432" y1="2.54" x2="28.448" y2="2.54" width="0.0254" layer="49"/>
<wire x1="27.94" y1="2.032" x2="27.94" y2="3.048" width="0.0254" layer="49"/>
<wire x1="29.972" y1="2.54" x2="30.988" y2="2.54" width="0.0254" layer="49"/>
<wire x1="30.48" y1="2.032" x2="30.48" y2="3.048" width="0.0254" layer="49"/>
<wire x1="63.5" y1="24.892" x2="63.5" y2="25.908" width="0.0254" layer="49"/>
<wire x1="64.008" y1="25.4" x2="62.992" y2="25.4" width="0.0254" layer="49"/>
<wire x1="63.5" y1="27.432" x2="63.5" y2="28.448" width="0.0254" layer="49"/>
<wire x1="64.008" y1="27.94" x2="62.992" y2="27.94" width="0.0254" layer="49"/>
<wire x1="63.5" y1="29.972" x2="63.5" y2="30.988" width="0.0254" layer="49"/>
<wire x1="64.008" y1="30.48" x2="62.992" y2="30.48" width="0.0254" layer="49"/>
<wire x1="66.04" y1="24.892" x2="66.04" y2="25.908" width="0.0254" layer="49"/>
<wire x1="66.548" y1="25.4" x2="65.532" y2="25.4" width="0.0254" layer="49"/>
<wire x1="66.04" y1="27.432" x2="66.04" y2="28.448" width="0.0254" layer="49"/>
<wire x1="66.548" y1="27.94" x2="65.532" y2="27.94" width="0.0254" layer="49"/>
<wire x1="66.04" y1="29.972" x2="66.04" y2="30.988" width="0.0254" layer="49"/>
<wire x1="66.548" y1="30.48" x2="65.532" y2="30.48" width="0.0254" layer="49"/>
<wire x1="62.23" y1="31.75" x2="67.31" y2="31.75" width="0.127" layer="21"/>
<wire x1="67.31" y1="31.75" x2="67.31" y2="24.13" width="0.127" layer="21"/>
<wire x1="67.31" y1="24.13" x2="62.23" y2="24.13" width="0.127" layer="21"/>
<wire x1="62.23" y1="24.13" x2="62.23" y2="31.75" width="0.127" layer="21"/>
<pad name="1" x="63.5" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="60.96" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="3" x="58.42" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="4" x="55.88" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="5" x="53.34" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="6" x="50.8" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="7" x="48.26" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="8" x="45.72" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="9" x="41.656" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="10" x="39.116" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="11" x="36.576" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="12" x="34.036" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="13" x="31.496" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="14" x="28.956" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="15" x="26.416" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="16" x="23.876" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="19" x="50.8" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="20" x="53.34" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="21" x="55.88" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="22" x="58.42" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="23" x="60.96" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="24" x="63.5" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="25" x="45.72" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="26" x="43.18" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="27" x="40.64" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="28" x="38.1" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="29" x="35.56" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="30" x="33.02" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="31" x="30.48" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="32" x="27.94" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="38" x="63.5" y="30.48" drill="0.889" diameter="1.651" shape="square"/>
<pad name="34" x="66.04" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="35" x="66.04" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="36" x="63.5" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="37" x="63.5" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="33" x="66.04" y="30.48" drill="0.889" diameter="1.651"/>
<hole x="15.24" y="50.8" drill="3.2004"/>
<hole x="66.04" y="35.56" drill="3.2004"/>
<hole x="66.04" y="7.62" drill="3.2004"/>
<pad name="17" x="21.336" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="18" x="18.796" y="50.8" drill="0.889" diameter="1.651"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="49"/>
<wire x1="42.926" y1="49.53" x2="22.606" y2="49.53" width="0.127" layer="49"/>
<wire x1="22.606" y1="49.53" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="22.606" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="52.07" x2="64.77" y2="52.07" width="0.127" layer="49"/>
<wire x1="64.77" y1="52.07" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="44.45" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="31.75" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="49"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="49"/>
<wire x1="46.99" y1="1.27" x2="31.75" y2="1.27" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="49"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="49"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="49"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="49"/>
<wire x1="14.732" y1="50.8" x2="15.748" y2="50.8" width="0.0254" layer="49"/>
<wire x1="15.24" y1="50.292" x2="15.24" y2="51.308" width="0.0254" layer="49"/>
<wire x1="65.532" y1="35.56" x2="66.548" y2="35.56" width="0.0254" layer="49"/>
<wire x1="66.04" y1="35.052" x2="66.04" y2="36.068" width="0.0254" layer="49"/>
<wire x1="65.532" y1="7.62" x2="66.548" y2="7.62" width="0.0254" layer="49"/>
<wire x1="66.04" y1="7.112" x2="66.04" y2="8.128" width="0.0254" layer="49"/>
<wire x1="41.148" y1="50.8" x2="42.164" y2="50.8" width="0.0254" layer="49"/>
<wire x1="41.656" y1="50.292" x2="41.656" y2="51.308" width="0.0254" layer="49"/>
<wire x1="23.368" y1="50.8" x2="24.384" y2="50.8" width="0.0254" layer="49"/>
<wire x1="23.876" y1="50.292" x2="23.876" y2="51.308" width="0.0254" layer="49"/>
<wire x1="25.908" y1="50.8" x2="26.924" y2="50.8" width="0.0254" layer="49"/>
<wire x1="26.416" y1="50.292" x2="26.416" y2="51.308" width="0.0254" layer="49"/>
<wire x1="28.448" y1="50.8" x2="29.464" y2="50.8" width="0.0254" layer="49"/>
<wire x1="28.956" y1="50.292" x2="28.956" y2="51.308" width="0.0254" layer="49"/>
<wire x1="30.988" y1="50.8" x2="32.004" y2="50.8" width="0.0254" layer="49"/>
<wire x1="31.496" y1="50.292" x2="31.496" y2="51.308" width="0.0254" layer="49"/>
<wire x1="33.528" y1="50.8" x2="34.544" y2="50.8" width="0.0254" layer="49"/>
<wire x1="34.036" y1="50.292" x2="34.036" y2="51.308" width="0.0254" layer="49"/>
<wire x1="36.068" y1="50.8" x2="37.084" y2="50.8" width="0.0254" layer="49"/>
<wire x1="36.576" y1="50.292" x2="36.576" y2="51.308" width="0.0254" layer="49"/>
<wire x1="38.608" y1="50.8" x2="39.624" y2="50.8" width="0.0254" layer="49"/>
<wire x1="39.116" y1="50.292" x2="39.116" y2="51.308" width="0.0254" layer="49"/>
<wire x1="62.992" y1="50.8" x2="64.008" y2="50.8" width="0.0254" layer="49"/>
<wire x1="63.5" y1="50.292" x2="63.5" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="50.8" x2="46.228" y2="50.8" width="0.0254" layer="49"/>
<wire x1="45.72" y1="50.292" x2="45.72" y2="51.308" width="0.0254" layer="49"/>
<wire x1="47.752" y1="50.8" x2="48.768" y2="50.8" width="0.0254" layer="49"/>
<wire x1="48.26" y1="50.292" x2="48.26" y2="51.308" width="0.0254" layer="49"/>
<wire x1="50.292" y1="50.8" x2="51.308" y2="50.8" width="0.0254" layer="49"/>
<wire x1="50.8" y1="50.292" x2="50.8" y2="51.308" width="0.0254" layer="49"/>
<wire x1="52.832" y1="50.8" x2="53.848" y2="50.8" width="0.0254" layer="49"/>
<wire x1="53.34" y1="50.292" x2="53.34" y2="51.308" width="0.0254" layer="49"/>
<wire x1="55.372" y1="50.8" x2="56.388" y2="50.8" width="0.0254" layer="49"/>
<wire x1="55.88" y1="50.292" x2="55.88" y2="51.308" width="0.0254" layer="49"/>
<wire x1="57.912" y1="50.8" x2="58.928" y2="50.8" width="0.0254" layer="49"/>
<wire x1="58.42" y1="50.292" x2="58.42" y2="51.308" width="0.0254" layer="49"/>
<wire x1="60.452" y1="50.8" x2="61.468" y2="50.8" width="0.0254" layer="49"/>
<wire x1="60.96" y1="50.292" x2="60.96" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="2.54" x2="46.228" y2="2.54" width="0.0254" layer="49"/>
<wire x1="45.72" y1="2.032" x2="45.72" y2="3.048" width="0.0254" layer="49"/>
<wire x1="32.512" y1="2.54" x2="33.528" y2="2.54" width="0.0254" layer="49"/>
<wire x1="33.02" y1="2.032" x2="33.02" y2="3.048" width="0.0254" layer="49"/>
<wire x1="35.052" y1="2.54" x2="36.068" y2="2.54" width="0.0254" layer="49"/>
<wire x1="35.56" y1="2.032" x2="35.56" y2="3.048" width="0.0254" layer="49"/>
<wire x1="37.592" y1="2.54" x2="38.608" y2="2.54" width="0.0254" layer="49"/>
<wire x1="38.1" y1="2.032" x2="38.1" y2="3.048" width="0.0254" layer="49"/>
<wire x1="40.132" y1="2.54" x2="41.148" y2="2.54" width="0.0254" layer="49"/>
<wire x1="40.64" y1="2.032" x2="40.64" y2="3.048" width="0.0254" layer="49"/>
<wire x1="42.672" y1="2.54" x2="43.688" y2="2.54" width="0.0254" layer="49"/>
<wire x1="43.18" y1="2.032" x2="43.18" y2="3.048" width="0.0254" layer="49"/>
<wire x1="50.292" y1="2.54" x2="51.308" y2="2.54" width="0.0254" layer="49"/>
<wire x1="50.8" y1="2.032" x2="50.8" y2="3.048" width="0.0254" layer="49"/>
<wire x1="52.832" y1="2.54" x2="53.848" y2="2.54" width="0.0254" layer="49"/>
<wire x1="53.34" y1="2.032" x2="53.34" y2="3.048" width="0.0254" layer="49"/>
<wire x1="55.372" y1="2.54" x2="56.388" y2="2.54" width="0.0254" layer="49"/>
<wire x1="55.88" y1="2.032" x2="55.88" y2="3.048" width="0.0254" layer="49"/>
<wire x1="57.912" y1="2.54" x2="58.928" y2="2.54" width="0.0254" layer="49"/>
<wire x1="58.42" y1="2.032" x2="58.42" y2="3.048" width="0.0254" layer="49"/>
<wire x1="60.452" y1="2.54" x2="61.468" y2="2.54" width="0.0254" layer="49"/>
<wire x1="60.96" y1="2.032" x2="60.96" y2="3.048" width="0.0254" layer="49"/>
<wire x1="62.992" y1="2.54" x2="64.008" y2="2.54" width="0.0254" layer="49"/>
<wire x1="63.5" y1="2.032" x2="63.5" y2="3.048" width="0.0254" layer="49"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="49"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="49"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="49"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="49"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="49"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="49"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="49"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="49"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="49"/>
<wire x1="47.99" y1="44.37" x2="45.99" y2="44.37" width="0.127" layer="49"/>
<wire x1="48.49" y1="34.37" x2="45.49" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="44.79" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="34.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.19" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="44.37" x2="44.19" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.19" y1="42.57" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="42.57" x2="44.79" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.19" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.19" y1="36.17" x2="44.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="34.37" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="56.88" y1="44.37" x2="54.88" y2="44.37" width="0.127" layer="49"/>
<wire x1="57.38" y1="34.37" x2="54.38" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="53.68" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="34.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.08" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="44.37" x2="53.08" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.08" y1="42.57" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="42.57" x2="53.68" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.08" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.08" y1="36.17" x2="53.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="34.37" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="46.736" y1="39.37" x2="47.244" y2="39.37" width="0.0254" layer="49"/>
<wire x1="46.99" y1="39.624" x2="46.99" y2="39.116" width="0.0254" layer="49"/>
<wire x1="55.626" y1="39.37" x2="56.134" y2="39.37" width="0.0254" layer="49"/>
<wire x1="55.88" y1="39.624" x2="55.88" y2="39.116" width="0.0254" layer="49"/>
<wire x1="-1.778" y1="3.302" x2="-1.778" y2="12.192" width="0.127" layer="49"/>
<wire x1="-1.778" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="1.651" y1="12.192" x2="1.651" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="-1.778" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="11.684" y2="3.302" width="0.127" layer="49"/>
<wire x1="11.684" y1="3.302" x2="11.684" y2="12.192" width="0.127" layer="49"/>
<wire x1="11.684" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="-6.35" y1="43.942" x2="9.525" y2="43.942" width="0.127" layer="49"/>
<wire x1="9.525" y1="43.942" x2="9.525" y2="32.258" width="0.127" layer="49"/>
<wire x1="9.525" y1="32.258" x2="-6.35" y2="32.258" width="0.127" layer="49"/>
<wire x1="-6.35" y1="32.258" x2="-6.35" y2="43.942" width="0.127" layer="49"/>
<text x="0.635" y="12.7" size="1.27" layer="49" ratio="12">DC JACK</text>
<text x="0.635" y="30.48" size="1.27" layer="49" ratio="12">USB CONNECTOR</text>
<text x="12.065" y="6.985" size="1.27" layer="49" ratio="12">10.9mmH</text>
<text x="10.16" y="37.465" size="1.27" layer="49" ratio="12">10.75mmH</text>
<wire x1="3.81" y1="43.815" x2="4.445" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="5.08" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="44.45" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="43.18" width="0.2032" layer="49"/>
<wire x1="46.355" y1="39.37" x2="46.99" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="47.625" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="40.005" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="38.735" width="0.2032" layer="49"/>
<wire x1="55.245" y1="39.37" x2="56.515" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.88" y1="40.005" x2="55.88" y2="38.735" width="0.2032" layer="49"/>
<wire x1="60.325" y1="40.005" x2="60.96" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="61.595" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="40.64" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.245" y1="15.875" x2="54.61" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="53.975" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="16.51" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="15.24" width="0.2032" layer="49"/>
<wire x1="59.055" y1="15.875" x2="59.69" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="60.325" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="16.51" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="15.24" width="0.2032" layer="49"/>
<wire x1="63.5" y1="15.875" x2="64.135" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.77" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="16.51" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="15.24" width="0.2032" layer="49"/>
<wire x1="19.05" y1="2.032" x2="20.32" y2="2.032" width="0.2032" layer="49"/>
<wire x1="19.685" y1="2.667" x2="19.685" y2="1.397" width="0.2032" layer="49"/>
<wire x1="22.225" y1="4.445" x2="22.86" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="23.495" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="5.08" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="3.302" width="0.2032" layer="49"/>
<wire x1="22.225" y1="1.905" x2="23.495" y2="1.905" width="0.2032" layer="49"/>
<wire x1="22.86" y1="2.54" x2="22.86" y2="1.27" width="0.2032" layer="49"/>
<wire x1="22.225" y1="6.985" x2="23.495" y2="6.985" width="0.2032" layer="49"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="6.35" width="0.2032" layer="49"/>
<wire x1="12.7" y1="44.196" x2="13.462" y2="44.196" width="0.2032" layer="49"/>
<wire x1="13.081" y1="44.577" x2="13.081" y2="43.815" width="0.2032" layer="49"/>
<wire x1="13.081" y1="12.573" x2="13.081" y2="11.557" width="0.2032" layer="49"/>
<wire x1="12.446" y1="12.065" x2="13.716" y2="12.065" width="0.2032" layer="49"/>
<wire x1="1.905" y1="51.435" x2="6.985" y2="51.435" width="0.2032" layer="49"/>
<wire x1="6.985" y1="51.435" x2="6.985" y2="36.195" width="0.2032" layer="49"/>
<wire x1="6.985" y1="36.195" x2="1.905" y2="36.195" width="0.2032" layer="49"/>
<wire x1="1.905" y1="36.195" x2="1.905" y2="51.435" width="0.2032" layer="49"/>
<wire x1="0.381" y1="45.466" x2="25.781" y2="45.466" width="0.2032" layer="49"/>
<wire x1="25.781" y1="45.466" x2="25.781" y2="42.926" width="0.2032" layer="49"/>
<wire x1="25.781" y1="42.926" x2="0.381" y2="42.926" width="0.2032" layer="49"/>
<wire x1="0.381" y1="42.926" x2="0.381" y2="45.466" width="0.2032" layer="49"/>
<wire x1="0.381" y1="13.335" x2="25.781" y2="13.335" width="0.2032" layer="49"/>
<wire x1="25.781" y1="13.335" x2="25.781" y2="10.795" width="0.2032" layer="49"/>
<wire x1="25.781" y1="10.795" x2="0.381" y2="10.795" width="0.2032" layer="49"/>
<wire x1="0.381" y1="10.795" x2="0.381" y2="13.335" width="0.2032" layer="49"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="1.016" x2="16.1925" y2="2.286" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="2.286" x2="16.7005" y2="2.794" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="2.794" x2="16.7005" y2="3.556" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="3.556" x2="22.6695" y2="3.556" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="3.556" x2="22.6695" y2="2.794" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="2.794" x2="23.0505" y2="2.286" width="0.2032" layer="49"/>
<wire x1="23.0505" y1="2.286" x2="23.0505" y2="1.016" width="0.2032" layer="49"/>
<wire x1="23.0505" y1="1.016" x2="20.6375" y2="1.016" width="0.2032" layer="49"/>
<wire x1="20.6375" y1="1.016" x2="20.3835" y2="0.762" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.762" x2="20.3835" y2="0.254" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.254" x2="20.193" y2="0.0635" width="0.2032" layer="49"/>
<wire x1="20.193" y1="0.0635" x2="19.1135" y2="0.0635" width="0.2032" layer="49"/>
<wire x1="19.1135" y1="0.0635" x2="18.923" y2="0.254" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.254" x2="18.923" y2="0.762" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.762" x2="18.669" y2="1.016" width="0.2032" layer="49"/>
<wire x1="18.669" y1="1.016" x2="16.1925" y2="1.016" width="0.2032" layer="49"/>
<wire x1="57.15" y1="23.495" x2="62.23" y2="23.495" width="0.2032" layer="49"/>
<wire x1="62.23" y1="23.495" x2="62.23" y2="8.255" width="0.2032" layer="49"/>
<wire x1="62.23" y1="8.255" x2="57.15" y2="8.255" width="0.2032" layer="49"/>
<wire x1="57.15" y1="8.255" x2="57.15" y2="23.495" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.828" x2="56.769" y2="20.828" width="0.2032" layer="49"/>
<wire x1="56.769" y1="20.828" x2="56.769" y2="10.922" width="0.2032" layer="49"/>
<wire x1="56.769" y1="10.922" x2="51.816" y2="10.922" width="0.2032" layer="49"/>
<wire x1="51.816" y1="10.922" x2="51.816" y2="12.7" width="0.2032" layer="49"/>
<wire x1="51.816" y1="12.7" x2="52.3875" y2="12.7" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="12.7" x2="52.3875" y2="19.05" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="19.05" x2="51.816" y2="19.05" width="0.2032" layer="49"/>
<wire x1="51.816" y1="19.05" x2="51.816" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.8915" x2="56.8325" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="61.341" y1="20.8915" x2="66.3575" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="20.8915" x2="66.3575" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="10.8585" x2="61.341" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="61.341" y1="10.8585" x2="61.341" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.341" y1="12.7" x2="61.9125" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="12.7" x2="61.9125" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="19.1135" x2="61.341" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.341" y1="19.1135" x2="61.341" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="58.42" y1="47.625" x2="63.5" y2="47.625" width="0.2032" layer="49"/>
<wire x1="63.5" y1="47.625" x2="63.5" y2="32.385" width="0.2032" layer="49"/>
<wire x1="63.5" y1="32.385" x2="58.42" y2="32.385" width="0.2032" layer="49"/>
<wire x1="58.42" y1="32.385" x2="58.42" y2="47.625" width="0.2032" layer="49"/>
<text x="6.35" y="39.37" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="14.351" y="43.561" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="59.69" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="58.42" y="11.43" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="13.97" y="11.43" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="17.78" y="2.54" size="0.889" layer="49" ratio="11">button</text>
<text x="24.13" y="6.35" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="3.81" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="1.27" size="0.889" layer="49" ratio="11">TP</text>
<text x="62.865" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">first choice</text>
<wire x1="26.67" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="21"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="21"/>
<wire x1="46.99" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="21"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="21"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="21"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="21"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="21"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="21"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="21"/>
<wire x1="64.77" y1="49.53" x2="64.77" y2="52.07" width="0.127" layer="21"/>
<wire x1="64.77" y1="52.07" x2="44.45" y2="52.07" width="0.127" layer="21"/>
<wire x1="44.45" y1="52.07" x2="44.45" y2="49.53" width="0.127" layer="21"/>
<wire x1="17.526" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="21"/>
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="21"/>
<wire x1="42.926" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="21"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="21"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="21"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="21"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="21"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="21"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="21"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="21"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="21"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="21"/>
<text x="44.069" y="49.53" size="0.889" layer="21" ratio="11" rot="R90">JP1</text>
<text x="65.8495" y="49.53" size="0.889" layer="21" ratio="11" rot="R90">JP2</text>
<text x="63.5" y="32.385" size="0.889" layer="21" ratio="11">JP3</text>
<text x="65.8495" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP4</text>
<text x="48.133" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP5</text>
</package>
<package name="ARDUINO-38P-WITH/-SILK-500519002">
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="21"/>
<wire x1="42.926" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="21"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="21"/>
<wire x1="17.526" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="21"/>
<wire x1="44.45" y1="52.07" x2="64.77" y2="52.07" width="0.127" layer="21"/>
<wire x1="64.77" y1="52.07" x2="64.77" y2="49.53" width="0.127" layer="21"/>
<wire x1="44.45" y1="49.53" x2="44.45" y2="52.07" width="0.127" layer="21"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="21"/>
<wire x1="26.67" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="21"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="21"/>
<wire x1="46.99" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="21"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="21"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="21"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="21"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="21"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="21"/>
<wire x1="62.23" y1="31.75" x2="67.31" y2="31.75" width="0.127" layer="22"/>
<wire x1="67.31" y1="31.75" x2="67.31" y2="24.13" width="0.127" layer="22"/>
<wire x1="67.31" y1="24.13" x2="62.23" y2="24.13" width="0.127" layer="22"/>
<wire x1="62.23" y1="24.13" x2="62.23" y2="31.75" width="0.127" layer="22"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="21"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="21"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="21"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="21"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="21"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="21"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="21"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="21"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<pad name="1" x="63.5" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="60.96" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="3" x="58.42" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="4" x="55.88" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="5" x="53.34" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="6" x="50.8" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="7" x="48.26" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="8" x="45.72" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="9" x="41.656" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="10" x="39.116" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="11" x="36.576" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="12" x="34.036" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="13" x="31.496" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="14" x="28.956" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="15" x="26.416" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="16" x="23.876" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="27" x="50.8" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="28" x="53.34" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="29" x="55.88" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="30" x="58.42" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="31" x="60.96" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="32" x="63.5" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="26" x="45.72" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="25" x="43.18" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="24" x="40.64" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="23" x="38.1" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="22" x="35.56" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="21" x="33.02" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="33" x="63.5" y="30.48" drill="0.889" diameter="1.651" shape="square"/>
<pad name="37" x="66.04" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="36" x="66.04" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="35" x="63.5" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="34" x="63.5" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="38" x="66.04" y="30.48" drill="0.889" diameter="1.651"/>
<text x="63.119" y="47.879" size="1.27" layer="21" ratio="10">0</text>
<text x="60.579" y="47.879" size="1.27" layer="21" ratio="10">1</text>
<text x="52.959" y="4.445" size="1.27" layer="21" ratio="10">1</text>
<text x="58.039" y="47.879" size="1.27" layer="21" ratio="10">2</text>
<text x="55.499" y="47.879" size="1.27" layer="21" ratio="10">3</text>
<text x="52.959" y="47.879" size="1.27" layer="21" ratio="10">4</text>
<text x="50.419" y="47.879" size="1.27" layer="21" ratio="10">5</text>
<text x="47.879" y="47.879" size="1.27" layer="21" ratio="10">6</text>
<text x="45.339" y="47.879" size="1.27" layer="21" ratio="10">7</text>
<text x="41.148" y="47.879" size="1.27" layer="21" ratio="10">8</text>
<text x="38.608" y="47.879" size="1.27" layer="21" ratio="10">9</text>
<text x="35.687" y="47.879" size="1.27" layer="21" ratio="10">10</text>
<text x="33.147" y="47.879" size="1.27" layer="21" ratio="10">11</text>
<text x="30.48" y="47.879" size="1.27" layer="21" ratio="10">12</text>
<text x="27.94" y="47.879" size="1.27" layer="21" ratio="10">13</text>
<text x="26.924" y="45.974" size="1.27" layer="21" ratio="10" rot="R90">GND</text>
<text x="50.292" y="4.445" size="1.27" layer="21" ratio="10">0</text>
<text x="55.499" y="4.445" size="1.27" layer="21" ratio="10">2</text>
<text x="60.452" y="4.445" size="1.27" layer="21" ratio="10">4</text>
<text x="62.992" y="4.445" size="1.27" layer="21" ratio="10">5</text>
<text x="53.848" y="6.096" size="1.27" layer="21" ratio="10">ANALOG</text>
<text x="33.528" y="4.445" size="1.27" layer="21" ratio="10" rot="R90">RST</text>
<text x="36.195" y="4.445" size="1.27" layer="21" ratio="10" rot="R90">3V3</text>
<text x="37.211" y="4.445" size="1.27" layer="21" ratio="10">5V</text>
<text x="40.132" y="4.445" size="1.27" layer="21" ratio="10">GND</text>
<text x="44.196" y="4.445" size="1.27" layer="21" ratio="10">Vin</text>
<text x="57.912" y="4.445" size="1.27" layer="21" ratio="10">3</text>
<wire x1="18.288" y1="50.8" x2="19.304" y2="50.8" width="0.0254" layer="49"/>
<wire x1="18.796" y1="50.292" x2="18.796" y2="51.308" width="0.0254" layer="49"/>
<wire x1="20.828" y1="50.8" x2="21.844" y2="50.8" width="0.0254" layer="49"/>
<wire x1="21.336" y1="50.292" x2="21.336" y2="51.308" width="0.0254" layer="49"/>
<pad name="17" x="21.336" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="18" x="18.796" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="20" x="30.48" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="19" x="27.94" y="2.54" drill="0.889" diameter="1.651"/>
<text x="0.635" y="12.7" size="1.27" layer="49" ratio="12">DC JACK</text>
<text x="0.635" y="30.48" size="1.27" layer="49" ratio="12">USB CONNECTOR</text>
<text x="12.065" y="6.985" size="1.27" layer="49" ratio="12">10.9mmH</text>
<text x="10.16" y="37.465" size="1.27" layer="49" ratio="12">10.75mmH</text>
<text x="24.638" y="44.831" size="1.27" layer="21" ratio="10" rot="R90">AREF</text>
<text x="22.098" y="45.974" size="1.27" layer="21" ratio="10" rot="R90">SDA</text>
<text x="19.558" y="45.974" size="1.27" layer="21" ratio="10" rot="R90">SCL</text>
<text x="31.115" y="4.445" size="1.27" layer="21" ratio="10" rot="R90">IOREF</text>
<hole x="15.24" y="50.8" drill="3.2004"/>
<hole x="66.04" y="35.56" drill="3.2004"/>
<hole x="66.04" y="7.62" drill="3.2004"/>
<wire x1="22.606" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="49"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="49"/>
<wire x1="17.526" y1="52.07" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="26.67" y1="3.81" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="49"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="49"/>
<wire x1="27.432" y1="2.54" x2="28.448" y2="2.54" width="0.0254" layer="49"/>
<wire x1="27.94" y1="2.032" x2="27.94" y2="3.048" width="0.0254" layer="49"/>
<wire x1="29.972" y1="2.54" x2="30.988" y2="2.54" width="0.0254" layer="49"/>
<wire x1="30.48" y1="2.032" x2="30.48" y2="3.048" width="0.0254" layer="49"/>
<wire x1="63.5" y1="24.892" x2="63.5" y2="25.908" width="0.0254" layer="49"/>
<wire x1="64.008" y1="25.4" x2="62.992" y2="25.4" width="0.0254" layer="49"/>
<wire x1="63.5" y1="27.432" x2="63.5" y2="28.448" width="0.0254" layer="49"/>
<wire x1="64.008" y1="27.94" x2="62.992" y2="27.94" width="0.0254" layer="49"/>
<wire x1="63.5" y1="29.972" x2="63.5" y2="30.988" width="0.0254" layer="49"/>
<wire x1="64.008" y1="30.48" x2="62.992" y2="30.48" width="0.0254" layer="49"/>
<wire x1="66.04" y1="24.892" x2="66.04" y2="25.908" width="0.0254" layer="49"/>
<wire x1="66.548" y1="25.4" x2="65.532" y2="25.4" width="0.0254" layer="49"/>
<wire x1="66.04" y1="27.432" x2="66.04" y2="28.448" width="0.0254" layer="49"/>
<wire x1="66.548" y1="27.94" x2="65.532" y2="27.94" width="0.0254" layer="49"/>
<wire x1="66.04" y1="29.972" x2="66.04" y2="30.988" width="0.0254" layer="49"/>
<wire x1="66.548" y1="30.48" x2="65.532" y2="30.48" width="0.0254" layer="49"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="49"/>
<wire x1="42.926" y1="49.53" x2="22.606" y2="49.53" width="0.127" layer="49"/>
<wire x1="22.606" y1="49.53" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="22.606" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="52.07" x2="64.77" y2="52.07" width="0.127" layer="49"/>
<wire x1="64.77" y1="52.07" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="44.45" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="31.75" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="49"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="49"/>
<wire x1="46.99" y1="1.27" x2="31.75" y2="1.27" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="49"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="49"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="49"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="49"/>
<wire x1="14.732" y1="50.8" x2="15.748" y2="50.8" width="0.0254" layer="49"/>
<wire x1="15.24" y1="50.292" x2="15.24" y2="51.308" width="0.0254" layer="49"/>
<wire x1="65.532" y1="35.56" x2="66.548" y2="35.56" width="0.0254" layer="49"/>
<wire x1="66.04" y1="35.052" x2="66.04" y2="36.068" width="0.0254" layer="49"/>
<wire x1="65.532" y1="7.62" x2="66.548" y2="7.62" width="0.0254" layer="49"/>
<wire x1="66.04" y1="7.112" x2="66.04" y2="8.128" width="0.0254" layer="49"/>
<wire x1="41.148" y1="50.8" x2="42.164" y2="50.8" width="0.0254" layer="49"/>
<wire x1="41.656" y1="50.292" x2="41.656" y2="51.308" width="0.0254" layer="49"/>
<wire x1="23.368" y1="50.8" x2="24.384" y2="50.8" width="0.0254" layer="49"/>
<wire x1="23.876" y1="50.292" x2="23.876" y2="51.308" width="0.0254" layer="49"/>
<wire x1="25.908" y1="50.8" x2="26.924" y2="50.8" width="0.0254" layer="49"/>
<wire x1="26.416" y1="50.292" x2="26.416" y2="51.308" width="0.0254" layer="49"/>
<wire x1="28.448" y1="50.8" x2="29.464" y2="50.8" width="0.0254" layer="49"/>
<wire x1="28.956" y1="50.292" x2="28.956" y2="51.308" width="0.0254" layer="49"/>
<wire x1="30.988" y1="50.8" x2="32.004" y2="50.8" width="0.0254" layer="49"/>
<wire x1="31.496" y1="50.292" x2="31.496" y2="51.308" width="0.0254" layer="49"/>
<wire x1="33.528" y1="50.8" x2="34.544" y2="50.8" width="0.0254" layer="49"/>
<wire x1="34.036" y1="50.292" x2="34.036" y2="51.308" width="0.0254" layer="49"/>
<wire x1="36.068" y1="50.8" x2="37.084" y2="50.8" width="0.0254" layer="49"/>
<wire x1="36.576" y1="50.292" x2="36.576" y2="51.308" width="0.0254" layer="49"/>
<wire x1="38.608" y1="50.8" x2="39.624" y2="50.8" width="0.0254" layer="49"/>
<wire x1="39.116" y1="50.292" x2="39.116" y2="51.308" width="0.0254" layer="49"/>
<wire x1="62.992" y1="50.8" x2="64.008" y2="50.8" width="0.0254" layer="49"/>
<wire x1="63.5" y1="50.292" x2="63.5" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="50.8" x2="46.228" y2="50.8" width="0.0254" layer="49"/>
<wire x1="45.72" y1="50.292" x2="45.72" y2="51.308" width="0.0254" layer="49"/>
<wire x1="47.752" y1="50.8" x2="48.768" y2="50.8" width="0.0254" layer="49"/>
<wire x1="48.26" y1="50.292" x2="48.26" y2="51.308" width="0.0254" layer="49"/>
<wire x1="50.292" y1="50.8" x2="51.308" y2="50.8" width="0.0254" layer="49"/>
<wire x1="50.8" y1="50.292" x2="50.8" y2="51.308" width="0.0254" layer="49"/>
<wire x1="52.832" y1="50.8" x2="53.848" y2="50.8" width="0.0254" layer="49"/>
<wire x1="53.34" y1="50.292" x2="53.34" y2="51.308" width="0.0254" layer="49"/>
<wire x1="55.372" y1="50.8" x2="56.388" y2="50.8" width="0.0254" layer="49"/>
<wire x1="55.88" y1="50.292" x2="55.88" y2="51.308" width="0.0254" layer="49"/>
<wire x1="57.912" y1="50.8" x2="58.928" y2="50.8" width="0.0254" layer="49"/>
<wire x1="58.42" y1="50.292" x2="58.42" y2="51.308" width="0.0254" layer="49"/>
<wire x1="60.452" y1="50.8" x2="61.468" y2="50.8" width="0.0254" layer="49"/>
<wire x1="60.96" y1="50.292" x2="60.96" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="2.54" x2="46.228" y2="2.54" width="0.0254" layer="49"/>
<wire x1="45.72" y1="2.032" x2="45.72" y2="3.048" width="0.0254" layer="49"/>
<wire x1="32.512" y1="2.54" x2="33.528" y2="2.54" width="0.0254" layer="49"/>
<wire x1="33.02" y1="2.032" x2="33.02" y2="3.048" width="0.0254" layer="49"/>
<wire x1="35.052" y1="2.54" x2="36.068" y2="2.54" width="0.0254" layer="49"/>
<wire x1="35.56" y1="2.032" x2="35.56" y2="3.048" width="0.0254" layer="49"/>
<wire x1="37.592" y1="2.54" x2="38.608" y2="2.54" width="0.0254" layer="49"/>
<wire x1="38.1" y1="2.032" x2="38.1" y2="3.048" width="0.0254" layer="49"/>
<wire x1="40.132" y1="2.54" x2="41.148" y2="2.54" width="0.0254" layer="49"/>
<wire x1="40.64" y1="2.032" x2="40.64" y2="3.048" width="0.0254" layer="49"/>
<wire x1="42.672" y1="2.54" x2="43.688" y2="2.54" width="0.0254" layer="49"/>
<wire x1="43.18" y1="2.032" x2="43.18" y2="3.048" width="0.0254" layer="49"/>
<wire x1="50.292" y1="2.54" x2="51.308" y2="2.54" width="0.0254" layer="49"/>
<wire x1="50.8" y1="2.032" x2="50.8" y2="3.048" width="0.0254" layer="49"/>
<wire x1="52.832" y1="2.54" x2="53.848" y2="2.54" width="0.0254" layer="49"/>
<wire x1="53.34" y1="2.032" x2="53.34" y2="3.048" width="0.0254" layer="49"/>
<wire x1="55.372" y1="2.54" x2="56.388" y2="2.54" width="0.0254" layer="49"/>
<wire x1="55.88" y1="2.032" x2="55.88" y2="3.048" width="0.0254" layer="49"/>
<wire x1="57.912" y1="2.54" x2="58.928" y2="2.54" width="0.0254" layer="49"/>
<wire x1="58.42" y1="2.032" x2="58.42" y2="3.048" width="0.0254" layer="49"/>
<wire x1="60.452" y1="2.54" x2="61.468" y2="2.54" width="0.0254" layer="49"/>
<wire x1="60.96" y1="2.032" x2="60.96" y2="3.048" width="0.0254" layer="49"/>
<wire x1="62.992" y1="2.54" x2="64.008" y2="2.54" width="0.0254" layer="49"/>
<wire x1="63.5" y1="2.032" x2="63.5" y2="3.048" width="0.0254" layer="49"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="49"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="49"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="49"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="49"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="49"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="49"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="49"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="49"/>
<wire x1="66.04" y1="0" x2="20.2565" y2="0" width="0.127" layer="49"/>
<wire x1="19.05" y1="0" x2="0" y2="0" width="0.127" layer="49"/>
<wire x1="47.99" y1="44.37" x2="45.99" y2="44.37" width="0.127" layer="49"/>
<wire x1="48.49" y1="34.37" x2="45.49" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="44.79" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="34.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.19" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="44.37" x2="44.19" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.19" y1="42.57" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="42.57" x2="44.79" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.19" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.19" y1="36.17" x2="44.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="34.37" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="56.88" y1="44.37" x2="54.88" y2="44.37" width="0.127" layer="49"/>
<wire x1="57.38" y1="34.37" x2="54.38" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="53.68" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="34.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.08" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="44.37" x2="53.08" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.08" y1="42.57" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="42.57" x2="53.68" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.08" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.08" y1="36.17" x2="53.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="34.37" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="46.736" y1="39.37" x2="47.244" y2="39.37" width="0.0254" layer="49"/>
<wire x1="46.99" y1="39.624" x2="46.99" y2="39.116" width="0.0254" layer="49"/>
<wire x1="55.626" y1="39.37" x2="56.134" y2="39.37" width="0.0254" layer="49"/>
<wire x1="55.88" y1="39.624" x2="55.88" y2="39.116" width="0.0254" layer="49"/>
<wire x1="-1.778" y1="3.302" x2="-1.778" y2="12.192" width="0.127" layer="49"/>
<wire x1="-1.778" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="1.651" y1="12.192" x2="1.651" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="-1.778" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="11.684" y2="3.302" width="0.127" layer="49"/>
<wire x1="11.684" y1="3.302" x2="11.684" y2="12.192" width="0.127" layer="49"/>
<wire x1="11.684" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="-6.35" y1="43.942" x2="9.525" y2="43.942" width="0.127" layer="49"/>
<wire x1="9.525" y1="43.942" x2="9.525" y2="32.258" width="0.127" layer="49"/>
<wire x1="9.525" y1="32.258" x2="-6.35" y2="32.258" width="0.127" layer="49"/>
<wire x1="-6.35" y1="32.258" x2="-6.35" y2="43.942" width="0.127" layer="49"/>
<text x="0.635" y="12.7" size="1.27" layer="49" ratio="12">DC JACK</text>
<text x="0.635" y="30.48" size="1.27" layer="49" ratio="12">USB CONNECTOR</text>
<text x="12.065" y="6.985" size="1.27" layer="49" ratio="12">10.9mmH</text>
<text x="10.16" y="37.465" size="1.27" layer="49" ratio="12">10.75mmH</text>
<wire x1="3.81" y1="43.815" x2="4.445" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="5.08" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="44.45" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="43.18" width="0.2032" layer="49"/>
<wire x1="46.355" y1="39.37" x2="46.99" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="47.625" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="40.005" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="38.735" width="0.2032" layer="49"/>
<wire x1="55.245" y1="39.37" x2="56.515" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.88" y1="40.005" x2="55.88" y2="38.735" width="0.2032" layer="49"/>
<wire x1="60.325" y1="40.005" x2="60.96" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="61.595" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="40.64" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.245" y1="15.875" x2="54.61" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="53.975" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="16.51" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="15.24" width="0.2032" layer="49"/>
<wire x1="59.055" y1="15.875" x2="59.69" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="60.325" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="16.51" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="15.24" width="0.2032" layer="49"/>
<wire x1="63.5" y1="15.875" x2="64.135" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.77" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="16.51" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="15.24" width="0.2032" layer="49"/>
<wire x1="19.05" y1="2.032" x2="20.32" y2="2.032" width="0.2032" layer="49"/>
<wire x1="19.685" y1="2.667" x2="19.685" y2="1.397" width="0.2032" layer="49"/>
<wire x1="22.225" y1="4.445" x2="22.86" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="23.495" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="5.08" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="3.81" width="0.2032" layer="49"/>
<wire x1="22.225" y1="1.905" x2="23.495" y2="1.905" width="0.2032" layer="49"/>
<wire x1="22.86" y1="2.54" x2="22.86" y2="1.27" width="0.2032" layer="49"/>
<wire x1="22.225" y1="6.985" x2="23.495" y2="6.985" width="0.2032" layer="49"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="6.35" width="0.2032" layer="49"/>
<wire x1="12.7" y1="44.196" x2="13.462" y2="44.196" width="0.2032" layer="49"/>
<wire x1="13.081" y1="44.577" x2="13.081" y2="43.815" width="0.2032" layer="49"/>
<wire x1="13.081" y1="12.573" x2="13.081" y2="11.557" width="0.2032" layer="49"/>
<wire x1="12.446" y1="12.065" x2="13.716" y2="12.065" width="0.2032" layer="49"/>
<wire x1="1.905" y1="51.435" x2="6.985" y2="51.435" width="0.2032" layer="49"/>
<wire x1="6.985" y1="51.435" x2="6.985" y2="36.195" width="0.2032" layer="49"/>
<wire x1="6.985" y1="36.195" x2="1.905" y2="36.195" width="0.2032" layer="49"/>
<wire x1="1.905" y1="36.195" x2="1.905" y2="51.435" width="0.2032" layer="49"/>
<wire x1="0.381" y1="45.466" x2="25.781" y2="45.466" width="0.2032" layer="49"/>
<wire x1="25.781" y1="45.466" x2="25.781" y2="42.926" width="0.2032" layer="49"/>
<wire x1="25.781" y1="42.926" x2="0.381" y2="42.926" width="0.2032" layer="49"/>
<wire x1="0.381" y1="42.926" x2="0.381" y2="45.466" width="0.2032" layer="49"/>
<wire x1="0.381" y1="13.335" x2="25.781" y2="13.335" width="0.2032" layer="49"/>
<wire x1="25.781" y1="13.335" x2="25.781" y2="10.795" width="0.2032" layer="49"/>
<wire x1="25.781" y1="10.795" x2="0.381" y2="10.795" width="0.2032" layer="49"/>
<wire x1="0.381" y1="10.795" x2="0.381" y2="13.335" width="0.2032" layer="49"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="1.0414" x2="16.1925" y2="2.2352" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="2.2352" x2="16.7005" y2="2.7432" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="2.7432" x2="16.7005" y2="3.5433" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="3.5433" x2="22.6695" y2="3.5433" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="3.5433" x2="22.6695" y2="2.7432" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="2.7432" x2="23.1775" y2="2.2352" width="0.2032" layer="49"/>
<wire x1="23.1775" y1="2.2352" x2="23.1775" y2="1.0414" width="0.2032" layer="49"/>
<wire x1="23.1775" y1="1.0414" x2="20.7264" y2="1.0414" width="0.2032" layer="49"/>
<wire x1="20.7264" y1="1.0414" x2="20.3835" y2="0.6985" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.6985" x2="20.3835" y2="0.127" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.127" x2="20.2565" y2="0" width="0.2032" layer="49"/>
<wire x1="20.2565" y1="0" x2="19.05" y2="0" width="0.2032" layer="49"/>
<wire x1="19.05" y1="0" x2="18.923" y2="0.127" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.127" x2="18.923" y2="0.6985" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.6985" x2="18.5801" y2="1.0414" width="0.2032" layer="49"/>
<wire x1="18.5801" y1="1.0414" x2="16.1925" y2="1.0414" width="0.2032" layer="49"/>
<wire x1="57.15" y1="23.495" x2="62.23" y2="23.495" width="0.2032" layer="49"/>
<wire x1="62.23" y1="23.495" x2="62.23" y2="8.255" width="0.2032" layer="49"/>
<wire x1="62.23" y1="8.255" x2="57.15" y2="8.255" width="0.2032" layer="49"/>
<wire x1="57.15" y1="8.255" x2="57.15" y2="23.495" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.828" x2="56.769" y2="20.828" width="0.2032" layer="49"/>
<wire x1="56.769" y1="20.828" x2="56.769" y2="10.922" width="0.2032" layer="49"/>
<wire x1="56.769" y1="10.922" x2="51.816" y2="10.922" width="0.2032" layer="49"/>
<wire x1="51.816" y1="10.922" x2="51.816" y2="12.7" width="0.2032" layer="49"/>
<wire x1="51.816" y1="12.7" x2="52.3875" y2="12.7" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="12.7" x2="52.3875" y2="19.05" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="19.05" x2="51.816" y2="19.05" width="0.2032" layer="49"/>
<wire x1="51.816" y1="19.05" x2="51.816" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.8915" x2="56.8325" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="61.341" y1="20.8915" x2="66.3575" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="20.8915" x2="66.3575" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="10.8585" x2="61.341" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="61.341" y1="10.8585" x2="61.341" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.341" y1="12.7" x2="61.9125" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="12.7" x2="61.9125" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="19.1135" x2="61.341" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.341" y1="19.1135" x2="61.341" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="58.42" y1="47.625" x2="63.5" y2="47.625" width="0.2032" layer="49"/>
<wire x1="63.5" y1="47.625" x2="63.5" y2="32.385" width="0.2032" layer="49"/>
<wire x1="63.5" y1="32.385" x2="58.42" y2="32.385" width="0.2032" layer="49"/>
<wire x1="58.42" y1="32.385" x2="58.42" y2="47.625" width="0.2032" layer="49"/>
<text x="6.35" y="39.37" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="14.351" y="43.561" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="59.69" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="58.42" y="11.43" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="13.97" y="11.43" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="17.78" y="2.413" size="0.889" layer="49" ratio="11">button</text>
<text x="24.13" y="6.35" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="3.81" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="1.27" size="0.889" layer="49" ratio="11">TP</text>
<text x="62.865" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">first choice</text>
<text x="28.575" y="4.445" size="1.27" layer="21" ratio="10" rot="R90">NC</text>
<text x="44.069" y="49.53" size="0.889" layer="21" ratio="11" rot="R90">JP1</text>
<text x="65.8495" y="49.403" size="0.889" layer="21" ratio="11" rot="R90">JP2</text>
<text x="66.04" y="32.385" size="0.889" layer="22" ratio="11" rot="MR0">JP3</text>
<text x="65.8495" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP4</text>
<text x="48.26" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP5</text>
</package>
<package name="ARDUINO-38P-500519002">
<wire x1="22.606" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="49"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="49"/>
<wire x1="17.526" y1="52.07" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="26.67" y1="3.81" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="49"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="49"/>
<wire x1="27.432" y1="2.54" x2="28.448" y2="2.54" width="0.0254" layer="49"/>
<wire x1="27.94" y1="2.032" x2="27.94" y2="3.048" width="0.0254" layer="49"/>
<wire x1="29.972" y1="2.54" x2="30.988" y2="2.54" width="0.0254" layer="49"/>
<wire x1="30.48" y1="2.032" x2="30.48" y2="3.048" width="0.0254" layer="49"/>
<wire x1="63.5" y1="24.892" x2="63.5" y2="25.908" width="0.0254" layer="49"/>
<wire x1="64.008" y1="25.4" x2="62.992" y2="25.4" width="0.0254" layer="49"/>
<wire x1="63.5" y1="27.432" x2="63.5" y2="28.448" width="0.0254" layer="49"/>
<wire x1="64.008" y1="27.94" x2="62.992" y2="27.94" width="0.0254" layer="49"/>
<wire x1="63.5" y1="29.972" x2="63.5" y2="30.988" width="0.0254" layer="49"/>
<wire x1="64.008" y1="30.48" x2="62.992" y2="30.48" width="0.0254" layer="49"/>
<wire x1="66.04" y1="24.892" x2="66.04" y2="25.908" width="0.0254" layer="49"/>
<wire x1="66.548" y1="25.4" x2="65.532" y2="25.4" width="0.0254" layer="49"/>
<wire x1="66.04" y1="27.432" x2="66.04" y2="28.448" width="0.0254" layer="49"/>
<wire x1="66.548" y1="27.94" x2="65.532" y2="27.94" width="0.0254" layer="49"/>
<wire x1="66.04" y1="29.972" x2="66.04" y2="30.988" width="0.0254" layer="49"/>
<wire x1="66.548" y1="30.48" x2="65.532" y2="30.48" width="0.0254" layer="49"/>
<wire x1="62.23" y1="31.75" x2="67.31" y2="31.75" width="0.127" layer="22"/>
<wire x1="67.31" y1="31.75" x2="67.31" y2="24.13" width="0.127" layer="22"/>
<wire x1="67.31" y1="24.13" x2="62.23" y2="24.13" width="0.127" layer="22"/>
<wire x1="62.23" y1="24.13" x2="62.23" y2="31.75" width="0.127" layer="22"/>
<pad name="1" x="63.5" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="60.96" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="3" x="58.42" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="4" x="55.88" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="5" x="53.34" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="6" x="50.8" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="7" x="48.26" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="8" x="45.72" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="9" x="41.656" y="50.8" drill="0.889" diameter="1.651" shape="square"/>
<pad name="10" x="39.116" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="11" x="36.576" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="12" x="34.036" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="13" x="31.496" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="14" x="28.956" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="15" x="26.416" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="16" x="23.876" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="19" x="50.8" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="20" x="53.34" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="21" x="55.88" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="22" x="58.42" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="23" x="60.96" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="24" x="63.5" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="25" x="45.72" y="2.54" drill="0.889" diameter="1.651" shape="square"/>
<pad name="26" x="43.18" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="27" x="40.64" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="28" x="38.1" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="29" x="35.56" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="30" x="33.02" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="31" x="30.48" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="32" x="27.94" y="2.54" drill="0.889" diameter="1.651"/>
<pad name="38" x="63.5" y="30.48" drill="0.889" diameter="1.651" shape="square"/>
<pad name="34" x="66.04" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="35" x="66.04" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="36" x="63.5" y="25.4" drill="0.889" diameter="1.651"/>
<pad name="37" x="63.5" y="27.94" drill="0.889" diameter="1.651"/>
<pad name="33" x="66.04" y="30.48" drill="0.889" diameter="1.651"/>
<hole x="15.24" y="50.8" drill="3.2004"/>
<hole x="66.04" y="35.56" drill="3.2004"/>
<hole x="66.04" y="7.62" drill="3.2004"/>
<pad name="17" x="21.336" y="50.8" drill="0.889" diameter="1.651"/>
<pad name="18" x="18.796" y="50.8" drill="0.889" diameter="1.651"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="49"/>
<wire x1="42.926" y1="49.53" x2="22.606" y2="49.53" width="0.127" layer="49"/>
<wire x1="22.606" y1="49.53" x2="22.606" y2="52.07" width="0.127" layer="49"/>
<wire x1="22.606" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="52.07" x2="64.77" y2="52.07" width="0.127" layer="49"/>
<wire x1="64.77" y1="52.07" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="44.45" y2="52.07" width="0.127" layer="49"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="49"/>
<wire x1="31.75" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="49"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="49"/>
<wire x1="46.99" y1="1.27" x2="31.75" y2="1.27" width="0.127" layer="49"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="3.81" width="0.127" layer="49"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="49"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="49"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="49"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="49"/>
<wire x1="14.732" y1="50.8" x2="15.748" y2="50.8" width="0.0254" layer="49"/>
<wire x1="15.24" y1="50.292" x2="15.24" y2="51.308" width="0.0254" layer="49"/>
<wire x1="65.532" y1="35.56" x2="66.548" y2="35.56" width="0.0254" layer="49"/>
<wire x1="66.04" y1="35.052" x2="66.04" y2="36.068" width="0.0254" layer="49"/>
<wire x1="65.532" y1="7.62" x2="66.548" y2="7.62" width="0.0254" layer="49"/>
<wire x1="66.04" y1="7.112" x2="66.04" y2="8.128" width="0.0254" layer="49"/>
<wire x1="41.148" y1="50.8" x2="42.164" y2="50.8" width="0.0254" layer="49"/>
<wire x1="41.656" y1="50.292" x2="41.656" y2="51.308" width="0.0254" layer="49"/>
<wire x1="23.368" y1="50.8" x2="24.384" y2="50.8" width="0.0254" layer="49"/>
<wire x1="23.876" y1="50.292" x2="23.876" y2="51.308" width="0.0254" layer="49"/>
<wire x1="25.908" y1="50.8" x2="26.924" y2="50.8" width="0.0254" layer="49"/>
<wire x1="26.416" y1="50.292" x2="26.416" y2="51.308" width="0.0254" layer="49"/>
<wire x1="28.448" y1="50.8" x2="29.464" y2="50.8" width="0.0254" layer="49"/>
<wire x1="28.956" y1="50.292" x2="28.956" y2="51.308" width="0.0254" layer="49"/>
<wire x1="30.988" y1="50.8" x2="32.004" y2="50.8" width="0.0254" layer="49"/>
<wire x1="31.496" y1="50.292" x2="31.496" y2="51.308" width="0.0254" layer="49"/>
<wire x1="33.528" y1="50.8" x2="34.544" y2="50.8" width="0.0254" layer="49"/>
<wire x1="34.036" y1="50.292" x2="34.036" y2="51.308" width="0.0254" layer="49"/>
<wire x1="36.068" y1="50.8" x2="37.084" y2="50.8" width="0.0254" layer="49"/>
<wire x1="36.576" y1="50.292" x2="36.576" y2="51.308" width="0.0254" layer="49"/>
<wire x1="38.608" y1="50.8" x2="39.624" y2="50.8" width="0.0254" layer="49"/>
<wire x1="39.116" y1="50.292" x2="39.116" y2="51.308" width="0.0254" layer="49"/>
<wire x1="62.992" y1="50.8" x2="64.008" y2="50.8" width="0.0254" layer="49"/>
<wire x1="63.5" y1="50.292" x2="63.5" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="50.8" x2="46.228" y2="50.8" width="0.0254" layer="49"/>
<wire x1="45.72" y1="50.292" x2="45.72" y2="51.308" width="0.0254" layer="49"/>
<wire x1="47.752" y1="50.8" x2="48.768" y2="50.8" width="0.0254" layer="49"/>
<wire x1="48.26" y1="50.292" x2="48.26" y2="51.308" width="0.0254" layer="49"/>
<wire x1="50.292" y1="50.8" x2="51.308" y2="50.8" width="0.0254" layer="49"/>
<wire x1="50.8" y1="50.292" x2="50.8" y2="51.308" width="0.0254" layer="49"/>
<wire x1="52.832" y1="50.8" x2="53.848" y2="50.8" width="0.0254" layer="49"/>
<wire x1="53.34" y1="50.292" x2="53.34" y2="51.308" width="0.0254" layer="49"/>
<wire x1="55.372" y1="50.8" x2="56.388" y2="50.8" width="0.0254" layer="49"/>
<wire x1="55.88" y1="50.292" x2="55.88" y2="51.308" width="0.0254" layer="49"/>
<wire x1="57.912" y1="50.8" x2="58.928" y2="50.8" width="0.0254" layer="49"/>
<wire x1="58.42" y1="50.292" x2="58.42" y2="51.308" width="0.0254" layer="49"/>
<wire x1="60.452" y1="50.8" x2="61.468" y2="50.8" width="0.0254" layer="49"/>
<wire x1="60.96" y1="50.292" x2="60.96" y2="51.308" width="0.0254" layer="49"/>
<wire x1="45.212" y1="2.54" x2="46.228" y2="2.54" width="0.0254" layer="49"/>
<wire x1="45.72" y1="2.032" x2="45.72" y2="3.048" width="0.0254" layer="49"/>
<wire x1="32.512" y1="2.54" x2="33.528" y2="2.54" width="0.0254" layer="49"/>
<wire x1="33.02" y1="2.032" x2="33.02" y2="3.048" width="0.0254" layer="49"/>
<wire x1="35.052" y1="2.54" x2="36.068" y2="2.54" width="0.0254" layer="49"/>
<wire x1="35.56" y1="2.032" x2="35.56" y2="3.048" width="0.0254" layer="49"/>
<wire x1="37.592" y1="2.54" x2="38.608" y2="2.54" width="0.0254" layer="49"/>
<wire x1="38.1" y1="2.032" x2="38.1" y2="3.048" width="0.0254" layer="49"/>
<wire x1="40.132" y1="2.54" x2="41.148" y2="2.54" width="0.0254" layer="49"/>
<wire x1="40.64" y1="2.032" x2="40.64" y2="3.048" width="0.0254" layer="49"/>
<wire x1="42.672" y1="2.54" x2="43.688" y2="2.54" width="0.0254" layer="49"/>
<wire x1="43.18" y1="2.032" x2="43.18" y2="3.048" width="0.0254" layer="49"/>
<wire x1="50.292" y1="2.54" x2="51.308" y2="2.54" width="0.0254" layer="49"/>
<wire x1="50.8" y1="2.032" x2="50.8" y2="3.048" width="0.0254" layer="49"/>
<wire x1="52.832" y1="2.54" x2="53.848" y2="2.54" width="0.0254" layer="49"/>
<wire x1="53.34" y1="2.032" x2="53.34" y2="3.048" width="0.0254" layer="49"/>
<wire x1="55.372" y1="2.54" x2="56.388" y2="2.54" width="0.0254" layer="49"/>
<wire x1="55.88" y1="2.032" x2="55.88" y2="3.048" width="0.0254" layer="49"/>
<wire x1="57.912" y1="2.54" x2="58.928" y2="2.54" width="0.0254" layer="49"/>
<wire x1="58.42" y1="2.032" x2="58.42" y2="3.048" width="0.0254" layer="49"/>
<wire x1="60.452" y1="2.54" x2="61.468" y2="2.54" width="0.0254" layer="49"/>
<wire x1="60.96" y1="2.032" x2="60.96" y2="3.048" width="0.0254" layer="49"/>
<wire x1="62.992" y1="2.54" x2="64.008" y2="2.54" width="0.0254" layer="49"/>
<wire x1="63.5" y1="2.032" x2="63.5" y2="3.048" width="0.0254" layer="49"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="49"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="49"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="49"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="49"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="49"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="49"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="49"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="49"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="49"/>
<wire x1="47.99" y1="44.37" x2="45.99" y2="44.37" width="0.127" layer="49"/>
<wire x1="48.49" y1="34.37" x2="45.49" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="44.79" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="34.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="49.19" y1="44.37" x2="49.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.79" y1="44.37" x2="44.19" y2="44.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="44.37" x2="44.19" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.19" y1="42.57" x2="44.79" y2="42.57" width="0.254" layer="49"/>
<wire x1="44.79" y1="42.57" x2="44.79" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.79" y1="36.17" x2="44.19" y2="36.17" width="0.254" layer="49"/>
<wire x1="44.19" y1="36.17" x2="44.19" y2="34.37" width="0.254" layer="49"/>
<wire x1="44.19" y1="34.37" x2="44.79" y2="34.37" width="0.254" layer="49"/>
<wire x1="56.88" y1="44.37" x2="54.88" y2="44.37" width="0.127" layer="49"/>
<wire x1="57.38" y1="34.37" x2="54.38" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="53.68" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="34.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="58.08" y1="44.37" x2="58.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.68" y1="44.37" x2="53.08" y2="44.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="44.37" x2="53.08" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.08" y1="42.57" x2="53.68" y2="42.57" width="0.254" layer="49"/>
<wire x1="53.68" y1="42.57" x2="53.68" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.68" y1="36.17" x2="53.08" y2="36.17" width="0.254" layer="49"/>
<wire x1="53.08" y1="36.17" x2="53.08" y2="34.37" width="0.254" layer="49"/>
<wire x1="53.08" y1="34.37" x2="53.68" y2="34.37" width="0.254" layer="49"/>
<wire x1="46.736" y1="39.37" x2="47.244" y2="39.37" width="0.0254" layer="49"/>
<wire x1="46.99" y1="39.624" x2="46.99" y2="39.116" width="0.0254" layer="49"/>
<wire x1="55.626" y1="39.37" x2="56.134" y2="39.37" width="0.0254" layer="49"/>
<wire x1="55.88" y1="39.624" x2="55.88" y2="39.116" width="0.0254" layer="49"/>
<wire x1="-1.778" y1="3.302" x2="-1.778" y2="12.192" width="0.127" layer="49"/>
<wire x1="-1.778" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="1.651" y1="12.192" x2="1.651" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="-1.778" y2="3.302" width="0.127" layer="49"/>
<wire x1="1.651" y1="3.302" x2="11.684" y2="3.302" width="0.127" layer="49"/>
<wire x1="11.684" y1="3.302" x2="11.684" y2="12.192" width="0.127" layer="49"/>
<wire x1="11.684" y1="12.192" x2="1.651" y2="12.192" width="0.127" layer="49"/>
<wire x1="-6.35" y1="43.942" x2="9.525" y2="43.942" width="0.127" layer="49"/>
<wire x1="9.525" y1="43.942" x2="9.525" y2="32.258" width="0.127" layer="49"/>
<wire x1="9.525" y1="32.258" x2="-6.35" y2="32.258" width="0.127" layer="49"/>
<wire x1="-6.35" y1="32.258" x2="-6.35" y2="43.942" width="0.127" layer="49"/>
<text x="0.635" y="12.7" size="1.27" layer="49" ratio="12">DC JACK</text>
<text x="0.635" y="30.48" size="1.27" layer="49" ratio="12">USB CONNECTOR</text>
<text x="12.065" y="6.985" size="1.27" layer="49" ratio="12">10.9mmH</text>
<text x="10.16" y="37.465" size="1.27" layer="49" ratio="12">10.75mmH</text>
<wire x1="3.81" y1="43.815" x2="4.445" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="5.08" y2="43.815" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="44.45" width="0.2032" layer="49"/>
<wire x1="4.445" y1="43.815" x2="4.445" y2="43.18" width="0.2032" layer="49"/>
<wire x1="46.355" y1="39.37" x2="46.99" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="47.625" y2="39.37" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="40.005" width="0.2032" layer="49"/>
<wire x1="46.99" y1="39.37" x2="46.99" y2="38.735" width="0.2032" layer="49"/>
<wire x1="55.245" y1="39.37" x2="56.515" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.88" y1="40.005" x2="55.88" y2="38.735" width="0.2032" layer="49"/>
<wire x1="60.325" y1="40.005" x2="60.96" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="61.595" y2="40.005" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="40.64" width="0.2032" layer="49"/>
<wire x1="60.96" y1="40.005" x2="60.96" y2="39.37" width="0.2032" layer="49"/>
<wire x1="55.245" y1="15.875" x2="54.61" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="53.975" y2="15.875" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="16.51" width="0.2032" layer="49"/>
<wire x1="54.61" y1="15.875" x2="54.61" y2="15.24" width="0.2032" layer="49"/>
<wire x1="59.055" y1="15.875" x2="59.69" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="60.325" y2="15.875" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="16.51" width="0.2032" layer="49"/>
<wire x1="59.69" y1="15.875" x2="59.69" y2="15.24" width="0.2032" layer="49"/>
<wire x1="63.5" y1="15.875" x2="64.135" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.77" y2="15.875" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="16.51" width="0.2032" layer="49"/>
<wire x1="64.135" y1="15.875" x2="64.135" y2="15.24" width="0.2032" layer="49"/>
<wire x1="19.05" y1="2.032" x2="20.32" y2="2.032" width="0.2032" layer="49"/>
<wire x1="19.685" y1="2.667" x2="19.685" y2="1.397" width="0.2032" layer="49"/>
<wire x1="22.225" y1="4.445" x2="22.86" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="23.495" y2="4.445" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="5.08" width="0.2032" layer="49"/>
<wire x1="22.86" y1="4.445" x2="22.86" y2="3.302" width="0.2032" layer="49"/>
<wire x1="22.225" y1="1.905" x2="23.495" y2="1.905" width="0.2032" layer="49"/>
<wire x1="22.86" y1="2.54" x2="22.86" y2="1.27" width="0.2032" layer="49"/>
<wire x1="22.225" y1="6.985" x2="23.495" y2="6.985" width="0.2032" layer="49"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="6.35" width="0.2032" layer="49"/>
<wire x1="12.7" y1="44.196" x2="13.462" y2="44.196" width="0.2032" layer="49"/>
<wire x1="13.081" y1="44.577" x2="13.081" y2="43.815" width="0.2032" layer="49"/>
<wire x1="13.081" y1="12.573" x2="13.081" y2="11.557" width="0.2032" layer="49"/>
<wire x1="12.446" y1="12.065" x2="13.716" y2="12.065" width="0.2032" layer="49"/>
<wire x1="1.905" y1="51.435" x2="6.985" y2="51.435" width="0.2032" layer="49"/>
<wire x1="6.985" y1="51.435" x2="6.985" y2="36.195" width="0.2032" layer="49"/>
<wire x1="6.985" y1="36.195" x2="1.905" y2="36.195" width="0.2032" layer="49"/>
<wire x1="1.905" y1="36.195" x2="1.905" y2="51.435" width="0.2032" layer="49"/>
<wire x1="0.381" y1="45.466" x2="25.781" y2="45.466" width="0.2032" layer="49"/>
<wire x1="25.781" y1="45.466" x2="25.781" y2="42.926" width="0.2032" layer="49"/>
<wire x1="25.781" y1="42.926" x2="0.381" y2="42.926" width="0.2032" layer="49"/>
<wire x1="0.381" y1="42.926" x2="0.381" y2="45.466" width="0.2032" layer="49"/>
<wire x1="0.381" y1="13.335" x2="25.781" y2="13.335" width="0.2032" layer="49"/>
<wire x1="25.781" y1="13.335" x2="25.781" y2="10.795" width="0.2032" layer="49"/>
<wire x1="25.781" y1="10.795" x2="0.381" y2="10.795" width="0.2032" layer="49"/>
<wire x1="0.381" y1="10.795" x2="0.381" y2="13.335" width="0.2032" layer="49"/>
<circle x="22.86" y="1.905" radius="0.683915625" width="0.2032" layer="49"/>
<circle x="22.86" y="4.445" radius="0.740528125" width="0.2032" layer="49"/>
<circle x="22.86" y="6.985" radius="0.71841875" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="1.016" x2="16.1925" y2="2.286" width="0.2032" layer="49"/>
<wire x1="16.1925" y1="2.286" x2="16.7005" y2="2.794" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="2.794" x2="16.7005" y2="3.556" width="0.2032" layer="49"/>
<wire x1="16.7005" y1="3.556" x2="22.6695" y2="3.556" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="3.556" x2="22.6695" y2="2.794" width="0.2032" layer="49"/>
<wire x1="22.6695" y1="2.794" x2="23.0505" y2="2.286" width="0.2032" layer="49"/>
<wire x1="23.0505" y1="2.286" x2="23.0505" y2="1.016" width="0.2032" layer="49"/>
<wire x1="23.0505" y1="1.016" x2="20.6375" y2="1.016" width="0.2032" layer="49"/>
<wire x1="20.6375" y1="1.016" x2="20.3835" y2="0.762" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.762" x2="20.3835" y2="0.254" width="0.2032" layer="49"/>
<wire x1="20.3835" y1="0.254" x2="20.193" y2="0.0635" width="0.2032" layer="49"/>
<wire x1="20.193" y1="0.0635" x2="19.1135" y2="0.0635" width="0.2032" layer="49"/>
<wire x1="19.1135" y1="0.0635" x2="18.923" y2="0.254" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.254" x2="18.923" y2="0.762" width="0.2032" layer="49"/>
<wire x1="18.923" y1="0.762" x2="18.669" y2="1.016" width="0.2032" layer="49"/>
<wire x1="18.669" y1="1.016" x2="16.1925" y2="1.016" width="0.2032" layer="49"/>
<wire x1="57.15" y1="23.495" x2="62.23" y2="23.495" width="0.2032" layer="49"/>
<wire x1="62.23" y1="23.495" x2="62.23" y2="8.255" width="0.2032" layer="49"/>
<wire x1="62.23" y1="8.255" x2="57.15" y2="8.255" width="0.2032" layer="49"/>
<wire x1="57.15" y1="8.255" x2="57.15" y2="23.495" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.828" x2="56.769" y2="20.828" width="0.2032" layer="49"/>
<wire x1="56.769" y1="20.828" x2="56.769" y2="10.922" width="0.2032" layer="49"/>
<wire x1="56.769" y1="10.922" x2="51.816" y2="10.922" width="0.2032" layer="49"/>
<wire x1="51.816" y1="10.922" x2="51.816" y2="12.7" width="0.2032" layer="49"/>
<wire x1="51.816" y1="12.7" x2="52.3875" y2="12.7" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="12.7" x2="52.3875" y2="19.05" width="0.2032" layer="49"/>
<wire x1="52.3875" y1="19.05" x2="51.816" y2="19.05" width="0.2032" layer="49"/>
<wire x1="51.816" y1="19.05" x2="51.816" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="51.816" y1="20.8915" x2="56.8325" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="61.341" y1="20.8915" x2="66.3575" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="20.8915" x2="66.3575" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="66.3575" y1="10.8585" x2="61.341" y2="10.8585" width="0.2032" layer="49"/>
<wire x1="61.341" y1="10.8585" x2="61.341" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.341" y1="12.7" x2="61.9125" y2="12.7" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="12.7" x2="61.9125" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.9125" y1="19.1135" x2="61.341" y2="19.1135" width="0.2032" layer="49"/>
<wire x1="61.341" y1="19.1135" x2="61.341" y2="20.8915" width="0.2032" layer="49"/>
<wire x1="58.42" y1="47.625" x2="63.5" y2="47.625" width="0.2032" layer="49"/>
<wire x1="63.5" y1="47.625" x2="63.5" y2="32.385" width="0.2032" layer="49"/>
<wire x1="63.5" y1="32.385" x2="58.42" y2="32.385" width="0.2032" layer="49"/>
<wire x1="58.42" y1="32.385" x2="58.42" y2="47.625" width="0.2032" layer="49"/>
<text x="6.35" y="39.37" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="14.351" y="43.561" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="59.69" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="58.42" y="11.43" size="0.889" layer="49" ratio="11" rot="R90">I/O interface</text>
<text x="13.97" y="11.43" size="0.889" layer="49" ratio="11">xbee interface</text>
<text x="17.78" y="2.54" size="0.889" layer="49" ratio="11">button</text>
<text x="24.13" y="6.35" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="3.81" size="0.889" layer="49" ratio="11">TP</text>
<text x="24.13" y="1.27" size="0.889" layer="49" ratio="11">TP</text>
<text x="62.865" y="34.925" size="0.889" layer="49" ratio="11" rot="R90">first choice</text>
<wire x1="26.67" y1="3.81" x2="46.99" y2="3.81" width="0.127" layer="21"/>
<wire x1="46.99" y1="3.81" x2="46.99" y2="1.27" width="0.127" layer="21"/>
<wire x1="46.99" y1="1.27" x2="26.67" y2="1.27" width="0.127" layer="21"/>
<wire x1="26.67" y1="1.27" x2="26.67" y2="3.81" width="0.127" layer="21"/>
<wire x1="49.53" y1="3.81" x2="64.77" y2="3.81" width="0.127" layer="21"/>
<wire x1="64.77" y1="3.81" x2="64.77" y2="1.27" width="0.127" layer="21"/>
<wire x1="64.77" y1="1.27" x2="49.53" y2="1.27" width="0.127" layer="21"/>
<wire x1="49.53" y1="1.27" x2="49.53" y2="3.81" width="0.127" layer="21"/>
<wire x1="44.45" y1="49.53" x2="64.77" y2="49.53" width="0.127" layer="21"/>
<wire x1="64.77" y1="49.53" x2="64.77" y2="52.07" width="0.127" layer="21"/>
<wire x1="64.77" y1="52.07" x2="44.45" y2="52.07" width="0.127" layer="21"/>
<wire x1="44.45" y1="52.07" x2="44.45" y2="49.53" width="0.127" layer="21"/>
<wire x1="17.526" y1="52.07" x2="42.926" y2="52.07" width="0.127" layer="21"/>
<wire x1="42.926" y1="52.07" x2="42.926" y2="49.53" width="0.127" layer="21"/>
<wire x1="42.926" y1="49.53" x2="17.526" y2="49.53" width="0.127" layer="21"/>
<wire x1="17.526" y1="49.53" x2="17.526" y2="52.07" width="0.127" layer="21"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="21"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="21"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="21"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="21"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="21"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="21"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="21"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="21"/>
<text x="44.069" y="49.53" size="0.889" layer="21" ratio="11" rot="R90">JP1</text>
<text x="65.8495" y="49.53" size="0.889" layer="21" ratio="11" rot="R90">JP2</text>
<text x="66.04" y="32.385" size="0.889" layer="22" ratio="11" rot="MR0">JP3</text>
<text x="65.8495" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP4</text>
<text x="48.133" y="1.27" size="0.889" layer="21" ratio="11" rot="R90">JP5</text>
</package>
</packages>
<symbols>
<symbol name="SEEEDSTUDIO_SCH_FRAME">
<wire x1="135.89" y1="3.81" x2="175.387" y2="3.81" width="0.1016" layer="94"/>
<wire x1="175.387" y1="3.81" x2="210.82" y2="3.81" width="0.1016" layer="94"/>
<wire x1="210.82" y1="3.81" x2="233.68" y2="3.81" width="0.1016" layer="94"/>
<wire x1="233.68" y1="3.81" x2="256.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="256.54" y1="3.81" x2="256.54" y2="10.16" width="0.1016" layer="94"/>
<wire x1="256.54" y1="10.16" x2="256.54" y2="16.51" width="0.1016" layer="94"/>
<wire x1="256.54" y1="16.51" x2="256.54" y2="25.4" width="0.1016" layer="94"/>
<wire x1="256.54" y1="25.4" x2="256.54" y2="36.83" width="0.1016" layer="94"/>
<wire x1="135.89" y1="3.81" x2="135.89" y2="10.16" width="0.1016" layer="94"/>
<wire x1="135.89" y1="10.16" x2="135.89" y2="16.51" width="0.1016" layer="94"/>
<wire x1="175.387" y1="36.83" x2="256.54" y2="36.83" width="0.1016" layer="94"/>
<wire x1="210.82" y1="3.81" x2="210.82" y2="10.16" width="0.1016" layer="94"/>
<wire x1="210.82" y1="10.16" x2="233.68" y2="10.16" width="0.1016" layer="94"/>
<wire x1="233.68" y1="10.16" x2="256.54" y2="10.16" width="0.1016" layer="94"/>
<wire x1="210.82" y1="10.16" x2="175.387" y2="10.16" width="0.1016" layer="94"/>
<wire x1="175.387" y1="10.16" x2="175.387" y2="3.81" width="0.1016" layer="94"/>
<wire x1="175.387" y1="10.16" x2="175.387" y2="16.51" width="0.1016" layer="94"/>
<wire x1="175.387" y1="16.51" x2="210.82" y2="16.51" width="0.1016" layer="94"/>
<wire x1="210.82" y1="16.51" x2="256.54" y2="16.51" width="0.1016" layer="94"/>
<wire x1="175.387" y1="16.51" x2="175.387" y2="25.4" width="0.1016" layer="94"/>
<wire x1="175.387" y1="25.4" x2="256.54" y2="25.4" width="0.1016" layer="94"/>
<wire x1="175.387" y1="25.4" x2="175.387" y2="36.83" width="0.1016" layer="94"/>
<text x="191.77" y="19.05" size="2.032" layer="94" font="vector" ratio="10">&gt;DRAWING_NAME</text>
<text x="246.38" y="5.08" size="1.778" layer="94" font="vector" ratio="10">&gt;SHEET</text>
<text x="234.95" y="5.08" size="2.032" layer="94" font="vector" ratio="10">Sheet:</text>
<text x="177.8" y="27.94" size="6.4516" layer="94" ratio="12">SeeedStudio</text>
<text x="177.8" y="19.05" size="2.54" layer="94" ratio="10">TITLE:</text>
<text x="177.8" y="11.43" size="2.032" layer="94" ratio="10">Design:</text>
<wire x1="210.82" y1="10.16" x2="210.82" y2="16.51" width="0.1016" layer="94"/>
<text x="212.09" y="11.43" size="2.032" layer="94" ratio="10">Check:</text>
<text x="177.8" y="5.08" size="2.032" layer="94" ratio="10">Date:</text>
<wire x1="135.89" y1="16.51" x2="175.26" y2="16.51" width="0.1016" layer="94"/>
<wire x1="233.68" y1="3.81" x2="233.68" y2="10.16" width="0.1016" layer="94"/>
<text x="212.09" y="5.08" size="2.032" layer="94" ratio="10">Vision:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="186.69" y="5.08" size="1.27" layer="94" ratio="10">&gt;Last_date_time</text>
</symbol>
<symbol name="SEEEDUINO">
<wire x1="-33.02" y1="25.4" x2="33.02" y2="25.4" width="0.1524" layer="94"/>
<wire x1="33.02" y1="25.4" x2="33.02" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-25.4" x2="-33.02" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-33.02" y1="-25.4" x2="-33.02" y2="25.4" width="0.1524" layer="94"/>
<pin name="PD0/D0/RX" x="19.05" y="30.48" length="middle" function="dotclk" rot="R270"/>
<pin name="PD1/D1/TX" x="16.51" y="30.48" length="middle" rot="R270"/>
<pin name="PD2/D2/INT0" x="13.97" y="30.48" length="middle" rot="R270"/>
<pin name="PD3/D3/INT1/PWM" x="11.43" y="30.48" length="middle" rot="R270"/>
<pin name="PD4/D4" x="8.89" y="30.48" length="middle" rot="R270"/>
<pin name="PD5/D5/PWM" x="6.35" y="30.48" length="middle" rot="R270"/>
<pin name="PD6/D6/PWM" x="3.81" y="30.48" length="middle" rot="R270"/>
<pin name="PD7/D7" x="1.27" y="30.48" length="middle" rot="R270"/>
<pin name="PB0/D8" x="-3.81" y="30.48" length="middle" function="dotclk" rot="R270"/>
<pin name="PB1/D9/PWM" x="-6.35" y="30.48" length="middle" rot="R270"/>
<pin name="PB2/D10/SS/PWM" x="-8.89" y="30.48" length="middle" rot="R270"/>
<pin name="PB3/D11/MOSI/PWM" x="-11.43" y="30.48" length="middle" rot="R270"/>
<pin name="PB4/D12/MISO" x="-13.97" y="30.48" length="middle" rot="R270"/>
<pin name="PB5/D13/SCK" x="-16.51" y="30.48" length="middle" rot="R270"/>
<pin name="GND@0" x="-19.05" y="30.48" length="middle" rot="R270"/>
<pin name="AREF" x="-21.59" y="30.48" length="middle" rot="R270"/>
<pin name="ADC6" x="-21.59" y="-30.48" length="middle" function="dotclk" rot="R90"/>
<pin name="ADC7" x="-19.05" y="-30.48" length="middle" rot="R90"/>
<pin name="RESET" x="-16.51" y="-30.48" length="middle" rot="R90"/>
<pin name="3V3" x="-13.97" y="-30.48" length="middle" rot="R90"/>
<pin name="5V" x="-11.43" y="-30.48" length="middle" rot="R90"/>
<pin name="GND@2" x="-8.89" y="-30.48" length="middle" rot="R90"/>
<pin name="GND@1" x="-6.35" y="-30.48" length="middle" rot="R90"/>
<pin name="VIN" x="-3.81" y="-30.48" length="middle" rot="R90"/>
<pin name="PC0/AD0/D14" x="6.35" y="-30.48" length="middle" function="dotclk" rot="R90"/>
<pin name="PC1/AD1/D15" x="8.89" y="-30.48" length="middle" rot="R90"/>
<pin name="PC2/AD2/D16" x="11.43" y="-30.48" length="middle" rot="R90"/>
<pin name="PC3/AD3/D17" x="13.97" y="-30.48" length="middle" rot="R90"/>
<pin name="PC4/AD4/D18/SDA" x="16.51" y="-30.48" length="middle" rot="R90"/>
<pin name="PC5/AD5/D19/SCL" x="19.05" y="-30.48" length="middle" rot="R90"/>
<pin name="VCC" x="38.1" y="6.35" length="middle" rot="R180"/>
<pin name="MOSI" x="38.1" y="3.81" length="middle" rot="R180"/>
<pin name="GND" x="38.1" y="1.27" length="middle" rot="R180"/>
<pin name="RST" x="38.1" y="-1.27" length="middle" rot="R180"/>
<pin name="SCK" x="38.1" y="-3.81" length="middle" rot="R180"/>
<pin name="MISO" x="38.1" y="-6.35" length="middle" rot="R180"/>
<pin name="SDA" x="-24.13" y="30.48" length="middle" rot="R270"/>
<pin name="SCL" x="-26.67" y="30.48" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SEEEDSTUDIO_SCH_FRAME">
<gates>
<gate name="G$1" symbol="SEEEDSTUDIO_SCH_FRAME" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ARDUINO-38P" prefix="U" uservalue="yes">
<gates>
<gate name="SEEEDUINO" symbol="SEEEDUINO" x="0" y="0"/>
</gates>
<devices>
<device name="'WITH/O-SILK'" package="ARDUINO-38P-500519002">
<connects>
<connect gate="SEEEDUINO" pin="3V3" pad="29"/>
<connect gate="SEEEDUINO" pin="5V" pad="28"/>
<connect gate="SEEEDUINO" pin="ADC6" pad="32"/>
<connect gate="SEEEDUINO" pin="ADC7" pad="31"/>
<connect gate="SEEEDUINO" pin="AREF" pad="16"/>
<connect gate="SEEEDUINO" pin="GND" pad="35"/>
<connect gate="SEEEDUINO" pin="GND@0" pad="15"/>
<connect gate="SEEEDUINO" pin="GND@1" pad="26"/>
<connect gate="SEEEDUINO" pin="GND@2" pad="27"/>
<connect gate="SEEEDUINO" pin="MISO" pad="38"/>
<connect gate="SEEEDUINO" pin="MOSI" pad="34"/>
<connect gate="SEEEDUINO" pin="PB0/D8" pad="9"/>
<connect gate="SEEEDUINO" pin="PB1/D9/PWM" pad="10"/>
<connect gate="SEEEDUINO" pin="PB2/D10/SS/PWM" pad="11"/>
<connect gate="SEEEDUINO" pin="PB3/D11/MOSI/PWM" pad="12"/>
<connect gate="SEEEDUINO" pin="PB4/D12/MISO" pad="13"/>
<connect gate="SEEEDUINO" pin="PB5/D13/SCK" pad="14"/>
<connect gate="SEEEDUINO" pin="PC0/AD0/D14" pad="19"/>
<connect gate="SEEEDUINO" pin="PC1/AD1/D15" pad="20"/>
<connect gate="SEEEDUINO" pin="PC2/AD2/D16" pad="21"/>
<connect gate="SEEEDUINO" pin="PC3/AD3/D17" pad="22"/>
<connect gate="SEEEDUINO" pin="PC4/AD4/D18/SDA" pad="23"/>
<connect gate="SEEEDUINO" pin="PC5/AD5/D19/SCL" pad="24"/>
<connect gate="SEEEDUINO" pin="PD0/D0/RX" pad="1"/>
<connect gate="SEEEDUINO" pin="PD1/D1/TX" pad="2"/>
<connect gate="SEEEDUINO" pin="PD2/D2/INT0" pad="3"/>
<connect gate="SEEEDUINO" pin="PD3/D3/INT1/PWM" pad="4"/>
<connect gate="SEEEDUINO" pin="PD4/D4" pad="5"/>
<connect gate="SEEEDUINO" pin="PD5/D5/PWM" pad="6"/>
<connect gate="SEEEDUINO" pin="PD6/D6/PWM" pad="7"/>
<connect gate="SEEEDUINO" pin="PD7/D7" pad="8"/>
<connect gate="SEEEDUINO" pin="RESET" pad="30"/>
<connect gate="SEEEDUINO" pin="RST" pad="36"/>
<connect gate="SEEEDUINO" pin="SCK" pad="37"/>
<connect gate="SEEEDUINO" pin="SCL" pad="18"/>
<connect gate="SEEEDUINO" pin="SDA" pad="17"/>
<connect gate="SEEEDUINO" pin="VCC" pad="33"/>
<connect gate="SEEEDUINO" pin="VIN" pad="25"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'WITH/-SILK'" package="ARDUINO-38P-WITH/-SILK-500519002">
<connects>
<connect gate="SEEEDUINO" pin="3V3" pad="22"/>
<connect gate="SEEEDUINO" pin="5V" pad="23"/>
<connect gate="SEEEDUINO" pin="ADC6" pad="19"/>
<connect gate="SEEEDUINO" pin="ADC7" pad="20"/>
<connect gate="SEEEDUINO" pin="AREF" pad="16"/>
<connect gate="SEEEDUINO" pin="GND" pad="36"/>
<connect gate="SEEEDUINO" pin="GND@0" pad="15"/>
<connect gate="SEEEDUINO" pin="GND@1" pad="25"/>
<connect gate="SEEEDUINO" pin="GND@2" pad="24"/>
<connect gate="SEEEDUINO" pin="MISO" pad="33"/>
<connect gate="SEEEDUINO" pin="MOSI" pad="37"/>
<connect gate="SEEEDUINO" pin="PB0/D8" pad="9"/>
<connect gate="SEEEDUINO" pin="PB1/D9/PWM" pad="10"/>
<connect gate="SEEEDUINO" pin="PB2/D10/SS/PWM" pad="11"/>
<connect gate="SEEEDUINO" pin="PB3/D11/MOSI/PWM" pad="12"/>
<connect gate="SEEEDUINO" pin="PB4/D12/MISO" pad="13"/>
<connect gate="SEEEDUINO" pin="PB5/D13/SCK" pad="14"/>
<connect gate="SEEEDUINO" pin="PC0/AD0/D14" pad="27"/>
<connect gate="SEEEDUINO" pin="PC1/AD1/D15" pad="28"/>
<connect gate="SEEEDUINO" pin="PC2/AD2/D16" pad="29"/>
<connect gate="SEEEDUINO" pin="PC3/AD3/D17" pad="30"/>
<connect gate="SEEEDUINO" pin="PC4/AD4/D18/SDA" pad="31"/>
<connect gate="SEEEDUINO" pin="PC5/AD5/D19/SCL" pad="32"/>
<connect gate="SEEEDUINO" pin="PD0/D0/RX" pad="1"/>
<connect gate="SEEEDUINO" pin="PD1/D1/TX" pad="2"/>
<connect gate="SEEEDUINO" pin="PD2/D2/INT0" pad="3"/>
<connect gate="SEEEDUINO" pin="PD3/D3/INT1/PWM" pad="4"/>
<connect gate="SEEEDUINO" pin="PD4/D4" pad="5"/>
<connect gate="SEEEDUINO" pin="PD5/D5/PWM" pad="6"/>
<connect gate="SEEEDUINO" pin="PD6/D6/PWM" pad="7"/>
<connect gate="SEEEDUINO" pin="PD7/D7" pad="8"/>
<connect gate="SEEEDUINO" pin="RESET" pad="21"/>
<connect gate="SEEEDUINO" pin="RST" pad="35"/>
<connect gate="SEEEDUINO" pin="SCK" pad="34"/>
<connect gate="SEEEDUINO" pin="SCL" pad="18"/>
<connect gate="SEEEDUINO" pin="SDA" pad="17"/>
<connect gate="SEEEDUINO" pin="VCC" pad="38"/>
<connect gate="SEEEDUINO" pin="VIN" pad="26"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="ARDUINO-38P">
<connects>
<connect gate="SEEEDUINO" pin="3V3" pad="29"/>
<connect gate="SEEEDUINO" pin="5V" pad="28"/>
<connect gate="SEEEDUINO" pin="ADC6" pad="32"/>
<connect gate="SEEEDUINO" pin="ADC7" pad="31"/>
<connect gate="SEEEDUINO" pin="AREF" pad="16"/>
<connect gate="SEEEDUINO" pin="GND" pad="35"/>
<connect gate="SEEEDUINO" pin="GND@0" pad="15"/>
<connect gate="SEEEDUINO" pin="GND@1" pad="26"/>
<connect gate="SEEEDUINO" pin="GND@2" pad="27"/>
<connect gate="SEEEDUINO" pin="MISO" pad="38"/>
<connect gate="SEEEDUINO" pin="MOSI" pad="34"/>
<connect gate="SEEEDUINO" pin="PB0/D8" pad="9"/>
<connect gate="SEEEDUINO" pin="PB1/D9/PWM" pad="10"/>
<connect gate="SEEEDUINO" pin="PB2/D10/SS/PWM" pad="11"/>
<connect gate="SEEEDUINO" pin="PB3/D11/MOSI/PWM" pad="12"/>
<connect gate="SEEEDUINO" pin="PB4/D12/MISO" pad="13"/>
<connect gate="SEEEDUINO" pin="PB5/D13/SCK" pad="14"/>
<connect gate="SEEEDUINO" pin="PC0/AD0/D14" pad="19"/>
<connect gate="SEEEDUINO" pin="PC1/AD1/D15" pad="20"/>
<connect gate="SEEEDUINO" pin="PC2/AD2/D16" pad="21"/>
<connect gate="SEEEDUINO" pin="PC3/AD3/D17" pad="22"/>
<connect gate="SEEEDUINO" pin="PC4/AD4/D18/SDA" pad="23"/>
<connect gate="SEEEDUINO" pin="PC5/AD5/D19/SCL" pad="24"/>
<connect gate="SEEEDUINO" pin="PD0/D0/RX" pad="1"/>
<connect gate="SEEEDUINO" pin="PD1/D1/TX" pad="2"/>
<connect gate="SEEEDUINO" pin="PD2/D2/INT0" pad="3"/>
<connect gate="SEEEDUINO" pin="PD3/D3/INT1/PWM" pad="4"/>
<connect gate="SEEEDUINO" pin="PD4/D4" pad="5"/>
<connect gate="SEEEDUINO" pin="PD5/D5/PWM" pad="6"/>
<connect gate="SEEEDUINO" pin="PD6/D6/PWM" pad="7"/>
<connect gate="SEEEDUINO" pin="PD7/D7" pad="8"/>
<connect gate="SEEEDUINO" pin="RESET" pad="30"/>
<connect gate="SEEEDUINO" pin="RST" pad="36"/>
<connect gate="SEEEDUINO" pin="SCK" pad="37"/>
<connect gate="SEEEDUINO" pin="SCL" pad="18"/>
<connect gate="SEEEDUINO" pin="SDA" pad="17"/>
<connect gate="SEEEDUINO" pin="VCC" pad="33"/>
<connect gate="SEEEDUINO" pin="VIN" pad="25"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector">
<packages>
<package name="9P-SMD-W/-RING">
<wire x1="1.143" y1="7.429" x2="14.986" y2="7.429" width="0.127" layer="21"/>
<wire x1="14.986" y1="7.429" x2="14.986" y2="4.8979" width="0.127" layer="21"/>
<wire x1="14.986" y1="4.8979" x2="14.802" y2="3.898" width="0.127" layer="21" curve="-20.855518"/>
<wire x1="14.802" y1="3.898" x2="13.97" y2="-0.6231" width="0.127" layer="21" curve="20.85427"/>
<wire x1="13.97" y1="-0.6231" x2="13.97" y2="-7.557" width="0.127" layer="21"/>
<wire x1="13.97" y1="-7.557" x2="0" y2="-7.557" width="0.127" layer="21"/>
<wire x1="0" y1="-7.557" x2="0" y2="6.032" width="0.127" layer="21"/>
<wire x1="0" y1="6.032" x2="1.143" y2="6.032" width="0.127" layer="21"/>
<wire x1="1.143" y1="6.032" x2="1.143" y2="7.429" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.25" dx="1.6" dy="0.7" layer="1"/>
<smd name="2" x="0" y="1.15" dx="1.6" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.05" dx="1.6" dy="0.7" layer="1"/>
<smd name="4" x="0" y="-1.05" dx="1.6" dy="0.7" layer="1"/>
<smd name="5" x="0" y="-2.15" dx="1.6" dy="0.7" layer="1"/>
<smd name="6" x="0" y="-3.25" dx="1.6" dy="0.7" layer="1"/>
<smd name="7" x="0" y="-4.35" dx="1.6" dy="0.7" layer="1"/>
<smd name="8" x="0" y="-5.45" dx="1.6" dy="0.7" layer="1"/>
<smd name="9" x="0" y="-6.55" dx="1.6" dy="0.7" layer="1"/>
<smd name="G1" x="0.6" y="-7.85" dx="1.6" dy="1.4" layer="1"/>
<smd name="G2" x="10.1" y="-7.85" dx="2.2" dy="1.4" layer="1"/>
<smd name="G4" x="0.5" y="6.95" dx="1.4" dy="1.8" layer="1"/>
<smd name="G3" x="10.1" y="7.85" dx="2.2" dy="1.4" layer="1"/>
<text x="2.54" y="7.747" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="2.54" y="-0.635" size="0.635" layer="27" ratio="10">&gt;value</text>
<text x="4.826" y="2.159" size="0.254" layer="33" ratio="10">&gt;name</text>
<rectangle x1="0" y1="-7.493" x2="13.97" y2="7.366" layer="39"/>
<hole x="10.5" y="-4.95" drill="0.9"/>
<hole x="10.5" y="3.05" drill="0.9"/>
<rectangle x1="4.4" y1="-6.1" x2="6.4" y2="2.9" layer="41"/>
</package>
<package name="2P-1.27">
<wire x1="-0.635" y1="1.7" x2="0.635" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.7" x2="0.635" y2="-1.7" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.7" x2="-0.635" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.7" x2="-0.635" y2="1.7" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.635" drill="0.635" diameter="1.016"/>
<pad name="2" x="0" y="-0.635" drill="0.635" diameter="1.016"/>
<text x="-0.889" y="-1.778" size="0.8128" layer="21" ratio="10" rot="R90">&gt;name</text>
<text x="1.651" y="-1.778" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<rectangle x1="-0.635" y1="-1.7" x2="0.635" y2="1.7" layer="39"/>
</package>
<package name="2P-1.5">
<wire x1="-0.762" y1="1.524" x2="0.762" y2="1.524" width="0.127" layer="21"/>
<wire x1="0.762" y1="1.524" x2="0.762" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.524" x2="-0.762" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-1.524" x2="-0.762" y2="1.524" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.75" drill="0.635" diameter="1.016" shape="square"/>
<pad name="2" x="0" y="-0.75" drill="0.635" diameter="1.016"/>
<text x="-1.016" y="-1.778" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="1.778" y="-2.032" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.762" y1="-1.524" x2="0.762" y2="1.524" layer="39"/>
</package>
<package name="2P-2.0">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<pad name="1" x="-1.016" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="1.016" y="0" drill="0.8128" diameter="1.397"/>
<text x="-1.778" y="1.143" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.032" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="1.016" layer="39"/>
</package>
<package name="2P-2.54-65/35MIL">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0" layer="39"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0" layer="39"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0" layer="39"/>
<pad name="1" x="0" y="1.27" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<text x="-1.524" y="-1.778" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<text x="2.286" y="-2.159" size="0.8128" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" layer="39"/>
</package>
<package name="2P-2.0-50/30MIL">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<pad name="1" x="-1.016" y="0" drill="0.762" diameter="1.27" shape="square"/>
<pad name="2" x="1.016" y="0" drill="0.762" diameter="1.27"/>
<text x="-2.159" y="-2.032" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.905" y="1.143" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="1.016" layer="39"/>
</package>
<package name="8P-2.54">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0" layer="39"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0" layer="39"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0" layer="39"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0" layer="39"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="0.508" y="-3.81" size="1.27" layer="27" rot="R90">&gt;value</text>
<text x="-2.667" y="10.287" size="1.27" layer="25">&gt;name</text>
<text x="0.635" y="-1.905" size="0.8128" layer="33" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-55/35MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.397" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.397"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.397"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.397"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.397"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.397"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.397"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.397"/>
<text x="-1.778" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.413" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0" y="-0.635" size="0.3048" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-80/40MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="1.016" diameter="2.032" shape="square"/>
<pad name="2" x="0" y="6.35" drill="1.016" diameter="2.032"/>
<pad name="3" x="0" y="3.81" drill="1.016" diameter="2.032"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="2.032"/>
<pad name="5" x="0" y="-1.27" drill="1.016" diameter="2.032"/>
<pad name="6" x="0" y="-3.81" drill="1.016" diameter="2.032"/>
<pad name="7" x="0" y="-6.35" drill="1.016" diameter="2.032"/>
<pad name="8" x="0" y="-8.89" drill="1.016" diameter="2.032"/>
<text x="-3.683" y="10.414" size="1.778" layer="25">&gt;name</text>
<text x="0.508" y="-3.81" size="1.27" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0" y="-0.635" size="0.254" layer="33" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-SMD">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="1.27" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="3" x="1.27" y="3.81" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="2" x="-1.27" y="6.35" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="1" x="1.27" y="8.89" dx="3.048" dy="1.524" layer="1"/>
<smd name="5" x="1.27" y="-1.27" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="6" x="-1.27" y="-3.81" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="7" x="1.27" y="-6.35" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="8" x="-1.27" y="-8.89" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<text x="-1.905" y="10.16" size="1.27" layer="25" ratio="10">&gt;name</text>
<text x="0" y="-1.27" size="0.889" layer="33" ratio="12" rot="R90">&gt;name</text>
<text x="0.889" y="-4.826" size="0.889" layer="27" ratio="10" rot="R90">&gt;value</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-FEMALE-D90">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.27" y="10.414" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.159" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-2.032" size="0.8128" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="10.16"/>
<vertex x="9.525" y="10.16" curve="-90"/>
<vertex x="10.16" y="9.525"/>
<vertex x="10.16" y="-9.525" curve="-90"/>
<vertex x="9.525" y="-10.16"/>
<vertex x="1.27" y="-10.16"/>
</polygon>
</package>
<package name="8P-2.54-MALE-D90">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.905" y1="8.89" x2="10.16" y2="8.89" width="0.6096" layer="21"/>
<wire x1="1.905" y1="6.35" x2="10.16" y2="6.35" width="0.6096" layer="21"/>
<wire x1="1.905" y1="3.81" x2="10.16" y2="3.81" width="0.6096" layer="21"/>
<wire x1="1.905" y1="1.27" x2="10.16" y2="1.27" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="10.16" y2="-1.27" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="10.16" y2="-3.81" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="10.16" y2="-6.35" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="10.16" y2="-8.89" width="0.6096" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.27" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.413" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-2.032" size="0.8128" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="9.525"/>
<vertex x="1.905" y="10.16"/>
<vertex x="3.175" y="10.16"/>
<vertex x="3.81" y="9.525"/>
<vertex x="3.81" y="8.255"/>
<vertex x="3.175" y="7.62"/>
<vertex x="1.905" y="7.62"/>
<vertex x="1.27" y="8.255"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="6.985"/>
<vertex x="1.905" y="7.62"/>
<vertex x="3.175" y="7.62"/>
<vertex x="3.81" y="6.985"/>
<vertex x="3.81" y="5.715"/>
<vertex x="3.175" y="5.08"/>
<vertex x="1.905" y="5.08"/>
<vertex x="1.27" y="5.715"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="4.445"/>
<vertex x="1.905" y="5.08"/>
<vertex x="3.175" y="5.08"/>
<vertex x="3.81" y="4.445"/>
<vertex x="3.81" y="3.175"/>
<vertex x="3.175" y="2.54"/>
<vertex x="1.905" y="2.54"/>
<vertex x="1.27" y="3.175"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="1.905"/>
<vertex x="1.905" y="2.54"/>
<vertex x="3.175" y="2.54"/>
<vertex x="3.81" y="1.905"/>
<vertex x="3.81" y="0.635"/>
<vertex x="3.175" y="0"/>
<vertex x="1.905" y="0"/>
<vertex x="1.27" y="0.635"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-0.635"/>
<vertex x="1.905" y="0"/>
<vertex x="3.175" y="0"/>
<vertex x="3.81" y="-0.635"/>
<vertex x="3.81" y="-1.905"/>
<vertex x="3.175" y="-2.54"/>
<vertex x="1.905" y="-2.54"/>
<vertex x="1.27" y="-1.905"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-3.175"/>
<vertex x="1.905" y="-2.54"/>
<vertex x="3.175" y="-2.54"/>
<vertex x="3.81" y="-3.175"/>
<vertex x="3.81" y="-4.445"/>
<vertex x="3.175" y="-5.08"/>
<vertex x="1.905" y="-5.08"/>
<vertex x="1.27" y="-4.445"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-5.715"/>
<vertex x="1.905" y="-5.08"/>
<vertex x="3.175" y="-5.08"/>
<vertex x="3.81" y="-5.715"/>
<vertex x="3.81" y="-6.985"/>
<vertex x="3.175" y="-7.62"/>
<vertex x="1.905" y="-7.62"/>
<vertex x="1.27" y="-6.985"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-8.255"/>
<vertex x="1.905" y="-7.62"/>
<vertex x="3.175" y="-7.62"/>
<vertex x="3.81" y="-8.255"/>
<vertex x="3.81" y="-9.525"/>
<vertex x="3.175" y="-10.16"/>
<vertex x="1.905" y="-10.16"/>
<vertex x="1.27" y="-9.525"/>
</polygon>
</package>
<package name="8P-2.54-65/35MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.778" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-1.905" size="0.635" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-1.524" size="0.635" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="MICROSD">
<wire x1="-12.7" y1="11.43" x2="-12.7" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-11.43" x2="12.7" y2="-11.43" width="0.254" layer="94"/>
<wire x1="12.7" y1="-11.43" x2="12.7" y2="13.97" width="0.254" layer="94"/>
<wire x1="12.7" y1="13.97" x2="6.35" y2="13.97" width="0.254" layer="94"/>
<wire x1="6.35" y1="13.97" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="3.81" y2="12.7" width="0.254" layer="94"/>
<wire x1="3.81" y1="12.7" x2="3.81" y2="13.97" width="0.254" layer="94"/>
<wire x1="3.81" y1="13.97" x2="1.27" y2="13.97" width="0.254" layer="94"/>
<wire x1="1.27" y1="13.97" x2="-1.27" y2="11.43" width="0.254" layer="94"/>
<wire x1="-1.27" y1="11.43" x2="-12.7" y2="11.43" width="0.254" layer="94"/>
<text x="-11.43" y="11.43" size="1.778" layer="95">&gt;name</text>
<text x="-2.54" y="0" size="1.778" layer="96">&gt;value</text>
<pin name="DATA2" x="-17.78" y="10.16" length="middle"/>
<pin name="CS" x="-17.78" y="7.62" length="middle"/>
<pin name="DI" x="-17.78" y="5.08" length="middle"/>
<pin name="VDD" x="-17.78" y="2.54" length="middle"/>
<pin name="SCLK" x="-17.78" y="0" length="middle"/>
<pin name="VSS" x="-17.78" y="-2.54" length="middle"/>
<pin name="DO" x="-17.78" y="-5.08" length="middle"/>
<pin name="DATA1" x="-17.78" y="-7.62" length="middle"/>
<pin name="CDN" x="-17.78" y="-10.16" length="middle"/>
<pin name="G4" x="7.62" y="19.05" length="middle" rot="R270"/>
<pin name="G3" x="10.16" y="19.05" length="middle" rot="R270"/>
<pin name="G1" x="7.62" y="-16.51" length="middle" rot="R90"/>
<pin name="G2" x="10.16" y="-16.51" length="middle" rot="R90"/>
</symbol>
<symbol name="CK_1X2">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="5.08" size="1.778" layer="95" ratio="10">&gt;name</text>
<text x="0" y="-3.81" size="1.27" layer="96" ratio="10" rot="R90">&gt;value</text>
<pin name="1" x="-7.62" y="2.54" length="middle" function="dotclk" swaplevel="1"/>
<pin name="2" x="-7.62" y="-2.54" length="middle" swaplevel="1"/>
</symbol>
<symbol name="CK_1X8">
<wire x1="-2.54" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;name</text>
<text x="1.27" y="-5.08" size="1.778" layer="96" rot="R90">&gt;value</text>
<pin name="2" x="-7.62" y="6.35" visible="pad" length="middle"/>
<pin name="3" x="-7.62" y="3.81" visible="pad" length="middle"/>
<pin name="4" x="-7.62" y="1.27" visible="pad" length="middle"/>
<pin name="5" x="-7.62" y="-1.27" visible="pad" length="middle"/>
<pin name="6" x="-7.62" y="-3.81" visible="pad" length="middle"/>
<pin name="7" x="-7.62" y="-6.35" visible="pad" length="middle"/>
<pin name="8" x="-7.62" y="-8.89" visible="pad" length="middle"/>
<pin name="1" x="-7.62" y="8.89" visible="pad" length="middle" function="dotclk"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HOLDER-MICROSD-9P" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="MICROSD" x="0" y="0"/>
</gates>
<devices>
<device name="'HOLDER'" package="9P-SMD-W/-RING">
<connects>
<connect gate="G$1" pin="CDN" pad="9"/>
<connect gate="G$1" pin="CS" pad="2"/>
<connect gate="G$1" pin="DATA1" pad="8"/>
<connect gate="G$1" pin="DATA2" pad="1"/>
<connect gate="G$1" pin="DI" pad="3"/>
<connect gate="G$1" pin="DO" pad="7"/>
<connect gate="G$1" pin="G1" pad="G1"/>
<connect gate="G$1" pin="G2" pad="G2"/>
<connect gate="G$1" pin="G3" pad="G3"/>
<connect gate="G$1" pin="G4" pad="G4"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-2P" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="CK_1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-1.27" package="2P-1.27">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.5" package="2P-1.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.0" package="2P-2.0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-65/35MIL" package="2P-2.54-65/35MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-50/30MIL" package="2P-2.0-50/30MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-8P" prefix="J" uservalue="yes">
<gates>
<gate name="J" symbol="CK_1X8" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="8P-2.54">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-55/35MIL" package="8P-2.54-55/35MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-80/40MIL" package="8P-2.54-80/40MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54-SMD" package="8P-2.54-SMD">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-F90" package="8P-2.54-FEMALE-D90">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M90" package="8P-2.54-MALE-D90">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-65/35MIL" package="8P-2.54-65/35MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.1524" drill="0.3048">
<clearance class="0" value="0.1524"/>
</class>
</classes>
<parts>
<part name="U1" library="template" deviceset="VC0706" device="'PREA'" value="VC0706PREA"/>
<part name="J2" library="template" deviceset="CAMERA-OV7670" device="" value="ov7725"/>
<part name="U$17" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U$19" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U4" library="IC" deviceset="XC6206" device="MR" value="XC6206P182MR"/>
<part name="U6" library="IC" deviceset="LD1117" device="'SOT89'" value="CJA1117B-3.3"/>
<part name="C1" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C31" library="Discrete" deviceset="C*" device="-0805" value="10uF-10V"/>
<part name="C3" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C4" library="Discrete" deviceset="C*" device="-0805" value="10uF-10V"/>
<part name="L1" library="Discrete" deviceset="L*" device="'0603'" value="330R-1000mA"/>
<part name="L2" library="Discrete" deviceset="L*" device="'0603'" value="330R-1000mA"/>
<part name="C5" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$20" library="Power or GND " deviceset="GND_SIGNAL" device="" value="AGND"/>
<part name="U$21" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C25" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C23" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C20" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C10" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C11" library="Discrete" deviceset="C*" device="-0805" value="10uF-10V"/>
<part name="U$25" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U3" library="IC" deviceset="XC6206" device="MR" value="XC6206P122MR"/>
<part name="C12" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C14" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C15" library="Discrete" deviceset="C*" device="-0805" value="10uF-10V"/>
<part name="U$24" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C24" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="U$22" library="moudle" deviceset="SEEEDSTUDIO_SCH_FRAME" device=""/>
<part name="U5" library="moudle" deviceset="ARDUINO-38P" device="'WITH/-SILK'"/>
<part name="R1" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="R2" library="Discrete" deviceset="R*" device="-0603" value="10k"/>
<part name="C13" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="U$23" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C16" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C17" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C18" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$27" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U$28" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U$29" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="R3" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="C19" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C8" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="R4" library="Discrete" deviceset="R*" device="-0603" value="75R-1%"/>
<part name="U$30" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C21" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="C22" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$31" library="Power or GND " deviceset="GND_SIGNAL" device="" value="AGND"/>
<part name="U$32" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C6" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$33" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="C26" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="C27" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$34" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="R22" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="U$35" library="moudle" deviceset="SEEEDSTUDIO_SCH_FRAME" device=""/>
<part name="J6" library="Connector" deviceset="HOLDER-MICROSD-9P" device="'HOLDER'" value="Micro SD Card;TF-01"/>
<part name="U$36" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="U$37" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="U$41" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="U7" library="IC" deviceset="CAT24C256" device="" value="24AA16"/>
<part name="C28" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="R10" library="Discrete" deviceset="R*" device="-0603" value="DNP"/>
<part name="R11" library="Discrete" deviceset="R*" device="-0603" value="DNP"/>
<part name="U$38" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U8" library="IC" deviceset="74V*125" device="PW" technology="HC" value="74VHC125PW"/>
<part name="U$40" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="U$42" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="U$43" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="C29" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="R14" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="R15" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="U$26" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="L3" library="Discrete" deviceset="L*" device="'0603'" value="330R-1000mA"/>
<part name="C2" library="Discrete" deviceset="C*" device="-0603" value="100nF"/>
<part name="U$45" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="R6" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="R5" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="PWR" library="Discrete" deviceset="LED*" device="'0603'" value="Green"/>
<part name="R7" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="U$10" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="RESET" library="Discrete" deviceset="LED*" device="'0603'" value="Red"/>
<part name="R8" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="R9" library="Discrete" deviceset="R*" device="-0603" value="1M"/>
<part name="R24" library="Discrete" deviceset="R*" device="-0603" value="100R"/>
<part name="R25" library="Discrete" deviceset="R*" device="-0603" value="100R"/>
<part name="C32" library="Discrete" deviceset="C*" device="-0603" value="1uF"/>
<part name="U$46" library="Power or GND " deviceset="GND_POWER" device="" value="GND"/>
<part name="R26" library="Discrete" deviceset="R*" device="-0603" value="100R"/>
<part name="U$48" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="U$49" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="V_OUT" library="Connector" deviceset="HEADER-2P" device="-65/35MIL" value="DNP"/>
<part name="U$50" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="CMR_RX" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DIP Blue Male header"/>
<part name="CMR_TX" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DIP Blue Male header"/>
<part name="IO" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DIP Blue Male header"/>
<part name="R29" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="SD" library="Discrete" deviceset="LED*" device="'0603'" value="Blue"/>
<part name="P1" library="Discrete" deviceset="PAD-JUMPER-3P" device="-40*40"/>
<part name="P2" library="Discrete" deviceset="PAD-JUMPER-3P" device="-40*40"/>
<part name="P3" library="Discrete" deviceset="PAD-JUMPER-3P" device="-40*40"/>
<part name="Q2" library="Discrete" deviceset="TRANSISTOR-NPN*" device="-SOT23" value="S9013"/>
<part name="R23" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="R30" library="Discrete" deviceset="R*" device="-0603" value="1k"/>
<part name="Q1" library="Discrete" deviceset="TRANSISTOR-NPN*" device="-SOT23" value="S9013"/>
<part name="R12" library="Discrete" deviceset="R*" device="-0603" value="4.7k"/>
<part name="R13" library="Discrete" deviceset="R*" device="-0603" value="1k"/>
<part name="R16" library="Discrete" deviceset="R*" device="-0603" value="270R"/>
<part name="CMR" library="Discrete" deviceset="LED*" device="'0603'" value="Yellwo"/>
<part name="P4" library="Discrete" deviceset="PAD-JUMPER-3P" device="-40*40"/>
<part name="P5" library="Discrete" deviceset="PAD-JUMPER-3P" device="-40*40"/>
<part name="P6" library="Discrete" deviceset="PAD-JUMPER-2P" device=""/>
<part name="RST" library="Discrete" deviceset="BUTTON-2P-REINFORCE" device="-3100060P1" value="TS-1188E"/>
<part name="U$1" library="Power or GND " deviceset="GND_POWER" device=""/>
<part name="X2" library="Discrete" deviceset="CRYSTAL-RESONATOR" device="'CSTCR6M00G53-R0'" value="CSTCR6M00G53-R0"/>
<part name="1V2" library="Discrete" deviceset="PAD-TEST-POINT" device="'4545'"/>
<part name="3V3" library="Discrete" deviceset="PAD-TEST-POINT" device="'4545'"/>
<part name="1V8" library="Discrete" deviceset="PAD-TEST-POINT" device="'4545'"/>
<part name="P7" library="Discrete" deviceset="PAD-MARK" device="'ANNULAR'"/>
<part name="P8" library="Discrete" deviceset="PAD-MARK" device="'ANNULAR'"/>
<part name="P9" library="Discrete" deviceset="PAD-MARK" device="'ANNULAR'"/>
<part name="P10" library="Discrete" deviceset="PAD-MARK" device="'ANNULAR'"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="457.2" y="11.43" size="1.778" layer="97" ratio="10">Xiangnan.Qu</text>
<text x="190.5" y="11.43" size="1.778" layer="97" ratio="10">Xiangnan.Qu</text>
<wire x1="80.01" y1="167.64" x2="99.06" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="140.97" width="0.1524" layer="97" style="shortdash"/>
<wire x1="99.06" y1="140.97" x2="80.01" y2="140.97" width="0.1524" layer="97" style="shortdash"/>
<wire x1="80.01" y1="140.97" x2="80.01" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<text x="81.28" y="168.91" size="1.27" layer="97" ratio="10">SPI Select Jumper</text>
<wire x1="373.38" y1="162.56" x2="391.16" y2="162.56" width="0.1524" layer="97" style="shortdash"/>
<wire x1="391.16" y1="162.56" x2="391.16" y2="143.51" width="0.1524" layer="97" style="shortdash"/>
<wire x1="391.16" y1="143.51" x2="373.38" y2="143.51" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="143.51" x2="373.38" y2="162.56" width="0.1524" layer="97" style="shortdash"/>
<text x="373.38" y="163.83" size="1.27" layer="97" ratio="10">Uart Select Jumper</text>
<wire x1="143.51" y1="77.47" x2="134.62" y2="77.47" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="77.47" x2="134.62" y2="67.31" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="67.31" x2="143.51" y2="67.31" width="0.1524" layer="97" style="shortdash"/>
<wire x1="143.51" y1="67.31" x2="143.51" y2="77.47" width="0.1524" layer="97" style="shortdash"/>
<text x="133.35" y="64.77" size="1.27" layer="97" ratio="10">SPI CS Jumper</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="351.79" y="95.25"/>
<instance part="J2" gate="G$1" x="494.03" y="130.81"/>
<instance part="U$17" gate="G$1" x="461.01" y="137.16" rot="R270"/>
<instance part="U$19" gate="G$1" x="461.01" y="119.38" rot="R270"/>
<instance part="U4" gate="G$1" x="34.29" y="22.86"/>
<instance part="U6" gate="G$1" x="36.83" y="46.99" smashed="yes">
<attribute name="NAME" x="29.21" y="50.8" size="1.27" layer="95"/>
<attribute name="VALUE" x="36.83" y="50.8" size="1.27" layer="96"/>
</instance>
<instance part="C1" gate="C" x="22.86" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="43.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="24.13" y="39.37" size="1.27" layer="96"/>
</instance>
<instance part="C31" gate="C" x="19.05" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="43.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="8.89" y="39.37" size="1.27" layer="96"/>
</instance>
<instance part="C3" gate="C" x="54.61" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="43.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="39.37" size="1.27" layer="96"/>
</instance>
<instance part="C4" gate="C" x="50.8" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="46.99" y="43.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="41.91" y="39.37" size="1.27" layer="96"/>
</instance>
<instance part="L1" gate="L" x="67.31" y="46.99" smashed="yes">
<attribute name="NAME" x="62.23" y="44.45" size="1.27" layer="95"/>
<attribute name="VALUE" x="62.23" y="48.26" size="1.27" layer="96"/>
</instance>
<instance part="L2" gate="L" x="67.31" y="36.83" smashed="yes">
<attribute name="NAME" x="62.23" y="38.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="60.96" y="34.29" size="1.27" layer="96"/>
</instance>
<instance part="C5" gate="C" x="74.93" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="44.45" size="1.27" layer="95"/>
<attribute name="VALUE" x="76.2" y="41.91" size="1.27" layer="96"/>
</instance>
<instance part="U$20" gate="G$1" x="74.93" y="34.29" smashed="yes">
<attribute name="VALUE" x="73.025" y="31.115" size="0.8128" layer="96"/>
</instance>
<instance part="U$21" gate="G$1" x="50.8" y="34.29" smashed="yes">
<attribute name="VALUE" x="48.895" y="31.115" size="0.8128" layer="96"/>
</instance>
<instance part="C25" gate="C" x="464.82" y="132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="464.82" y="135.89" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="464.82" y="130.81" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="C23" gate="C" x="467.36" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="463.55" y="124.46" size="1.27" layer="95"/>
<attribute name="VALUE" x="468.63" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="C20" gate="C" x="19.05" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="20.32" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="20.32" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="C10" gate="C" x="55.88" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="57.15" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="57.15" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="C11" gate="C" x="52.07" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="48.26" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="43.18" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="U$25" gate="G$1" x="52.07" y="10.16" smashed="yes">
<attribute name="VALUE" x="50.165" y="6.985" size="0.8128" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="88.9" y="22.86"/>
<instance part="C12" gate="C" x="73.66" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="74.93" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="C14" gate="C" x="110.49" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="111.76" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="111.76" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="C15" gate="C" x="106.68" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="102.87" y="19.05" size="1.27" layer="95"/>
<attribute name="VALUE" x="97.79" y="15.24" size="1.27" layer="96"/>
</instance>
<instance part="U$24" gate="G$1" x="106.68" y="10.16" smashed="yes">
<attribute name="VALUE" x="104.775" y="6.985" size="0.8128" layer="96"/>
</instance>
<instance part="C24" gate="C" x="467.36" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="463.55" y="109.22" size="1.27" layer="95"/>
<attribute name="VALUE" x="468.63" y="109.22" size="1.27" layer="96"/>
</instance>
<instance part="U$22" gate="G$1" x="265.43" y="0"/>
<instance part="U5" gate="SEEEDUINO" x="48.26" y="109.22"/>
<instance part="R1" gate="R" x="336.55" y="39.37" rot="R90"/>
<instance part="R2" gate="R" x="346.71" y="39.37" rot="R90"/>
<instance part="C13" gate="C" x="356.87" y="39.37" smashed="yes" rot="R90">
<attribute name="NAME" x="356.235" y="34.925" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="356.235" y="40.64" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U$23" gate="G$1" x="356.87" y="33.02" smashed="yes">
<attribute name="VALUE" x="354.965" y="29.845" size="0.8128" layer="96"/>
</instance>
<instance part="C16" gate="C" x="367.03" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="363.22" y="38.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="36.83" size="1.27" layer="96"/>
</instance>
<instance part="C17" gate="C" x="377.19" y="40.64" smashed="yes">
<attribute name="NAME" x="373.38" y="38.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="373.38" y="36.83" size="1.27" layer="96"/>
</instance>
<instance part="C18" gate="C" x="389.89" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="392.43" y="38.1" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="393.7" y="38.1" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U$27" gate="G$1" x="372.11" y="34.29" smashed="yes">
<attribute name="VALUE" x="370.205" y="31.115" size="0.8128" layer="96"/>
</instance>
<instance part="U$28" gate="G$1" x="386.08" y="34.29" smashed="yes">
<attribute name="VALUE" x="384.175" y="31.115" size="0.8128" layer="96"/>
</instance>
<instance part="U$29" gate="G$1" x="400.05" y="58.42" smashed="yes">
<attribute name="VALUE" x="398.145" y="55.245" size="0.8128" layer="96"/>
</instance>
<instance part="R3" gate="R" x="403.86" y="64.77" rot="R90"/>
<instance part="C19" gate="C" x="407.67" y="64.77" smashed="yes" rot="R90">
<attribute name="NAME" x="407.035" y="60.325" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="407.035" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="C" x="411.48" y="64.77" smashed="yes" rot="R90">
<attribute name="NAME" x="414.02" y="59.69" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="414.02" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R4" gate="R" x="417.83" y="63.5" rot="R90"/>
<instance part="U$30" gate="G$1" x="417.83" y="53.34" smashed="yes">
<attribute name="VALUE" x="415.925" y="50.165" size="0.8128" layer="96"/>
</instance>
<instance part="C21" gate="C" x="412.75" y="90.17" smashed="yes" rot="R270">
<attribute name="NAME" x="413.385" y="94.615" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="413.385" y="88.9" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="C22" gate="C" x="408.94" y="90.17" smashed="yes" rot="R270">
<attribute name="NAME" x="406.4" y="95.25" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="406.4" y="90.17" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="U$31" gate="G$1" x="417.83" y="95.25" smashed="yes" rot="R90">
<attribute name="VALUE" x="421.005" y="93.345" size="0.8128" layer="96" rot="R90"/>
</instance>
<instance part="U$32" gate="G$1" x="416.56" y="111.76" smashed="yes" rot="R90">
<attribute name="VALUE" x="419.735" y="109.855" size="0.8128" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="C" x="408.94" y="120.65" smashed="yes">
<attribute name="NAME" x="403.86" y="120.65" size="1.27" layer="95"/>
<attribute name="VALUE" x="410.21" y="120.65" size="1.27" layer="96"/>
</instance>
<instance part="U$33" gate="G$1" x="419.1" y="125.73" smashed="yes" rot="R90">
<attribute name="VALUE" x="422.275" y="123.825" size="0.8128" layer="96" rot="R90"/>
</instance>
<instance part="C26" gate="C" x="320.04" y="151.13" smashed="yes">
<attribute name="NAME" x="316.23" y="152.4" size="1.27" layer="95"/>
<attribute name="VALUE" x="316.23" y="148.59" size="1.27" layer="96"/>
</instance>
<instance part="C27" gate="C" x="327.66" y="151.13" smashed="yes">
<attribute name="NAME" x="325.12" y="152.4" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="148.59" size="1.27" layer="96"/>
</instance>
<instance part="U$34" gate="G$1" x="323.85" y="153.67" smashed="yes" rot="R180">
<attribute name="VALUE" x="325.755" y="156.845" size="0.8128" layer="96" rot="R180"/>
</instance>
<instance part="R22" gate="R" x="365.76" y="152.4" rot="R90"/>
<instance part="U$35" gate="G$1" x="0" y="0"/>
<instance part="J6" gate="G$1" x="229.87" y="143.51"/>
<instance part="U$36" gate="G$1" x="238.76" y="166.37" smashed="yes" rot="R180"/>
<instance part="U$37" gate="G$1" x="238.76" y="123.19" smashed="yes"/>
<instance part="U$41" gate="G$1" x="209.55" y="140.97" smashed="yes" rot="R270"/>
<instance part="U7" gate="G$1" x="464.82" y="60.96"/>
<instance part="C28" gate="C" x="483.87" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="488.95" y="73.66" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="482.6" y="73.66" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R10" gate="R" x="492.76" y="66.04" rot="R90"/>
<instance part="R11" gate="R" x="496.57" y="66.04" rot="R90"/>
<instance part="U$38" gate="G$1" x="450.85" y="53.34" smashed="yes">
<attribute name="VALUE" x="448.945" y="50.165" size="0.8128" layer="96"/>
</instance>
<instance part="U8" gate="A" x="147.32" y="157.48"/>
<instance part="U8" gate="B" x="148.59" y="119.38" rot="MR0"/>
<instance part="U8" gate="C" x="147.32" y="138.43"/>
<instance part="U8" gate="D" x="148.59" y="92.71" rot="MR0"/>
<instance part="U$40" gate="G$1" x="147.32" y="128.27" smashed="yes"/>
<instance part="U$42" gate="G$1" x="147.32" y="147.32" smashed="yes"/>
<instance part="U$43" gate="G$1" x="106.68" y="60.96" smashed="yes"/>
<instance part="C29" gate="C" x="100.33" y="74.93" rot="R90"/>
<instance part="R14" gate="R" x="133.35" y="138.43" rot="R180"/>
<instance part="R15" gate="R" x="133.35" y="157.48" rot="R180"/>
<instance part="U$26" gate="G$1" x="461.01" y="104.14" rot="R270"/>
<instance part="L3" gate="L" x="92.71" y="46.99" smashed="yes">
<attribute name="NAME" x="87.63" y="44.45" size="1.27" layer="95"/>
<attribute name="VALUE" x="87.63" y="48.26" size="1.27" layer="96"/>
</instance>
<instance part="C2" gate="C" x="101.6" y="41.91" smashed="yes" rot="R90">
<attribute name="NAME" x="102.87" y="44.45" size="1.27" layer="95"/>
<attribute name="VALUE" x="102.87" y="41.91" size="1.27" layer="96"/>
</instance>
<instance part="U$45" gate="G$1" x="101.6" y="34.29" smashed="yes">
<attribute name="VALUE" x="99.695" y="31.115" size="0.8128" layer="96"/>
</instance>
<instance part="R6" gate="R" x="478.79" y="96.52" rot="R270"/>
<instance part="R5" gate="R" x="482.6" y="96.52" rot="R270"/>
<instance part="PWR" gate="LED" x="156.21" y="30.48"/>
<instance part="R7" gate="R" x="144.78" y="30.48"/>
<instance part="U$10" gate="G$1" x="162.56" y="26.67" smashed="yes">
<attribute name="VALUE" x="160.655" y="23.495" size="0.8128" layer="96"/>
</instance>
<instance part="RESET" gate="LED" x="153.67" y="38.1"/>
<instance part="R8" gate="R" x="142.24" y="38.1"/>
<instance part="R9" gate="R" x="402.59" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="403.86" y="111.76" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="403.86" y="110.49" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R24" gate="R" x="203.2" y="148.59" rot="R180"/>
<instance part="R25" gate="R" x="203.2" y="143.51" rot="R180"/>
<instance part="C32" gate="C" x="184.15" y="140.97" smashed="yes" rot="R270">
<attribute name="NAME" x="184.15" y="137.16" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="184.15" y="142.24" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U$46" gate="G$1" x="184.15" y="135.89" smashed="yes"/>
<instance part="R26" gate="R" x="203.2" y="138.43" rot="R180"/>
<instance part="U8" gate="PWR" x="106.68" y="74.93"/>
<instance part="U$48" gate="G$1" x="39.37" y="72.39" smashed="yes">
<attribute name="VALUE" x="37.465" y="69.215" size="0.8128" layer="96"/>
</instance>
<instance part="U$49" gate="G$1" x="87.63" y="110.49" smashed="yes" rot="R90">
<attribute name="VALUE" x="90.805" y="108.585" size="0.8128" layer="96" rot="R90"/>
</instance>
<instance part="V_OUT" gate="G$1" x="231.14" y="53.34"/>
<instance part="U$50" gate="G$1" x="218.44" y="50.8" smashed="yes" rot="R270">
<attribute name="VALUE" x="215.265" y="52.705" size="0.8128" layer="96" rot="R270"/>
</instance>
<instance part="CMR_RX" gate="J" x="198.12" y="90.17"/>
<instance part="CMR_TX" gate="J" x="232.41" y="90.17"/>
<instance part="IO" gate="J" x="219.71" y="90.17"/>
<instance part="R29" gate="R" x="142.24" y="45.72"/>
<instance part="SD" gate="LED" x="153.67" y="45.72"/>
<instance part="P1" gate="G$1" x="85.09" y="147.32" rot="MR0"/>
<instance part="P2" gate="G$1" x="88.9" y="154.94" rot="MR0"/>
<instance part="P3" gate="G$1" x="92.71" y="162.56" rot="MR0"/>
<instance part="Q2" gate="Q" x="148.59" y="77.47" rot="R270"/>
<instance part="R23" gate="R" x="143.51" y="83.82" rot="R180"/>
<instance part="R30" gate="R" x="143.51" y="80.01" rot="R180"/>
<instance part="Q1" gate="Q" x="148.59" y="104.14" rot="R270"/>
<instance part="R12" gate="R" x="143.51" y="110.49" rot="R180"/>
<instance part="R13" gate="R" x="143.51" y="106.68" rot="R180"/>
<instance part="R16" gate="R" x="142.24" y="53.34"/>
<instance part="CMR" gate="LED" x="153.67" y="53.34"/>
<instance part="P4" gate="G$1" x="386.08" y="148.59" rot="R180"/>
<instance part="P5" gate="G$1" x="377.19" y="156.21" rot="R180"/>
<instance part="P6" gate="G$1" x="139.7" y="72.39" rot="R90"/>
<instance part="RST" gate="K" x="345.44" y="22.86" rot="MR180"/>
<instance part="U$1" gate="G$1" x="335.28" y="21.59" smashed="yes" rot="R270">
<attribute name="VALUE" x="332.105" y="23.495" size="0.8128" layer="96" rot="R270"/>
</instance>
<instance part="X2" gate="G$1" x="408.94" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="106.426" size="1.27" layer="95"/>
<attribute name="VALUE" x="408.94" y="105.41" size="0.8128" layer="96"/>
</instance>
<instance part="1V2" gate="TP" x="110.49" y="25.4" rot="R90"/>
<instance part="3V3" gate="TP" x="59.69" y="49.53" rot="R90"/>
<instance part="1V8" gate="TP" x="55.88" y="25.4" rot="R90"/>
<instance part="P7" gate="G$1" x="170.18" y="60.96"/>
<instance part="P8" gate="G$1" x="172.72" y="60.96"/>
<instance part="P9" gate="G$1" x="175.26" y="60.96"/>
<instance part="P10" gate="G$1" x="177.8" y="60.96"/>
</instances>
<busses>
</busses>
<nets>
<net name="CS_D1" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y1"/>
<wire x1="485.14" y1="157.48" x2="480.06" y2="157.48" width="0.1524" layer="91"/>
<label x="480.06" y="157.48" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D1"/>
<wire x1="306.07" y1="90.17" x2="299.72" y2="90.17" width="0.1524" layer="91"/>
<label x="299.72" y="90.17" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D4" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y4"/>
<wire x1="485.14" y1="154.94" x2="480.06" y2="154.94" width="0.1524" layer="91"/>
<label x="480.06" y="154.94" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D4"/>
<wire x1="306.07" y1="77.47" x2="299.72" y2="77.47" width="0.1524" layer="91"/>
<label x="299.72" y="77.47" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D3" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y3"/>
<wire x1="485.14" y1="152.4" x2="480.06" y2="152.4" width="0.1524" layer="91"/>
<label x="480.06" y="152.4" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D3"/>
<wire x1="306.07" y1="80.01" x2="299.72" y2="80.01" width="0.1524" layer="91"/>
<label x="299.72" y="80.01" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D5" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y5"/>
<wire x1="485.14" y1="149.86" x2="480.06" y2="149.86" width="0.1524" layer="91"/>
<label x="480.06" y="149.86" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D5"/>
<wire x1="306.07" y1="69.85" x2="299.72" y2="69.85" width="0.1524" layer="91"/>
<label x="299.72" y="69.85" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D2" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y2"/>
<wire x1="485.14" y1="147.32" x2="480.06" y2="147.32" width="0.1524" layer="91"/>
<label x="480.06" y="147.32" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D2"/>
<wire x1="306.07" y1="87.63" x2="299.72" y2="87.63" width="0.1524" layer="91"/>
<label x="299.72" y="87.63" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D6" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y6"/>
<wire x1="485.14" y1="144.78" x2="480.06" y2="144.78" width="0.1524" layer="91"/>
<label x="480.06" y="144.78" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D6"/>
<wire x1="306.07" y1="67.31" x2="299.72" y2="67.31" width="0.1524" layer="91"/>
<label x="299.72" y="67.31" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_PCLK" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="PCLK"/>
<wire x1="485.14" y1="142.24" x2="480.06" y2="142.24" width="0.1524" layer="91"/>
<label x="480.06" y="142.24" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D0"/>
<wire x1="306.07" y1="95.25" x2="299.72" y2="95.25" width="0.1524" layer="91"/>
<label x="299.72" y="95.25" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D7" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y7"/>
<wire x1="485.14" y1="139.7" x2="480.06" y2="139.7" width="0.1524" layer="91"/>
<label x="480.06" y="139.7" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D7"/>
<wire x1="326.39" y1="49.53" x2="326.39" y2="39.37" width="0.1524" layer="91"/>
<label x="326.39" y="39.37" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
</net>
<net name="CS_D8" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y8"/>
<wire x1="485.14" y1="134.62" x2="480.06" y2="134.62" width="0.1524" layer="91"/>
<label x="480.06" y="134.62" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D8"/>
<wire x1="328.93" y1="49.53" x2="328.93" y2="39.37" width="0.1524" layer="91"/>
<label x="328.93" y="39.37" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
</net>
<net name="CS_CLK" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="XCLK1"/>
<wire x1="485.14" y1="132.08" x2="480.06" y2="132.08" width="0.1524" layer="91"/>
<label x="480.06" y="132.08" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_CLK"/>
<wire x1="306.07" y1="100.33" x2="299.72" y2="100.33" width="0.1524" layer="91"/>
<label x="299.72" y="100.33" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_D9" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="Y9"/>
<wire x1="485.14" y1="129.54" x2="480.06" y2="129.54" width="0.1524" layer="91"/>
<label x="480.06" y="129.54" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_D9"/>
<wire x1="331.47" y1="49.53" x2="331.47" y2="39.37" width="0.1524" layer="91"/>
<label x="331.47" y="39.37" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
</net>
<net name="1V8" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="DVDD"/>
<pinref part="C23" gate="C" pin="1"/>
<wire x1="471.17" y1="124.46" x2="485.14" y2="124.46" width="0.1524" layer="91"/>
<label x="477.52" y="124.46" size="1.27" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="C11" gate="C" pin="2"/>
<wire x1="55.88" y1="22.86" x2="52.07" y2="22.86" width="0.1524" layer="91"/>
<wire x1="52.07" y1="22.86" x2="46.99" y2="22.86" width="0.1524" layer="91"/>
<wire x1="52.07" y1="21.59" x2="52.07" y2="22.86" width="0.1524" layer="91"/>
<junction x="52.07" y="22.86"/>
<pinref part="C10" gate="C" pin="2"/>
<wire x1="55.88" y1="21.59" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<junction x="55.88" y="22.86"/>
<wire x1="55.88" y1="22.86" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VOUT"/>
<label x="58.42" y="22.86" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="1V8" gate="TP" pin="T"/>
</segment>
</net>
<net name="CS_HSYNC" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="HREF"/>
<wire x1="485.14" y1="121.92" x2="480.06" y2="121.92" width="0.1524" layer="91"/>
<label x="480.06" y="121.92" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_HSYNC"/>
<wire x1="306.07" y1="115.57" x2="299.72" y2="115.57" width="0.1524" layer="91"/>
<label x="299.72" y="115.57" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_VSYNC" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="VSYNC"/>
<wire x1="485.14" y1="116.84" x2="480.06" y2="116.84" width="0.1524" layer="91"/>
<label x="480.06" y="116.84" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CS_VSYNC"/>
<wire x1="306.07" y1="110.49" x2="299.72" y2="110.49" width="0.1524" layer="91"/>
<label x="299.72" y="110.49" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CS_SCK"/>
<wire x1="306.07" y1="120.65" x2="299.72" y2="120.65" width="0.1524" layer="91"/>
<label x="299.72" y="120.65" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="474.98" y1="59.69" x2="492.76" y2="59.69" width="0.1524" layer="91"/>
<pinref part="R10" gate="R" pin="1"/>
<wire x1="492.76" y1="59.69" x2="500.38" y2="59.69" width="0.1524" layer="91"/>
<wire x1="492.76" y1="62.23" x2="492.76" y2="59.69" width="0.1524" layer="91"/>
<junction x="492.76" y="59.69"/>
<label x="500.38" y="59.69" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="R" pin="1"/>
<pinref part="J2" gate="G$1" pin="SIO_C"/>
<wire x1="485.14" y1="111.76" x2="482.6" y2="111.76" width="0.1524" layer="91"/>
<wire x1="482.6" y1="111.76" x2="476.25" y2="111.76" width="0.1524" layer="91"/>
<wire x1="482.6" y1="100.33" x2="482.6" y2="111.76" width="0.1524" layer="91"/>
<junction x="482.6" y="111.76"/>
<label x="476.25" y="111.76" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CS_SDA"/>
<wire x1="299.72" y1="125.73" x2="306.07" y2="125.73" width="0.1524" layer="91"/>
<label x="299.72" y="125.73" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="474.98" y1="57.15" x2="496.57" y2="57.15" width="0.1524" layer="91"/>
<pinref part="R11" gate="R" pin="1"/>
<wire x1="496.57" y1="57.15" x2="500.38" y2="57.15" width="0.1524" layer="91"/>
<wire x1="496.57" y1="62.23" x2="496.57" y2="57.15" width="0.1524" layer="91"/>
<junction x="496.57" y="57.15"/>
<label x="500.38" y="57.15" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="R" pin="1"/>
<pinref part="J2" gate="G$1" pin="SIO_D"/>
<wire x1="485.14" y1="106.68" x2="478.79" y2="106.68" width="0.1524" layer="91"/>
<wire x1="478.79" y1="106.68" x2="476.25" y2="106.68" width="0.1524" layer="91"/>
<wire x1="478.79" y1="100.33" x2="478.79" y2="106.68" width="0.1524" layer="91"/>
<junction x="478.79" y="106.68"/>
<label x="476.25" y="106.68" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="L2" gate="L" pin="2"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="72.39" y1="36.83" x2="74.93" y2="36.83" width="0.1524" layer="91"/>
<wire x1="74.93" y1="36.83" x2="74.93" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$20" gate="G$1" pin="AGND"/>
<wire x1="74.93" y1="34.29" x2="74.93" y2="36.83" width="0.1524" layer="91"/>
<junction x="74.93" y="36.83"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSSA_DAC"/>
<wire x1="397.51" y1="77.47" x2="403.86" y2="77.47" width="0.1524" layer="91"/>
<wire x1="403.86" y1="77.47" x2="403.86" y2="79.375" width="0.1524" layer="91"/>
<wire x1="403.86" y1="80.645" x2="403.86" y2="82.55" width="0.1524" layer="91"/>
<wire x1="403.86" y1="82.55" x2="403.86" y2="84.455" width="0.1524" layer="91"/>
<wire x1="403.86" y1="85.725" x2="403.86" y2="95.25" width="0.1524" layer="91"/>
<wire x1="403.86" y1="95.25" x2="408.94" y2="95.25" width="0.1524" layer="91"/>
<pinref part="C22" gate="C" pin="1"/>
<wire x1="408.94" y1="95.25" x2="412.75" y2="95.25" width="0.1524" layer="91"/>
<wire x1="412.75" y1="95.25" x2="417.83" y2="95.25" width="0.1524" layer="91"/>
<wire x1="408.94" y1="93.98" x2="408.94" y2="95.25" width="0.1524" layer="91"/>
<junction x="408.94" y="95.25"/>
<pinref part="C21" gate="C" pin="1"/>
<wire x1="412.75" y1="93.98" x2="412.75" y2="95.25" width="0.1524" layer="91"/>
<junction x="412.75" y="95.25"/>
<pinref part="U1" gate="G$1" pin="VSSA_PLL"/>
<wire x1="397.51" y1="82.55" x2="403.86" y2="82.55" width="0.1524" layer="91"/>
<junction x="403.86" y="82.55"/>
<wire x1="403.86" y1="85.725" x2="403.86" y2="84.455" width="0.1524" layer="91" curve="-180"/>
<wire x1="403.86" y1="80.645" x2="403.86" y2="79.375" width="0.1524" layer="91" curve="-180"/>
<pinref part="U$31" gate="G$1" pin="AGND"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C31" gate="C" pin="1"/>
<pinref part="L2" gate="L" pin="1"/>
<wire x1="19.05" y1="38.1" x2="19.05" y2="36.83" width="0.1524" layer="91"/>
<wire x1="19.05" y1="36.83" x2="22.86" y2="36.83" width="0.1524" layer="91"/>
<pinref part="C1" gate="C" pin="1"/>
<wire x1="22.86" y1="36.83" x2="36.83" y2="36.83" width="0.1524" layer="91"/>
<wire x1="36.83" y1="36.83" x2="50.8" y2="36.83" width="0.1524" layer="91"/>
<wire x1="50.8" y1="36.83" x2="54.61" y2="36.83" width="0.1524" layer="91"/>
<wire x1="54.61" y1="36.83" x2="62.23" y2="36.83" width="0.1524" layer="91"/>
<wire x1="22.86" y1="38.1" x2="22.86" y2="36.83" width="0.1524" layer="91"/>
<junction x="22.86" y="36.83"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="36.83" y1="39.37" x2="36.83" y2="36.83" width="0.1524" layer="91"/>
<junction x="36.83" y="36.83"/>
<pinref part="C4" gate="C" pin="1"/>
<wire x1="50.8" y1="38.1" x2="50.8" y2="36.83" width="0.1524" layer="91"/>
<junction x="50.8" y="36.83"/>
<pinref part="C3" gate="C" pin="1"/>
<wire x1="54.61" y1="38.1" x2="54.61" y2="36.83" width="0.1524" layer="91"/>
<junction x="54.61" y="36.83"/>
<pinref part="U$21" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="34.29" x2="50.8" y2="36.83" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="DGND"/>
<pinref part="U$17" gate="G$1" pin="GND"/>
<wire x1="485.14" y1="137.16" x2="464.82" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C25" gate="C" pin="2"/>
<wire x1="464.82" y1="137.16" x2="461.01" y2="137.16" width="0.1524" layer="91"/>
<wire x1="464.82" y1="135.89" x2="464.82" y2="137.16" width="0.1524" layer="91"/>
<junction x="464.82" y="137.16"/>
</segment>
<segment>
<pinref part="C23" gate="C" pin="2"/>
<wire x1="462.28" y1="124.46" x2="463.55" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="PWDN"/>
<wire x1="485.14" y1="119.38" x2="462.28" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="GND"/>
<wire x1="462.28" y1="119.38" x2="461.01" y2="119.38" width="0.1524" layer="91"/>
<wire x1="462.28" y1="124.46" x2="462.28" y2="119.38" width="0.1524" layer="91"/>
<junction x="462.28" y="119.38"/>
</segment>
<segment>
<wire x1="52.07" y1="12.7" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C11" gate="C" pin="1"/>
<wire x1="52.07" y1="13.97" x2="52.07" y2="12.7" width="0.1524" layer="91"/>
<junction x="52.07" y="12.7"/>
<pinref part="C10" gate="C" pin="1"/>
<wire x1="55.88" y1="13.97" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="GND"/>
<wire x1="52.07" y1="10.16" x2="52.07" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS"/>
<wire x1="34.29" y1="12.7" x2="34.29" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C20" gate="C" pin="1"/>
<wire x1="19.05" y1="12.7" x2="34.29" y2="12.7" width="0.1524" layer="91"/>
<wire x1="19.05" y1="13.97" x2="19.05" y2="12.7" width="0.1524" layer="91"/>
<junction x="34.29" y="12.7"/>
<wire x1="52.07" y1="12.7" x2="34.29" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="106.68" y1="12.7" x2="110.49" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C15" gate="C" pin="1"/>
<wire x1="106.68" y1="13.97" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<junction x="106.68" y="12.7"/>
<pinref part="C14" gate="C" pin="1"/>
<wire x1="110.49" y1="13.97" x2="110.49" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$24" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="10.16" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS"/>
<wire x1="88.9" y1="12.7" x2="88.9" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C12" gate="C" pin="1"/>
<wire x1="73.66" y1="12.7" x2="88.9" y2="12.7" width="0.1524" layer="91"/>
<wire x1="73.66" y1="13.97" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<junction x="88.9" y="12.7"/>
<wire x1="106.68" y1="12.7" x2="88.9" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="C" pin="1"/>
<pinref part="U$23" gate="G$1" pin="GND"/>
<wire x1="356.87" y1="33.02" x2="356.87" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS"/>
<wire x1="372.11" y1="49.53" x2="372.11" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="GND"/>
<pinref part="C16" gate="C" pin="1"/>
<wire x1="372.11" y1="40.64" x2="372.11" y2="34.29" width="0.1524" layer="91"/>
<wire x1="370.84" y1="40.64" x2="372.11" y2="40.64" width="0.1524" layer="91"/>
<junction x="372.11" y="40.64"/>
<pinref part="C17" gate="C" pin="1"/>
<wire x1="373.38" y1="40.64" x2="372.11" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSSA"/>
<wire x1="377.19" y1="49.53" x2="377.19" y2="45.72" width="0.1524" layer="91"/>
<wire x1="377.19" y1="45.72" x2="386.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="386.08" y1="45.72" x2="386.08" y2="36.83" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="GND"/>
<pinref part="C18" gate="C" pin="1"/>
<wire x1="386.08" y1="36.83" x2="386.08" y2="34.29" width="0.1524" layer="91"/>
<wire x1="389.89" y1="38.1" x2="389.89" y2="36.83" width="0.1524" layer="91"/>
<wire x1="389.89" y1="36.83" x2="386.08" y2="36.83" width="0.1524" layer="91"/>
<junction x="386.08" y="36.83"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="GND"/>
<wire x1="400.05" y1="58.42" x2="400.05" y2="59.69" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSSA@1"/>
<wire x1="400.05" y1="59.69" x2="400.05" y2="67.31" width="0.1524" layer="91"/>
<wire x1="397.51" y1="67.31" x2="400.05" y2="67.31" width="0.1524" layer="91"/>
<pinref part="R3" gate="R" pin="1"/>
<wire x1="403.86" y1="60.96" x2="403.86" y2="59.69" width="0.1524" layer="91"/>
<wire x1="403.86" y1="59.69" x2="400.05" y2="59.69" width="0.1524" layer="91"/>
<junction x="400.05" y="59.69"/>
</segment>
<segment>
<pinref part="R4" gate="R" pin="1"/>
<pinref part="U$30" gate="G$1" pin="GND"/>
<wire x1="417.83" y1="53.34" x2="417.83" y2="59.69" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="TEST"/>
<wire x1="397.51" y1="125.73" x2="417.83" y2="125.73" width="0.1524" layer="91"/>
<wire x1="417.83" y1="125.73" x2="417.83" y2="120.65" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS@1"/>
<wire x1="417.83" y1="120.65" x2="417.83" y2="118.11" width="0.1524" layer="91"/>
<wire x1="417.83" y1="118.11" x2="397.51" y2="118.11" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="412.75" y1="120.65" x2="417.83" y2="120.65" width="0.1524" layer="91"/>
<junction x="417.83" y="120.65"/>
<pinref part="U$33" gate="G$1" pin="GND"/>
<wire x1="417.83" y1="125.73" x2="419.1" y2="125.73" width="0.1524" layer="91"/>
<junction x="417.83" y="125.73"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS@2"/>
<wire x1="323.85" y1="140.97" x2="323.85" y2="151.13" width="0.1524" layer="91"/>
<pinref part="C26" gate="C" pin="2"/>
<wire x1="323.85" y1="151.13" x2="323.85" y2="153.67" width="0.1524" layer="91"/>
<junction x="323.85" y="151.13"/>
<pinref part="C27" gate="C" pin="1"/>
<pinref part="U$34" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="G1"/>
<wire x1="237.49" y1="127" x2="237.49" y2="124.46" width="0.1524" layer="91"/>
<wire x1="237.49" y1="124.46" x2="238.76" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="G2"/>
<wire x1="238.76" y1="124.46" x2="240.03" y2="124.46" width="0.1524" layer="91"/>
<wire x1="240.03" y1="124.46" x2="240.03" y2="127" width="0.1524" layer="91"/>
<pinref part="U$37" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="124.46" x2="238.76" y2="123.19" width="0.1524" layer="91"/>
<junction x="238.76" y="124.46"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="G4"/>
<wire x1="237.49" y1="162.56" x2="237.49" y2="165.1" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="G3"/>
<wire x1="237.49" y1="165.1" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<wire x1="238.76" y1="165.1" x2="240.03" y2="165.1" width="0.1524" layer="91"/>
<wire x1="240.03" y1="165.1" x2="240.03" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U$36" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="166.37" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<junction x="238.76" y="165.1"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="VSS"/>
<wire x1="212.09" y1="140.97" x2="209.55" y2="140.97" width="0.1524" layer="91"/>
<pinref part="U$41" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="WP"/>
<wire x1="474.98" y1="62.23" x2="478.79" y2="62.23" width="0.1524" layer="91"/>
<wire x1="478.79" y1="62.23" x2="478.79" y2="71.12" width="0.1524" layer="91"/>
<wire x1="478.79" y1="71.12" x2="450.85" y2="71.12" width="0.1524" layer="91"/>
<wire x1="450.85" y1="71.12" x2="450.85" y2="64.77" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VSS"/>
<wire x1="450.85" y1="64.77" x2="450.85" y2="62.23" width="0.1524" layer="91"/>
<wire x1="450.85" y1="62.23" x2="450.85" y2="59.69" width="0.1524" layer="91"/>
<wire x1="450.85" y1="59.69" x2="450.85" y2="57.15" width="0.1524" layer="91"/>
<wire x1="450.85" y1="57.15" x2="454.66" y2="57.15" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="A2"/>
<wire x1="454.66" y1="59.69" x2="450.85" y2="59.69" width="0.1524" layer="91"/>
<junction x="450.85" y="59.69"/>
<pinref part="U7" gate="G$1" pin="A1"/>
<wire x1="454.66" y1="62.23" x2="450.85" y2="62.23" width="0.1524" layer="91"/>
<junction x="450.85" y="62.23"/>
<pinref part="U7" gate="G$1" pin="A0"/>
<wire x1="454.66" y1="64.77" x2="450.85" y2="64.77" width="0.1524" layer="91"/>
<junction x="450.85" y="64.77"/>
<pinref part="C28" gate="C" pin="2"/>
<wire x1="480.06" y1="71.12" x2="478.79" y2="71.12" width="0.1524" layer="91"/>
<junction x="478.79" y="71.12"/>
<pinref part="U$38" gate="G$1" pin="GND"/>
<wire x1="450.85" y1="53.34" x2="450.85" y2="57.15" width="0.1524" layer="91"/>
<junction x="450.85" y="57.15"/>
</segment>
<segment>
<pinref part="U8" gate="C" pin="!OE"/>
<pinref part="U$40" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="128.27" x2="147.32" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="!OE"/>
<pinref part="U$42" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="147.32" x2="147.32" y2="148.59" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$43" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="62.23" width="0.1524" layer="91"/>
<pinref part="C29" gate="C" pin="1"/>
<wire x1="106.68" y1="62.23" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="100.33" y1="71.12" x2="100.33" y2="62.23" width="0.1524" layer="91"/>
<wire x1="100.33" y1="62.23" x2="106.68" y2="62.23" width="0.1524" layer="91"/>
<junction x="106.68" y="62.23"/>
<pinref part="U8" gate="PWR" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="AGND"/>
<wire x1="485.14" y1="104.14" x2="462.28" y2="104.14" width="0.1524" layer="91"/>
<label x="464.82" y="104.14" size="1.27" layer="95" ratio="10"/>
<pinref part="C24" gate="C" pin="2"/>
<wire x1="462.28" y1="104.14" x2="461.01" y2="104.14" width="0.1524" layer="91"/>
<wire x1="463.55" y1="109.22" x2="462.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="462.28" y1="109.22" x2="462.28" y2="104.14" width="0.1524" layer="91"/>
<junction x="462.28" y="104.14"/>
<pinref part="U$26" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U$45" gate="G$1" pin="GND"/>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="101.6" y1="34.29" x2="101.6" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWR" gate="LED" pin="-"/>
<wire x1="160.02" y1="30.48" x2="162.56" y2="30.48" width="0.1524" layer="91"/>
<wire x1="162.56" y1="30.48" x2="162.56" y2="26.67" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="C" pin="2"/>
<pinref part="U$46" gate="G$1" pin="GND"/>
<wire x1="184.15" y1="137.16" x2="184.15" y2="135.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="GND@2"/>
<pinref part="U$48" gate="G$1" pin="GND"/>
<wire x1="39.37" y1="72.39" x2="39.37" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="GND@1"/>
<wire x1="39.37" y1="73.66" x2="39.37" y2="78.74" width="0.1524" layer="91"/>
<wire x1="41.91" y1="78.74" x2="41.91" y2="73.66" width="0.1524" layer="91"/>
<wire x1="41.91" y1="73.66" x2="39.37" y2="73.66" width="0.1524" layer="91"/>
<junction x="39.37" y="73.66"/>
<label x="39.37" y="68.58" size="1.27" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="GND"/>
<pinref part="U$49" gate="G$1" pin="GND"/>
<wire x1="87.63" y1="110.49" x2="86.36" y2="110.49" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V_OUT" gate="G$1" pin="2"/>
<wire x1="223.52" y1="50.8" x2="218.44" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$50" gate="G$1" pin="GND"/>
<label x="213.36" y="51.435" size="1.27" layer="95" ratio="10" rot="R180"/>
</segment>
<segment>
<pinref part="RST" gate="K" pin="S1"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="339.09" y1="21.59" x2="335.28" y2="21.59" width="0.1524" layer="91"/>
<pinref part="RST" gate="K" pin="S2"/>
<wire x1="351.79" y1="21.59" x2="339.09" y2="21.59" width="0.1524" layer="91"/>
<junction x="339.09" y="21.59"/>
<pinref part="RST" gate="K" pin="1"/>
<wire x1="339.09" y1="24.13" x2="339.09" y2="21.59" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$32" gate="G$1" pin="GND"/>
<pinref part="X2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="C1" gate="C" pin="2"/>
<pinref part="U6" gate="G$1" pin="IN"/>
<wire x1="22.86" y1="45.72" x2="22.86" y2="46.99" width="0.1524" layer="91"/>
<wire x1="22.86" y1="46.99" x2="25.4" y2="46.99" width="0.1524" layer="91"/>
<pinref part="C31" gate="C" pin="2"/>
<wire x1="19.05" y1="45.72" x2="19.05" y2="46.99" width="0.1524" layer="91"/>
<wire x1="19.05" y1="46.99" x2="22.86" y2="46.99" width="0.1524" layer="91"/>
<junction x="22.86" y="46.99"/>
<label x="19.05" y="46.99" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="5V"/>
<wire x1="36.83" y1="78.74" x2="36.83" y2="69.85" width="0.1524" layer="91"/>
<label x="36.83" y="69.85" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="L1" gate="L" pin="1"/>
<pinref part="U6" gate="G$1" pin="OUT"/>
<wire x1="62.23" y1="46.99" x2="59.69" y2="46.99" width="0.1524" layer="91"/>
<pinref part="C4" gate="C" pin="2"/>
<wire x1="59.69" y1="46.99" x2="54.61" y2="46.99" width="0.1524" layer="91"/>
<wire x1="54.61" y1="46.99" x2="50.8" y2="46.99" width="0.1524" layer="91"/>
<wire x1="50.8" y1="46.99" x2="48.26" y2="46.99" width="0.1524" layer="91"/>
<wire x1="50.8" y1="45.72" x2="50.8" y2="46.99" width="0.1524" layer="91"/>
<junction x="50.8" y="46.99"/>
<pinref part="C3" gate="C" pin="2"/>
<wire x1="54.61" y1="45.72" x2="54.61" y2="46.99" width="0.1524" layer="91"/>
<junction x="54.61" y="46.99"/>
<wire x1="54.61" y1="46.99" x2="54.61" y2="52.07" width="0.1524" layer="91"/>
<label x="54.61" y="52.07" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
<pinref part="3V3" gate="TP" pin="T"/>
<junction x="59.69" y="46.99"/>
</segment>
<segment>
<pinref part="C20" gate="C" pin="2"/>
<wire x1="19.05" y1="21.59" x2="19.05" y2="22.86" width="0.1524" layer="91"/>
<wire x1="19.05" y1="22.86" x2="21.59" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VIN"/>
<label x="19.05" y="22.86" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C12" gate="C" pin="2"/>
<wire x1="73.66" y1="21.59" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="22.86" x2="76.2" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<label x="71.12" y="24.13" size="1.27" layer="95" ratio="10"/>
<label x="73.66" y="22.86" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C25" gate="C" pin="1"/>
<pinref part="J2" gate="G$1" pin="DOVDD"/>
<wire x1="464.82" y1="128.27" x2="464.82" y2="127" width="0.1524" layer="91"/>
<wire x1="464.82" y1="127" x2="485.14" y2="127" width="0.1524" layer="91"/>
<label x="477.52" y="127" size="1.27" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="R1" gate="R" pin="1"/>
<wire x1="336.55" y1="35.56" x2="336.55" y2="33.02" width="0.1524" layer="91"/>
<label x="336.55" y="33.02" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="R" pin="1"/>
<wire x1="346.71" y1="35.56" x2="346.71" y2="33.02" width="0.1524" layer="91"/>
<label x="346.71" y="33.02" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C16" gate="C" pin="2"/>
<wire x1="363.22" y1="40.64" x2="361.95" y2="40.64" width="0.1524" layer="91"/>
<wire x1="361.95" y1="40.64" x2="361.95" y2="44.45" width="0.1524" layer="91"/>
<wire x1="361.95" y1="44.45" x2="369.57" y2="44.45" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD_IO"/>
<wire x1="369.57" y1="44.45" x2="369.57" y2="49.53" width="0.1524" layer="91"/>
<wire x1="361.95" y1="40.64" x2="361.95" y2="35.56" width="0.1524" layer="91"/>
<junction x="361.95" y="40.64"/>
<label x="361.95" y="35.56" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C19" gate="C" pin="1"/>
<wire x1="407.67" y1="60.96" x2="407.67" y2="59.69" width="0.1524" layer="91"/>
<wire x1="407.67" y1="59.69" x2="411.48" y2="59.69" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="1"/>
<wire x1="411.48" y1="59.69" x2="411.48" y2="60.96" width="0.1524" layer="91"/>
<wire x1="407.67" y1="59.69" x2="407.67" y2="57.15" width="0.1524" layer="91"/>
<junction x="407.67" y="59.69"/>
<label x="407.67" y="57.15" size="1.27" layer="95" ratio="10"/>
</segment>
<segment>
<wire x1="389.89" y1="48.26" x2="389.89" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C18" gate="C" pin="2"/>
<pinref part="U1" gate="G$1" pin="VDDA"/>
<wire x1="382.27" y1="49.53" x2="382.27" y2="48.26" width="0.1524" layer="91"/>
<wire x1="382.27" y1="48.26" x2="389.89" y2="48.26" width="0.1524" layer="91"/>
<wire x1="392.43" y1="48.26" x2="389.89" y2="48.26" width="0.1524" layer="91"/>
<junction x="389.89" y="48.26"/>
<label x="392.43" y="48.26" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD_IO@1"/>
<wire x1="397.51" y1="120.65" x2="405.13" y2="120.65" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="1"/>
<label x="398.78" y="120.65" size="1.27" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD_IO@2"/>
<wire x1="321.31" y1="140.97" x2="321.31" y2="147.32" width="0.1524" layer="91"/>
<wire x1="321.31" y1="147.32" x2="314.96" y2="147.32" width="0.1524" layer="91"/>
<wire x1="314.96" y1="147.32" x2="314.96" y2="151.13" width="0.1524" layer="91"/>
<pinref part="C26" gate="C" pin="1"/>
<wire x1="314.96" y1="151.13" x2="316.23" y2="151.13" width="0.1524" layer="91"/>
<wire x1="314.96" y1="147.32" x2="311.15" y2="147.32" width="0.1524" layer="91"/>
<junction x="314.96" y="147.32"/>
<label x="311.15" y="147.32" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R22" gate="R" pin="2"/>
<wire x1="365.76" y1="156.21" x2="365.76" y2="160.02" width="0.1524" layer="91"/>
<label x="365.76" y="160.02" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="L3" gate="L" pin="1"/>
<wire x1="87.63" y1="46.99" x2="85.09" y2="46.99" width="0.1524" layer="91"/>
<label x="85.09" y="46.99" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="R" pin="2"/>
<wire x1="478.79" y1="92.71" x2="478.79" y2="88.9" width="0.1524" layer="91"/>
<label x="478.79" y="88.9" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="R" pin="2"/>
<wire x1="482.6" y1="92.71" x2="482.6" y2="88.9" width="0.1524" layer="91"/>
<label x="482.6" y="88.9" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="R" pin="1"/>
<wire x1="140.97" y1="30.48" x2="137.16" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R8" gate="R" pin="1"/>
<wire x1="137.16" y1="30.48" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="138.43" y1="38.1" x2="137.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="137.16" y1="38.1" x2="137.16" y2="30.48" width="0.1524" layer="91"/>
<junction x="137.16" y="30.48"/>
<pinref part="R29" gate="R" pin="1"/>
<wire x1="137.16" y1="38.1" x2="137.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="137.16" y1="45.72" x2="138.43" y2="45.72" width="0.1524" layer="91"/>
<junction x="137.16" y="38.1"/>
<label x="134.62" y="30.48" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="R16" gate="R" pin="1"/>
<wire x1="137.16" y1="45.72" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="137.16" y1="53.34" x2="138.43" y2="53.34" width="0.1524" layer="91"/>
<junction x="137.16" y="45.72"/>
</segment>
<segment>
<wire x1="496.57" y1="71.12" x2="500.38" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R11" gate="R" pin="2"/>
<wire x1="496.57" y1="69.85" x2="496.57" y2="71.12" width="0.1524" layer="91"/>
<junction x="496.57" y="71.12"/>
<wire x1="492.76" y1="71.12" x2="496.57" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R10" gate="R" pin="2"/>
<wire x1="492.76" y1="69.85" x2="492.76" y2="71.12" width="0.1524" layer="91"/>
<junction x="492.76" y="71.12"/>
<pinref part="C28" gate="C" pin="1"/>
<wire x1="488.95" y1="71.12" x2="487.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="488.95" y1="71.12" x2="492.76" y2="71.12" width="0.1524" layer="91"/>
<junction x="488.95" y="71.12"/>
<wire x1="488.95" y1="64.77" x2="488.95" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VCC"/>
<wire x1="474.98" y1="64.77" x2="488.95" y2="64.77" width="0.1524" layer="91"/>
<label x="500.38" y="71.12" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<wire x1="106.68" y1="86.36" x2="106.68" y2="87.63" width="0.1524" layer="91"/>
<pinref part="C29" gate="C" pin="2"/>
<wire x1="106.68" y1="87.63" x2="106.68" y2="90.17" width="0.1524" layer="91"/>
<wire x1="100.33" y1="78.74" x2="100.33" y2="87.63" width="0.1524" layer="91"/>
<wire x1="100.33" y1="87.63" x2="106.68" y2="87.63" width="0.1524" layer="91"/>
<junction x="106.68" y="87.63"/>
<pinref part="U8" gate="PWR" pin="VCC"/>
<label x="106.68" y="90.17" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C32" gate="C" pin="1"/>
<wire x1="184.15" y1="144.78" x2="184.15" y2="146.05" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="VDD"/>
<wire x1="184.15" y1="146.05" x2="212.09" y2="146.05" width="0.1524" layer="91"/>
<label x="181.61" y="146.05" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<wire x1="181.61" y1="146.05" x2="184.15" y2="146.05" width="0.1524" layer="91"/>
<junction x="184.15" y="146.05"/>
</segment>
<segment>
<pinref part="R23" gate="R" pin="2"/>
<wire x1="139.7" y1="83.82" x2="133.35" y2="83.82" width="0.1524" layer="91"/>
<label x="133.35" y="83.82" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="R30" gate="R" pin="2"/>
<wire x1="139.7" y1="80.01" x2="139.7" y2="83.82" width="0.1524" layer="91"/>
<junction x="139.7" y="83.82"/>
</segment>
<segment>
<pinref part="R12" gate="R" pin="2"/>
<wire x1="139.7" y1="110.49" x2="133.35" y2="110.49" width="0.1524" layer="91"/>
<label x="133.35" y="110.49" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="R13" gate="R" pin="2"/>
<wire x1="139.7" y1="106.68" x2="139.7" y2="110.49" width="0.1524" layer="91"/>
<junction x="139.7" y="110.49"/>
</segment>
</net>
<net name="A3V3" class="0">
<segment>
<pinref part="L1" gate="L" pin="2"/>
<pinref part="C5" gate="C" pin="2"/>
<wire x1="72.39" y1="46.99" x2="74.93" y2="46.99" width="0.1524" layer="91"/>
<wire x1="74.93" y1="46.99" x2="74.93" y2="45.72" width="0.1524" layer="91"/>
<wire x1="74.93" y1="46.99" x2="74.93" y2="52.07" width="0.1524" layer="91"/>
<junction x="74.93" y="46.99"/>
<label x="74.93" y="52.07" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDDA_PLL"/>
<wire x1="397.51" y1="85.09" x2="408.94" y2="85.09" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDA_DAC"/>
<wire x1="408.94" y1="85.09" x2="412.75" y2="85.09" width="0.1524" layer="91"/>
<wire x1="412.75" y1="85.09" x2="417.83" y2="85.09" width="0.1524" layer="91"/>
<wire x1="397.51" y1="80.01" x2="408.94" y2="80.01" width="0.1524" layer="91"/>
<wire x1="408.94" y1="80.01" x2="408.94" y2="85.09" width="0.1524" layer="91"/>
<junction x="408.94" y="85.09"/>
<pinref part="C22" gate="C" pin="2"/>
<wire x1="408.94" y1="86.36" x2="408.94" y2="85.09" width="0.1524" layer="91"/>
<pinref part="C21" gate="C" pin="2"/>
<wire x1="412.75" y1="86.36" x2="412.75" y2="85.09" width="0.1524" layer="91"/>
<junction x="412.75" y="85.09"/>
<label x="417.83" y="85.09" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="1V2" class="0">
<segment>
<pinref part="C15" gate="C" pin="2"/>
<wire x1="110.49" y1="22.86" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<wire x1="106.68" y1="22.86" x2="101.6" y2="22.86" width="0.1524" layer="91"/>
<wire x1="106.68" y1="21.59" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="106.68" y="22.86"/>
<pinref part="C14" gate="C" pin="2"/>
<wire x1="110.49" y1="21.59" x2="110.49" y2="22.86" width="0.1524" layer="91"/>
<junction x="110.49" y="22.86"/>
<wire x1="110.49" y1="22.86" x2="113.03" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VOUT"/>
<label x="113.03" y="22.86" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="1V2" gate="TP" pin="T"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="374.65" y1="49.53" x2="374.65" y2="43.18" width="0.1524" layer="91"/>
<wire x1="374.65" y1="43.18" x2="382.27" y2="43.18" width="0.1524" layer="91"/>
<wire x1="382.27" y1="43.18" x2="382.27" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C17" gate="C" pin="2"/>
<wire x1="382.27" y1="40.64" x2="381" y2="40.64" width="0.1524" layer="91"/>
<wire x1="382.27" y1="40.64" x2="382.27" y2="35.56" width="0.1524" layer="91"/>
<junction x="382.27" y="40.64"/>
<label x="382.27" y="35.56" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD@1"/>
<wire x1="326.39" y1="140.97" x2="326.39" y2="147.32" width="0.1524" layer="91"/>
<wire x1="326.39" y1="147.32" x2="332.74" y2="147.32" width="0.1524" layer="91"/>
<wire x1="332.74" y1="147.32" x2="332.74" y2="151.13" width="0.1524" layer="91"/>
<pinref part="C27" gate="C" pin="2"/>
<wire x1="332.74" y1="151.13" x2="331.47" y2="151.13" width="0.1524" layer="91"/>
<wire x1="332.74" y1="147.32" x2="336.55" y2="147.32" width="0.1524" layer="91"/>
<junction x="332.74" y="147.32"/>
<label x="336.55" y="147.32" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CS_RSTB" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CS_RSTB"/>
<wire x1="341.63" y1="49.53" x2="341.63" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R1" gate="R" pin="2"/>
<wire x1="341.63" y1="45.72" x2="341.63" y2="39.37" width="0.1524" layer="91"/>
<wire x1="336.55" y1="43.18" x2="336.55" y2="45.72" width="0.1524" layer="91"/>
<wire x1="336.55" y1="45.72" x2="341.63" y2="45.72" width="0.1524" layer="91"/>
<junction x="341.63" y="45.72"/>
<label x="341.63" y="39.37" size="1.27" layer="95" ratio="10" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="RESET"/>
<wire x1="485.14" y1="114.3" x2="480.06" y2="114.3" width="0.1524" layer="91"/>
<label x="480.06" y="114.3" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RSTN"/>
<wire x1="351.79" y1="49.53" x2="351.79" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R2" gate="R" pin="2"/>
<wire x1="346.71" y1="43.18" x2="346.71" y2="45.72" width="0.1524" layer="91"/>
<wire x1="346.71" y1="45.72" x2="351.79" y2="45.72" width="0.1524" layer="91"/>
<junction x="351.79" y="45.72"/>
<pinref part="C13" gate="C" pin="2"/>
<wire x1="351.79" y1="45.72" x2="356.87" y2="45.72" width="0.1524" layer="91"/>
<wire x1="356.87" y1="45.72" x2="356.87" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RST" gate="K" pin="2"/>
<wire x1="351.79" y1="45.72" x2="351.79" y2="24.13" width="0.1524" layer="91"/>
<label x="351.79" y="24.13" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="RESET" gate="LED" pin="-"/>
<wire x1="157.48" y1="38.1" x2="160.02" y2="38.1" width="0.1524" layer="91"/>
<label x="160.02" y="38.1" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="V_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DAC_CVBS"/>
<wire x1="397.51" y1="74.93" x2="417.83" y2="74.93" width="0.1524" layer="91"/>
<pinref part="R4" gate="R" pin="2"/>
<wire x1="417.83" y1="74.93" x2="422.91" y2="74.93" width="0.1524" layer="91"/>
<wire x1="417.83" y1="67.31" x2="417.83" y2="74.93" width="0.1524" layer="91"/>
<junction x="417.83" y="74.93"/>
<label x="422.91" y="74.93" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="V_OUT" gate="G$1" pin="1"/>
<wire x1="223.52" y1="55.88" x2="218.44" y2="55.88" width="0.1524" layer="91"/>
<label x="218.44" y="55.88" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DAC_COMP"/>
<wire x1="397.51" y1="72.39" x2="407.67" y2="72.39" width="0.1524" layer="91"/>
<pinref part="C19" gate="C" pin="2"/>
<wire x1="407.67" y1="72.39" x2="411.48" y2="72.39" width="0.1524" layer="91"/>
<wire x1="407.67" y1="68.58" x2="407.67" y2="72.39" width="0.1524" layer="91"/>
<junction x="407.67" y="72.39"/>
<pinref part="C8" gate="C" pin="2"/>
<wire x1="411.48" y1="72.39" x2="411.48" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CLK_OUT"/>
<wire x1="397.51" y1="115.57" x2="402.59" y2="115.57" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="2"/>
<pinref part="X2" gate="G$1" pin="3"/>
<wire x1="408.94" y1="114.3" x2="408.94" y2="115.57" width="0.1524" layer="91"/>
<wire x1="408.94" y1="115.57" x2="402.59" y2="115.57" width="0.1524" layer="91"/>
<junction x="402.59" y="115.57"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SPI_MOSI"/>
<wire x1="344.17" y1="140.97" x2="344.17" y2="148.59" width="0.1524" layer="91"/>
<label x="344.17" y="148.59" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R24" gate="R" pin="2"/>
<wire x1="199.39" y1="148.59" x2="195.58" y2="148.59" width="0.1524" layer="91"/>
<label x="195.58" y="148.59" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="Y"/>
<wire x1="156.21" y1="157.48" x2="158.75" y2="157.48" width="0.1524" layer="91"/>
<label x="158.75" y="157.48" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="SPI_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SPI_SCK"/>
<wire x1="346.71" y1="140.97" x2="346.71" y2="148.59" width="0.1524" layer="91"/>
<label x="346.71" y="148.59" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="C" pin="Y"/>
<wire x1="158.75" y1="138.43" x2="156.21" y2="138.43" width="0.1524" layer="91"/>
<label x="158.75" y="138.43" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="R25" gate="R" pin="2"/>
<wire x1="199.39" y1="143.51" x2="195.58" y2="143.51" width="0.1524" layer="91"/>
<label x="195.58" y="143.51" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DAC_REXT"/>
<pinref part="R3" gate="R" pin="2"/>
<wire x1="397.51" y1="69.85" x2="403.86" y2="69.85" width="0.1524" layer="91"/>
<wire x1="403.86" y1="69.85" x2="403.86" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CLK_IN"/>
<wire x1="397.51" y1="113.03" x2="400.05" y2="113.03" width="0.1524" layer="91"/>
<wire x1="400.05" y1="113.03" x2="400.05" y2="107.95" width="0.1524" layer="91"/>
<wire x1="400.05" y1="107.95" x2="402.59" y2="107.95" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="1"/>
<wire x1="402.59" y1="107.95" x2="408.94" y2="107.95" width="0.1524" layer="91"/>
<junction x="402.59" y="107.95"/>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="408.94" y1="107.95" x2="408.94" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="CS"/>
<wire x1="195.58" y1="151.13" x2="212.09" y2="151.13" width="0.1524" layer="91"/>
<label x="195.58" y="151.13" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Q1" gate="Q" pin="C"/>
<pinref part="R12" gate="R" pin="1"/>
<wire x1="147.32" y1="110.49" x2="148.59" y2="110.49" width="0.1524" layer="91"/>
<wire x1="148.59" y1="110.49" x2="153.67" y2="110.49" width="0.1524" layer="91"/>
<wire x1="153.67" y1="110.49" x2="153.67" y2="101.6" width="0.1524" layer="91"/>
<wire x1="153.67" y1="110.49" x2="158.75" y2="110.49" width="0.1524" layer="91"/>
<junction x="153.67" y="110.49"/>
<label x="158.75" y="110.49" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="U8" gate="B" pin="!OE"/>
<junction x="148.59" y="110.49"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U8" gate="D" pin="Y"/>
<wire x1="139.7" y1="92.71" x2="133.35" y2="92.71" width="0.1524" layer="91"/>
<label x="133.35" y="92.71" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="B" pin="Y"/>
<wire x1="139.7" y1="119.38" x2="133.35" y2="119.38" width="0.1524" layer="91"/>
<label x="133.35" y="119.38" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="P3" gate="G$1" pin="C"/>
<wire x1="95.25" y1="162.56" x2="101.6" y2="162.56" width="0.1524" layer="91"/>
<label x="101.6" y="162.56" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="R14" gate="R" pin="2"/>
<wire x1="129.54" y1="138.43" x2="125.73" y2="138.43" width="0.1524" layer="91"/>
<label x="125.73" y="138.43" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="C"/>
<wire x1="91.44" y1="154.94" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<label x="101.6" y="154.94" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="R15" gate="R" pin="2"/>
<wire x1="129.54" y1="157.48" x2="125.73" y2="157.48" width="0.1524" layer="91"/>
<label x="125.73" y="157.48" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="P1" gate="G$1" pin="C"/>
<wire x1="87.63" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<label x="101.6" y="147.32" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="CMR_CS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SPI_SS"/>
<wire x1="349.25" y1="140.97" x2="349.25" y2="148.59" width="0.1524" layer="91"/>
<label x="349.25" y="148.59" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="Q2" gate="Q" pin="C"/>
<pinref part="R23" gate="R" pin="1"/>
<wire x1="147.32" y1="83.82" x2="148.59" y2="83.82" width="0.1524" layer="91"/>
<wire x1="148.59" y1="83.82" x2="153.67" y2="83.82" width="0.1524" layer="91"/>
<wire x1="153.67" y1="83.82" x2="153.67" y2="74.93" width="0.1524" layer="91"/>
<wire x1="153.67" y1="83.82" x2="158.75" y2="83.82" width="0.1524" layer="91"/>
<junction x="153.67" y="83.82"/>
<label x="158.75" y="83.82" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="U8" gate="D" pin="!OE"/>
<junction x="148.59" y="83.82"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U8" gate="A" pin="A"/>
<wire x1="138.43" y1="157.48" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R15" gate="R" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U8" gate="C" pin="A"/>
<wire x1="138.43" y1="138.43" x2="137.16" y2="138.43" width="0.1524" layer="91"/>
<pinref part="R14" gate="R" pin="1"/>
</segment>
</net>
<net name="CS_A3V3" class="0">
<segment>
<pinref part="L3" gate="L" pin="2"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="97.79" y1="46.99" x2="101.6" y2="46.99" width="0.1524" layer="91"/>
<wire x1="101.6" y1="46.99" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<wire x1="101.6" y1="46.99" x2="104.14" y2="46.99" width="0.1524" layer="91"/>
<junction x="101.6" y="46.99"/>
<label x="104.14" y="46.99" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="AVDD"/>
<wire x1="485.14" y1="109.22" x2="471.17" y2="109.22" width="0.1524" layer="91"/>
<label x="472.44" y="109.22" size="1.27" layer="95" ratio="10"/>
<pinref part="C24" gate="C" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R7" gate="R" pin="2"/>
<pinref part="PWR" gate="LED" pin="+"/>
<wire x1="148.59" y1="30.48" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R8" gate="R" pin="2"/>
<pinref part="RESET" gate="LED" pin="+"/>
<wire x1="146.05" y1="38.1" x2="149.86" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="DI"/>
<wire x1="212.09" y1="148.59" x2="207.01" y2="148.59" width="0.1524" layer="91"/>
<pinref part="R24" gate="R" pin="1"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="SCLK"/>
<wire x1="212.09" y1="143.51" x2="207.01" y2="143.51" width="0.1524" layer="91"/>
<pinref part="R25" gate="R" pin="1"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<wire x1="62.23" y1="139.7" x2="62.23" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD2/D2/INT0"/>
<label x="62.23" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="93.98" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="3"/>
<wire x1="212.09" y1="93.98" x2="208.28" y2="93.98" width="0.1524" layer="91"/>
<junction x="212.09" y="93.98"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<wire x1="59.69" y1="139.7" x2="59.69" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD3/D3/INT1/PWM"/>
<label x="59.69" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="91.44" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="4"/>
<wire x1="212.09" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="212.09" y="91.44"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<wire x1="57.15" y1="139.7" x2="57.15" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD4/D4"/>
<label x="57.15" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="88.9" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="5"/>
<wire x1="212.09" y1="88.9" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<junction x="212.09" y="88.9"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<wire x1="54.61" y1="139.7" x2="54.61" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD5/D5/PWM"/>
<label x="54.61" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="86.36" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="6"/>
<wire x1="212.09" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<junction x="212.09" y="86.36"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<wire x1="52.07" y1="139.7" x2="52.07" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD6/D6/PWM"/>
<label x="52.07" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="83.82" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="7"/>
<wire x1="212.09" y1="83.82" x2="208.28" y2="83.82" width="0.1524" layer="91"/>
<junction x="212.09" y="83.82"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<wire x1="49.53" y1="139.7" x2="49.53" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PD7/D7"/>
<label x="49.53" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="208.28" y="81.28" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="IO" gate="J" pin="8"/>
<wire x1="212.09" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="212.09" y="81.28"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="PD0/D0/RX"/>
<wire x1="67.31" y1="139.7" x2="67.31" y2="143.51" width="0.1524" layer="91"/>
<label x="67.31" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IO" gate="J" pin="1"/>
<wire x1="212.09" y1="99.06" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<label x="208.28" y="99.06" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<junction x="212.09" y="99.06"/>
</segment>
</net>
<net name="D$2" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="DO"/>
<pinref part="R26" gate="R" pin="1"/>
<wire x1="212.09" y1="138.43" x2="207.01" y2="138.43" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R29" gate="R" pin="2"/>
<pinref part="SD" gate="LED" pin="+"/>
<wire x1="146.05" y1="45.72" x2="149.86" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="PD1/D1/TX"/>
<wire x1="64.77" y1="139.7" x2="64.77" y2="143.51" width="0.1524" layer="91"/>
<label x="64.77" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IO" gate="J" pin="2"/>
<wire x1="212.09" y1="96.52" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<label x="208.28" y="96.52" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<junction x="212.09" y="96.52"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q2" gate="Q" pin="B"/>
<pinref part="R30" gate="R" pin="1"/>
<wire x1="147.32" y1="80.01" x2="148.59" y2="80.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q1" gate="Q" pin="B"/>
<pinref part="R13" gate="R" pin="1"/>
<wire x1="147.32" y1="106.68" x2="148.59" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SD_MISO" class="0">
<segment>
<pinref part="R26" gate="R" pin="2"/>
<wire x1="199.39" y1="138.43" x2="195.58" y2="138.43" width="0.1524" layer="91"/>
<label x="195.58" y="138.43" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="B" pin="A"/>
<wire x1="157.48" y1="119.38" x2="158.75" y2="119.38" width="0.1524" layer="91"/>
<label x="158.75" y="119.38" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="CMR_MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SPI_MISO"/>
<wire x1="341.63" y1="140.97" x2="341.63" y2="148.59" width="0.1524" layer="91"/>
<label x="341.63" y="148.59" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="D" pin="A"/>
<wire x1="157.48" y1="92.71" x2="158.75" y2="92.71" width="0.1524" layer="91"/>
<label x="158.75" y="92.71" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R16" gate="R" pin="2"/>
<pinref part="CMR" gate="LED" pin="+"/>
<wire x1="146.05" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="UART_TXD"/>
<pinref part="P4" gate="G$1" pin="R"/>
<wire x1="372.11" y1="140.97" x2="372.11" y2="146.05" width="0.1524" layer="91"/>
<wire x1="372.11" y1="146.05" x2="372.11" y2="151.13" width="0.1524" layer="91"/>
<wire x1="372.11" y1="151.13" x2="388.62" y2="151.13" width="0.1524" layer="91"/>
<pinref part="R22" gate="R" pin="1"/>
<wire x1="365.76" y1="148.59" x2="365.76" y2="146.05" width="0.1524" layer="91"/>
<wire x1="365.76" y1="146.05" x2="368.935" y2="146.05" width="0.1524" layer="91"/>
<junction x="372.11" y="146.05"/>
<wire x1="370.205" y1="146.05" x2="372.11" y2="146.05" width="0.1524" layer="91"/>
<wire x1="370.205" y1="146.05" x2="368.935" y2="146.05" width="0.1524" layer="91" curve="-180"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="UART_RXD"/>
<pinref part="P5" gate="G$1" pin="R"/>
<wire x1="369.57" y1="140.97" x2="369.57" y2="158.75" width="0.1524" layer="91"/>
<wire x1="369.57" y1="158.75" x2="379.73" y2="158.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CMR_RX" class="0">
<segment>
<pinref part="P5" gate="G$1" pin="C"/>
<wire x1="379.73" y1="156.21" x2="393.7" y2="156.21" width="0.1524" layer="91"/>
<label x="393.7" y="156.21" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="U1" gate="G$1" pin="GPIO1/RX"/>
<pinref part="P5" gate="G$1" pin="L"/>
<wire x1="379.73" y1="140.97" x2="379.73" y2="153.67" width="0.1524" layer="91"/>
<wire x1="379.73" y1="153.67" x2="379.73" y2="156.21" width="0.1524" layer="91"/>
<junction x="379.73" y="153.67"/>
<junction x="379.73" y="156.21"/>
</segment>
<segment>
<wire x1="190.5" y1="81.28" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<wire x1="190.5" y1="86.36" x2="190.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="190.5" y1="88.9" x2="190.5" y2="91.44" width="0.1524" layer="91"/>
<wire x1="190.5" y1="91.44" x2="190.5" y2="93.98" width="0.1524" layer="91"/>
<wire x1="190.5" y1="93.98" x2="190.5" y2="96.52" width="0.1524" layer="91"/>
<wire x1="190.5" y1="96.52" x2="190.5" y2="99.06" width="0.1524" layer="91"/>
<wire x1="190.5" y1="99.06" x2="190.5" y2="104.14" width="0.1524" layer="91"/>
<junction x="190.5" y="99.06"/>
<junction x="190.5" y="96.52"/>
<junction x="190.5" y="93.98"/>
<junction x="190.5" y="91.44"/>
<junction x="190.5" y="88.9"/>
<junction x="190.5" y="86.36"/>
<junction x="190.5" y="83.82"/>
<pinref part="CMR_RX" gate="J" pin="2"/>
<pinref part="CMR_RX" gate="J" pin="3"/>
<pinref part="CMR_RX" gate="J" pin="4"/>
<pinref part="CMR_RX" gate="J" pin="5"/>
<pinref part="CMR_RX" gate="J" pin="6"/>
<pinref part="CMR_RX" gate="J" pin="7"/>
<pinref part="CMR_RX" gate="J" pin="8"/>
<pinref part="CMR_RX" gate="J" pin="1"/>
<label x="190.5" y="104.14" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CMR_TX" class="0">
<segment>
<pinref part="P4" gate="G$1" pin="C"/>
<wire x1="388.62" y1="148.59" x2="393.7" y2="148.59" width="0.1524" layer="91"/>
<label x="393.7" y="148.59" size="1.27" layer="95" ratio="10" xref="yes"/>
<pinref part="U1" gate="G$1" pin="GPIO0/TX"/>
<pinref part="P4" gate="G$1" pin="L"/>
<wire x1="382.27" y1="140.97" x2="382.27" y2="146.05" width="0.1524" layer="91"/>
<wire x1="382.27" y1="146.05" x2="388.62" y2="146.05" width="0.1524" layer="91"/>
<junction x="388.62" y="146.05"/>
<wire x1="388.62" y1="146.05" x2="388.62" y2="148.59" width="0.1524" layer="91"/>
<junction x="388.62" y="148.59"/>
</segment>
<segment>
<wire x1="224.79" y1="81.28" x2="224.79" y2="83.82" width="0.1524" layer="91"/>
<wire x1="224.79" y1="83.82" x2="224.79" y2="86.36" width="0.1524" layer="91"/>
<wire x1="224.79" y1="86.36" x2="224.79" y2="88.9" width="0.1524" layer="91"/>
<wire x1="224.79" y1="88.9" x2="224.79" y2="91.44" width="0.1524" layer="91"/>
<wire x1="224.79" y1="91.44" x2="224.79" y2="93.98" width="0.1524" layer="91"/>
<wire x1="224.79" y1="93.98" x2="224.79" y2="96.52" width="0.1524" layer="91"/>
<wire x1="224.79" y1="96.52" x2="224.79" y2="99.06" width="0.1524" layer="91"/>
<wire x1="224.79" y1="99.06" x2="224.79" y2="104.14" width="0.1524" layer="91"/>
<junction x="224.79" y="99.06"/>
<junction x="224.79" y="96.52"/>
<junction x="224.79" y="93.98"/>
<junction x="224.79" y="91.44"/>
<junction x="224.79" y="88.9"/>
<junction x="224.79" y="86.36"/>
<junction x="224.79" y="83.82"/>
<pinref part="CMR_TX" gate="J" pin="2"/>
<pinref part="CMR_TX" gate="J" pin="3"/>
<pinref part="CMR_TX" gate="J" pin="4"/>
<pinref part="CMR_TX" gate="J" pin="5"/>
<pinref part="CMR_TX" gate="J" pin="6"/>
<pinref part="CMR_TX" gate="J" pin="7"/>
<pinref part="CMR_TX" gate="J" pin="8"/>
<pinref part="CMR_TX" gate="J" pin="1"/>
<label x="224.79" y="104.14" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SD" class="0">
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="PB2/D10/SS/PWM"/>
<wire x1="39.37" y1="139.7" x2="39.37" y2="143.51" width="0.1524" layer="91"/>
<label x="39.37" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="133.35" y="101.6" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="Q1" gate="Q" pin="E"/>
<wire x1="143.51" y1="101.6" x2="133.35" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SD" gate="LED" pin="-"/>
<wire x1="157.48" y1="45.72" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
<label x="160.02" y="45.72" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="CMR" class="0">
<segment>
<wire x1="44.45" y1="139.7" x2="44.45" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PB0/D8"/>
<label x="44.45" y="143.51" size="1.27" layer="95" ratio="10" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="133.35" y="74.93" size="1.27" layer="95" ratio="10" rot="R180" xref="yes"/>
<pinref part="P6" gate="G$1" pin="2"/>
<wire x1="138.43" y1="74.93" x2="133.35" y2="74.93" width="0.1524" layer="91"/>
<pinref part="Q2" gate="Q" pin="E"/>
<wire x1="143.51" y1="74.93" x2="140.97" y2="74.93" width="0.1524" layer="91"/>
<pinref part="P6" gate="G$1" pin="1"/>
<wire x1="138.43" y1="74.93" x2="140.97" y2="74.93" width="0.1524" layer="91"/>
<junction x="138.43" y="74.93"/>
<junction x="140.97" y="74.93"/>
</segment>
<segment>
<pinref part="CMR" gate="LED" pin="-"/>
<wire x1="157.48" y1="53.34" x2="160.02" y2="53.34" width="0.1524" layer="91"/>
<label x="160.02" y="53.34" size="1.27" layer="95" ratio="10" xref="yes"/>
</segment>
</net>
<net name="MISOP" class="0">
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="PB4/D12/MISO"/>
<wire x1="34.29" y1="139.7" x2="34.29" y2="165.1" width="0.1524" layer="91"/>
<wire x1="34.29" y1="165.1" x2="95.25" y2="165.1" width="0.1524" layer="91"/>
<pinref part="P3" gate="G$1" pin="L"/>
</segment>
</net>
<net name="SCKP" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="L"/>
<wire x1="91.44" y1="157.48" x2="31.75" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PB5/D13/SCK"/>
<wire x1="31.75" y1="157.48" x2="31.75" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSIP" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="L"/>
<wire x1="87.63" y1="149.86" x2="36.83" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U5" gate="SEEEDUINO" pin="PB3/D11/MOSI/PWM"/>
<wire x1="36.83" y1="149.86" x2="36.83" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U5" gate="SEEEDUINO" pin="SCK"/>
<wire x1="91.44" y1="105.41" x2="86.36" y2="105.41" width="0.1524" layer="91"/>
<pinref part="P2" gate="G$1" pin="R"/>
<wire x1="91.44" y1="152.4" x2="91.44" y2="105.41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="R"/>
<pinref part="U5" gate="SEEEDUINO" pin="MOSI"/>
<wire x1="86.36" y1="113.03" x2="87.63" y2="113.03" width="0.1524" layer="91"/>
<wire x1="87.63" y1="113.03" x2="87.63" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISOT" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="R"/>
<pinref part="U5" gate="SEEEDUINO" pin="MISO"/>
<wire x1="86.36" y1="102.87" x2="95.25" y2="102.87" width="0.1524" layer="91"/>
<wire x1="95.25" y1="102.87" x2="95.25" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
