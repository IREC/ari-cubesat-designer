EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT9V034_Breakout:MT9V034 U?
U 1 1 5D5EFAE7
P 3350 2900
F 0 "U?" H 3475 4081 50  0000 C CNN
F 1 "MT9V034" H 3475 3990 50  0000 C CNN
F 2 "" H 3950 1900 50  0001 C CNN
F 3 "" H 3950 1900 50  0001 C CNN
	1    3350 2900
	1    0    0    -1  
$EndComp
$Comp
L VC0703PLEB:VC0703PLEB U?
U 1 1 5D5F0A56
P 8000 2900
F 0 "U?" H 8000 1811 50  0000 C CNN
F 1 "VC0703PLEB" H 8000 1720 50  0000 C CNN
F 2 "" H 8800 1900 50  0001 C CNN
F 3 "" H 8800 1900 50  0001 C CNN
	1    8000 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5D5F2FF8
P 9850 5300
F 0 "J?" H 9930 5292 50  0000 L CNN
F 1 "Conn_01x04" H 9930 5201 50  0000 L CNN
F 2 "" H 9850 5300 50  0001 C CNN
F 3 "~" H 9850 5300 50  0001 C CNN
	1    9850 5300
	1    0    0    -1  
$EndComp
Text Notes 9200 5700 0    50   ~ 0
PicoBlade Connector to PC/104
$EndSCHEMATC
