EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title "MT9V034 & VC0703 Test Board"
Date "2019-08-30"
Rev "1.0"
Comp "Aurora Research Institute"
Comment1 "Author: Patrick Gall - patrick.gall@auroracollege.nt.ca"
Comment2 "Northern Images Mission - Canadian CubeSat Project"
Comment3 "CON-CAM-040-010"
Comment4 ""
$EndDescr
Text Notes 3950 8000 0    50   ~ 0
5V to 3.3V Regulator
Connection ~ 3950 7350
$Comp
L Connector:TestPoint TP2
U 1 1 5D5A9EAA
P 3950 7250
F 0 "TP2" H 4008 7368 50  0000 L CNN
F 1 "TestPoint" H 4008 7277 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 4150 7250 50  0001 C CNN
F 3 "~" H 4150 7250 50  0001 C CNN
	1    3950 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 7250 3950 7350
$Comp
L Connector:TestPoint TP1
U 1 1 5D5ADBCA
P 2600 7200
F 0 "TP1" H 2658 7318 50  0000 L CNN
F 1 "TestPoint" H 2658 7227 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 2800 7200 50  0001 C CNN
F 3 "~" H 2800 7200 50  0001 C CNN
	1    2600 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 7200 2600 7350
Wire Wire Line
	2350 7350 2400 7350
Connection ~ 2600 7350
Wire Wire Line
	8100 8100 7850 8100
$Comp
L Switch:SW_Push_Dual SW1
U 1 1 5D584192
P 7250 8450
F 0 "SW1" H 7250 8735 50  0000 C CNN
F 1 "SW_Push_Dual" H 7250 8644 50  0000 C CNN
F 2 "Button_Switch_THT:SW_TH_Tactile_Omron_B3F-10xx" H 7250 8650 50  0001 C CNN
F 3 "~" H 7250 8650 50  0001 C CNN
	1    7250 8450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 8450 6800 8450
$Comp
L power:GND #PWR024
U 1 1 5D58AA04
P 7350 8850
F 0 "#PWR024" H 7350 8600 50  0001 C CNN
F 1 "GND" H 7355 8677 50  0000 C CNN
F 2 "" H 7350 8850 50  0001 C CNN
F 3 "" H 7350 8850 50  0001 C CNN
	1    7350 8850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5D5A298F
P 7850 7850
F 0 "R6" H 7920 7896 50  0000 L CNN
F 1 "10K" H 7920 7805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7780 7850 50  0001 C CNN
F 3 "~" H 7850 7850 50  0001 C CNN
	1    7850 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5D5A310B
P 7850 8400
F 0 "C11" H 7965 8446 50  0000 L CNN
F 1 "1uF" H 7965 8355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7888 8250 50  0001 C CNN
F 3 "~" H 7850 8400 50  0001 C CNN
	1    7850 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 8000 7850 8100
Connection ~ 7850 8100
Wire Wire Line
	7850 8100 7850 8250
Wire Wire Line
	7450 8450 7650 8450
Wire Wire Line
	7650 8450 7650 8100
Wire Wire Line
	7650 8100 7850 8100
$Comp
L power:+3V3 #PWR026
U 1 1 5D5AD7EA
P 7850 7600
F 0 "#PWR026" H 7850 7450 50  0001 C CNN
F 1 "+3V3" H 7865 7773 50  0000 C CNN
F 2 "" H 7850 7600 50  0001 C CNN
F 3 "" H 7850 7600 50  0001 C CNN
	1    7850 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 7600 7850 7700
Wire Wire Line
	6800 8850 7350 8850
Wire Wire Line
	6800 8450 6800 8850
Wire Wire Line
	7350 8850 7850 8850
Wire Wire Line
	7850 8850 7850 8550
Connection ~ 7350 8850
Text Label 8100 8100 0    50   ~ 0
RESET
Text Notes 7750 9100 0    50   ~ 0
VC0703 Reset\n
Wire Notes Line
	8500 9200 8500 7350
Wire Notes Line
	8500 7350 6700 7350
Wire Notes Line
	6700 7350 6700 9200
Wire Notes Line
	6700 9200 8500 9200
Wire Notes Line
	8300 650  8300 2750
Wire Notes Line
	10750 650  8300 650 
Wire Notes Line
	10750 2750 10750 650 
Wire Notes Line
	8300 2750 10750 2750
Wire Wire Line
	9150 2450 9150 2800
Wire Wire Line
	9050 2200 9050 2800
Wire Wire Line
	8750 1950 8750 2800
Wire Wire Line
	8650 1850 8650 2800
Wire Wire Line
	8550 1750 8550 2800
Wire Wire Line
	8450 1650 8450 2800
Wire Wire Line
	9150 2450 9650 2450
Connection ~ 9150 2450
Wire Wire Line
	9050 2200 9650 2200
Connection ~ 9050 2200
Text Label 10250 2450 0    50   ~ 0
TEST_TX
Text Label 10250 2200 0    50   ~ 0
TEST_RX
$Comp
L Jumper:Jumper_2_Bridged JP3
U 1 1 5D533CEB
P 9850 2200
F 0 "JP3" H 9850 2395 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 9850 2304 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 9850 2200 50  0001 C CNN
F 3 "~" H 9850 2200 50  0001 C CNN
	1    9850 2200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP4
U 1 1 5D53A038
P 9850 2450
F 0 "JP4" H 9850 2645 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 9850 2554 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 9850 2450 50  0001 C CNN
F 3 "~" H 9850 2450 50  0001 C CNN
	1    9850 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2200 10250 2200
Wire Wire Line
	10050 2450 10250 2450
Wire Wire Line
	8750 1950 10250 1950
Connection ~ 8750 1950
Wire Wire Line
	8750 1950 8750 1550
Wire Wire Line
	8550 1750 10250 1750
Connection ~ 8550 1750
Wire Wire Line
	8550 1750 8550 1100
Wire Wire Line
	8450 1650 10250 1650
Connection ~ 8450 1650
Wire Wire Line
	8450 850  8450 1650
Text Notes 10050 2650 0    50   ~ 0
MCU Interfaces
Text Label 10250 1650 0    50   ~ 0
MCU_MISO
Text Label 10250 1750 0    50   ~ 0
MCU_MOSI
Text Label 10250 1850 0    50   ~ 0
MCU_SCK
Text Label 10250 1950 0    50   ~ 0
MCU_CS_CS
Wire Wire Line
	8650 1850 10250 1850
Connection ~ 8650 1850
Wire Wire Line
	8650 1300 8650 1850
Text Label 8150 5250 0    50   ~ 0
RESET
Wire Notes Line
	7500 1800 7500 5400
Wire Notes Line
	3950 5400 3950 1800
Wire Wire Line
	6250 2450 6300 2450
Connection ~ 6250 2450
Wire Wire Line
	6250 2450 6250 2100
$Comp
L Connector:TestPoint TP9
U 1 1 5D855661
P 6250 2100
F 0 "TP9" H 6308 2218 50  0000 L CNN
F 1 "TestPoint" H 6308 2127 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 6450 2100 50  0001 C CNN
F 3 "~" H 6450 2100 50  0001 C CNN
	1    6250 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2900 4550 2900
Wire Wire Line
	4250 2800 4550 2800
Wire Wire Line
	4250 2700 4550 2700
Text Label 4250 2900 0    50   ~ 0
IMG_D9
Text Label 4250 2800 0    50   ~ 0
IMG_D8
Text Label 4250 2700 0    50   ~ 0
IMG_D7
Wire Wire Line
	4250 2600 4550 2600
Wire Wire Line
	4250 2500 4550 2500
Wire Wire Line
	4250 2400 4550 2400
Wire Wire Line
	4550 2300 4250 2300
Wire Wire Line
	4250 2200 4550 2200
Wire Wire Line
	4550 2100 4250 2100
Wire Wire Line
	4250 2000 4550 2000
Text Label 4250 2600 0    50   ~ 0
IMG_D6
Text Label 4250 2500 0    50   ~ 0
IMG_D5
Text Label 4250 2400 0    50   ~ 0
IMG_D4
Text Label 4250 2300 0    50   ~ 0
IMG_D3
Text Label 4250 2200 0    50   ~ 0
IMG_D2
Text Label 4250 2100 0    50   ~ 0
IMG_D1
Text Label 4250 2000 0    50   ~ 0
IMG_D0
$Comp
L Connector:Conn_01x10_Male J5
U 1 1 5D7A7068
P 4750 2400
F 0 "J5" H 4722 2374 50  0000 R CNN
F 1 "Conn_01x10_Male" H 4722 2283 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical" H 4750 2400 50  0001 C CNN
F 3 "~" H 4750 2400 50  0001 C CNN
	1    4750 2400
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5D7A62B8
P 6550 3450
F 0 "TP10" H 6608 3568 50  0000 L CNN
F 1 "TestPoint" H 6608 3477 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 6750 3450 50  0001 C CNN
F 3 "~" H 6750 3450 50  0001 C CNN
	1    6550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3650 7500 3650
Wire Wire Line
	6550 3650 6550 3450
Wire Wire Line
	6000 4300 6150 4300
Connection ~ 5250 4300
Wire Wire Line
	5250 4300 5600 4300
Wire Wire Line
	5250 3950 5250 4300
Wire Wire Line
	4650 4300 5250 4300
Wire Wire Line
	4650 4950 5250 4950
Text Label 4650 4300 0    50   ~ 0
FRAME_VALID
$Comp
L Connector:TestPoint TP5
U 1 1 5D743DA9
P 5250 3950
F 0 "TP5" H 5308 4068 50  0000 L CNN
F 1 "TestPoint" H 5308 3977 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 5450 3950 50  0001 C CNN
F 3 "~" H 5450 3950 50  0001 C CNN
	1    5250 3950
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP1
U 1 1 5D7433B5
P 5800 4300
F 0 "JP1" H 5800 4495 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 5800 4404 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 5800 4300 50  0001 C CNN
F 3 "~" H 5800 4300 50  0001 C CNN
	1    5800 4300
	1    0    0    -1  
$EndComp
Text Label 6350 4300 0    50   ~ 0
CS_VSYNC
Wire Wire Line
	7050 3550 7500 3550
Wire Wire Line
	7050 3450 7500 3450
Text Label 7050 3550 0    50   ~ 0
CS_VSYNC
Text Label 7050 3450 0    50   ~ 0
CS_HSYNC
Text Label 6300 4950 0    50   ~ 0
CS_HSYNC
Wire Wire Line
	6000 4950 6150 4950
Connection ~ 5250 4950
Wire Wire Line
	5250 4950 5600 4950
Wire Wire Line
	5250 4950 5250 4700
Text Label 4650 4950 0    50   ~ 0
LINE_VALID
Wire Wire Line
	5850 2450 6250 2450
Text Label 5850 2450 0    50   ~ 0
LED_OUT
Text Label 4350 3150 0    50   ~ 0
LED_OUT
Text Label 4350 3250 0    50   ~ 0
PICCLK
Text Label 4350 3350 0    50   ~ 0
FRAME_VALID
Text Label 4350 3450 0    50   ~ 0
LINE_VALID
Wire Wire Line
	3850 3150 4350 3150
$Comp
L Device:LED D2
U 1 1 5D55EFFA
P 6450 2450
F 0 "D2" H 6443 2195 50  0000 C CNN
F 1 "LED" H 6443 2286 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6450 2450 50  0001 C CNN
F 3 "~" H 6450 2450 50  0001 C CNN
	1    6450 2450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5D5604FA
P 6850 2450
F 0 "R5" V 6643 2450 50  0000 C CNN
F 1 "270" V 6734 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6780 2450 50  0001 C CNN
F 3 "~" H 6850 2450 50  0001 C CNN
	1    6850 2450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5D560B92
P 7100 2500
F 0 "#PWR023" H 7100 2250 50  0001 C CNN
F 1 "GND" H 7105 2327 50  0000 C CNN
F 2 "" H 7100 2500 50  0001 C CNN
F 3 "" H 7100 2500 50  0001 C CNN
	1    7100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2450 6700 2450
Wire Wire Line
	7000 2450 7100 2450
Wire Wire Line
	7100 2450 7100 2500
Wire Wire Line
	3850 3350 4350 3350
Wire Wire Line
	3850 3450 4350 3450
$Comp
L Jumper:Jumper_2_Bridged JP2
U 1 1 5D675920
P 5800 4950
F 0 "JP2" H 5800 5145 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 5800 5054 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 5800 4950 50  0001 C CNN
F 3 "~" H 5800 4950 50  0001 C CNN
	1    5800 4950
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5D66DF48
P 5250 4700
F 0 "TP6" H 5308 4818 50  0000 L CNN
F 1 "TestPoint" H 5308 4727 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 5450 4700 50  0001 C CNN
F 3 "~" H 5450 4700 50  0001 C CNN
	1    5250 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3250 7500 3250
Wire Wire Line
	7050 3350 7500 3350
Text Label 7050 3350 0    50   ~ 0
CAM_SCK
Text Label 7050 3250 0    50   ~ 0
CAM_SDA
Text Label 2000 4300 0    50   ~ 0
CAM_SDA
Text Label 2000 4200 0    50   ~ 0
CAM_SCK
Wire Wire Line
	11200 5500 11200 5550
Wire Wire Line
	11050 5500 11200 5500
Connection ~ 9900 5650
Wire Wire Line
	10850 5650 9900 5650
Wire Wire Line
	9900 5350 10850 5350
$Comp
L Device:R R8
U 1 1 5D5CCA28
P 9900 5500
F 0 "R8" H 9970 5546 50  0000 L CNN
F 1 "1M" H 9970 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9830 5500 50  0001 C CNN
F 3 "~" H 9900 5500 50  0001 C CNN
	1    9900 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5D5C63AC
P 11200 5550
F 0 "#PWR042" H 11200 5300 50  0001 C CNN
F 1 "GND" H 11205 5377 50  0000 C CNN
F 2 "" H 11200 5550 50  0001 C CNN
F 3 "" H 11200 5550 50  0001 C CNN
	1    11200 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:Resonator Y1
U 1 1 5D5C3BF2
P 10850 5500
F 0 "Y1" V 10896 5611 50  0000 L CNN
F 1 "Resonator 6MHz" V 10805 5611 50  0000 L CNN
F 2 "Resonator:Ceramic_Res_6MHz_SMD_Custom" H 10825 5500 50  0001 C CNN
F 3 "~" H 10825 5500 50  0001 C CNN
	1    10850 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8550 5250 8150 5250
Wire Wire Line
	8550 4800 8550 5250
$Comp
L Connector:TestPoint TP16
U 1 1 5D580751
P 9150 2450
F 0 "TP16" H 9208 2568 50  0000 L CNN
F 1 "TestPoint" H 9208 2477 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 9350 2450 50  0001 C CNN
F 3 "~" H 9350 2450 50  0001 C CNN
	1    9150 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP15
U 1 1 5D580320
P 9050 2200
F 0 "TP15" H 9108 2318 50  0000 L CNN
F 1 "TestPoint" H 9108 2227 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 9250 2200 50  0001 C CNN
F 3 "~" H 9250 2200 50  0001 C CNN
	1    9050 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5D572EDD
P 8750 1550
F 0 "TP14" H 8808 1668 50  0000 L CNN
F 1 "TestPoint" H 8808 1577 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 8950 1550 50  0001 C CNN
F 3 "~" H 8950 1550 50  0001 C CNN
	1    8750 1550
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP13
U 1 1 5D56B609
P 8650 1300
F 0 "TP13" H 8708 1418 50  0000 L CNN
F 1 "TestPoint" H 8708 1327 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 8850 1300 50  0001 C CNN
F 3 "~" H 8850 1300 50  0001 C CNN
	1    8650 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5D56B361
P 8550 1100
F 0 "TP12" H 8608 1218 50  0000 L CNN
F 1 "TestPoint" H 8608 1127 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 8750 1100 50  0001 C CNN
F 3 "~" H 8750 1100 50  0001 C CNN
	1    8550 1100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5D56AEC7
P 8450 850
F 0 "TP11" H 8508 968 50  0000 L CNN
F 1 "TestPoint" H 8508 877 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 8650 850 50  0001 C CNN
F 3 "~" H 8650 850 50  0001 C CNN
	1    8450 850 
	1    0    0    -1  
$EndComp
Text Notes 6400 5350 0    50   ~ 0
Image Sensor to DSP Link
Wire Wire Line
	8000 2450 8000 2650
Wire Wire Line
	8250 2650 8250 2800
Wire Wire Line
	8000 2650 8250 2650
Wire Wire Line
	7900 2300 7900 2700
Wire Wire Line
	8150 2700 8150 2800
Wire Wire Line
	7900 2700 8150 2700
Wire Wire Line
	7800 2450 7800 2750
Wire Wire Line
	8050 2750 7800 2750
Wire Wire Line
	8050 2800 8050 2750
Wire Wire Line
	3850 4550 4350 4550
Wire Wire Line
	3850 4450 4350 4450
Wire Wire Line
	3850 4350 4350 4350
Wire Wire Line
	3850 4250 4350 4250
Wire Wire Line
	3850 4150 4350 4150
Wire Wire Line
	3850 4050 4350 4050
Wire Wire Line
	3850 3950 4350 3950
Wire Wire Line
	3850 3850 4350 3850
Wire Wire Line
	4350 3750 3850 3750
Wire Wire Line
	3850 3650 4350 3650
Text Label 4350 3650 2    50   ~ 0
IMG_D9
Text Label 4350 3750 2    50   ~ 0
IMG_D8
Text Label 4350 3850 2    50   ~ 0
IMG_D7
Text Label 4350 3950 2    50   ~ 0
IMG_D6
Text Label 4350 4050 2    50   ~ 0
IMG_D5
Text Label 4350 4150 2    50   ~ 0
IMG_D4
Text Label 4350 4250 2    50   ~ 0
IMG_D3
Text Label 4350 4350 2    50   ~ 0
IMG_D2
Text Label 4350 4450 2    50   ~ 0
IMG_D1
Text Label 4350 4550 2    50   ~ 0
IMG_D0
Wire Wire Line
	8250 5100 8250 4800
Wire Wire Line
	7200 5100 8250 5100
Wire Wire Line
	8150 5000 8150 4800
Wire Wire Line
	7200 5000 8150 5000
Wire Wire Line
	8050 4900 7200 4900
Wire Wire Line
	8050 4800 8050 4900
Text Label 7200 5100 0    50   ~ 0
IMG_D9
Text Label 7200 5000 0    50   ~ 0
IMG_D8
Text Label 7200 4900 0    50   ~ 0
IMG_D7
Wire Wire Line
	7200 4350 7500 4350
Wire Wire Line
	7200 4250 7500 4250
Wire Wire Line
	7200 4150 7500 4150
Wire Wire Line
	7500 4050 7200 4050
Wire Wire Line
	7200 3950 7500 3950
Text Label 7200 4350 0    50   ~ 0
IMG_D6
Text Label 7200 4250 0    50   ~ 0
IMG_D5
Text Label 7200 4150 0    50   ~ 0
IMG_D4
Text Label 7200 4050 0    50   ~ 0
IMG_D3
Wire Wire Line
	8750 5500 8750 5800
Connection ~ 7600 2300
Wire Wire Line
	7600 2000 7600 2300
Wire Wire Line
	7900 2000 7900 2300
Wire Wire Line
	8200 2000 8200 2300
Wire Wire Line
	10950 4200 10950 3950
Wire Wire Line
	9500 3950 10950 3950
Wire Wire Line
	10950 4600 10950 4500
$Comp
L power:GND #PWR041
U 1 1 5D52EDDD
P 10950 4600
F 0 "#PWR041" H 10950 4350 50  0001 C CNN
F 1 "GND" H 10955 4427 50  0000 C CNN
F 2 "" H 10950 4600 50  0001 C CNN
F 3 "" H 10950 4600 50  0001 C CNN
	1    10950 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5D52E6BA
P 10950 4350
F 0 "R11" H 11020 4396 50  0000 L CNN
F 1 "75" H 11020 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10880 4350 50  0001 C CNN
F 3 "~" H 10950 4350 50  0001 C CNN
	1    10950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 4050 10500 4050
Connection ~ 10100 4050
Wire Wire Line
	10100 4200 10100 4050
Wire Wire Line
	10500 4050 10500 4200
Wire Wire Line
	9500 4050 10100 4050
Wire Wire Line
	10100 4550 10100 4500
Connection ~ 10100 4550
Wire Wire Line
	10500 4550 10100 4550
Wire Wire Line
	10500 4500 10500 4550
Wire Wire Line
	10100 4600 10100 4550
$Comp
L Device:C C18
U 1 1 5D529C91
P 10500 4350
F 0 "C18" H 10615 4396 50  0000 L CNN
F 1 "100nF" H 10615 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10538 4200 50  0001 C CNN
F 3 "~" H 10500 4350 50  0001 C CNN
	1    10500 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5D5298C3
P 10100 4350
F 0 "C16" H 10215 4396 50  0000 L CNN
F 1 "1uF" H 10215 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10138 4200 50  0001 C CNN
F 3 "~" H 10100 4350 50  0001 C CNN
	1    10100 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR038
U 1 1 5D528693
P 10100 4600
F 0 "#PWR038" H 10100 4450 50  0001 C CNN
F 1 "+3V3" H 10115 4773 50  0000 C CNN
F 2 "" H 10100 4600 50  0001 C CNN
F 3 "" H 10100 4600 50  0001 C CNN
	1    10100 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9750 4150 9750 4200
Wire Wire Line
	9500 4150 9750 4150
Wire Wire Line
	9600 4500 9600 4600
Connection ~ 9600 4500
Wire Wire Line
	9750 4500 9600 4500
Wire Wire Line
	9600 4250 9600 4500
$Comp
L Device:R R7
U 1 1 5D525961
P 9750 4350
F 0 "R7" H 9820 4396 50  0000 L CNN
F 1 "270" H 9820 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9680 4350 50  0001 C CNN
F 3 "~" H 9750 4350 50  0001 C CNN
	1    9750 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4250 9600 4250
$Comp
L power:GND #PWR034
U 1 1 5D525028
P 9600 4600
F 0 "#PWR034" H 9600 4350 50  0001 C CNN
F 1 "GND" H 9605 4427 50  0000 C CNN
F 2 "" H 9600 4600 50  0001 C CNN
F 3 "" H 9600 4600 50  0001 C CNN
	1    9600 4600
	1    0    0    -1  
$EndComp
$Comp
L VC0703PLEB:VC0703PLEB U4
U 1 1 5D513637
P 8500 3800
F 0 "U4" H 9544 3846 50  0000 L CNN
F 1 "VC0703PLEB" H 9544 3755 50  0000 L CNN
F 2 "VC0703:VC0703PREB-QFP-48_7x7_Pitch0.5mm" H 9300 2800 50  0001 C CNN
F 3 "" H 9300 2800 50  0001 C CNN
	1    8500 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 2300 7600 2450
Wire Wire Line
	7600 2450 7800 2450
$Comp
L power:+3V3 #PWR025
U 1 1 5D522E37
P 7600 2000
F 0 "#PWR025" H 7600 1850 50  0001 C CNN
F 1 "+3V3" H 7615 2173 50  0000 C CNN
F 2 "" H 7600 2000 50  0001 C CNN
F 3 "" H 7600 2000 50  0001 C CNN
	1    7600 2000
	1    0    0    -1  
$EndComp
Connection ~ 8200 2300
Wire Wire Line
	8200 2450 8000 2450
Wire Wire Line
	8200 2300 8200 2450
$Comp
L power:+1V2 #PWR028
U 1 1 5D521E14
P 8200 2000
F 0 "#PWR028" H 8200 1850 50  0001 C CNN
F 1 "+1V2" H 8215 2173 50  0000 C CNN
F 2 "" H 8200 2000 50  0001 C CNN
F 3 "" H 8200 2000 50  0001 C CNN
	1    8200 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5D521917
P 7750 2300
F 0 "C10" V 7498 2300 50  0000 C CNN
F 1 "100nF" V 7589 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7788 2150 50  0001 C CNN
F 3 "~" H 7750 2300 50  0001 C CNN
	1    7750 2300
	0    1    1    0   
$EndComp
Connection ~ 7900 2300
$Comp
L Device:C C12
U 1 1 5D521049
P 8050 2300
F 0 "C12" V 7798 2300 50  0000 C CNN
F 1 "100nF" V 7889 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8088 2150 50  0001 C CNN
F 3 "~" H 8050 2300 50  0001 C CNN
	1    8050 2300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5D520714
P 7900 2000
F 0 "#PWR027" H 7900 1750 50  0001 C CNN
F 1 "GND" H 7905 1827 50  0000 C CNN
F 2 "" H 7900 2000 50  0001 C CNN
F 3 "" H 7900 2000 50  0001 C CNN
	1    7900 2000
	-1   0    0    1   
$EndComp
Connection ~ 8450 5500
Wire Wire Line
	8450 5500 8450 5800
Connection ~ 9050 5500
Wire Wire Line
	9050 5800 9050 5500
$Comp
L power:+3V3 #PWR029
U 1 1 5D51EEC2
P 8450 5800
F 0 "#PWR029" H 8450 5650 50  0001 C CNN
F 1 "+3V3" H 8465 5973 50  0000 C CNN
F 2 "" H 8450 5800 50  0001 C CNN
F 3 "" H 8450 5800 50  0001 C CNN
	1    8450 5800
	-1   0    0    1   
$EndComp
$Comp
L power:+1V2 #PWR032
U 1 1 5D51E536
P 9050 5800
F 0 "#PWR032" H 9050 5650 50  0001 C CNN
F 1 "+1V2" H 9065 5973 50  0000 C CNN
F 2 "" H 9050 5800 50  0001 C CNN
F 3 "" H 9050 5800 50  0001 C CNN
	1    9050 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	8650 5350 8650 4800
Wire Wire Line
	8450 5350 8650 5350
Wire Wire Line
	8450 5500 8450 5350
Wire Wire Line
	9050 5350 9050 5500
Wire Wire Line
	8850 5350 9050 5350
Wire Wire Line
	8850 4800 8850 5350
$Comp
L Device:C C13
U 1 1 5D51D4FA
P 8600 5500
F 0 "C13" V 8348 5500 50  0000 C CNN
F 1 "100nF" V 8439 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8638 5350 50  0001 C CNN
F 3 "~" H 8600 5500 50  0001 C CNN
	1    8600 5500
	0    1    1    0   
$EndComp
Connection ~ 8750 5500
$Comp
L Device:C C14
U 1 1 5D51CDA5
P 8900 5500
F 0 "C14" V 8648 5500 50  0000 C CNN
F 1 "100nF" V 8739 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8938 5350 50  0001 C CNN
F 3 "~" H 8900 5500 50  0001 C CNN
	1    8900 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 4800 8750 5500
$Comp
L power:GND #PWR030
U 1 1 5D51BC1B
P 8750 5800
F 0 "#PWR030" H 8750 5550 50  0001 C CNN
F 1 "GND" H 8755 5627 50  0000 C CNN
F 2 "" H 8750 5800 50  0001 C CNN
F 3 "" H 8750 5800 50  0001 C CNN
	1    8750 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 4850 9050 4850
Connection ~ 9300 4850
Wire Wire Line
	9300 4900 9300 4850
Wire Wire Line
	9050 4850 9050 4800
Wire Wire Line
	9650 4850 9300 4850
Connection ~ 9300 5250
Wire Wire Line
	9300 5250 9300 5200
Wire Wire Line
	9300 5250 9300 5300
Wire Wire Line
	8950 5250 9300 5250
Wire Wire Line
	8950 4800 8950 5250
$Comp
L power:+3V3 #PWR036
U 1 1 5D51AC06
P 9650 4850
F 0 "#PWR036" H 9650 4700 50  0001 C CNN
F 1 "+3V3" H 9665 5023 50  0000 C CNN
F 2 "" H 9650 4850 50  0001 C CNN
F 3 "" H 9650 4850 50  0001 C CNN
	1    9650 4850
	0    1    1    0   
$EndComp
$Comp
L Device:C C15
U 1 1 5D51A003
P 9300 5050
F 0 "C15" H 9415 5096 50  0000 L CNN
F 1 "100nF" H 9415 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9338 4900 50  0001 C CNN
F 3 "~" H 9300 5050 50  0001 C CNN
	1    9300 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5D5191EC
P 9300 5300
F 0 "#PWR033" H 9300 5050 50  0001 C CNN
F 1 "GND" H 9305 5127 50  0000 C CNN
F 2 "" H 9300 5300 50  0001 C CNN
F 3 "" H 9300 5300 50  0001 C CNN
	1    9300 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR019
U 1 1 5D9FC1DE
P 5350 8700
F 0 "#PWR019" H 5350 8550 50  0001 C CNN
F 1 "+3V3" H 5365 8873 50  0000 C CNN
F 2 "" H 5350 8700 50  0001 C CNN
F 3 "" H 5350 8700 50  0001 C CNN
	1    5350 8700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5D9FC9E1
P 6200 8700
F 0 "#PWR022" H 6200 8450 50  0001 C CNN
F 1 "GND" H 6205 8527 50  0000 C CNN
F 2 "" H 6200 8700 50  0001 C CNN
F 3 "" H 6200 8700 50  0001 C CNN
	1    6200 8700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5D9FD0BE
P 5550 8700
F 0 "R4" H 5480 8654 50  0000 R CNN
F 1 "270" H 5480 8745 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5480 8700 50  0001 C CNN
F 3 "~" H 5550 8700 50  0001 C CNN
	1    5550 8700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 5DA1A7C3
P 5950 8700
F 0 "D1" V 5989 8583 50  0000 R CNN
F 1 "LED" V 5898 8583 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5950 8700 50  0001 C CNN
F 3 "~" H 5950 8700 50  0001 C CNN
	1    5950 8700
	-1   0    0    1   
$EndComp
Text Notes 6550 9000 2    50   ~ 0
Power Indicator
Text Label 12050 3350 0    50   ~ 0
TEST_TX
Text Label 12050 3250 0    50   ~ 0
TEST_RX
$Comp
L Device:LED D3
U 1 1 5DA7760A
P 12700 3050
F 0 "D3" V 12739 2933 50  0000 R CNN
F 1 "LED" V 12648 2933 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 12700 3050 50  0001 C CNN
F 3 "~" H 12700 3050 50  0001 C CNN
	1    12700 3050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R17
U 1 1 5DA78E16
P 12700 2700
F 0 "R17" H 12770 2746 50  0000 L CNN
F 1 "270" H 12770 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12630 2700 50  0001 C CNN
F 3 "~" H 12700 2700 50  0001 C CNN
	1    12700 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5700 8700 5800 8700
Wire Wire Line
	6100 8700 6200 8700
Wire Wire Line
	5350 8700 5400 8700
Wire Notes Line
	5100 8300 5100 9050
Wire Notes Line
	5100 9050 6600 9050
Wire Notes Line
	6600 9050 6600 8300
Wire Notes Line
	5100 8300 6600 8300
Text Notes 12900 3550 0    50   ~ 0
UART Indicators\n
$Comp
L Device:R R19
U 1 1 5DA791E7
P 13250 2700
F 0 "R19" H 13320 2746 50  0000 L CNN
F 1 "270" H 13320 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 13180 2700 50  0001 C CNN
F 3 "~" H 13250 2700 50  0001 C CNN
	1    13250 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D4
U 1 1 5DA7864A
P 13250 3050
F 0 "D4" V 13289 2933 50  0000 R CNN
F 1 "LED" V 13198 2933 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 13250 3050 50  0001 C CNN
F 3 "~" H 13250 3050 50  0001 C CNN
	1    13250 3050
	0    -1   -1   0   
$EndComp
Wire Notes Line
	4050 1850 4050 3000
Wire Notes Line
	4050 3000 5650 3000
Wire Notes Line
	5650 3000 5650 1850
Wire Notes Line
	5650 1850 4050 1850
Text Notes 5000 2950 0    50   ~ 0
Test Connector
$Comp
L Connector:TestPoint TP7
U 1 1 5D5D647D
P 6150 3950
F 0 "TP7" H 6208 4068 50  0000 L CNN
F 1 "TestPoint" H 6208 3977 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 6350 3950 50  0001 C CNN
F 3 "~" H 6350 3950 50  0001 C CNN
	1    6150 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5D5D70AF
P 6150 4650
F 0 "TP8" H 6208 4768 50  0000 L CNN
F 1 "TestPoint" H 6208 4677 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 6350 4650 50  0001 C CNN
F 3 "~" H 6350 4650 50  0001 C CNN
	1    6150 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4650 6150 4950
Connection ~ 6150 4950
Wire Wire Line
	6150 4950 6300 4950
Wire Wire Line
	6150 3950 6150 4300
Connection ~ 6150 4300
Wire Wire Line
	6150 4300 6350 4300
$Comp
L Connector:TestPoint TP17
U 1 1 5D61BBBF
P 11750 2600
F 0 "TP17" H 11808 2718 50  0000 L CNN
F 1 "TestPoint" H 11808 2627 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 11950 2600 50  0001 C CNN
F 3 "~" H 11950 2600 50  0001 C CNN
	1    11750 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP18
U 1 1 5D61C36B
P 11850 2800
F 0 "TP18" H 11908 2918 50  0000 L CNN
F 1 "TestPoint" H 11908 2827 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 12050 2800 50  0001 C CNN
F 3 "~" H 12050 2800 50  0001 C CNN
	1    11850 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR054
U 1 1 5D678AEA
P 12950 2500
F 0 "#PWR054" H 12950 2350 50  0001 C CNN
F 1 "+3V3" H 12965 2673 50  0000 C CNN
F 2 "" H 12950 2500 50  0001 C CNN
F 3 "" H 12950 2500 50  0001 C CNN
	1    12950 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11750 2600 11750 3350
Wire Wire Line
	11850 2800 11850 3250
Wire Wire Line
	12700 2850 12700 2900
Wire Wire Line
	12700 2550 12950 2550
Wire Wire Line
	12950 2550 12950 2500
Wire Wire Line
	12950 2550 13250 2550
Connection ~ 12950 2550
Wire Wire Line
	13250 2850 13250 2900
Wire Wire Line
	12700 3250 12700 3200
Wire Wire Line
	11850 3250 12700 3250
Wire Wire Line
	13250 3350 13250 3200
Wire Wire Line
	11750 3350 13250 3350
Text Notes 6950 2950 0    50   ~ 0
Indicator
Wire Notes Line
	7400 3000 5750 3000
Wire Notes Line
	5750 3000 5750 1850
Wire Notes Line
	5750 1850 7400 1850
Wire Notes Line
	7400 1850 7400 3000
Text Notes 4000 9150 0    50   ~ 0
3.3V to 1.2V Regulator
$Comp
L power:+5V #PWR05
U 1 1 5D67B8B4
P 2350 7350
F 0 "#PWR05" H 2350 7200 50  0001 C CNN
F 1 "+5V" V 2365 7478 50  0000 L CNN
F 2 "" H 2350 7350 50  0001 C CNN
F 3 "" H 2350 7350 50  0001 C CNN
	1    2350 7350
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR031
U 1 1 5D6879DA
P 8850 7750
F 0 "#PWR031" H 8850 7600 50  0001 C CNN
F 1 "+3.3V" H 8865 7923 50  0000 C CNN
F 2 "" H 8850 7750 50  0001 C CNN
F 3 "" H 8850 7750 50  0001 C CNN
	1    8850 7750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 5D688CC8
P 9150 7750
F 0 "J7" V 9212 7794 50  0000 L CNN
F 1 "Conn_01x02_Male" V 9303 7794 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 9150 7750 50  0001 C CNN
F 3 "~" H 9150 7750 50  0001 C CNN
	1    9150 7750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 5D69393F
P 9150 8200
F 0 "J8" V 9212 8244 50  0000 L CNN
F 1 "Conn_01x02_Male" V 9303 8244 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 9150 8200 50  0001 C CNN
F 3 "~" H 9150 8200 50  0001 C CNN
	1    9150 8200
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5D69E5B5
P 9950 8100
F 0 "R9" H 10020 8146 50  0000 L CNN
F 1 "10K" H 10020 8055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9880 8100 50  0001 C CNN
F 3 "~" H 9950 8100 50  0001 C CNN
	1    9950 8100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5D69ED90
P 10100 8550
F 0 "R10" H 10170 8596 50  0000 L CNN
F 1 "10K" H 10170 8505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10030 8550 50  0001 C CNN
F 3 "~" H 10100 8550 50  0001 C CNN
	1    10100 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 7750 8850 7950
Wire Wire Line
	8850 7950 9050 7950
Wire Wire Line
	8850 8400 9050 8400
Wire Wire Line
	9150 8400 10100 8400
Wire Wire Line
	9150 7950 9950 7950
$Comp
L power:GND #PWR037
U 1 1 5D73F62F
P 9950 8800
F 0 "#PWR037" H 9950 8550 50  0001 C CNN
F 1 "GND" H 9955 8627 50  0000 C CNN
F 2 "" H 9950 8800 50  0001 C CNN
F 3 "" H 9950 8800 50  0001 C CNN
	1    9950 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 8250 9950 8700
Wire Wire Line
	10100 8700 9950 8700
Connection ~ 9950 8700
Wire Wire Line
	9950 8700 9950 8800
Wire Wire Line
	8850 8400 8850 7950
Connection ~ 8850 7950
Wire Wire Line
	9950 7950 10300 7950
Connection ~ 9950 7950
Wire Wire Line
	10100 8400 10300 8400
Connection ~ 10100 8400
Text Label 10300 7950 0    50   ~ 0
CAM_ADDR_0
Text Label 10300 8400 0    50   ~ 0
CAM_ADDR_1
Text Label 2000 4000 0    50   ~ 0
CAM_ADDR_0
Text Label 2000 4100 0    50   ~ 0
CAM_ADDR_1
Wire Wire Line
	2000 4100 2600 4100
Wire Wire Line
	2000 4000 2600 4000
Text Notes 9600 9100 0    50   ~ 0
Image Sensor Address Selector
Wire Notes Line
	10900 9150 8650 9150
Wire Notes Line
	8650 9150 8650 7500
Wire Notes Line
	8650 7500 10900 7500
Wire Notes Line
	10900 7500 10900 9150
Wire Wire Line
	2600 3800 1900 3800
$Comp
L power:GND #PWR09
U 1 1 5D81DE90
P 3050 5150
F 0 "#PWR09" H 3050 4900 50  0001 C CNN
F 1 "GND" H 3055 4977 50  0000 C CNN
F 2 "" H 3050 5150 50  0001 C CNN
F 3 "" H 3050 5150 50  0001 C CNN
	1    3050 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4950 2850 5100
Connection ~ 2850 5100
Wire Wire Line
	2850 5100 2950 5100
Wire Wire Line
	2950 5100 2950 4950
Connection ~ 2950 5100
Wire Wire Line
	2950 5100 3050 5100
Wire Wire Line
	3050 4950 3050 5100
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D86D0F1
P 2100 4600
F 0 "J2" H 2208 4781 50  0000 C CNN
F 1 "Conn_01x02_Male" H 2208 4690 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 2100 4600 50  0001 C CNN
F 3 "~" H 2100 4600 50  0001 C CNN
	1    2100 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 5100 2850 5100
Text Label 2000 3900 0    50   ~ 0
STANDBY
Wire Wire Line
	2000 3900 2600 3900
$Comp
L Device:C C1
U 1 1 5D8D439E
P 1250 4900
F 0 "C1" H 1365 4946 50  0000 L CNN
F 1 "0.1uF" H 1365 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1288 4750 50  0001 C CNN
F 3 "~" H 1250 4900 50  0001 C CNN
	1    1250 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5100 1250 5100
Wire Wire Line
	1250 5100 1250 5050
Connection ~ 1900 5100
Wire Wire Line
	1250 4750 1250 3700
Wire Wire Line
	1250 3700 2600 3700
Text Label 2000 3800 0    50   ~ 0
Exposure
Wire Wire Line
	2000 4200 2600 4200
$Comp
L Device:R R2
U 1 1 5D92DB1D
P 1250 3500
F 0 "R2" H 1320 3546 50  0000 L CNN
F 1 "10K" H 1320 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1180 3500 50  0001 C CNN
F 3 "~" H 1250 3500 50  0001 C CNN
	1    1250 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D9EFCC8
P 950 3500
F 0 "R1" H 1020 3546 50  0000 L CNN
F 1 "1.5K" H 1020 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 880 3500 50  0001 C CNN
F 3 "~" H 950 3500 50  0001 C CNN
	1    950  3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  4300 950  3650
$Comp
L power:+3V3 #PWR07
U 1 1 5D9FF93A
P 2700 2550
F 0 "#PWR07" H 2700 2400 50  0001 C CNN
F 1 "+3V3" H 2715 2723 50  0000 C CNN
F 2 "" H 2700 2550 50  0001 C CNN
F 3 "" H 2700 2550 50  0001 C CNN
	1    2700 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2900 2850 2700
Connection ~ 2850 2700
Wire Wire Line
	2700 2700 2700 2550
Wire Wire Line
	3050 2900 3050 2700
Wire Wire Line
	9600 5350 9900 5350
Connection ~ 9900 5350
Text Label 7200 3850 0    50   ~ 0
IMG_D1
Text Label 7200 3950 0    50   ~ 0
IMG_D2
Wire Wire Line
	7200 3750 7500 3750
Wire Wire Line
	7500 3850 7200 3850
Text Label 7200 3750 0    50   ~ 0
PICCLK
Text Label 2000 3500 0    50   ~ 0
SYSCLK
Text Label 7200 3650 0    50   ~ 0
SYSCLK
Wire Wire Line
	2000 3500 2600 3500
Wire Wire Line
	1250 3700 1250 3650
Connection ~ 1250 3700
Wire Wire Line
	2600 3600 1550 3600
Wire Wire Line
	1550 3600 1550 2700
Wire Wire Line
	2850 2700 2700 2700
Wire Wire Line
	950  2700 950  3350
Connection ~ 1550 2700
Wire Wire Line
	1550 2700 1250 2700
Wire Wire Line
	1250 3350 1250 2700
Connection ~ 1250 2700
$Comp
L power:GNDA #PWR010
U 1 1 5DBBC986
P 3300 5150
F 0 "#PWR010" H 3300 4900 50  0001 C CNN
F 1 "GNDA" H 3305 4977 50  0000 C CNN
F 2 "" H 3300 5150 50  0001 C CNN
F 3 "" H 3300 5150 50  0001 C CNN
	1    3300 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4950 3150 5100
Wire Wire Line
	3150 5100 3300 5100
$Comp
L Regulator_Linear:TLV1117-33 U2
U 1 1 5DC63643
P 3300 7350
F 0 "U2" H 3300 7592 50  0000 C CNN
F 1 "TLV1117-33" H 3300 7501 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 3300 7350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlv1117.pdf" H 3300 7350 50  0001 C CNN
	1    3300 7350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR015
U 1 1 5DCA9F05
P 4650 7250
F 0 "#PWR015" H 4650 7100 50  0001 C CNN
F 1 "+3V3" V 4665 7378 50  0000 L CNN
F 2 "" H 4650 7250 50  0001 C CNN
F 3 "" H 4650 7250 50  0001 C CNN
	1    4650 7250
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AP2127N-1.2 U3
U 1 1 5DCEEF28
P 3300 8450
F 0 "U3" H 3300 8692 50  0000 C CNN
F 1 "AP2127N-1.2" H 3300 8601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3300 8675 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AP2127.pdf" H 3300 8450 50  0001 C CNN
	1    3300 8450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DD129E6
P 2650 8700
F 0 "C3" H 2765 8746 50  0000 L CNN
F 1 "1uF" H 2765 8655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2688 8550 50  0001 C CNN
F 3 "~" H 2650 8700 50  0001 C CNN
	1    2650 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 8550 2650 8450
Wire Wire Line
	2650 8450 3000 8450
Wire Wire Line
	2650 8850 3300 8850
Wire Wire Line
	3300 8850 3300 8750
$Comp
L power:GND #PWR012
U 1 1 5DD49D35
P 3300 8950
F 0 "#PWR012" H 3300 8700 50  0001 C CNN
F 1 "GND" H 3305 8777 50  0000 C CNN
F 2 "" H 3300 8950 50  0001 C CNN
F 3 "" H 3300 8950 50  0001 C CNN
	1    3300 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 8950 3300 8850
Connection ~ 3300 8850
$Comp
L Device:C C6
U 1 1 5DD5CDA1
P 3800 8700
F 0 "C6" H 3915 8746 50  0000 L CNN
F 1 "10uF" H 3915 8655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3838 8550 50  0001 C CNN
F 3 "~" H 3800 8700 50  0001 C CNN
	1    3800 8700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5DD5D67E
P 4250 8700
F 0 "C7" H 4365 8746 50  0000 L CNN
F 1 "100nF" H 4365 8655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4288 8550 50  0001 C CNN
F 3 "~" H 4250 8700 50  0001 C CNN
	1    4250 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 8850 3800 8850
Connection ~ 3800 8850
Wire Wire Line
	3800 8850 4250 8850
Wire Wire Line
	3600 8450 3800 8450
Wire Wire Line
	3800 8450 3800 8550
Wire Wire Line
	4250 8450 4250 8550
Connection ~ 3800 8450
Wire Wire Line
	4250 8450 4550 8450
Connection ~ 4250 8450
$Comp
L power:+1V2 #PWR014
U 1 1 5DDAB1B4
P 4550 8450
F 0 "#PWR014" H 4550 8300 50  0001 C CNN
F 1 "+1V2" V 4565 8578 50  0000 L CNN
F 2 "" H 4550 8450 50  0001 C CNN
F 3 "" H 4550 8450 50  0001 C CNN
	1    4550 8450
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 7350 2850 7350
$Comp
L Device:C C4
U 1 1 5DE97760
P 2850 7550
F 0 "C4" H 2965 7596 50  0000 L CNN
F 1 "100nF" H 2965 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2888 7400 50  0001 C CNN
F 3 "~" H 2850 7550 50  0001 C CNN
	1    2850 7550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5DE980C0
P 3750 7550
F 0 "C5" H 3865 7596 50  0000 L CNN
F 1 "10uF" H 3865 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3788 7400 50  0001 C CNN
F 3 "~" H 3750 7550 50  0001 C CNN
	1    3750 7550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5DE98999
P 4300 7550
F 0 "C8" H 4415 7596 50  0000 L CNN
F 1 "100nF" H 4415 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4338 7400 50  0001 C CNN
F 3 "~" H 4300 7550 50  0001 C CNN
	1    4300 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 7400 2400 7350
Wire Wire Line
	4300 7350 4300 7400
Wire Wire Line
	3950 7350 4300 7350
Wire Wire Line
	3750 7400 3750 7350
Connection ~ 2850 7700
$Comp
L Device:C C2
U 1 1 5DF0EF6E
P 2400 7550
F 0 "C2" H 2515 7596 50  0000 L CNN
F 1 "10uF" H 2515 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2438 7400 50  0001 C CNN
F 3 "~" H 2400 7550 50  0001 C CNN
	1    2400 7550
	1    0    0    -1  
$EndComp
Connection ~ 2400 7350
Wire Wire Line
	2400 7350 2600 7350
Wire Wire Line
	2850 7350 2850 7400
Connection ~ 2850 7350
Wire Wire Line
	2850 7350 3000 7350
Wire Wire Line
	2400 7700 2850 7700
Wire Wire Line
	2850 7700 3300 7700
Connection ~ 3750 7700
Wire Wire Line
	3750 7700 4300 7700
Wire Wire Line
	3600 7350 3750 7350
Connection ~ 3750 7350
Wire Wire Line
	3750 7350 3950 7350
Wire Wire Line
	4300 7350 4650 7350
Wire Wire Line
	4650 7350 4650 7250
Connection ~ 4300 7350
$Comp
L power:GND #PWR011
U 1 1 5E0075C7
P 3300 7800
F 0 "#PWR011" H 3300 7550 50  0001 C CNN
F 1 "GND" H 3305 7627 50  0000 C CNN
F 2 "" H 3300 7800 50  0001 C CNN
F 3 "" H 3300 7800 50  0001 C CNN
	1    3300 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7650 3300 7700
Connection ~ 3300 7700
Wire Wire Line
	3300 7700 3750 7700
Wire Wire Line
	3300 7800 3300 7700
$Comp
L power:+3V3 #PWR06
U 1 1 5E176929
P 2350 8450
F 0 "#PWR06" H 2350 8300 50  0001 C CNN
F 1 "+3V3" V 2365 8578 50  0000 L CNN
F 2 "" H 2350 8450 50  0001 C CNN
F 3 "" H 2350 8450 50  0001 C CNN
	1    2350 8450
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5E19106D
P 4050 8400
F 0 "TP3" H 4108 8518 50  0000 L CNN
F 1 "TestPoint" H 4108 8427 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 4250 8400 50  0001 C CNN
F 3 "~" H 4250 8400 50  0001 C CNN
	1    4050 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 8450 4050 8450
Wire Wire Line
	4050 8400 4050 8450
Connection ~ 4050 8450
Wire Wire Line
	4050 8450 4250 8450
Wire Notes Line
	4800 8050 4800 6850
Wire Notes Line
	2000 6850 2000 8050
Wire Wire Line
	2350 8450 2650 8450
Connection ~ 2650 8450
Wire Notes Line
	2000 9200 5000 9200
Wire Notes Line
	5000 9200 5000 8150
Wire Notes Line
	5000 8150 2000 8150
Wire Notes Line
	2000 8150 2000 9200
Wire Wire Line
	2850 2700 3050 2700
$Comp
L power:VAA #PWR08
U 1 1 5E320607
P 2950 2550
F 0 "#PWR08" H 2950 2400 50  0001 C CNN
F 1 "VAA" H 2967 2723 50  0000 C CNN
F 2 "" H 2950 2550 50  0001 C CNN
F 3 "" H 2950 2550 50  0001 C CNN
	1    2950 2550
	1    0    0    -1  
$EndComp
Connection ~ 2700 2700
Wire Wire Line
	2700 2700 1550 2700
Wire Wire Line
	2950 2550 2950 2900
$Comp
L Device:L_Core_Ferrite L1
U 1 1 5E35390B
P 5200 7350
F 0 "L1" V 5019 7350 50  0000 C CNN
F 1 "330R-1000mA" V 5110 7350 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5200 7350 50  0001 C CNN
F 3 "~" H 5200 7350 50  0001 C CNN
	1    5200 7350
	0    1    1    0   
$EndComp
$Comp
L Device:L_Core_Ferrite L2
U 1 1 5E355E47
P 5200 7700
F 0 "L2" V 5019 7700 50  0000 C CNN
F 1 "330R-1000mA" V 5110 7700 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5200 7700 50  0001 C CNN
F 3 "~" H 5200 7700 50  0001 C CNN
	1    5200 7700
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 7700 5050 7700
Connection ~ 4300 7700
Wire Wire Line
	4650 7350 5050 7350
Connection ~ 4650 7350
Wire Wire Line
	5350 7350 5750 7350
Wire Wire Line
	5350 7700 5750 7700
$Comp
L Device:C C9
U 1 1 5E3BE69B
P 5750 7550
F 0 "C9" H 5865 7596 50  0000 L CNN
F 1 "C" H 5865 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5788 7400 50  0001 C CNN
F 3 "~" H 5750 7550 50  0001 C CNN
	1    5750 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 7400 5750 7350
Wire Wire Line
	5750 7350 5750 7250
Connection ~ 5750 7350
$Comp
L power:GNDA #PWR021
U 1 1 5E3F3AC9
P 5750 7750
F 0 "#PWR021" H 5750 7500 50  0001 C CNN
F 1 "GNDA" H 5755 7577 50  0000 C CNN
F 2 "" H 5750 7750 50  0001 C CNN
F 3 "" H 5750 7750 50  0001 C CNN
	1    5750 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 7750 5750 7700
Connection ~ 5750 7700
$Comp
L power:VAA #PWR020
U 1 1 5E40EBBD
P 5750 7250
F 0 "#PWR020" H 5750 7100 50  0001 C CNN
F 1 "VAA" H 5767 7423 50  0000 C CNN
F 2 "" H 5750 7250 50  0001 C CNN
F 3 "" H 5750 7250 50  0001 C CNN
	1    5750 7250
	1    0    0    -1  
$EndComp
Text Notes 5950 8000 0    50   ~ 0
Analog\nVoltage\nSupply
Wire Notes Line
	6300 8050 6300 6850
Wire Notes Line
	2000 8050 6300 8050
Wire Notes Line
	2000 6850 6300 6850
Text Label 9600 5350 0    50   ~ 0
CLK_OUT
Text Label 9600 5650 0    50   ~ 0
CLK_IN
Wire Wire Line
	9600 5650 9900 5650
Wire Wire Line
	9500 3450 9650 3450
Wire Wire Line
	9500 3350 9650 3350
Text Label 9650 3350 0    50   ~ 0
CLK_OUT
Text Label 9650 3450 0    50   ~ 0
CLK_IN
Wire Wire Line
	10250 3400 10250 3650
Wire Wire Line
	10050 3050 10050 3550
Connection ~ 10250 3050
Wire Wire Line
	10050 3050 10250 3050
Connection ~ 10600 3050
Wire Wire Line
	10250 3050 10250 3100
Wire Wire Line
	10600 3050 10250 3050
Wire Wire Line
	10600 3050 10600 3100
Wire Wire Line
	10700 3050 10600 3050
Wire Wire Line
	10250 3850 10600 3850
Wire Wire Line
	10600 3850 10700 3850
Connection ~ 10600 3850
Wire Wire Line
	10600 3400 10600 3850
$Comp
L power:GNDA #PWR040
U 1 1 5E688F77
P 10700 3850
F 0 "#PWR040" H 10700 3600 50  0001 C CNN
F 1 "GNDA" V 10705 3722 50  0000 R CNN
F 2 "" H 10700 3850 50  0001 C CNN
F 3 "" H 10700 3850 50  0001 C CNN
	1    10700 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C17
U 1 1 5E6723D5
P 10250 3250
F 0 "C17" H 10135 3296 50  0000 R CNN
F 1 "100nF" H 10135 3205 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10288 3100 50  0001 C CNN
F 3 "~" H 10250 3250 50  0001 C CNN
	1    10250 3250
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5E50383A
P 10600 3250
F 0 "C19" H 10485 3296 50  0000 R CNN
F 1 "1uF" H 10485 3205 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10638 3100 50  0001 C CNN
F 3 "~" H 10600 3250 50  0001 C CNN
	1    10600 3250
	-1   0    0    -1  
$EndComp
$Comp
L power:VAA #PWR039
U 1 1 5E501F01
P 10700 3050
F 0 "#PWR039" H 10700 2900 50  0001 C CNN
F 1 "VAA" V 10717 3178 50  0000 L CNN
F 2 "" H 10700 3050 50  0001 C CNN
F 3 "" H 10700 3050 50  0001 C CNN
	1    10700 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	9500 3550 10050 3550
Connection ~ 10050 3550
Wire Wire Line
	10050 3550 10050 3750
Wire Wire Line
	10050 3750 9500 3750
Wire Wire Line
	9500 3650 10250 3650
Connection ~ 10250 3650
Wire Wire Line
	9500 3850 10250 3850
Wire Wire Line
	10950 3950 11200 3950
Connection ~ 10950 3950
Text Label 11200 3950 0    50   ~ 0
V_OUT
Text Notes 9800 7350 0    50   ~ 0
CVBS Output\nFor Focusing
$Comp
L power:GND #PWR035
U 1 1 5E88867E
P 9600 7000
F 0 "#PWR035" H 9600 6750 50  0001 C CNN
F 1 "GND" H 9605 6827 50  0000 C CNN
F 2 "" H 9600 7000 50  0001 C CNN
F 3 "" H 9600 7000 50  0001 C CNN
	1    9600 7000
	1    0    0    -1  
$EndComp
Text Label 8850 6750 0    50   ~ 0
V_OUT
Wire Wire Line
	8850 6750 9400 6750
Wire Notes Line
	10350 7400 8700 7400
$Comp
L Connector:Conn_Coaxial J9
U 1 1 5E8EDB3D
P 9600 6750
F 0 "J9" H 9700 6725 50  0000 L CNN
F 1 "Conn_Coaxial" H 9700 6634 50  0000 L CNN
F 2 "RCA-Jack:RCJ-04_THT_Horz" H 9600 6750 50  0001 C CNN
F 3 " ~" H 9600 6750 50  0001 C CNN
	1    9600 6750
	1    0    0    -1  
$EndComp
Wire Notes Line
	8700 6600 10350 6600
Wire Wire Line
	9600 6950 9600 7000
Wire Wire Line
	3050 5100 3050 5150
Connection ~ 3050 5100
Wire Wire Line
	3300 5100 3300 5150
Text Notes 3550 5350 0    50   ~ 0
Image\nSensor
Wire Notes Line
	750  5400 750  1800
Text Notes 10650 5900 0    50   ~ 0
VC0703 Resonator
Wire Notes Line
	9450 5250 11400 5250
Wire Notes Line
	11400 5250 11400 6000
Wire Notes Line
	11400 6000 9450 6000
Wire Notes Line
	9450 6000 9450 5250
Wire Wire Line
	3850 3250 4950 3250
$Comp
L Connector:TestPoint TP4
U 1 1 5EC001DA
P 4950 3250
F 0 "TP4" V 4904 3438 50  0000 L CNN
F 1 "TestPoint" V 4995 3438 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 5150 3250 50  0001 C CNN
F 3 "~" H 5150 3250 50  0001 C CNN
	1    4950 3250
	0    1    1    0   
$EndComp
Wire Notes Line
	10350 6600 10350 7400
Wire Notes Line
	8700 6600 8700 7400
$Comp
L Connector:Conn_01x04_Male J11
U 1 1 5D6982A5
P 12750 1900
F 0 "J11" H 12722 1782 50  0000 R CNN
F 1 "Conn_01x04_Male" H 12722 1873 50  0000 R CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 12750 1900 50  0001 C CNN
F 3 "~" H 12750 1900 50  0001 C CNN
	1    12750 1900
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR052
U 1 1 5D6997D1
P 12400 1700
F 0 "#PWR052" H 12400 1550 50  0001 C CNN
F 1 "+5V" H 12415 1873 50  0000 C CNN
F 2 "" H 12400 1700 50  0001 C CNN
F 3 "" H 12400 1700 50  0001 C CNN
	1    12400 1700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR053
U 1 1 5D69A210
P 12400 1800
F 0 "#PWR053" H 12400 1550 50  0001 C CNN
F 1 "GND" V 12405 1672 50  0000 R CNN
F 2 "" H 12400 1800 50  0001 C CNN
F 3 "" H 12400 1800 50  0001 C CNN
	1    12400 1800
	0    1    1    0   
$EndComp
Text Label 11800 1900 0    50   ~ 0
TEST_RX
Text Label 11800 2000 0    50   ~ 0
TEST_TX
Wire Wire Line
	11800 2000 12550 2000
Wire Wire Line
	11800 1900 12550 1900
Wire Wire Line
	12400 1800 12550 1800
Wire Wire Line
	12400 1700 12550 1700
Text Notes 12950 3700 0    50   ~ 0
MCU Connector
Text Notes 12900 2150 0    50   ~ 0
UART (Serial X)
$Comp
L Connector:Conn_01x06_Male J10
U 1 1 5D7929D4
P 12750 1000
F 0 "J10" H 12722 882 50  0000 R CNN
F 1 "Conn_01x06_Male" H 12722 973 50  0000 R CNN
F 2 "Connector_JST:JST_PH_B6B-PH-K_1x06_P2.00mm_Vertical" H 12750 1000 50  0001 C CNN
F 3 "~" H 12750 1000 50  0001 C CNN
	1    12750 1000
	-1   0    0    1   
$EndComp
Text Label 11700 900  0    50   ~ 0
MCU_MISO_5V
Text Label 11700 1000 0    50   ~ 0
MCU_MOSI_5V
Text Label 11700 1100 0    50   ~ 0
MCU_SCK_5v
Text Label 11700 1200 0    50   ~ 0
MCU_CS_5v
$Comp
L power:+5V #PWR050
U 1 1 5D79822C
P 12350 700
F 0 "#PWR050" H 12350 550 50  0001 C CNN
F 1 "+5V" V 12365 828 50  0000 L CNN
F 2 "" H 12350 700 50  0001 C CNN
F 3 "" H 12350 700 50  0001 C CNN
	1    12350 700 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR051
U 1 1 5D799520
P 12350 800
F 0 "#PWR051" H 12350 550 50  0001 C CNN
F 1 "GND" V 12355 672 50  0000 R CNN
F 2 "" H 12350 800 50  0001 C CNN
F 3 "" H 12350 800 50  0001 C CNN
	1    12350 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	11700 1200 12550 1200
Wire Wire Line
	11700 1100 12550 1100
Wire Wire Line
	11700 1000 12550 1000
Wire Wire Line
	11700 900  12550 900 
Wire Wire Line
	12350 800  12550 800 
Wire Wire Line
	12350 700  12550 700 
Text Notes 12950 1350 0    50   ~ 0
SPI Breakout
Wire Notes Line
	13550 1400 11650 1400
Wire Notes Line
	11650 1400 11650 600 
Wire Notes Line
	11650 600  13550 600 
Wire Notes Line
	13550 600  13550 1400
Wire Notes Line
	13550 2200 11650 2200
Wire Notes Line
	11650 2200 11650 1550
Wire Notes Line
	11650 1550 13550 1550
Wire Notes Line
	13550 1550 13550 2200
Wire Notes Line
	11600 550  13600 550 
Wire Wire Line
	1900 4600 1900 5100
Wire Wire Line
	1900 4500 1900 3800
Text Notes 1950 5000 0    50   ~ 0
Exposure\nOveride
Wire Notes Line
	2350 5050 1600 5050
Wire Notes Line
	1600 5050 1600 4400
Wire Notes Line
	1600 4400 2350 4400
Wire Notes Line
	2350 4400 2350 5050
Text Notes 5950 5100 0    50   ~ 0
Sync Line Jumpers\n
Wire Notes Line
	4600 5150 4600 3750
Wire Notes Line
	4600 3750 6800 3750
Wire Notes Line
	6800 3750 6800 5150
Wire Notes Line
	4600 5150 6800 5150
Wire Wire Line
	950  4300 2600 4300
Wire Wire Line
	950  2700 1250 2700
Wire Notes Line
	13550 3600 13550 2250
Wire Notes Line
	13550 2250 11650 2250
Text Notes 10550 9500 0    50   ~ 0
Support
Text Notes 11300 6200 0    50   ~ 0
Core
Wire Notes Line
	11550 6250 650  6250
Wire Notes Line
	650  6250 650  550 
Wire Notes Line
	650  550  11550 550 
Wire Notes Line
	11550 550  11550 6250
Wire Notes Line
	750  1800 7500 1800
Wire Notes Line
	750  5400 7500 5400
Wire Notes Line
	650  9550 650  6450
Wire Notes Line
	11100 6450 11100 9550
Wire Notes Line
	13600 550  13600 3750
Connection ~ 10250 3850
Wire Wire Line
	10250 3650 10250 3850
$Comp
L Connector:Micro_SD_Card J3
U 1 1 5D65E2BC
P 2950 10600
F 0 "J3" H 2900 11317 50  0000 C CNN
F 1 "Micro_SD_Card" H 2900 11226 50  0000 C CNN
F 2 "SD-Conn-Molex:MOLEX_503182-1852" H 4100 10900 50  0001 C CNN
F 3 "http://katalog.we-online.de/em/datasheet/693072010801.pdf" H 2950 10600 50  0001 C CNN
	1    2950 10600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5D6E09BA
P 3750 11400
F 0 "#PWR013" H 3750 11150 50  0001 C CNN
F 1 "GND" H 3755 11227 50  0000 C CNN
F 2 "" H 3750 11400 50  0001 C CNN
F 3 "" H 3750 11400 50  0001 C CNN
	1    3750 11400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 11400 3750 11200
Wire Wire Line
	2050 10400 1650 10400
Wire Wire Line
	2050 10600 1100 10600
Wire Wire Line
	1100 10600 1100 10000
Wire Wire Line
	2050 10900 1650 10900
Wire Wire Line
	2050 10800 1150 10800
Wire Wire Line
	1150 10800 1150 11250
Wire Wire Line
	2050 10700 1650 10700
Wire Wire Line
	2050 10500 1650 10500
Text Label 13300 8300 0    50   ~ 0
MCU_MISO_5V
Text Label 11900 8300 0    50   ~ 0
MCU_MISO
Wire Notes Line
	11600 550  11600 3750
Wire Notes Line
	13600 3750 11600 3750
Wire Notes Line
	11650 3600 13550 3600
Wire Notes Line
	11650 2250 11650 3600
Text Notes 13200 9800 0    50   ~ 0
Level Shifting
$Comp
L Arduino_Mega:ARDUINO_MEGA2560_REV3_-_RETAIL A1
U 1 1 5DED357B
P 16150 3150
F 0 "A1" H 16150 5617 50  0000 C CNN
F 1 "ARDUINO_MEGA2560_REV3_-_RETAIL" H 16150 5526 50  0000 C CNN
F 2 "Arduino_Mega:ARDUINO_MEGA2560_REV3_-_RETAIL" H 16150 3150 50  0001 L BNN
F 3 "" H 16150 3150 50  0001 L BNN
F 4 "None" H 16150 3150 50  0001 L BNN "Field5"
F 5 "Arduino" H 16150 3150 50  0001 L BNN "Field6"
F 6 "ARDUINO MEGA2560 REV3 - RETAIL" H 16150 3150 50  0001 L BNN "Field7"
F 7 "Dev.kit: Arduino; SPI, TWI, UART; ICSP, USB B, pin strips, supply" H 16150 3150 50  0001 L BNN "Field8"
	1    16150 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR056
U 1 1 5DFE03FA
P 15000 1550
F 0 "#PWR056" H 15000 1400 50  0001 C CNN
F 1 "+5V" V 15015 1678 50  0000 L CNN
F 2 "" H 15000 1550 50  0001 C CNN
F 3 "" H 15000 1550 50  0001 C CNN
	1    15000 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR059
U 1 1 5DFE12E0
P 15050 3850
F 0 "#PWR059" H 15050 3700 50  0001 C CNN
F 1 "+5V" V 15065 3978 50  0000 L CNN
F 2 "" H 15050 3850 50  0001 C CNN
F 3 "" H 15050 3850 50  0001 C CNN
	1    15050 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15000 1550 15350 1550
Wire Wire Line
	15050 3850 15350 3850
$Comp
L power:GND #PWR058
U 1 1 5E01CCF2
P 15000 5550
F 0 "#PWR058" H 15000 5300 50  0001 C CNN
F 1 "GND" V 15005 5422 50  0000 R CNN
F 2 "" H 15000 5550 50  0001 C CNN
F 3 "" H 15000 5550 50  0001 C CNN
	1    15000 5550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR057
U 1 1 5E01D9E7
P 15000 1650
F 0 "#PWR057" H 15000 1400 50  0001 C CNN
F 1 "GND" V 15005 1522 50  0000 R CNN
F 2 "" H 15000 1650 50  0001 C CNN
F 3 "" H 15000 1650 50  0001 C CNN
	1    15000 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	15000 1650 15200 1650
Wire Wire Line
	15350 1750 15200 1750
Wire Wire Line
	15200 1750 15200 1650
Connection ~ 15200 1650
Wire Wire Line
	15200 1650 15350 1650
Wire Wire Line
	15000 5550 15350 5550
Wire Wire Line
	16950 3250 17150 3250
Wire Wire Line
	16950 3150 17150 3150
Text Label 17150 3150 0    50   ~ 0
TEST_TX_5V
Text Label 17150 3250 0    50   ~ 0
TEST_RX_5V
Text Label 17100 5350 0    50   ~ 0
MCU_MISO_5V
Text Label 17100 5450 0    50   ~ 0
MCU_SCK_5v
Text Label 14750 5450 0    50   ~ 0
MCU_SD_CS_5v
Text Label 14750 5350 0    50   ~ 0
MCU_MOSI_5V
Wire Wire Line
	14750 5450 15350 5450
Wire Wire Line
	14750 5350 15350 5350
Wire Wire Line
	16950 5350 17100 5350
Wire Wire Line
	16950 5450 17100 5450
$Comp
L Connector:USB_B J1
U 1 1 5D664E4E
P 1150 7350
F 0 "J1" H 1207 7817 50  0000 C CNN
F 1 "USB_B" H 1207 7726 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 1300 7300 50  0001 C CNN
F 3 " ~" H 1300 7300 50  0001 C CNN
	1    1150 7350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5D668862
P 1550 7150
F 0 "#PWR04" H 1550 7000 50  0001 C CNN
F 1 "+5V" V 1565 7278 50  0000 L CNN
F 2 "" H 1550 7150 50  0001 C CNN
F 3 "" H 1550 7150 50  0001 C CNN
	1    1550 7150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D669656
P 1150 7850
F 0 "#PWR02" H 1150 7600 50  0001 C CNN
F 1 "GND" H 1155 7677 50  0000 C CNN
F 2 "" H 1150 7850 50  0001 C CNN
F 3 "" H 1150 7850 50  0001 C CNN
	1    1150 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7750 1050 7800
Wire Wire Line
	1050 7800 1150 7800
Wire Wire Line
	1150 7800 1150 7750
Wire Wire Line
	1150 7800 1150 7850
Connection ~ 1150 7800
Wire Wire Line
	1550 7150 1450 7150
Wire Notes Line
	650  6450 11100 6450
Wire Notes Line
	650  9550 11100 9550
Text Notes 1100 8250 0    50   ~ 0
External 5V In Only\nNo Data
Wire Notes Line
	1900 8250 750  8250
Wire Notes Line
	750  8250 750  6800
Wire Notes Line
	750  6800 1900 6800
Wire Notes Line
	1900 6800 1900 8250
$Comp
L power:GND #PWR060
U 1 1 5D8017CB
P 17050 1250
F 0 "#PWR060" H 17050 1000 50  0001 C CNN
F 1 "GND" V 17055 1122 50  0000 R CNN
F 2 "" H 17050 1250 50  0001 C CNN
F 3 "" H 17050 1250 50  0001 C CNN
	1    17050 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 1250 17050 1250
Text Label 1650 10400 0    50   ~ 0
MCU_SD_CS
Text Label 1650 10500 0    50   ~ 0
MCU_MISO
Text Label 1650 10700 0    50   ~ 0
MCU_SCK
Text Label 1650 10900 0    50   ~ 0
MCU_MISO
$Comp
L power:+3V3 #PWR01
U 1 1 5D844DF7
P 1100 10000
F 0 "#PWR01" H 1100 9850 50  0001 C CNN
F 1 "+3V3" H 1115 10173 50  0000 C CNN
F 2 "" H 1100 10000 50  0001 C CNN
F 3 "" H 1100 10000 50  0001 C CNN
	1    1100 10000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5D845A27
P 1150 11250
F 0 "#PWR03" H 1150 11000 50  0001 C CNN
F 1 "GND" H 1155 11077 50  0000 C CNN
F 2 "" H 1150 11250 50  0001 C CNN
F 3 "" H 1150 11250 50  0001 C CNN
	1    1150 11250
	1    0    0    -1  
$EndComp
Text Notes 3550 11800 0    50   ~ 0
SD Card
Wire Notes Line
	700  11900 3950 11900
Wire Notes Line
	3950 11900 3950 9750
Wire Notes Line
	3950 9750 700  9750
Wire Notes Line
	700  9750 700  11900
Text Label 17100 5250 0    50   ~ 0
MCU_CS_CS_5V
Wire Wire Line
	16950 5250 17100 5250
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BSS138 Q2
U 1 1 5DE0A37E
P 12900 8300
F 0 "Q2" V 13061 8300 60  0000 C CNN
F 1 "BSS138" V 13167 8300 60  0000 C CNN
F 2 "digikey-footprints:SOT-23-3" H 13100 8500 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 8600 60  0001 L CNN
F 4 "BSS138CT-ND" H 13100 8700 60  0001 L CNN "Digi-Key_PN"
F 5 "BSS138" H 13100 8800 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 13100 8900 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 13100 9000 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 9100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BSS138/BSS138CT-ND/244294" H 13100 9200 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 50V 220MA SOT-23" H 13100 9300 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 13100 9400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 13100 9500 60  0001 L CNN "Status"
	1    12900 8300
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5DE0A388
P 12500 8050
F 0 "R13" H 12570 8096 50  0000 L CNN
F 1 "10K" H 12570 8005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12430 8050 50  0001 C CNN
F 3 "~" H 12500 8050 50  0001 C CNN
	1    12500 8050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5DE0A392
P 13200 8050
F 0 "R15" H 13270 8096 50  0000 L CNN
F 1 "10K" H 13270 8005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 13130 8050 50  0001 C CNN
F 3 "~" H 13200 8050 50  0001 C CNN
	1    13200 8050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR044
U 1 1 5DE0A39C
P 12500 7800
F 0 "#PWR044" H 12500 7650 50  0001 C CNN
F 1 "+3V3" H 12515 7973 50  0000 C CNN
F 2 "" H 12500 7800 50  0001 C CNN
F 3 "" H 12500 7800 50  0001 C CNN
	1    12500 7800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR048
U 1 1 5DE0A3A6
P 13200 7800
F 0 "#PWR048" H 13200 7650 50  0001 C CNN
F 1 "+5V" H 13215 7973 50  0000 C CNN
F 2 "" H 13200 7800 50  0001 C CNN
F 3 "" H 13200 7800 50  0001 C CNN
	1    13200 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	12500 7800 12500 7850
Wire Wire Line
	12500 7850 12800 7850
Wire Wire Line
	12800 7850 12800 8000
Connection ~ 12500 7850
Wire Wire Line
	12500 7850 12500 7900
Wire Wire Line
	13200 7800 13200 7900
Wire Wire Line
	13100 8300 13200 8300
Wire Wire Line
	13200 8300 13200 8200
Wire Wire Line
	12700 8300 12500 8300
Wire Wire Line
	12500 8300 12500 8200
Wire Wire Line
	13300 8300 13200 8300
Connection ~ 13200 8300
Connection ~ 12500 8300
Wire Wire Line
	11900 8300 12500 8300
Wire Notes Line
	17100 3050 17100 3350
Wire Notes Line
	17100 3350 18200 3350
Wire Notes Line
	18200 3350 18200 3050
Wire Notes Line
	18200 3050 17100 3050
Text Notes 17850 3300 0    50   ~ 0
Serial1
Text Label 14750 5250 0    50   ~ 0
OLED_CS_5V
Wire Wire Line
	14750 5250 15350 5250
Text Label 11900 5150 0    50   ~ 0
OLED_CS_5V
Text Label 13250 5250 0    50   ~ 0
OLED_CS
$Comp
L power:+5V #PWR017
U 1 1 5E0BE6A8
P 5000 10150
F 0 "#PWR017" H 5000 10000 50  0001 C CNN
F 1 "+5V" V 5015 10278 50  0000 L CNN
F 2 "" H 5000 10150 50  0001 C CNN
F 3 "" H 5000 10150 50  0001 C CNN
	1    5000 10150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5E0BF60D
P 5000 10050
F 0 "#PWR016" H 5000 9800 50  0001 C CNN
F 1 "GND" V 5005 9922 50  0000 R CNN
F 2 "" H 5000 10050 50  0001 C CNN
F 3 "" H 5000 10050 50  0001 C CNN
	1    5000 10050
	0    -1   -1   0   
$EndComp
Text Label 5000 10450 0    50   ~ 0
MCU_MISO
Text Label 5000 11050 0    50   ~ 0
MCU_MOSI
Text Label 5000 10950 0    50   ~ 0
MCU_SCK
Text Label 5000 10650 0    50   ~ 0
OLED_CS
Text Label 5000 10850 0    50   ~ 0
OLED_DC
Text Label 14750 5150 0    50   ~ 0
OLED_DC_5V
Wire Wire Line
	14750 5150 15350 5150
Text Label 13250 5350 0    50   ~ 0
OLED_DC
Text Label 11900 5250 0    50   ~ 0
OLED_DC_5V
Text Label 5000 10550 0    50   ~ 0
MCU_EXT_SD_CS
Text Label 5000 10750 0    50   ~ 0
OLED_RS
Text Label 14750 5050 0    50   ~ 0
OLED_RS_5V
Wire Wire Line
	14750 5050 15350 5050
Text Label 11900 5350 0    50   ~ 0
OLED_RS_5V
Text Label 13250 5450 0    50   ~ 0
OLED_RS
$Comp
L Connector:Conn_01x11_Male J4
U 1 1 5E3F138E
P 4500 10550
F 0 "J4" H 4608 11231 50  0000 C CNN
F 1 "Conn_01x11_Male" H 4608 11140 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x11_P2.54mm_Vertical" H 4500 10550 50  0001 C CNN
F 3 "~" H 4500 10550 50  0001 C CNN
	1    4500 10550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 10050 5000 10050
Wire Wire Line
	4700 10150 5000 10150
Wire Wire Line
	4700 10450 5000 10450
Wire Wire Line
	4700 10550 5000 10550
Wire Wire Line
	4700 10650 5000 10650
Wire Wire Line
	4700 10750 5000 10750
Wire Wire Line
	4700 10850 5000 10850
Wire Wire Line
	4700 10950 5000 10950
Wire Wire Line
	4700 11050 5000 11050
Text Notes 4800 10350 0    50   ~ 0
3v3 and CD Unused
Wire Notes Line
	4100 11250 5750 11250
Wire Notes Line
	5750 9750 4100 9750
Wire Notes Line
	14050 5900 18550 5900
Wire Notes Line
	18550 5900 18550 600 
Wire Notes Line
	18550 600  14050 600 
Wire Notes Line
	14050 600  14050 5900
Text Notes 4900 11200 0    50   ~ 0
Adafruit OLED Screen
Wire Notes Line
	5750 9750 5750 11250
Wire Notes Line
	4100 9750 4100 11250
Text Label 5250 12000 0    50   ~ 0
MCU_EXT_SD_CS
Text Label 4150 12000 0    50   ~ 0
MCU_SD_CS
$Comp
L Device:R R3
U 1 1 5D894616
P 5100 11750
F 0 "R3" H 5170 11796 50  0000 L CNN
F 1 "10K" H 5170 11705 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5030 11750 50  0001 C CNN
F 3 "~" H 5100 11750 50  0001 C CNN
	1    5100 11750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR018
U 1 1 5D923472
P 5100 11550
F 0 "#PWR018" H 5100 11400 50  0001 C CNN
F 1 "+3V3" H 5115 11723 50  0000 C CNN
F 2 "" H 5100 11550 50  0001 C CNN
F 3 "" H 5100 11550 50  0001 C CNN
	1    5100 11550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 11550 5100 11600
Wire Wire Line
	5100 11900 5100 12000
Wire Wire Line
	5100 12000 5250 12000
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 5D9B5E9B
P 4850 12200
F 0 "J6" V 5004 12012 50  0000 R CNN
F 1 "Conn_01x02_Male" V 4913 12012 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4850 12200 50  0001 C CNN
F 3 "~" H 4850 12200 50  0001 C CNN
	1    4850 12200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 12000 5100 12000
Connection ~ 5100 12000
Wire Wire Line
	4150 12000 4850 12000
Text Notes 4950 12350 0    50   ~ 0
External SD Card Dissable
Wire Notes Line
	6050 12400 4100 12400
Wire Notes Line
	4100 12400 4100 11300
Wire Notes Line
	4100 11300 6050 11300
Wire Notes Line
	6050 11300 6050 12400
$Comp
L power:GND #PWR061
U 1 1 5DB10FB6
P 17100 5550
F 0 "#PWR061" H 17100 5300 50  0001 C CNN
F 1 "GND" V 17105 5422 50  0000 R CNN
F 2 "" H 17100 5550 50  0001 C CNN
F 3 "" H 17100 5550 50  0001 C CNN
	1    17100 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 5550 17100 5550
$Comp
L power:+5V #PWR062
U 1 1 5DB44FF9
P 17400 3850
F 0 "#PWR062" H 17400 3700 50  0001 C CNN
F 1 "+5V" V 17415 3978 50  0000 L CNN
F 2 "" H 17400 3850 50  0001 C CNN
F 3 "" H 17400 3850 50  0001 C CNN
	1    17400 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	16950 3850 17400 3850
Text Notes 17350 5700 0    50   ~ 0
SPI
Wire Notes Line
	17800 5750 14600 5750
Wire Notes Line
	14600 5750 14600 4950
Wire Notes Line
	14600 4950 17800 4950
Wire Notes Line
	17800 4950 17800 5750
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BSS138 Q1
U 1 1 5DC75C9D
P 12900 6950
F 0 "Q1" V 13061 6950 60  0000 C CNN
F 1 "BSS138" V 13167 6950 60  0000 C CNN
F 2 "digikey-footprints:SOT-23-3" H 13100 7150 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 7250 60  0001 L CNN
F 4 "BSS138CT-ND" H 13100 7350 60  0001 L CNN "Digi-Key_PN"
F 5 "BSS138" H 13100 7450 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 13100 7550 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 13100 7650 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 7750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BSS138/BSS138CT-ND/244294" H 13100 7850 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 50V 220MA SOT-23" H 13100 7950 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 13100 8050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 13100 8150 60  0001 L CNN "Status"
	1    12900 6950
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DC75CA7
P 12500 6700
F 0 "R12" H 12570 6746 50  0000 L CNN
F 1 "10K" H 12570 6655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12430 6700 50  0001 C CNN
F 3 "~" H 12500 6700 50  0001 C CNN
	1    12500 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5DC75CB1
P 13200 6700
F 0 "R14" H 13270 6746 50  0000 L CNN
F 1 "10K" H 13270 6655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 13130 6700 50  0001 C CNN
F 3 "~" H 13200 6700 50  0001 C CNN
	1    13200 6700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR043
U 1 1 5DC75CBB
P 12500 6450
F 0 "#PWR043" H 12500 6300 50  0001 C CNN
F 1 "+3V3" H 12515 6623 50  0000 C CNN
F 2 "" H 12500 6450 50  0001 C CNN
F 3 "" H 12500 6450 50  0001 C CNN
	1    12500 6450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR047
U 1 1 5DC75CC5
P 13200 6450
F 0 "#PWR047" H 13200 6300 50  0001 C CNN
F 1 "+5V" H 13215 6623 50  0000 C CNN
F 2 "" H 13200 6450 50  0001 C CNN
F 3 "" H 13200 6450 50  0001 C CNN
	1    13200 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	12500 6450 12500 6500
Wire Wire Line
	12500 6500 12800 6500
Wire Wire Line
	12800 6500 12800 6650
Connection ~ 12500 6500
Wire Wire Line
	12500 6500 12500 6550
Wire Wire Line
	13200 6450 13200 6550
Wire Wire Line
	13100 6950 13200 6950
Wire Wire Line
	13200 6950 13200 6850
Wire Wire Line
	12700 6950 12500 6950
Wire Wire Line
	12500 6950 12500 6850
Text Label 13300 6950 0    50   ~ 0
TEST_RX_5V
Wire Wire Line
	13300 6950 13200 6950
Connection ~ 13200 6950
Text Label 11950 6950 0    50   ~ 0
TEST_RX
Wire Wire Line
	11950 6950 12500 6950
Connection ~ 12500 6950
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BSS138 Q3
U 1 1 5DC75CE8
P 12900 9400
F 0 "Q3" V 13061 9400 60  0000 C CNN
F 1 "BSS138" V 13167 9400 60  0000 C CNN
F 2 "digikey-footprints:SOT-23-3" H 13100 9600 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 9700 60  0001 L CNN
F 4 "BSS138CT-ND" H 13100 9800 60  0001 L CNN "Digi-Key_PN"
F 5 "BSS138" H 13100 9900 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 13100 10000 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 13100 10100 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BSS138-D.PDF" H 13100 10200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BSS138/BSS138CT-ND/244294" H 13100 10300 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 50V 220MA SOT-23" H 13100 10400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 13100 10500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 13100 10600 60  0001 L CNN "Status"
	1    12900 9400
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5DC75CF2
P 12500 9150
F 0 "R16" H 12570 9196 50  0000 L CNN
F 1 "10K" H 12570 9105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12430 9150 50  0001 C CNN
F 3 "~" H 12500 9150 50  0001 C CNN
	1    12500 9150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5DC75CFC
P 13200 9150
F 0 "R18" H 13270 9196 50  0000 L CNN
F 1 "10K" H 13270 9105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 13130 9150 50  0001 C CNN
F 3 "~" H 13200 9150 50  0001 C CNN
	1    13200 9150
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR049
U 1 1 5DC75D06
P 12500 8900
F 0 "#PWR049" H 12500 8750 50  0001 C CNN
F 1 "+3V3" H 12515 9073 50  0000 C CNN
F 2 "" H 12500 8900 50  0001 C CNN
F 3 "" H 12500 8900 50  0001 C CNN
	1    12500 8900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR055
U 1 1 5DC75D10
P 13200 8900
F 0 "#PWR055" H 13200 8750 50  0001 C CNN
F 1 "+5V" H 13215 9073 50  0000 C CNN
F 2 "" H 13200 8900 50  0001 C CNN
F 3 "" H 13200 8900 50  0001 C CNN
	1    13200 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12500 8900 12500 8950
Wire Wire Line
	12500 8950 12800 8950
Wire Wire Line
	12800 8950 12800 9100
Connection ~ 12500 8950
Wire Wire Line
	12500 8950 12500 9000
Wire Wire Line
	13200 8900 13200 9000
Wire Wire Line
	13100 9400 13200 9400
Wire Wire Line
	13200 9400 13200 9300
Wire Wire Line
	12700 9400 12500 9400
Wire Wire Line
	12500 9400 12500 9300
Wire Wire Line
	13300 9400 13200 9400
Connection ~ 13200 9400
Wire Wire Line
	11950 9400 12500 9400
Connection ~ 12500 9400
Text Label 11950 9400 0    50   ~ 0
TEST_TX
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:CD4050BE U5
U 1 1 5DD219A1
P 12950 4950
F 0 "U5" H 12850 5453 60  0000 C CNN
F 1 "CD4050BE" H 12850 5347 60  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 13150 5150 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fcd4050b" H 13150 5250 60  0001 L CNN
F 4 "296-2056-ND" H 13150 5350 60  0001 L CNN "Digi-Key_PN"
F 5 "CD4050BE" H 13150 5450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 13150 5550 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 13150 5650 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fcd4050b" H 13150 5750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/CD4050BE/296-2056-ND/67303" H 13150 5850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUFFER NON-INVERT 18 V 16DIP" H 13150 5950 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 13150 6050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 13150 6150 60  0001 L CNN "Status"
	1    12950 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR045
U 1 1 5DD59D5C
P 12950 4350
F 0 "#PWR045" H 12950 4200 50  0001 C CNN
F 1 "+3V3" H 12965 4523 50  0000 C CNN
F 2 "" H 12950 4350 50  0001 C CNN
F 3 "" H 12950 4350 50  0001 C CNN
	1    12950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 4350 12950 4650
Text Label 13250 4950 0    50   ~ 0
MCU_MOSI
Text Label 11900 4850 0    50   ~ 0
MCU_MOSI_5V
Text Label 13250 5050 0    50   ~ 0
MCU_SCK
Text Label 11900 4950 0    50   ~ 0
MCU_SCK_5v
Text Label 13250 5150 0    50   ~ 0
MCU_SD_CS
Text Label 11900 5050 0    50   ~ 0
MCU_SD_CS_5v
Wire Wire Line
	11900 4850 12550 4850
Wire Wire Line
	11900 4950 12550 4950
Wire Wire Line
	11900 5050 12550 5050
Wire Wire Line
	11900 5150 12550 5150
Wire Wire Line
	11900 5250 12550 5250
Wire Wire Line
	11900 5350 12550 5350
Wire Wire Line
	13150 4950 13250 4950
Wire Wire Line
	13150 5050 13250 5050
Wire Wire Line
	13150 5150 13250 5150
Wire Wire Line
	13150 5250 13250 5250
Wire Wire Line
	13150 5350 13250 5350
Wire Wire Line
	13150 5450 13250 5450
$Comp
L power:GND #PWR046
U 1 1 5E38D287
P 12950 5850
F 0 "#PWR046" H 12950 5600 50  0001 C CNN
F 1 "GND" H 12955 5677 50  0000 C CNN
F 2 "" H 12950 5850 50  0001 C CNN
F 3 "" H 12950 5850 50  0001 C CNN
	1    12950 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 5850 12950 5750
Text Label 13300 9400 0    50   ~ 0
TEST_TX_5V
Wire Notes Line
	11700 4050 13950 4050
Wire Notes Line
	13950 4050 13950 9850
Wire Notes Line
	13950 9850 11700 9850
Wire Notes Line
	11700 4050 11700 9850
$Comp
L MT9V034_Breakout:MT9V034 U?
U 1 1 5DB0D122
P 3100 3900
F 0 "U?" H 3225 5081 50  0000 C CNN
F 1 "MT9V034" H 3225 4990 50  0000 C CNN
F 2 "" H 3700 2900 50  0001 C CNN
F 3 "" H 3700 2900 50  0001 C CNN
	1    3100 3900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
