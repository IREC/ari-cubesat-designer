EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 8500 11000 portrait
encoding utf-8
Sheet 9 9
Title "MCU - Atmega 1281"
Date "2022-01-11"
Rev "C.2"
Comp "Aurora Research Institute"
Comment1 "Northern Images Mission Payload"
Comment2 "QUALIFICATION MODEL"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Resonator Y?
U 1 1 5FE484E3
P 2500 2300
AR Path="/5FE484E3" Ref="Y?"  Part="1" 
AR Path="/5FE16375/5FE484E3" Ref="Y1"  Part="1" 
F 0 "Y1" V 2454 2410 50  0000 L CNN
F 1 "8MHz" V 2545 2410 50  0000 L CNN
F 2 "Crystal:Resonator_SMD_muRata_CSTxExxV-3Pin_3.0x1.1mm_HandSoldering" H 2475 2300 50  0001 C CNN
F 3 "~" H 2475 2300 50  0001 C CNN
	1    2500 2300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE484E9
P 2150 2300
AR Path="/5FE484E9" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/5FE484E9" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 2150 2050 50  0001 C CNN
F 1 "GND" H 2155 2127 50  0000 C CNN
F 2 "" H 2150 2300 50  0001 C CNN
F 3 "" H 2150 2300 50  0001 C CNN
	1    2150 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 2300 2300 2300
$Comp
L Device:R R?
U 1 1 5FE484F4
P 1700 2300
AR Path="/5FE484F4" Ref="R?"  Part="1" 
AR Path="/5FE16375/5FE484F4" Ref="R1"  Part="1" 
F 0 "R1" H 1770 2346 50  0000 L CNN
F 1 "1M" H 1770 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 2300 50  0001 C CNN
F 3 "~" H 1700 2300 50  0001 C CNN
	1    1700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2150 2500 2150
Wire Wire Line
	2500 2450 1700 2450
$Comp
L power:+3V3 #PWR?
U 1 1 5FE48505
P 3950 800
AR Path="/5FE48505" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/5FE48505" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 3950 650 50  0001 C CNN
F 1 "+3V3" H 3965 973 50  0000 C CNN
F 2 "" H 3950 800 50  0001 C CNN
F 3 "" H 3950 800 50  0001 C CNN
	1    3950 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1100 3950 1100
Connection ~ 3950 1100
Wire Wire Line
	3950 800  3950 850 
$Comp
L Device:C C?
U 1 1 5FE4850E
P 4250 1000
AR Path="/5FE4850E" Ref="C?"  Part="1" 
AR Path="/5FE16375/5FE4850E" Ref="C2"  Part="1" 
F 0 "C2" H 4365 1046 50  0000 L CNN
F 1 "0.1uF" H 4365 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4288 850 50  0001 C CNN
F 3 "~" H 4250 1000 50  0001 C CNN
	1    4250 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE48514
P 4650 1000
AR Path="/5FE48514" Ref="C?"  Part="1" 
AR Path="/5FE16375/5FE48514" Ref="C3"  Part="1" 
F 0 "C3" H 4765 1046 50  0000 L CNN
F 1 "0.1uF" H 4765 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4688 850 50  0001 C CNN
F 3 "~" H 4650 1000 50  0001 C CNN
	1    4650 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE4851A
P 5050 1000
AR Path="/5FE4851A" Ref="C?"  Part="1" 
AR Path="/5FE16375/5FE4851A" Ref="C4"  Part="1" 
F 0 "C4" H 5165 1046 50  0000 L CNN
F 1 "10uF" H 5165 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5088 850 50  0001 C CNN
F 3 "~" H 5050 1000 50  0001 C CNN
	1    5050 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 850  4250 850 
Connection ~ 3950 850 
Wire Wire Line
	3950 850  3950 1100
Wire Wire Line
	5500 1150 5500 1250
$Comp
L power:GND #PWR?
U 1 1 5FE48524
P 5500 1250
AR Path="/5FE48524" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/5FE48524" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 5500 1000 50  0001 C CNN
F 1 "GND" H 5505 1077 50  0000 C CNN
F 2 "" H 5500 1250 50  0001 C CNN
F 3 "" H 5500 1250 50  0001 C CNN
	1    5500 1250
	1    0    0    -1  
$EndComp
Connection ~ 5050 1150
Wire Wire Line
	4650 850  5050 850 
Connection ~ 4650 850 
Wire Wire Line
	4650 1150 5050 1150
Connection ~ 4650 1150
Wire Wire Line
	4250 1150 4650 1150
Wire Wire Line
	4250 850  4650 850 
Connection ~ 4250 850 
Text Label 2800 2600 0    50   ~ 0
AVR_AREF
$Comp
L Device:C C?
U 1 1 5FE48536
P 1450 2600
AR Path="/5FE48536" Ref="C?"  Part="1" 
AR Path="/5FE16375/5FE48536" Ref="C1"  Part="1" 
F 0 "C1" H 1565 2646 50  0000 L CNN
F 1 "0.1uF" H 1565 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1488 2450 50  0001 C CNN
F 3 "~" H 1450 2600 50  0001 C CNN
	1    1450 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE4853C
P 1200 2600
AR Path="/5FE4853C" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/5FE4853C" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 1200 2350 50  0001 C CNN
F 1 "GND" H 1205 2427 50  0000 C CNN
F 2 "" H 1200 2600 50  0001 C CNN
F 3 "" H 1200 2600 50  0001 C CNN
	1    1200 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 2600 1300 2600
Text Label 3100 3800 0    50   ~ 0
ADC0
Text Label 3100 3900 0    50   ~ 0
ADC1
Text Label 3100 4000 0    50   ~ 0
ADC2
Text Label 3100 4100 0    50   ~ 0
ADC3
Wire Wire Line
	2950 4400 3350 4400
Wire Wire Line
	2950 3900 3350 3900
Wire Wire Line
	2950 4100 3350 4100
Wire Wire Line
	2950 4000 3350 4000
Text Label 3100 4200 0    50   ~ 0
ADC4
Text Label 3100 4300 0    50   ~ 0
ADC5
Text Label 3100 4400 0    50   ~ 0
ADC6
Text Label 3100 4500 0    50   ~ 0
ADC7
Wire Wire Line
	2950 4200 3350 4200
Wire Wire Line
	2950 4300 3350 4300
Wire Wire Line
	2950 4500 3350 4500
Text Notes 2100 3800 0    50   ~ 0
IDE: A0
Text Notes 2100 3900 0    50   ~ 0
IDE: A1\n
Text Notes 2100 4000 0    50   ~ 0
IDE: A2\n
Text Notes 2100 4100 0    50   ~ 0
IDE: A3\n
Text Notes 2100 4200 0    50   ~ 0
IDE: A4\n
Text Notes 2100 4300 0    50   ~ 0
IDE: A5\n
Text Notes 2100 4550 0    50   ~ 0
IDE: A7
Text Notes 5250 5000 0    50   ~ 0
IDE: Serial1
Text Notes 5250 4700 0    50   ~ 0
IDE: I2C
Wire Wire Line
	4050 1100 4050 1350
Wire Wire Line
	3950 1100 3950 1700
$Comp
L Device:Ferrite_Bead FB?
U 1 1 5FE4857C
P 4050 1500
AR Path="/5FE4857C" Ref="FB?"  Part="1" 
AR Path="/5FE16375/5FE4857C" Ref="FB1"  Part="1" 
F 0 "FB1" H 4187 1546 50  0000 L CNN
F 1 "MH2029-300Y" H 4187 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 1500 50  0001 C CNN
F 3 "~" H 4050 1500 50  0001 C CNN
	1    4050 1500
	1    0    0    -1  
$EndComp
Text Notes 1100 700  0    79   ~ 0
Arduino MEGA 1281 Core
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5FE48583
P 6750 5400
AR Path="/5FE48583" Ref="J?"  Part="1" 
AR Path="/5FE16375/5FE48583" Ref="J6"  Part="1" 
F 0 "J6" H 6858 5581 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6858 5490 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6750 5400 50  0001 C CNN
F 3 "~" H 6750 5400 50  0001 C CNN
	1    6750 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4550 5000 5850 5000
Wire Wire Line
	4550 4900 5900 4900
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5FE4858F
P 7700 4400
AR Path="/5FE4858F" Ref="J?"  Part="1" 
AR Path="/5FE16375/5FE4858F" Ref="J3"  Part="1" 
F 0 "J3" H 7808 4581 50  0000 C CNN
F 1 "Conn_01x02_Male" H 7808 4490 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7700 4400 50  0001 C CNN
F 3 "~" H 7700 4400 50  0001 C CNN
	1    7700 4400
	-1   0    0    -1  
$EndComp
Text Notes 1300 2950 0    50   ~ 0
IDE: D26\n
Text Notes 1300 3050 0    50   ~ 0
IDE: D27\n
Wire Wire Line
	2450 2900 3350 2900
Wire Wire Line
	2450 3000 3350 3000
Text HLabel 2950 4400 0    50   Input ~ 0
TEMP00
Text HLabel 2950 3900 0    50   Input ~ 0
TEMP01
Text HLabel 2950 4000 0    50   Input ~ 0
TEMP02
Text HLabel 2950 4100 0    50   Input ~ 0
TEMP03
Text HLabel 2950 4200 0    50   Input ~ 0
TEMP04
Text HLabel 2950 4300 0    50   Input ~ 0
TEMP05
Text HLabel 4750 3800 2    50   Input ~ 0
ENIM-ENABLE
Text HLabel 2950 4500 0    50   Input ~ 0
ENIM-LIGHT
Text HLabel 6100 4900 2    50   Input ~ 0
AVR_RX1
Wire Wire Line
	6550 5500 5850 5500
Wire Wire Line
	5850 5000 5850 5500
Wire Wire Line
	5900 4900 5900 5400
Wire Wire Line
	5900 4900 6100 4900
Connection ~ 5900 4900
Text HLabel 6100 5000 2    50   Input ~ 0
AVR_TX1
Wire Wire Line
	5850 5000 6100 5000
Connection ~ 5850 5000
Text HLabel 2450 2900 0    50   Input ~ 0
Screen_PWR_Control
Text HLabel 2450 3000 0    50   Input ~ 0
Cam_PWR_Control
$Comp
L NIM-PC104-rescue:ATmega1281-16AU-MCU_Microchip_ATmega U1
U 1 1 5FC2F34B
P 3950 3700
AR Path="/5FC2F34B" Ref="U1"  Part="1" 
AR Path="/5FE16375/5FC2F34B" Ref="U1"  Part="1" 
F 0 "U1" H 3950 1611 50  0000 C CNN
F 1 "ATmega1281-16AU" H 3950 1520 50  0000 C CNN
F 2 "Package_QFP:TQFP-64_14x14mm_P0.8mm" H 3950 3700 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf" H 3950 3700 50  0001 C CNN
	1    3950 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3800 4750 3800
Wire Wire Line
	4550 3900 4750 3900
Wire Wire Line
	4050 1650 4050 1700
Text HLabel 2000 4800 0    50   Input ~ 0
AVR_TX0
Text HLabel 2000 4700 0    50   Input ~ 0
AVR_RX0
Wire Wire Line
	3350 5200 1650 5200
Wire Wire Line
	3350 5100 1650 5100
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5FE48597
P 1450 5100
AR Path="/5FE48597" Ref="J?"  Part="1" 
AR Path="/5FE16375/5FE48597" Ref="J14"  Part="1" 
F 0 "J14" H 1558 5281 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1558 5190 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1450 5100 50  0001 C CNN
F 3 "~" H 1450 5100 50  0001 C CNN
	1    1450 5100
	1    0    0    -1  
$EndComp
Text Notes 2650 5200 2    50   ~ 0
IDE: D3 - INT5
Text Notes 2650 5100 2    50   ~ 0
IDE: D2 - INT4
Text Label 3050 5200 2    50   ~ 0
EncoderB
Text Label 3050 5100 2    50   ~ 0
EncoderA
Text Notes 1550 4750 2    50   ~ 0
IDE: Serial
Wire Notes Line
	5500 3400 5300 3400
Wire Notes Line
	5350 3500 5500 3500
Text Notes 5500 3500 0    50   ~ 0
IDE: D14
Wire Notes Line
	5500 3300 5200 3300
Wire Notes Line
	5100 2900 5500 2900
Text Notes 5500 3400 0    50   ~ 0
IDE: D13
Wire Wire Line
	4550 3500 4750 3500
Wire Wire Line
	4550 3400 4750 3400
Text HLabel 4750 3500 2    50   Input ~ 0
OLED-RegSel
Text HLabel 4750 3400 2    50   Input ~ 0
OLED-Reset
Text HLabel 4750 3300 2    50   Input ~ 0
OLED-CS
Text HLabel 4750 3200 2    50   Input ~ 0
AVR-MISO
Text HLabel 4750 3100 2    50   Input ~ 0
AVR-MOSI
Text HLabel 4750 3000 2    50   Input ~ 0
AVR-CLK
Text HLabel 4750 2900 2    50   Input ~ 0
SD-CS
Text Notes 5500 3300 0    50   ~ 0
IDE: D12 - OLED Chip Select
Wire Wire Line
	4750 3300 4550 3300
Wire Wire Line
	4550 2900 4750 2900
Wire Wire Line
	4550 3200 4750 3200
Wire Wire Line
	4550 3100 4750 3100
Wire Wire Line
	4750 3000 4550 3000
Text Notes 5550 3100 0    50   ~ 0
IDE: D8 - Hardware SPI Chip Select
Wire Wire Line
	2500 2150 3050 2150
Wire Wire Line
	3050 2150 3050 2200
Wire Wire Line
	3050 2200 3350 2200
Connection ~ 2500 2150
Wire Wire Line
	2500 2450 3050 2450
Wire Wire Line
	3050 2450 3050 2400
Wire Wire Line
	3050 2400 3350 2400
Connection ~ 2500 2450
Wire Wire Line
	1600 2600 3350 2600
$Comp
L Device:R_Pack04 RN3
U 1 1 5FD45987
P 5050 5450
F 0 "RN3" V 4633 5450 50  0000 C CNN
F 1 "20K" V 4724 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_Cat16-4" V 5325 5450 50  0001 C CNN
F 3 "~" H 5050 5450 50  0001 C CNN
	1    5050 5450
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Pack04 RN2
U 1 1 5FD4E9A2
P 2800 5950
F 0 "RN2" V 2383 5950 50  0000 C CNN
F 1 "20K" V 2474 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_Cat16-4" V 3075 5950 50  0001 C CNN
F 3 "~" H 2800 5950 50  0001 C CNN
	1    2800 5950
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5FD516C3
P 1850 3800
F 0 "R10" V 1643 3800 50  0000 C CNN
F 1 "20K" V 1734 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1780 3800 50  0001 C CNN
F 3 "~" H 1850 3800 50  0001 C CNN
	1    1850 3800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5FD5413E
P 6700 2700
F 0 "#PWR015" H 6700 2450 50  0001 C CNN
F 1 "GND" V 6705 2572 50  0000 R CNN
F 2 "" H 6700 2700 50  0001 C CNN
F 3 "" H 6700 2700 50  0001 C CNN
	1    6700 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 2700 6350 2700
Wire Wire Line
	5350 2000 4550 2000
Wire Wire Line
	4550 2100 5350 2100
Wire Wire Line
	4550 2200 5350 2200
Wire Wire Line
	4550 2300 5350 2300
Wire Wire Line
	4550 5100 4800 5100
Wire Wire Line
	4800 5100 4800 5350
Wire Wire Line
	4800 5350 4850 5350
Wire Wire Line
	4550 5200 4750 5200
Wire Wire Line
	4750 5200 4750 5450
Wire Wire Line
	4750 5450 4850 5450
Wire Wire Line
	4550 5300 4700 5300
Wire Wire Line
	4700 5300 4700 5550
Wire Wire Line
	4700 5550 4850 5550
Wire Wire Line
	4550 5400 4650 5400
Wire Wire Line
	4650 5400 4650 5650
Wire Wire Line
	4650 5650 4850 5650
Wire Wire Line
	3350 5400 3350 6050
Wire Wire Line
	3350 6050 3000 6050
Wire Wire Line
	3350 5300 3300 5300
Wire Wire Line
	3300 5300 3300 5950
Wire Wire Line
	3300 5950 3000 5950
Wire Wire Line
	3350 5000 3250 5000
Wire Wire Line
	3250 5000 3250 5750
Wire Wire Line
	3350 4900 3200 4900
Wire Wire Line
	2950 3100 3350 3100
Wire Wire Line
	4550 3600 5500 3600
Wire Wire Line
	5500 3600 5500 3800
$Comp
L power:GND #PWR011
U 1 1 5FDADC1B
P 1600 3800
F 0 "#PWR011" H 1600 3550 50  0001 C CNN
F 1 "GND" V 1605 3672 50  0000 R CNN
F 2 "" H 1600 3800 50  0001 C CNN
F 3 "" H 1600 3800 50  0001 C CNN
	1    1600 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 3800 1700 3800
$Comp
L power:GND #PWR013
U 1 1 5FDB78C2
P 2500 6250
F 0 "#PWR013" H 2500 6000 50  0001 C CNN
F 1 "GND" H 2505 6077 50  0000 C CNN
F 2 "" H 2500 6250 50  0001 C CNN
F 3 "" H 2500 6250 50  0001 C CNN
	1    2500 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5FDB7C60
P 5400 5800
F 0 "#PWR014" H 5400 5550 50  0001 C CNN
F 1 "GND" H 5405 5627 50  0000 C CNN
F 2 "" H 5400 5800 50  0001 C CNN
F 3 "" H 5400 5800 50  0001 C CNN
	1    5400 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5FDB8184
P 7250 3800
F 0 "#PWR016" H 7250 3550 50  0001 C CNN
F 1 "GND" V 7255 3672 50  0000 R CNN
F 2 "" H 7250 3800 50  0001 C CNN
F 3 "" H 7250 3800 50  0001 C CNN
	1    7250 3800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5FDB8FC6
P 1600 3400
F 0 "#PWR012" H 1600 3150 50  0001 C CNN
F 1 "GND" V 1605 3272 50  0000 R CNN
F 2 "" H 1600 3400 50  0001 C CNN
F 3 "" H 1600 3400 50  0001 C CNN
	1    1600 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 3400 1800 3400
Wire Wire Line
	2600 5750 2500 5750
Wire Wire Line
	2500 5750 2500 5850
Wire Wire Line
	2600 6050 2500 6050
Connection ~ 2500 6050
Wire Wire Line
	2500 6050 2500 6250
Wire Wire Line
	2600 5950 2500 5950
Connection ~ 2500 5950
Wire Wire Line
	2500 5950 2500 6050
Wire Wire Line
	2500 5850 2600 5850
Connection ~ 2500 5850
Wire Wire Line
	2500 5850 2500 5950
Wire Wire Line
	5250 5350 5400 5350
Wire Wire Line
	5400 5350 5400 5450
Wire Wire Line
	5250 5650 5400 5650
Connection ~ 5400 5650
Wire Wire Line
	5400 5650 5400 5800
Wire Wire Line
	5250 5550 5400 5550
Connection ~ 5400 5550
Wire Wire Line
	5400 5550 5400 5650
Wire Wire Line
	5250 5450 5400 5450
Connection ~ 5400 5450
Wire Wire Line
	5400 5450 5400 5550
$Comp
L Device:R_Pack04 RN4
U 1 1 5FE02064
P 5550 2100
F 0 "RN4" V 5133 2100 50  0000 C CNN
F 1 "20K" V 5224 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_Cat16-4" V 5825 2100 50  0001 C CNN
F 3 "~" H 5550 2100 50  0001 C CNN
	1    5550 2100
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Pack04 RN6
U 1 1 5FE1BD33
P 6000 2500
F 0 "RN6" V 5583 2500 50  0000 C CNN
F 1 "20K" V 5674 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_Cat16-4" V 6275 2500 50  0001 C CNN
F 3 "~" H 6000 2500 50  0001 C CNN
	1    6000 2500
	0    1    -1   0   
$EndComp
Wire Wire Line
	4550 2500 5800 2500
Wire Wire Line
	4550 2600 5800 2600
Wire Wire Line
	4550 2700 5800 2700
Wire Wire Line
	5750 2000 5750 2100
Connection ~ 5750 2100
Wire Wire Line
	5750 2100 5750 2200
Connection ~ 5750 2200
Wire Wire Line
	5750 2200 5750 2300
Wire Wire Line
	6350 2000 6350 2400
Wire Wire Line
	6200 2400 6350 2400
Connection ~ 6350 2400
Wire Wire Line
	6350 2400 6350 2500
Wire Wire Line
	6200 2500 6350 2500
Connection ~ 6350 2500
Wire Wire Line
	6350 2500 6350 2600
Wire Wire Line
	6200 2600 6350 2600
Connection ~ 6350 2600
Wire Wire Line
	6350 2600 6350 2700
Wire Wire Line
	6200 2700 6350 2700
Connection ~ 6350 2700
Wire Wire Line
	5750 2000 6350 2000
$Comp
L Device:R_Pack04 RN5
U 1 1 5FE6DA95
P 6900 4400
F 0 "RN5" V 6483 4400 50  0000 C CNN
F 1 "20K" V 6574 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_Cat16-4" V 7175 4400 50  0001 C CNN
F 3 "~" H 6900 4400 50  0001 C CNN
	1    6900 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 3800 6150 3800
Wire Wire Line
	4550 4300 6700 4300
Wire Wire Line
	4550 4400 6700 4400
Wire Wire Line
	4550 4500 6700 4500
Wire Wire Line
	4550 4700 6750 4700
Wire Wire Line
	4550 4800 6850 4800
Wire Wire Line
	4550 4200 6700 4200
Wire Wire Line
	6550 3800 7100 3800
Wire Wire Line
	7100 4500 7100 4400
Connection ~ 7100 3800
Wire Wire Line
	7100 3800 7250 3800
Connection ~ 7100 4200
Wire Wire Line
	7100 4200 7100 3800
Connection ~ 7100 4300
Wire Wire Line
	7100 4300 7100 4200
Connection ~ 7100 4400
Wire Wire Line
	7100 4400 7100 4300
Wire Wire Line
	3200 5850 3000 5850
Wire Wire Line
	3200 4900 3200 5850
Wire Wire Line
	3000 5750 3250 5750
Wire Notes Line
	3000 5700 3000 5900
Wire Notes Line
	3000 5900 3450 5900
Wire Notes Line
	3450 5900 3450 5700
Wire Notes Line
	3450 5700 3000 5700
Wire Notes Line
	3400 5900 3400 6400
Wire Notes Line
	3400 6400 3550 6400
Text Notes 3550 6400 0    50   ~ 0
Pin swapped to Improve layout.
Connection ~ 5750 2000
Wire Wire Line
	4550 2400 5800 2400
Wire Notes Line
	5250 1900 5250 2350
Wire Notes Line
	5250 2350 5800 2350
Wire Notes Line
	5800 1900 5250 1900
Wire Notes Line
	5800 1550 6100 1550
Wire Notes Line
	5800 1550 5800 2350
Text Notes 6100 1550 0    50   ~ 0
Flipped vertically to improve layout
$Comp
L power:GND #PWR0117
U 1 1 5FC9C2CF
P 3950 5950
F 0 "#PWR0117" H 3950 5700 50  0001 C CNN
F 1 "GND" H 3955 5777 50  0000 C CNN
F 2 "" H 3950 5950 50  0001 C CNN
F 3 "" H 3950 5950 50  0001 C CNN
	1    3950 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5700 3950 5950
Wire Wire Line
	2000 3800 3350 3800
Wire Wire Line
	6550 3800 6550 4000
$Comp
L Device:R R23
U 1 1 5FCFA483
P 6300 3800
F 0 "R23" V 6093 3800 50  0000 C CNN
F 1 "20K" V 6184 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 3800 50  0001 C CNN
F 3 "~" H 6300 3800 50  0001 C CNN
	1    6300 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5FD0780D
P 6000 4000
F 0 "R14" V 5793 4000 50  0000 C CNN
F 1 "20K" V 5884 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5930 4000 50  0001 C CNN
F 3 "~" H 6000 4000 50  0001 C CNN
	1    6000 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5FD07DF6
P 5650 4100
F 0 "R12" V 5443 4100 50  0000 C CNN
F 1 "20K" V 5534 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5580 4100 50  0001 C CNN
F 3 "~" H 5650 4100 50  0001 C CNN
	1    5650 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 4100 5500 4100
Wire Wire Line
	4550 4000 5850 4000
Wire Wire Line
	5800 4100 6550 4100
Wire Wire Line
	6150 4000 6550 4000
Connection ~ 6550 4000
Wire Wire Line
	6550 4000 6550 4100
Wire Wire Line
	6450 3800 6550 3800
Connection ~ 6550 3800
Wire Notes Line
	5550 3550 5550 4150
Wire Notes Line
	5550 4150 6600 4150
Text Notes 6950 3650 0    50   ~ 0
Broken out of\nNetwork to improve\nlayout.
Wire Notes Line
	6950 3400 6600 3400
Wire Notes Line
	6600 3400 6600 4150
Wire Notes Line
	6600 3550 5550 3550
Wire Notes Line
	5500 2900 5500 3100
Wire Notes Line
	5500 3100 5550 3100
$Comp
L Device:R R27
U 1 1 5FCB6A95
P 2800 3100
F 0 "R27" V 2593 3100 50  0000 C CNN
F 1 "20K" V 2684 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2730 3100 50  0001 C CNN
F 3 "~" H 2800 3100 50  0001 C CNN
	1    2800 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R26
U 1 1 5FCC4B5C
P 2550 3200
F 0 "R26" V 2343 3200 50  0000 C CNN
F 1 "20K" V 2434 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2480 3200 50  0001 C CNN
F 3 "~" H 2550 3200 50  0001 C CNN
	1    2550 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5FCC4FAF
P 2300 3300
F 0 "R25" V 2093 3300 50  0000 C CNN
F 1 "20K" V 2184 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2230 3300 50  0001 C CNN
F 3 "~" H 2300 3300 50  0001 C CNN
	1    2300 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 5FCC54A6
P 2000 3400
F 0 "R24" V 1793 3400 50  0000 C CNN
F 1 "20K" V 1884 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1930 3400 50  0001 C CNN
F 3 "~" H 2000 3400 50  0001 C CNN
	1    2000 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 3100 1800 3100
Wire Wire Line
	1800 3100 1800 3200
Connection ~ 1800 3400
Wire Wire Line
	1800 3400 1850 3400
Wire Wire Line
	2400 3200 1800 3200
Connection ~ 1800 3200
Wire Wire Line
	1800 3200 1800 3300
Wire Wire Line
	2150 3300 1800 3300
Connection ~ 1800 3300
Wire Wire Line
	1800 3300 1800 3400
Wire Wire Line
	2150 3400 3350 3400
Wire Wire Line
	2450 3300 3350 3300
Wire Wire Line
	2700 3200 3350 3200
Text HLabel 4750 3900 2    50   Input ~ 0
OLED-PWM
Wire Wire Line
	5050 1150 5500 1150
$Comp
L power:GND #PWR?
U 1 1 61221E05
P 6000 8300
AR Path="/61221E05" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E05" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E05" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 6000 8050 50  0001 C CNN
F 1 "GND" H 6005 8127 50  0000 C CNN
F 2 "" H 6000 8300 50  0001 C CNN
F 3 "" H 6000 8300 50  0001 C CNN
	1    6000 8300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 8300 6000 8250
Text Notes 5650 8850 0    50   ~ 0
Reset Circuit
Text Notes 1600 9350 0    50   ~ 0
ISP Programming Interface\n(for bootloader)
$Comp
L power:+3V3 #PWR?
U 1 1 61221E15
P 2750 8650
AR Path="/61221E15" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E15" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E15" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 2750 8500 50  0001 C CNN
F 1 "+3V3" V 2765 8778 50  0000 L CNN
F 2 "" H 2750 8650 50  0001 C CNN
F 3 "" H 2750 8650 50  0001 C CNN
	1    2750 8650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61221E1B
P 2800 8850
AR Path="/61221E1B" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E1B" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E1B" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 2800 8600 50  0001 C CNN
F 1 "GND" V 2805 8722 50  0000 R CNN
F 2 "" H 2800 8850 50  0001 C CNN
F 3 "" H 2800 8850 50  0001 C CNN
	1    2800 8850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 8650 2750 8650
Wire Wire Line
	2300 8850 2800 8850
Wire Wire Line
	2300 8750 2400 8750
Wire Notes Line
	3200 9450 3200 8200
$Comp
L power:+3V3 #PWR?
U 1 1 61221E27
P 1150 7200
AR Path="/61221E27" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E27" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E27" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 1150 7050 50  0001 C CNN
F 1 "+3V3" H 1165 7373 50  0000 C CNN
F 2 "" H 1150 7200 50  0001 C CNN
F 3 "" H 1150 7200 50  0001 C CNN
	1    1150 7200
	1    0    0    -1  
$EndComp
Text Label 1300 7200 0    50   ~ 0
UART_DTR
Text Notes 1350 7950 0    50   ~ 0
General Programming Port\n(for sketch)
Wire Notes Line
	800  6800 800  8100
Wire Notes Line
	3300 8100 3300 6800
Text Notes 1500 6950 0    50   ~ 0
AUX Power Input
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 61221E44
P 2000 8750
AR Path="/61221E44" Ref="J?"  Part="1" 
AR Path="/5FE52742/61221E44" Ref="J?"  Part="1" 
AR Path="/5FE16375/61221E44" Ref="J7"  Part="1" 
F 0 "J7" H 2050 9067 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even 609-3234-ND" H 2050 8976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2000 8750 50  0001 C CNN
F 3 "~" H 2000 8750 50  0001 C CNN
	1    2000 8750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61221E4A
P 7250 8500
AR Path="/61221E4A" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E4A" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E4A" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 7250 8250 50  0001 C CNN
F 1 "GND" H 7255 8327 50  0000 C CNN
F 2 "" H 7250 8500 50  0001 C CNN
F 3 "" H 7250 8500 50  0001 C CNN
	1    7250 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 8400 7250 8400
Wire Wire Line
	7250 8400 7250 8500
Wire Wire Line
	7150 8200 7150 8400
Wire Wire Line
	6750 8400 6750 8200
$Comp
L NIM-PC104-rescue:B3F-1000-dk_Tactile-Switches S?
U 1 1 61221E5F
P 6950 8300
AR Path="/61221E5F" Ref="S?"  Part="1" 
AR Path="/5FE52742/61221E5F" Ref="S?"  Part="1" 
AR Path="/5FE16375/61221E5F" Ref="S1"  Part="1" 
F 0 "S1" H 6950 8647 60  0000 C CNN
F 1 "B3F-1000" H 6950 8541 60  0000 C CNN
F 2 "digikey-footprints:Switch_Tactile_THT_B3F-1xxx" H 7150 8500 60  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3f.pdf" H 7150 8600 60  0001 L CNN
F 4 "SW400-ND" H 7150 8700 60  0001 L CNN "Digi-Key_PN"
F 5 "B3F-1000" H 7150 8800 60  0001 L CNN "MPN"
F 6 "Switches" H 7150 8900 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 7150 9000 60  0001 L CNN "Family"
F 8 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3f.pdf" H 7150 9100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/omron-electronics-inc-emc-div/B3F-1000/SW400-ND/33150" H 7150 9200 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 24V" H 7150 9300 60  0001 L CNN "Description"
F 11 "Omron Electronics Inc-EMC Div" H 7150 9400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7150 9500 60  0001 L CNN "Status"
	1    6950 8300
	1    0    0    -1  
$EndComp
Connection ~ 7150 8400
Text Label 4350 7850 0    50   ~ 0
UART_DTR
Text HLabel 1650 7300 0    50   Input ~ 0
AVR_TX0
Text HLabel 1650 7400 0    50   Input ~ 0
AVR_RX0
Wire Wire Line
	1800 8750 1550 8750
Wire Notes Line
	900  8200 900  9450
Wire Notes Line
	900  9450 3200 9450
Wire Notes Line
	900  8200 3200 8200
$Comp
L Connector_Generic+MP:Conn_01x05_MP J?
U 1 1 61221E79
P 2000 7400
AR Path="/5FE52742/61221E79" Ref="J?"  Part="1" 
AR Path="/5FE16375/61221E79" Ref="J4"  Part="1" 
F 0 "J4" H 2080 7392 50  0000 L CNN
F 1 "Conn_01x05_MP WM7609CT-ND" H 2080 7301 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0571_1x05-1MP_P1.25mm_Vertical" H 2000 7400 50  0001 C CNN
F 3 "~" H 2000 7400 50  0001 C CNN
	1    2000 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61221E7F
P 1350 7700
AR Path="/5FE52742/61221E7F" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E7F" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 1350 7450 50  0001 C CNN
F 1 "GND" V 1355 7572 50  0000 R CNN
F 2 "" H 1350 7700 50  0001 C CNN
F 3 "" H 1350 7700 50  0001 C CNN
	1    1350 7700
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 7700 1600 7700
Wire Notes Line
	800  6800 3300 6800
Wire Notes Line
	800  8100 3300 8100
$Comp
L Device:C C17
U 1 1 61221E88
P 5000 7850
AR Path="/5FE16375/61221E88" Ref="C17"  Part="1" 
AR Path="/5FE52742/61221E88" Ref="C?"  Part="1" 
F 0 "C17" V 4748 7850 50  0000 C CNN
F 1 "0.1uF" V 4839 7850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 7700 50  0001 C CNN
F 3 "~" H 5000 7850 50  0001 C CNN
	1    5000 7850
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 2000 3350 2000
$Comp
L power:+3V3 #PWR?
U 1 1 61221E0C
P 6000 7200
AR Path="/61221E0C" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/61221E0C" Ref="#PWR?"  Part="1" 
AR Path="/5FE16375/61221E0C" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 6000 7050 50  0001 C CNN
F 1 "+3V3" H 6015 7373 50  0000 C CNN
F 2 "" H 6000 7200 50  0001 C CNN
F 3 "" H 6000 7200 50  0001 C CNN
	1    6000 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 7950 6000 7850
$Comp
L Device:C C?
U 1 1 61221DFE
P 6000 8100
AR Path="/61221DFE" Ref="C?"  Part="1" 
AR Path="/5FE52742/61221DFE" Ref="C?"  Part="1" 
AR Path="/5FE16375/61221DFE" Ref="C7"  Part="1" 
F 0 "C7" H 6115 8146 50  0000 L CNN
F 1 "22pF" H 6115 8055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6038 7950 50  0001 C CNN
F 3 "~" H 6000 8100 50  0001 C CNN
	1    6000 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 7400 6000 7200
Wire Wire Line
	6350 7400 6000 7400
Wire Wire Line
	6000 7850 6350 7850
Connection ~ 6000 7850
Wire Wire Line
	6000 7700 6000 7850
Connection ~ 6350 7850
Wire Wire Line
	6350 7700 6350 7850
$Comp
L Device:R R?
U 1 1 61221DEB
P 6350 7550
AR Path="/61221DEB" Ref="R?"  Part="1" 
AR Path="/5FE52742/61221DEB" Ref="R?"  Part="1" 
AR Path="/5FE16375/61221DEB" Ref="R13"  Part="1" 
F 0 "R13" H 6420 7596 50  0000 L CNN
F 1 "10K" H 6420 7505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6280 7550 50  0001 C CNN
F 3 "~" H 6350 7550 50  0001 C CNN
	1    6350 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 7850 4800 7850
Text HLabel 1550 8750 0    50   Input ~ 0
AVR-CLK
Text Label 6750 7850 0    50   ~ 0
AVR_RESET
Wire Wire Line
	5150 7850 6000 7850
Wire Wire Line
	6350 7850 6750 7850
Wire Wire Line
	6350 8200 6750 8200
Wire Wire Line
	6350 7850 6350 8200
Connection ~ 6750 8200
Wire Notes Line
	4200 6800 4200 8900
Wire Notes Line
	7500 8900 7500 6800
Wire Notes Line
	4200 6800 7500 6800
Wire Notes Line
	4200 8900 7500 8900
Text Label 2600 2000 0    50   ~ 0
AVR_RESET
Wire Wire Line
	4700 7700 4800 7700
Wire Wire Line
	4800 7700 4800 7850
Connection ~ 4800 7850
Wire Wire Line
	4800 7850 4850 7850
Text HLabel 4700 7700 0    50   Input ~ 0
AVR_DTR
Text Label 1150 8850 0    50   ~ 0
AVR_RESET
Wire Wire Line
	1150 8850 1800 8850
Wire Wire Line
	2000 4700 3350 4700
Wire Wire Line
	2000 4800 3350 4800
Text Label 2300 4700 0    50   ~ 0
ICSP_MOSI
Text Label 2300 4800 0    50   ~ 0
ICSP_MISO
Text Label 1150 8650 0    50   ~ 0
ICSP_MISO
Wire Wire Line
	1150 8650 1800 8650
Text Label 2400 8750 0    50   ~ 0
ICSP_MOSI
$Comp
L Device:D D2
U 1 1 612B394A
P 6000 7550
F 0 "D2" V 5954 7629 50  0000 L CNN
F 1 "D" V 6045 7629 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6000 7550 50  0001 C CNN
F 3 "~" H 6000 7550 50  0001 C CNN
F 4 "1N4448WXTPMSCT-ND" V 6000 7550 50  0001 R CNN "PN"
	1    6000 7550
	0    1    1    0   
$EndComp
Connection ~ 6000 7400
Wire Wire Line
	1300 7200 1800 7200
Wire Wire Line
	1150 7500 1150 7200
Wire Wire Line
	1800 7500 1150 7500
Wire Wire Line
	1800 7600 1600 7600
Wire Wire Line
	1600 7600 1600 7700
Connection ~ 1600 7700
Wire Wire Line
	1600 7700 1800 7700
Wire Wire Line
	1650 7300 1800 7300
Wire Wire Line
	1650 7400 1800 7400
$Comp
L Device:R R30
U 1 1 614066B6
P 7150 5200
F 0 "R30" H 7220 5246 50  0000 L CNN
F 1 "4.7K" H 7220 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7080 5200 50  0001 C CNN
F 3 "~" H 7150 5200 50  0001 C CNN
	1    7150 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R31
U 1 1 61407540
P 7500 5200
F 0 "R31" H 7570 5246 50  0000 L CNN
F 1 "4.7K" H 7570 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 5200 50  0001 C CNN
F 3 "~" H 7500 5200 50  0001 C CNN
	1    7500 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R28
U 1 1 6140772F
P 5850 5750
F 0 "R28" H 5920 5796 50  0000 L CNN
F 1 "10K" H 5920 5705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 5750 50  0001 C CNN
F 3 "~" H 5850 5750 50  0001 C CNN
	1    5850 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R29
U 1 1 61407BAF
P 6300 5750
F 0 "R29" H 6370 5796 50  0000 L CNN
F 1 "10K" H 6370 5705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 5750 50  0001 C CNN
F 3 "~" H 6300 5750 50  0001 C CNN
	1    6300 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 61407E01
P 5850 6050
F 0 "#PWR0126" H 5850 5800 50  0001 C CNN
F 1 "GND" H 5855 5877 50  0000 C CNN
F 2 "" H 5850 6050 50  0001 C CNN
F 3 "" H 5850 6050 50  0001 C CNN
	1    5850 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 6000 6300 5900
$Comp
L power:+3.3V #PWR0130
U 1 1 6141FEB1
P 7500 5500
F 0 "#PWR0130" H 7500 5350 50  0001 C CNN
F 1 "+3.3V" H 7515 5673 50  0000 C CNN
F 2 "" H 7500 5500 50  0001 C CNN
F 3 "" H 7500 5500 50  0001 C CNN
	1    7500 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 5350 7500 5400
Wire Wire Line
	7150 5400 7150 5350
Connection ~ 7500 5400
Wire Wire Line
	7500 5400 7500 5500
Wire Wire Line
	5850 5600 5850 5500
Connection ~ 5850 5500
Wire Wire Line
	6300 5600 6300 5400
Wire Wire Line
	7150 5050 7150 4950
Wire Wire Line
	7150 4950 6750 4950
Wire Wire Line
	6750 4950 6750 4700
Connection ~ 6750 4700
Wire Wire Line
	6850 4800 6850 4900
Wire Wire Line
	7500 4900 7500 5050
Connection ~ 6850 4800
Wire Wire Line
	6850 4900 7500 4900
Wire Wire Line
	7150 5400 7500 5400
Connection ~ 6300 5400
Wire Wire Line
	6300 5400 6550 5400
Wire Wire Line
	5900 5400 6300 5400
Wire Wire Line
	5850 5900 5850 6000
Wire Wire Line
	5850 6000 6300 6000
Connection ~ 5850 6000
Wire Wire Line
	5850 6000 5850 6050
Text Notes 6050 5950 0    50   ~ 0
DNP
Wire Notes Line
	6650 6050 6650 5600
Wire Notes Line
	6650 5600 5700 5600
Wire Notes Line
	5700 5600 5700 6050
Wire Notes Line
	5700 6050 6650 6050
Text Notes 6750 5500 0    50   ~ 0
DNP
Text Notes 5050 7350 0    50   ~ 0
1N4448WXTPMSCT-ND
Wire Notes Line
	5050 7350 5050 7400
Wire Notes Line
	5050 7400 5900 7400
Wire Notes Line
	5900 7400 5900 7350
Wire Notes Line
	5650 7400 5650 7550
Wire Notes Line
	5650 7550 5950 7550
Text HLabel 7500 4700 2    50   Input ~ 0
AVR_SCL
Text HLabel 7500 4800 2    50   Input ~ 0
AVR_SDA
Wire Wire Line
	6750 4700 7200 4700
Wire Wire Line
	6850 4800 7400 4800
Wire Wire Line
	7500 4400 7200 4400
Wire Wire Line
	7200 4400 7200 4700
Connection ~ 7200 4700
Wire Wire Line
	7200 4700 7500 4700
Wire Wire Line
	7500 4500 7400 4500
Wire Wire Line
	7400 4500 7400 4800
Connection ~ 7400 4800
Wire Wire Line
	7400 4800 7500 4800
$EndSCHEMATC
