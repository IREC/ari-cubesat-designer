EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 8500 11000 portrait
encoding utf-8
Sheet 2 9
Title "PC/104 Stack Header Connections"
Date "2022-01-11"
Rev "C.2"
Comp "Aurora Research Institute"
Comment1 "Northern Images Mission Payload"
Comment2 "QUALIFICATION MODEL"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3100 1650 0    79   ~ 0
PC104 Header
NoConn ~ 3900 5000
NoConn ~ 3900 5100
NoConn ~ 3900 6100
NoConn ~ 3900 6600
NoConn ~ 3900 6700
NoConn ~ 3900 2100
NoConn ~ 3900 2200
NoConn ~ 3900 2300
NoConn ~ 3900 2400
NoConn ~ 3900 2500
NoConn ~ 3900 2600
NoConn ~ 3900 2700
NoConn ~ 3900 2900
NoConn ~ 3900 3300
NoConn ~ 3900 3400
NoConn ~ 3900 3900
NoConn ~ 3900 4000
NoConn ~ 4400 4600
NoConn ~ 4400 4500
NoConn ~ 4400 4400
NoConn ~ 4400 4200
NoConn ~ 4400 4100
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J13
U 1 1 5FBF29CD
P 4100 6200
F 0 "J13" H 4150 7617 50  0000 C CNN
F 1 "Conn_02x26_Odd_Even 67997-252HLF-ND" H 4150 7526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 4100 6200 50  0001 C CNN
F 3 "~" H 4100 6200 50  0001 C CNN
	1    4100 6200
	1    0    0    -1  
$EndComp
Text Notes 2350 4950 0    50   ~ 0
'Back' Header\n
Text Notes 2600 1950 0    50   ~ 0
'Front' Header
NoConn ~ 3900 7200
NoConn ~ 4400 7200
Text Notes 2600 2050 0    50   ~ 0
H1 in UofA Docs
Text Notes 2350 5050 0    50   ~ 0
H2 in UofA Docs
Text Notes 5150 7500 0    50   ~ 0
2U_PAY-3V3\n
Text Notes 5150 7400 0    50   ~ 0
2U_PAY_3V3
Text Notes 2350 5150 0    50   ~ 0
"At the PCB Edge"
Text Notes 2600 2150 0    50   ~ 0
"Closer to Middle of PCB"
$Comp
L power:+3V3 #PWR?
U 1 1 5FBF29E5
P 4750 7400
AR Path="/5FBF29E5" Ref="#PWR?"  Part="1" 
AR Path="/5FBC2EB0/5FBF29E5" Ref="#PWR0163"  Part="1" 
F 0 "#PWR0163" H 4750 7250 50  0001 C CNN
F 1 "+3V3" V 4765 7528 50  0000 L CNN
F 2 "" H 4750 7400 50  0001 C CNN
F 3 "" H 4750 7400 50  0001 C CNN
	1    4750 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 7400 4650 7400
Wire Wire Line
	4400 7500 4650 7500
Wire Wire Line
	4650 7500 4650 7400
Connection ~ 4650 7400
Wire Wire Line
	4650 7400 4750 7400
Wire Notes Line
	5100 7400 5150 7400
Wire Notes Line
	4650 7500 5150 7500
Wire Wire Line
	4400 5200 4700 5200
Wire Wire Line
	3900 5200 3600 5200
Text HLabel 4700 5200 2    50   Input ~ 0
OBC_RX
Text HLabel 3600 5200 0    50   Input ~ 0
OBC_TX
Text HLabel 4750 3400 2    50   Input ~ 0
OBC_DTR
Wire Wire Line
	4750 3400 4400 3400
NoConn ~ 3900 4500
NoConn ~ 3900 4400
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J12
U 1 1 5FBF29AA
P 4100 3300
F 0 "J12" H 4150 4717 50  0000 C CNN
F 1 "Conn_02x26_Odd_Even 67997-252HLF-ND" H 4150 4626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 4100 3300 50  0001 C CNN
F 3 "~" H 4100 3300 50  0001 C CNN
	1    4100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0144
U 1 1 5F95D08E
P 3650 4600
F 0 "#PWR0144" H 3650 4450 50  0001 C CNN
F 1 "+5V" V 3665 4728 50  0000 L CNN
F 2 "" H 3650 4600 50  0001 C CNN
F 3 "" H 3650 4600 50  0001 C CNN
	1    3650 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 4600 3900 4600
Text Notes 2550 4600 0    50   ~ 0
2U_PAY_5V
Wire Notes Line
	3000 4600 3350 4600
$Comp
L power:GND #PWR0145
U 1 1 5F95DCFB
P 3600 4300
F 0 "#PWR0145" H 3600 4050 50  0001 C CNN
F 1 "GND" V 3605 4172 50  0000 R CNN
F 2 "" H 3600 4300 50  0001 C CNN
F 3 "" H 3600 4300 50  0001 C CNN
	1    3600 4300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 5F95E3B6
P 4700 4300
F 0 "#PWR0146" H 4700 4050 50  0001 C CNN
F 1 "GND" V 4705 4172 50  0000 R CNN
F 2 "" H 4700 4300 50  0001 C CNN
F 3 "" H 4700 4300 50  0001 C CNN
	1    4700 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3600 4300 3900 4300
Wire Wire Line
	4400 4300 4700 4300
NoConn ~ 3900 4200
NoConn ~ 3900 4100
NoConn ~ 4400 4000
NoConn ~ 4400 3900
NoConn ~ 4400 3800
NoConn ~ 3900 3800
NoConn ~ 3900 3700
NoConn ~ 3900 3600
NoConn ~ 3900 3500
NoConn ~ 4400 3500
NoConn ~ 4400 3600
NoConn ~ 4400 3700
NoConn ~ 3900 3200
NoConn ~ 3900 3100
NoConn ~ 3900 3000
NoConn ~ 3900 2800
NoConn ~ 4400 3300
NoConn ~ 4400 3200
NoConn ~ 4400 3100
NoConn ~ 4400 3000
NoConn ~ 4400 2900
NoConn ~ 4400 2800
NoConn ~ 4400 2700
NoConn ~ 4400 2600
NoConn ~ 4400 2500
NoConn ~ 4400 2400
NoConn ~ 4400 2300
NoConn ~ 4400 2200
NoConn ~ 4400 2100
NoConn ~ 4400 5000
NoConn ~ 4400 5100
NoConn ~ 4400 5300
NoConn ~ 3900 5300
NoConn ~ 3900 5400
NoConn ~ 3900 5500
NoConn ~ 3900 5600
NoConn ~ 3900 5700
NoConn ~ 3900 5800
NoConn ~ 3900 5900
NoConn ~ 3900 6000
NoConn ~ 4400 5400
NoConn ~ 4400 5500
NoConn ~ 4400 5600
NoConn ~ 4400 5700
NoConn ~ 4400 5800
NoConn ~ 4400 5900
NoConn ~ 4400 6000
NoConn ~ 4400 6100
NoConn ~ 4400 6200
NoConn ~ 3900 6200
NoConn ~ 3900 6300
NoConn ~ 3900 6400
NoConn ~ 4400 6300
NoConn ~ 4400 6400
NoConn ~ 3900 6500
NoConn ~ 4400 6500
NoConn ~ 4400 6600
NoConn ~ 4400 6700
NoConn ~ 4400 6800
NoConn ~ 4400 6900
NoConn ~ 3900 6800
NoConn ~ 3900 6900
NoConn ~ 3900 7000
NoConn ~ 3900 7100
NoConn ~ 4400 7100
NoConn ~ 4400 7000
NoConn ~ 3900 7300
NoConn ~ 3900 7400
NoConn ~ 3900 7500
NoConn ~ 4400 7300
$EndSCHEMATC
