EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 8500 11000 portrait
encoding utf-8
Sheet 3 9
Title "Power"
Date "2022-01-11"
Rev "C.2"
Comp "Aurora Research Institute"
Comment1 "Northern Images Mission Payload"
Comment2 "QUALIFICATION MODEL"
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	6000 6000 2450 6000
Wire Notes Line
	2450 8850 6000 8850
Wire Notes Line
	2450 6000 2450 8850
Text Notes 2600 7600 0    50   ~ 0
Camera Power Switch
Text Notes 2500 6300 0    50   ~ 0
Screen Power Switch
Wire Wire Line
	3150 8150 3150 8300
$Comp
L power:GND #PWR?
U 1 1 5FD3746E
P 3150 8300
AR Path="/5FD3746E" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD3746E" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 3150 8050 50  0001 C CNN
F 1 "GND" H 3155 8127 50  0000 C CNN
F 2 "" H 3150 8300 50  0001 C CNN
F 3 "" H 3150 8300 50  0001 C CNN
	1    3150 8300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 7800 3700 7800
Wire Wire Line
	3150 7850 3150 7800
Connection ~ 3150 7800
Wire Wire Line
	3050 7800 3150 7800
Wire Wire Line
	3150 6550 3700 6550
Wire Wire Line
	3150 6600 3150 6550
Connection ~ 3150 6550
Wire Wire Line
	3050 6550 3150 6550
Wire Wire Line
	3150 6900 3150 7000
$Comp
L power:GND #PWR?
U 1 1 5FD3747D
P 3150 7000
AR Path="/5FD3747D" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD3747D" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 3150 6750 50  0001 C CNN
F 1 "GND" H 3155 6827 50  0000 C CNN
F 2 "" H 3150 7000 50  0001 C CNN
F 3 "" H 3150 7000 50  0001 C CNN
	1    3150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6750 3700 6750
Wire Wire Line
	3700 8000 3600 8000
Wire Wire Line
	5250 8300 5250 8350
Wire Wire Line
	4550 8350 4550 8450
Wire Wire Line
	4800 8350 5250 8350
Wire Wire Line
	4800 8350 4550 8350
Connection ~ 4800 8350
Wire Wire Line
	4800 8300 4800 8350
Connection ~ 4550 8350
Wire Wire Line
	4550 8000 4550 8350
Wire Wire Line
	4450 7800 5250 7800
Wire Wire Line
	5250 7800 5400 7800
Connection ~ 5250 7800
Wire Wire Line
	5250 8000 5250 7800
Wire Wire Line
	4800 7900 4800 8000
Wire Wire Line
	4450 7900 4800 7900
Wire Wire Line
	4800 6650 4800 6700
Wire Wire Line
	4450 6650 4800 6650
Wire Wire Line
	4450 6550 5250 6550
Wire Wire Line
	5250 6550 5450 6550
Connection ~ 5250 6550
Wire Wire Line
	5250 6700 5250 6550
Wire Wire Line
	5250 7000 5250 7100
Wire Wire Line
	4550 7100 4550 7150
Wire Wire Line
	4800 7100 5250 7100
Wire Wire Line
	4800 7100 4550 7100
Connection ~ 4800 7100
Wire Wire Line
	4800 7000 4800 7100
Connection ~ 4550 7100
Wire Wire Line
	4550 6750 4550 7100
$Comp
L Device:R R?
U 1 1 5FD374A5
P 4800 6850
AR Path="/5FD374A5" Ref="R?"  Part="1" 
AR Path="/5FCCA0A1/5FD374A5" Ref="R21"  Part="1" 
F 0 "R21" H 4870 6896 50  0000 L CNN
F 1 "350" H 4870 6805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 6850 50  0001 C CNN
F 3 "~" H 4800 6850 50  0001 C CNN
	1    4800 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FD374AB
P 4800 8150
AR Path="/5FD374AB" Ref="R?"  Part="1" 
AR Path="/5FCCA0A1/5FD374AB" Ref="R22"  Part="1" 
F 0 "R22" H 4870 8196 50  0000 L CNN
F 1 "350" H 4870 8105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 8150 50  0001 C CNN
F 3 "~" H 4800 8150 50  0001 C CNN
	1    4800 8150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD374B3
P 3150 6750
AR Path="/5FD374B3" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD374B3" Ref="C13"  Part="1" 
F 0 "C13" H 3265 6796 50  0000 L CNN
F 1 "4.7uF" H 3265 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3188 6600 50  0001 C CNN
F 3 "~" H 3150 6750 50  0001 C CNN
	1    3150 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD374B9
P 3150 8000
AR Path="/5FD374B9" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD374B9" Ref="C14"  Part="1" 
F 0 "C14" H 3265 8046 50  0000 L CNN
F 1 "4.7uF" H 3265 7955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3188 7850 50  0001 C CNN
F 3 "~" H 3150 8000 50  0001 C CNN
	1    3150 8000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD374BF
P 5250 8150
AR Path="/5FD374BF" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD374BF" Ref="C16"  Part="1" 
F 0 "C16" H 5365 8196 50  0000 L CNN
F 1 "0.1uF" H 5365 8105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5288 8000 50  0001 C CNN
F 3 "~" H 5250 8150 50  0001 C CNN
	1    5250 8150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD374C5
P 5250 6850
AR Path="/5FD374C5" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD374C5" Ref="C15"  Part="1" 
F 0 "C15" H 5365 6896 50  0000 L CNN
F 1 "0.1uF" H 5365 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5288 6700 50  0001 C CNN
F 3 "~" H 5250 6850 50  0001 C CNN
	1    5250 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 6750 4550 6750
Wire Wire Line
	4450 8000 4550 8000
$Comp
L power:GND #PWR?
U 1 1 5FD374CD
P 4550 7150
AR Path="/5FD374CD" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD374CD" Ref="#PWR0150"  Part="1" 
F 0 "#PWR0150" H 4550 6900 50  0001 C CNN
F 1 "GND" H 4555 6977 50  0000 C CNN
F 2 "" H 4550 7150 50  0001 C CNN
F 3 "" H 4550 7150 50  0001 C CNN
	1    4550 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD374D3
P 4550 8450
AR Path="/5FD374D3" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD374D3" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 4550 8200 50  0001 C CNN
F 1 "GND" H 4555 8277 50  0000 C CNN
F 2 "" H 4550 8450 50  0001 C CNN
F 3 "" H 4550 8450 50  0001 C CNN
	1    4550 8450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5FD374D9
P 3050 7800
AR Path="/5FD374D9" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD374D9" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 3050 7650 50  0001 C CNN
F 1 "+5V" V 3065 7928 50  0000 L CNN
F 2 "" H 3050 7800 50  0001 C CNN
F 3 "" H 3050 7800 50  0001 C CNN
	1    3050 7800
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FD374DF
P 3050 6550
AR Path="/5FD374DF" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD374DF" Ref="#PWR0165"  Part="1" 
F 0 "#PWR0165" H 3050 6400 50  0001 C CNN
F 1 "+3V3" V 3065 6678 50  0000 L CNN
F 2 "" H 3050 6550 50  0001 C CNN
F 3 "" H 3050 6550 50  0001 C CNN
	1    3050 6550
	0    -1   -1   0   
$EndComp
Text Notes 2500 6150 0    79   ~ 0
Screen & Camera Power Switching
$Comp
L FPF2123:FPF2123 U?
U 1 1 5FD374E8
P 4050 7900
AR Path="/5FD374E8" Ref="U?"  Part="1" 
AR Path="/5FCCA0A1/5FD374E8" Ref="U5"  Part="1" 
F 0 "U5" H 4075 8225 50  0000 C CNN
F 1 "FPF2125" H 4075 8134 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4300 7700 50  0001 C CNN
F 3 "" H 4300 7700 50  0001 C CNN
	1    4050 7900
	1    0    0    -1  
$EndComp
$Comp
L FPF2123:FPF2123 U?
U 1 1 5FD374EE
P 4050 6650
AR Path="/5FD374EE" Ref="U?"  Part="1" 
AR Path="/5FCCA0A1/5FD374EE" Ref="U4"  Part="1" 
F 0 "U4" H 4075 6975 50  0000 C CNN
F 1 "FPF2125" H 4075 6884 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4300 6450 50  0001 C CNN
F 3 "" H 4300 6450 50  0001 C CNN
	1    4050 6650
	1    0    0    -1  
$EndComp
Text Notes 3900 2500 0    50   ~ 0
External Power Input
$Comp
L power:GND #PWR?
U 1 1 5FD76910
P 3950 2050
AR Path="/5FD76910" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD76910" Ref="#PWR0166"  Part="1" 
F 0 "#PWR0166" H 3950 1800 50  0001 C CNN
F 1 "GND" H 3955 1877 50  0000 C CNN
F 2 "" H 3950 2050 50  0001 C CNN
F 3 "" H 3950 2050 50  0001 C CNN
	1    3950 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2050 3950 2000
Wire Wire Line
	3850 2000 3950 2000
Wire Wire Line
	4250 1400 4400 1400
Text Notes 2650 5650 0    50   ~ 0
3v3 Linear Regulator for Ground Power
$Comp
L Device:C C?
U 1 1 5FD76928
P 2750 4900
AR Path="/5FD76928" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD76928" Ref="C11"  Part="1" 
F 0 "C11" H 2865 4946 50  0000 L CNN
F 1 "0.1uF" H 2865 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2788 4750 50  0001 C CNN
F 3 "~" H 2750 4900 50  0001 C CNN
	1    2750 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD7692E
P 3750 4900
AR Path="/5FD7692E" Ref="C?"  Part="1" 
AR Path="/5FCCA0A1/5FD7692E" Ref="C12"  Part="1" 
F 0 "C12" H 3865 4946 50  0000 L CNN
F 1 "0.1uF" H 3865 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3788 4750 50  0001 C CNN
F 3 "~" H 3750 4900 50  0001 C CNN
	1    3750 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD76934
P 3750 5200
AR Path="/5FD76934" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD76934" Ref="#PWR0168"  Part="1" 
F 0 "#PWR0168" H 3750 4950 50  0001 C CNN
F 1 "GND" H 3755 5027 50  0000 C CNN
F 2 "" H 3750 5200 50  0001 C CNN
F 3 "" H 3750 5200 50  0001 C CNN
	1    3750 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5200 3750 5050
Wire Wire Line
	2750 5050 3250 5050
Connection ~ 3750 5050
Wire Wire Line
	3250 5050 3250 5000
Connection ~ 3250 5050
Wire Wire Line
	3250 5050 3750 5050
Wire Wire Line
	3750 4750 3750 4700
Wire Wire Line
	3750 4700 3550 4700
Wire Wire Line
	2750 4750 2750 4700
Wire Wire Line
	2750 4700 2950 4700
$Comp
L power:+3V3 #PWR?
U 1 1 5FD76944
P 3750 4600
AR Path="/5FD76944" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FD76944" Ref="#PWR0169"  Part="1" 
F 0 "#PWR0169" H 3750 4450 50  0001 C CNN
F 1 "+3V3" H 3765 4773 50  0000 C CNN
F 2 "" H 3750 4600 50  0001 C CNN
F 3 "" H 3750 4600 50  0001 C CNN
	1    3750 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4600 2750 4700
Connection ~ 2750 4700
Wire Wire Line
	3750 4600 3750 4700
Connection ~ 3750 4700
Wire Notes Line
	2450 5700 4200 5700
Wire Notes Line
	4200 5700 4200 4300
Wire Notes Line
	4200 4300 2450 4300
$Comp
L Regulator_Linear:LD1117S33TR_SOT223 U?
U 1 1 5FD76958
P 3250 4700
AR Path="/5FD76958" Ref="U?"  Part="1" 
AR Path="/5FCCA0A1/5FD76958" Ref="U3"  Part="1" 
F 0 "U3" H 3250 4942 50  0000 C CNN
F 1 "LD1117S33TR_SOT223" H 3250 4851 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 3250 4900 50  0001 C CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000544.pdf" H 3350 4450 50  0001 C CNN
	1    3250 4700
	1    0    0    -1  
$EndComp
Text Notes 4200 3950 0    50   ~ 0
Ground\nPower Indicator
$Comp
L Device:LED D?
U 1 1 5FDA19CD
P 3900 3200
AR Path="/5FDA19CD" Ref="D?"  Part="1" 
AR Path="/5FCCA0A1/5FDA19CD" Ref="D1"  Part="1" 
F 0 "D1" V 3939 3083 50  0000 R CNN
F 1 " APL3015ZGCK-F01" V 3848 3083 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3900 3200 50  0001 C CNN
F 3 "~" H 3900 3200 50  0001 C CNN
	1    3900 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FDA19D3
P 3900 3550
AR Path="/5FDA19D3" Ref="R?"  Part="1" 
AR Path="/5FCCA0A1/5FDA19D3" Ref="R5"  Part="1" 
F 0 "R5" H 3970 3596 50  0000 L CNN
F 1 "1K" H 3970 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3830 3550 50  0001 C CNN
F 3 "~" H 3900 3550 50  0001 C CNN
	1    3900 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDA19D9
P 3900 3750
AR Path="/5FDA19D9" Ref="#PWR?"  Part="1" 
AR Path="/5FCCA0A1/5FDA19D9" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 3900 3500 50  0001 C CNN
F 1 "GND" H 3905 3577 50  0000 C CNN
F 2 "" H 3900 3750 50  0001 C CNN
F 3 "" H 3900 3750 50  0001 C CNN
	1    3900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3750 3900 3700
Wire Wire Line
	3900 3400 3900 3350
Wire Wire Line
	3900 3050 3900 3000
Wire Notes Line
	3700 2750 3700 4000
Wire Notes Line
	6000 8850 6000 6000
Text HLabel 5450 6550 2    50   Input ~ 0
3v3_Screen
Text HLabel 5400 7800 2    50   Input ~ 0
5v_Cam
Wire Notes Line
	5850 1000 5850 2550
Wire Notes Line
	3550 1000 3550 2550
Text HLabel 3600 7200 2    50   Input ~ 0
Screen_PWR_Control
Text HLabel 3650 8400 2    50   Input ~ 0
Cam_PWR_Control
Wire Wire Line
	3600 8400 3650 8400
Wire Wire Line
	3600 8000 3600 8400
Wire Wire Line
	3500 7200 3600 7200
Wire Wire Line
	3500 6750 3500 7200
$Comp
L power:+5VP #PWR01
U 1 1 5F99FDA2
P 2750 4600
F 0 "#PWR01" H 2750 4450 50  0001 C CNN
F 1 "+5VP" H 2765 4773 50  0000 C CNN
F 2 "" H 2750 4600 50  0001 C CNN
F 3 "" H 2750 4600 50  0001 C CNN
	1    2750 4600
	1    0    0    -1  
$EndComp
Wire Notes Line
	2450 4300 2450 5700
$Comp
L power:+5VP #PWR03
U 1 1 5F9A544A
P 4400 1400
F 0 "#PWR03" H 4400 1250 50  0001 C CNN
F 1 "+5VP" V 4415 1528 50  0000 L CNN
F 2 "" H 4400 1400 50  0001 C CNN
F 3 "" H 4400 1400 50  0001 C CNN
	1    4400 1400
	0    1    1    0   
$EndComp
Wire Notes Line
	4850 4000 4850 2750
$Comp
L power:+5VP #PWR02
U 1 1 5F9AB889
P 3900 3000
F 0 "#PWR02" H 3900 2850 50  0001 C CNN
F 1 "+5VP" H 3915 3173 50  0000 C CNN
F 2 "" H 3900 3000 50  0001 C CNN
F 3 "" H 3900 3000 50  0001 C CNN
	1    3900 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 5F9B0B4D
P 5350 4800
F 0 "J15" H 5430 4792 50  0000 L CNN
F 1 "Conn_01x02 S9682-ND" H 5430 4701 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5350 4800 50  0001 C CNN
F 3 "~" H 5350 4800 50  0001 C CNN
	1    5350 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5F9B12D3
P 4950 4900
F 0 "#PWR05" H 4950 4750 50  0001 C CNN
F 1 "+5V" V 4965 5028 50  0000 L CNN
F 2 "" H 4950 4900 50  0001 C CNN
F 3 "" H 4950 4900 50  0001 C CNN
	1    4950 4900
	0    -1   -1   0   
$EndComp
$Comp
L power:+5VP #PWR04
U 1 1 5F9B1837
P 4950 4800
F 0 "#PWR04" H 4950 4650 50  0001 C CNN
F 1 "+5VP" V 4965 4928 50  0000 L CNN
F 2 "" H 4950 4800 50  0001 C CNN
F 3 "" H 4950 4800 50  0001 C CNN
	1    4950 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 4800 5150 4800
Wire Wire Line
	4950 4900 5150 4900
Wire Notes Line
	4500 4300 4500 5700
Wire Notes Line
	6400 5700 6400 4300
Text Notes 5450 5650 0    50   ~ 0
Ground Power Jumper\n"Remove for Flight"
Connection ~ 3950 2000
$Comp
L NIM-PC104-rescue:USB_B_Micro-Connector J?
U 1 1 5FD7690A
P 3950 1600
AR Path="/5FD7690A" Ref="J?"  Part="1" 
AR Path="/5FCCA0A1/5FD7690A" Ref="J18"  Part="1" 
F 0 "J18" H 4007 2067 50  0000 C CNN
F 1 "USB_B_Micro" H 4007 1976 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 4100 1550 50  0001 C CNN
F 3 "~" H 4100 1550 50  0001 C CNN
	1    3950 1600
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP1
U 1 1 5FCFC625
P 5100 1600
F 0 "JP1" H 5100 1795 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 5100 1704 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5100 1600 50  0001 C CNN
F 3 "~" H 5100 1600 50  0001 C CNN
	1    5100 1600
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP2
U 1 1 5FD012B6
P 5100 1900
F 0 "JP2" H 5100 2095 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 5100 2004 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5100 1900 50  0001 C CNN
F 3 "~" H 5100 1900 50  0001 C CNN
	1    5100 1900
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP3
U 1 1 5FD01406
P 5100 2200
F 0 "JP3" H 5100 2395 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 5100 2304 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5100 2200 50  0001 C CNN
F 3 "~" H 5100 2200 50  0001 C CNN
	1    5100 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1600 4900 1600
Wire Wire Line
	4250 1700 4750 1700
Wire Wire Line
	4750 1700 4750 1900
Wire Wire Line
	4750 1900 4900 1900
Wire Wire Line
	4250 1800 4650 1800
Wire Wire Line
	4650 1800 4650 2200
Wire Wire Line
	4650 2200 4900 2200
$Comp
L power:GND #PWR017
U 1 1 5FD0AF81
P 5650 2300
F 0 "#PWR017" H 5650 2050 50  0001 C CNN
F 1 "GND" H 5655 2127 50  0000 C CNN
F 2 "" H 5650 2300 50  0001 C CNN
F 3 "" H 5650 2300 50  0001 C CNN
	1    5650 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2200 5650 2200
Wire Wire Line
	5650 2200 5650 2300
Wire Wire Line
	5300 1900 5650 1900
Wire Wire Line
	5650 1900 5650 2200
Connection ~ 5650 2200
Wire Wire Line
	5300 1600 5650 1600
Wire Wire Line
	5650 1600 5650 1900
Connection ~ 5650 1900
Wire Notes Line
	3550 2550 5850 2550
Wire Notes Line
	3550 1000 5850 1000
Wire Notes Line
	3700 2750 4850 2750
Wire Notes Line
	3700 4000 4850 4000
Wire Notes Line
	4500 5700 6400 5700
Wire Notes Line
	4500 4300 6400 4300
$Comp
L Connector:TestPoint TP2
U 1 1 61207081
P 5250 7650
F 0 "TP2" H 5308 7768 50  0000 L CNN
F 1 "TestPoint" H 5308 7677 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5450 7650 50  0001 C CNN
F 3 "~" H 5450 7650 50  0001 C CNN
	1    5250 7650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 61207A22
P 5250 6400
F 0 "TP1" H 5308 6518 50  0000 L CNN
F 1 "TestPoint" H 5308 6427 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5450 6400 50  0001 C CNN
F 3 "~" H 5450 6400 50  0001 C CNN
	1    5250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6400 5250 6550
Wire Wire Line
	5250 7650 5250 7800
$EndSCHEMATC
