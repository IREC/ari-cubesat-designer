EESchema Schematic File Version 4
LIBS:NIM-PC104-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 8500 11000 portrait
encoding utf-8
Sheet 6 10
Title "Programming"
Date "2020-12-08"
Rev "B"
Comp "Aurora Research Institute"
Comment1 "Northern Images Mission Payload"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5FE61E87
P 4550 2250
AR Path="/5FE61E87" Ref="R?"  Part="1" 
AR Path="/5FE52742/5FE61E87" Ref="R13"  Part="1" 
F 0 "R13" H 4620 2296 50  0000 L CNN
F 1 "10K" H 4620 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4480 2250 50  0001 C CNN
F 3 "~" H 4550 2250 50  0001 C CNN
	1    4550 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FE61E8D
P 4200 2250
AR Path="/5FE61E8D" Ref="D?"  Part="1" 
AR Path="/5FE52742/5FE61E8D" Ref="D2"  Part="1" 
F 0 "D2" V 4154 2329 50  0000 L CNN
F 1 "CD1206_S01575" V 4245 2329 50  0000 L CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4200 2250 50  0001 C CNN
F 3 "https://www.bourns.com/pdfs/CD1206_S01575.pdf" H 4200 2250 50  0001 C CNN
	1    4200 2250
	0    -1   1    0   
$EndComp
Wire Wire Line
	4550 2400 4550 2550
Wire Wire Line
	4200 2400 4200 2550
Connection ~ 4200 2550
Wire Wire Line
	4200 2550 4550 2550
Wire Wire Line
	4550 2100 4200 2100
Wire Wire Line
	4200 2100 4200 1900
Connection ~ 4200 2100
$Comp
L Device:C C?
U 1 1 5FE61E9A
P 4200 2800
AR Path="/5FE61E9A" Ref="C?"  Part="1" 
AR Path="/5FE52742/5FE61E9A" Ref="C7"  Part="1" 
F 0 "C7" H 4315 2846 50  0000 L CNN
F 1 "22pF" H 4315 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4238 2650 50  0001 C CNN
F 3 "~" H 4200 2800 50  0001 C CNN
	1    4200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2650 4200 2550
$Comp
L power:GND #PWR?
U 1 1 5FE61EA1
P 4200 3000
AR Path="/5FE61EA1" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61EA1" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 4200 2750 50  0001 C CNN
F 1 "GND" H 4205 2827 50  0000 C CNN
F 2 "" H 4200 3000 50  0001 C CNN
F 3 "" H 4200 3000 50  0001 C CNN
	1    4200 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3000 4200 2950
$Comp
L power:+3V3 #PWR?
U 1 1 5FE61EA8
P 4200 1900
AR Path="/5FE61EA8" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61EA8" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 4200 1750 50  0001 C CNN
F 1 "+3V3" H 4215 2073 50  0000 C CNN
F 2 "" H 4200 1900 50  0001 C CNN
F 3 "" H 4200 1900 50  0001 C CNN
	1    4200 1900
	1    0    0    -1  
$EndComp
Wire Notes Line
	3100 1600 3100 3500
Text Notes 4800 3450 0    50   ~ 0
Reset Circuit
Text Notes 3900 6200 0    50   ~ 0
ISP Programming Interface\n(for bootloader)
$Comp
L power:+3V3 #PWR?
U 1 1 5FE61EB1
P 5050 5500
AR Path="/5FE61EB1" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61EB1" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 5050 5350 50  0001 C CNN
F 1 "+3V3" V 5065 5628 50  0000 L CNN
F 2 "" H 5050 5500 50  0001 C CNN
F 3 "" H 5050 5500 50  0001 C CNN
	1    5050 5500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE61EB7
P 5100 5700
AR Path="/5FE61EB7" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61EB7" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 5100 5450 50  0001 C CNN
F 1 "GND" V 5105 5572 50  0000 R CNN
F 2 "" H 5100 5700 50  0001 C CNN
F 3 "" H 5100 5700 50  0001 C CNN
	1    5100 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 5500 5050 5500
Wire Wire Line
	4600 5700 5100 5700
Wire Wire Line
	4600 5600 4700 5600
Wire Notes Line
	5500 6300 5500 5050
Wire Wire Line
	4550 2550 4750 2550
Connection ~ 4550 2550
$Comp
L power:+3V3 #PWR?
U 1 1 5FE61ED1
P 3650 6850
AR Path="/5FE61ED1" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61ED1" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 3650 6700 50  0001 C CNN
F 1 "+3V3" H 3665 7023 50  0000 C CNN
F 2 "" H 3650 6850 50  0001 C CNN
F 3 "" H 3650 6850 50  0001 C CNN
	1    3650 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 6850 3650 6950
Wire Wire Line
	3650 6950 4100 6950
Wire Wire Line
	4100 7050 3450 7050
$Comp
L power:GND #PWR?
U 1 1 5FE61EDB
P 3450 7050
AR Path="/5FE61EDB" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE61EDB" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 3450 6800 50  0001 C CNN
F 1 "GND" H 3455 6877 50  0000 C CNN
F 2 "" H 3450 7050 50  0001 C CNN
F 3 "" H 3450 7050 50  0001 C CNN
	1    3450 7050
	0    1    1    0   
$EndComp
Text Label 3550 7150 0    50   ~ 0
UART_DTR
Wire Wire Line
	3550 7150 4100 7150
Wire Wire Line
	3550 7250 4100 7250
Wire Wire Line
	3550 7350 4100 7350
Text Notes 3650 7700 0    50   ~ 0
General Programming Port\n(for sketch)
Wire Notes Line
	3100 1600 5350 1600
Wire Notes Line
	3100 3500 5350 3500
Wire Notes Line
	3100 6550 3100 7850
Wire Notes Line
	5600 7850 5600 6550
Text Notes 3800 6700 0    50   ~ 0
AUX Power Input
Wire Notes Line
	5350 3500 5350 1600
Text Label 4750 2550 0    50   ~ 0
ISP-RESET
Text Label 4750 2650 0    50   ~ 0
HUMAN-RESET
Wire Wire Line
	4750 2650 4550 2650
Wire Wire Line
	4550 2650 4550 2550
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 5FE61EF6
P 4300 5600
AR Path="/5FE61EF6" Ref="J?"  Part="1" 
AR Path="/5FE52742/5FE61EF6" Ref="J7"  Part="1" 
F 0 "J7" H 4350 5917 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even 609-3234-ND" H 4350 5826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4300 5600 50  0001 C CNN
F 3 "~" H 4300 5600 50  0001 C CNN
	1    4300 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE68EBB
P 4800 4400
AR Path="/5FE68EBB" Ref="#PWR?"  Part="1" 
AR Path="/5FE52742/5FE68EBB" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 4800 4150 50  0001 C CNN
F 1 "GND" H 4805 4227 50  0000 C CNN
F 2 "" H 4800 4400 50  0001 C CNN
F 3 "" H 4800 4400 50  0001 C CNN
	1    4800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4300 4800 4300
Wire Wire Line
	4800 4300 4800 4400
Wire Wire Line
	4700 4100 4700 4300
Text Label 3650 4100 0    50   ~ 0
HUMAN-RESET
Wire Notes Line
	5050 4750 5050 3750
Wire Notes Line
	3450 3750 3450 4750
Text Notes 4350 4700 0    50   ~ 0
Human UI Reset
Wire Wire Line
	4300 4300 4300 4100
$Comp
L NIM-PC104-rescue:B3F-1000-dk_Tactile-Switches S?
U 1 1 5FE68ED5
P 4500 4200
AR Path="/5FE68ED5" Ref="S?"  Part="1" 
AR Path="/5FE52742/5FE68ED5" Ref="S1"  Part="1" 
F 0 "S1" H 4500 4547 60  0000 C CNN
F 1 "B3F-1000" H 4500 4441 60  0000 C CNN
F 2 "digikey-footprints:Switch_Tactile_THT_B3F-1xxx" H 4700 4400 60  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3f.pdf" H 4700 4500 60  0001 L CNN
F 4 "SW400-ND" H 4700 4600 60  0001 L CNN "Digi-Key_PN"
F 5 "B3F-1000" H 4700 4700 60  0001 L CNN "MPN"
F 6 "Switches" H 4700 4800 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 4700 4900 60  0001 L CNN "Family"
F 8 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3f.pdf" H 4700 5000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/omron-electronics-inc-emc-div/B3F-1000/SW400-ND/33150" H 4700 5100 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 24V" H 4700 5200 60  0001 L CNN "Description"
F 11 "Omron Electronics Inc-EMC Div" H 4700 5300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4700 5400 60  0001 L CNN "Status"
	1    4500 4200
	1    0    0    -1  
$EndComp
Connection ~ 4300 4100
Connection ~ 4700 4300
Text HLabel 3750 2550 0    50   Input ~ 0
EXT_RESET
Wire Wire Line
	3750 2550 4200 2550
Text Label 4750 2750 0    50   ~ 0
UART_DTR
Wire Wire Line
	4550 2650 4550 2750
Wire Wire Line
	4550 2750 4750 2750
Connection ~ 4550 2650
Text HLabel 3550 7250 0    50   Input ~ 0
EXT_TX
Text HLabel 3550 7350 0    50   Input ~ 0
EXT_RX
Wire Wire Line
	3650 4100 4300 4100
Wire Notes Line
	3450 3750 5050 3750
Wire Notes Line
	3450 4750 5050 4750
Text HLabel 3850 5500 0    50   Input ~ 0
ISP-MISO
Text HLabel 3850 5600 0    50   Input ~ 0
ISP-CLK
Text HLabel 3850 5700 0    50   Input ~ 0
ISP-RESET
Text HLabel 4700 5600 2    50   Input ~ 0
ISP-MOSI
Wire Wire Line
	3850 5500 4100 5500
Wire Wire Line
	4100 5600 3850 5600
Wire Wire Line
	3850 5700 4100 5700
Wire Notes Line
	3200 5050 3200 6300
Wire Notes Line
	3200 6300 5500 6300
Wire Notes Line
	3200 5050 5500 5050
$Comp
L Connector_Generic+MP:Conn_01x05_MP J4
U 1 1 5FC1278A
P 4300 7150
F 0 "J4" H 4380 7142 50  0000 L CNN
F 1 "Conn_01x05_MP WM7609CT-ND" H 4380 7051 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0571_1x05-1MP_P1.25mm_Vertical" H 4300 7150 50  0001 C CNN
F 3 "~" H 4300 7150 50  0001 C CNN
	1    4300 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FC15AF4
P 3650 7450
F 0 "#PWR06" H 3650 7200 50  0001 C CNN
F 1 "GND" V 3655 7322 50  0000 R CNN
F 2 "" H 3650 7450 50  0001 C CNN
F 3 "" H 3650 7450 50  0001 C CNN
	1    3650 7450
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 7450 4100 7450
Wire Notes Line
	3100 6550 5600 6550
Wire Notes Line
	3100 7850 5600 7850
$EndSCHEMATC
