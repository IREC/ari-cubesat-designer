EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 8500 11000 portrait
encoding utf-8
Sheet 8 9
Title "SPI Mux"
Date "2022-01-11"
Rev "C.2"
Comp "Aurora Research Institute"
Comment1 "Northern Images Mission Payload"
Comment2 "QUALIFICATION MODEL"
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	2600 4650 3600 4650
Wire Notes Line
	2600 4000 2600 4650
Wire Notes Line
	3600 4000 2600 4000
Wire Notes Line
	3600 4650 3600 4000
Text Notes 2650 4600 0    50   ~ 0
Programming Selector
$Comp
L power:+3V3 #PWR?
U 1 1 5F906396
P 3400 4250
AR Path="/5F906396" Ref="#PWR?"  Part="1" 
AR Path="/5FE69B10/5F906396" Ref="#PWR?"  Part="1" 
AR Path="/5F8F9E96/5F906396" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 3400 4100 50  0001 C CNN
F 1 "+3V3" H 3415 4423 50  0000 C CNN
F 2 "" H 3400 4250 50  0001 C CNN
F 3 "" H 3400 4250 50  0001 C CNN
	1    3400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4400 3400 4250
Wire Wire Line
	3100 4400 3400 4400
Wire Wire Line
	4200 4500 4350 4500
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5F9063A1
P 2900 4400
AR Path="/5F9063A1" Ref="J?"  Part="1" 
AR Path="/5FE69B10/5F9063A1" Ref="J?"  Part="1" 
AR Path="/5F8F9E96/5F9063A1" Ref="J5"  Part="1" 
F 0 "J5" H 3008 4581 50  0000 C CNN
F 1 "Conn_01x02_Male" H 3008 4490 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2900 4400 50  0001 C CNN
F 3 "~" H 2900 4400 50  0001 C CNN
	1    2900 4400
	1    0    0    -1  
$EndComp
Connection ~ 3800 3950
Wire Wire Line
	4350 4500 4350 4700
$Comp
L Device:R R?
U 1 1 5F9063A9
P 4050 4500
AR Path="/5F9063A9" Ref="R?"  Part="1" 
AR Path="/5FE69B10/5F9063A9" Ref="R?"  Part="1" 
AR Path="/5F8F9E96/5F9063A9" Ref="R11"  Part="1" 
F 0 "R11" H 4120 4546 50  0000 L CNN
F 1 "10K" H 4120 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 4500 50  0001 C CNN
F 3 "~" H 4050 4500 50  0001 C CNN
	1    4050 4500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F9063AF
P 4350 4700
AR Path="/5F9063AF" Ref="#PWR?"  Part="1" 
AR Path="/5FE69B10/5F9063AF" Ref="#PWR?"  Part="1" 
AR Path="/5F8F9E96/5F9063AF" Ref="#PWR0171"  Part="1" 
F 0 "#PWR0171" H 4350 4450 50  0001 C CNN
F 1 "GND" H 4355 4527 50  0000 C CNN
F 2 "" H 4350 4700 50  0001 C CNN
F 3 "" H 4350 4700 50  0001 C CNN
	1    4350 4700
	1    0    0    -1  
$EndComp
Wire Notes Line
	2050 1450 6650 1450
Wire Notes Line
	6650 5100 2050 5100
Wire Wire Line
	5850 2150 5850 2200
$Comp
L power:+3V3 #PWR?
U 1 1 5F9063B8
P 5850 2150
AR Path="/5F9063B8" Ref="#PWR?"  Part="1" 
AR Path="/5FE69B10/5F9063B8" Ref="#PWR?"  Part="1" 
AR Path="/5F8F9E96/5F9063B8" Ref="#PWR0172"  Part="1" 
F 0 "#PWR0172" H 5850 2000 50  0001 C CNN
F 1 "+3V3" H 5865 2323 50  0000 C CNN
F 2 "" H 5850 2150 50  0001 C CNN
F 3 "" H 5850 2150 50  0001 C CNN
	1    5850 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3250 5850 3200
$Comp
L power:GND #PWR?
U 1 1 5F9063BF
P 5850 3250
AR Path="/5F9063BF" Ref="#PWR?"  Part="1" 
AR Path="/5FE69B10/5F9063BF" Ref="#PWR?"  Part="1" 
AR Path="/5F8F9E96/5F9063BF" Ref="#PWR0173"  Part="1" 
F 0 "#PWR0173" H 5850 3000 50  0001 C CNN
F 1 "GND" H 5855 3077 50  0000 C CNN
F 2 "" H 5850 3250 50  0001 C CNN
F 3 "" H 5850 3250 50  0001 C CNN
	1    5850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2200 4800 2200
Connection ~ 4650 2200
Wire Wire Line
	4650 2300 4800 2300
Wire Wire Line
	4650 2200 4650 2300
Wire Wire Line
	4550 2200 4650 2200
Wire Wire Line
	4550 2950 4800 2950
Wire Wire Line
	4650 3700 4800 3700
Connection ~ 4650 3700
Wire Wire Line
	4650 3800 4800 3800
Wire Wire Line
	4650 3700 4650 3800
Wire Wire Line
	4550 3700 4650 3700
Wire Wire Line
	3150 3700 3500 3700
Wire Wire Line
	3800 3200 3800 3950
Connection ~ 3800 3200
Wire Wire Line
	4250 3200 3800 3200
Wire Wire Line
	3800 3950 4250 3950
Wire Wire Line
	3800 2450 3800 3200
Wire Wire Line
	4250 2450 3800 2450
$Comp
L 74xx:74LS125 U?
U 5 1 5F9063DC
P 5850 2700
AR Path="/5F9063DC" Ref="U?"  Part="5" 
AR Path="/5FE69B10/5F9063DC" Ref="U?"  Part="5" 
AR Path="/5F8F9E96/5F9063DC" Ref="U2"  Part="5" 
F 0 "U2" H 6080 2746 50  0000 L CNN
F 1 "74LS125" H 6080 2655 50  0000 L CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 5850 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 5850 2700 50  0001 C CNN
	5    5850 2700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U?
U 4 1 5F9063E2
P 5900 4100
AR Path="/5F9063E2" Ref="U?"  Part="4" 
AR Path="/5FE69B10/5F9063E2" Ref="U?"  Part="4" 
AR Path="/5F8F9E96/5F9063E2" Ref="U2"  Part="4" 
F 0 "U2" H 5900 4417 50  0000 C CNN
F 1 "74LS125" H 5900 4326 50  0000 C CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 5900 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 5900 4100 50  0001 C CNN
	4    5900 4100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U?
U 3 1 5F9063E8
P 4250 3700
AR Path="/5F9063E8" Ref="U?"  Part="3" 
AR Path="/5FE69B10/5F9063E8" Ref="U?"  Part="3" 
AR Path="/5F8F9E96/5F9063E8" Ref="U2"  Part="3" 
F 0 "U2" H 4250 4017 50  0000 C CNN
F 1 "74LS125" H 4250 3926 50  0000 C CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 4250 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 4250 3700 50  0001 C CNN
	3    4250 3700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U?
U 2 1 5F9063EE
P 4250 2950
AR Path="/5F9063EE" Ref="U?"  Part="2" 
AR Path="/5FE69B10/5F9063EE" Ref="U?"  Part="2" 
AR Path="/5F8F9E96/5F9063EE" Ref="U2"  Part="2" 
F 0 "U2" H 4250 3267 50  0000 C CNN
F 1 "74LS125" H 4250 3176 50  0000 C CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 4250 2950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 4250 2950 50  0001 C CNN
	2    4250 2950
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U?
U 1 1 5F9063F4
P 4250 2200
AR Path="/5F9063F4" Ref="U?"  Part="1" 
AR Path="/5FE69B10/5F9063F4" Ref="U?"  Part="1" 
AR Path="/5F8F9E96/5F9063F4" Ref="U2"  Part="1" 
F 0 "U2" H 4250 2517 50  0000 C CNN
F 1 "74LS125" H 4250 2426 50  0000 C CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 4250 2200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 4250 2200 50  0001 C CNN
	1    4250 2200
	1    0    0    -1  
$EndComp
Text Notes 4800 2000 0    50   ~ 0
Always Operative
Text Notes 2150 1650 0    98   ~ 0
SPI - BUS
Text HLabel 4800 2950 2    50   Input ~ 0
SD-MISO
Text HLabel 4800 2300 2    50   Input ~ 0
SD-CLK
Text HLabel 4800 2200 2    50   Input ~ 0
OLED-CLK
Text HLabel 4800 3700 2    50   Input ~ 0
OLED-MOSI
Text HLabel 4800 3800 2    50   Input ~ 0
SD-MOSI
Text HLabel 3150 3700 0    50   Input ~ 0
ISP-MOSI
Wire Wire Line
	3100 4500 3800 4500
Wire Wire Line
	3800 3950 3800 4500
Connection ~ 3800 4500
Wire Wire Line
	3800 4500 3900 4500
Wire Notes Line
	2050 1450 2050 5100
Wire Notes Line
	6650 1450 6650 5100
Text HLabel 3150 3850 0    50   Input ~ 0
AVR-MOSI
Wire Wire Line
	3150 3850 3500 3850
Wire Wire Line
	3500 3850 3500 3700
Wire Wire Line
	3500 3700 3950 3700
Connection ~ 3500 3700
Text HLabel 3100 2950 0    50   Input ~ 0
ISP-MISO
Text HLabel 3100 3100 0    50   Input ~ 0
AVR-MISO
Text HLabel 3100 2200 0    50   Input ~ 0
ISP-CLK
Text HLabel 3100 2350 0    50   Input ~ 0
AVR-CLK
Wire Wire Line
	3100 2950 3500 2950
Wire Wire Line
	3100 3100 3500 3100
Wire Wire Line
	3500 3100 3500 2950
Connection ~ 3500 2950
Wire Wire Line
	3500 2950 3950 2950
Wire Wire Line
	3100 2200 3500 2200
Wire Wire Line
	3100 2350 3500 2350
Wire Wire Line
	3500 2350 3500 2200
Connection ~ 3500 2200
Wire Wire Line
	3500 2200 3950 2200
$Comp
L power:GND #PWR08
U 1 1 5FC1930B
P 5500 4100
F 0 "#PWR08" H 5500 3850 50  0001 C CNN
F 1 "GND" V 5505 3972 50  0000 R CNN
F 2 "" H 5500 4100 50  0001 C CNN
F 3 "" H 5500 4100 50  0001 C CNN
	1    5500 4100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5FC19D6F
P 6300 4100
F 0 "#PWR010" H 6300 3850 50  0001 C CNN
F 1 "GND" V 6305 3972 50  0000 R CNN
F 2 "" H 6300 4100 50  0001 C CNN
F 3 "" H 6300 4100 50  0001 C CNN
	1    6300 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 4100 5600 4100
Wire Wire Line
	6200 4100 6300 4100
$Comp
L power:GND #PWR?
U 1 1 5FC1FC64
P 5900 4400
AR Path="/5FC1FC64" Ref="#PWR?"  Part="1" 
AR Path="/5FE69B10/5FC1FC64" Ref="#PWR?"  Part="1" 
AR Path="/5F8F9E96/5FC1FC64" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 5900 4150 50  0001 C CNN
F 1 "GND" H 5905 4227 50  0000 C CNN
F 2 "" H 5900 4400 50  0001 C CNN
F 3 "" H 5900 4400 50  0001 C CNN
	1    5900 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4400 5900 4350
$EndSCHEMATC
