EESchema Schematic File Version 4
LIBS:CAM-PC104SubBoard-cache
EELAYER 29 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2800 7200 0    50   ~ 0
5V to 3.3V Regulator
Connection ~ 2800 6550
$Comp
L Connector:TestPoint TP2
U 1 1 5D8FB71E
P 2800 6450
F 0 "TP2" H 2858 6568 50  0000 L CNN
F 1 "TestPoint" H 2858 6477 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 3000 6450 50  0001 C CNN
F 3 "~" H 3000 6450 50  0001 C CNN
	1    2800 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 6450 2800 6550
$Comp
L Connector:TestPoint TP1
U 1 1 5D8FB725
P 1450 6400
F 0 "TP1" H 1508 6518 50  0000 L CNN
F 1 "TestPoint" H 1508 6427 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 1650 6400 50  0001 C CNN
F 3 "~" H 1650 6400 50  0001 C CNN
	1    1450 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 6400 1450 6550
Wire Wire Line
	1200 6550 1250 6550
Connection ~ 1450 6550
Wire Wire Line
	6650 6800 6400 6800
$Comp
L power:GND #PWR014
U 1 1 5D8FB736
P 5900 7550
F 0 "#PWR014" H 5900 7300 50  0001 C CNN
F 1 "GND" H 5905 7377 50  0000 C CNN
F 2 "" H 5900 7550 50  0001 C CNN
F 3 "" H 5900 7550 50  0001 C CNN
	1    5900 7550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5D8FB73C
P 6400 6550
F 0 "R3" H 6470 6596 50  0000 L CNN
F 1 "10K" H 6470 6505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6330 6550 50  0001 C CNN
F 3 "~" H 6400 6550 50  0001 C CNN
	1    6400 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5D8FB742
P 6400 7100
F 0 "C12" H 6515 7146 50  0000 L CNN
F 1 "1uF" H 6515 7055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6438 6950 50  0001 C CNN
F 3 "~" H 6400 7100 50  0001 C CNN
	1    6400 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 6700 6400 6800
Connection ~ 6400 6800
Wire Wire Line
	6400 6800 6400 6950
Wire Wire Line
	6000 7150 6200 7150
Wire Wire Line
	6200 7150 6200 6800
Wire Wire Line
	6200 6800 6400 6800
$Comp
L power:+3V3 #PWR017
U 1 1 5D8FB74E
P 6400 6300
F 0 "#PWR017" H 6400 6150 50  0001 C CNN
F 1 "+3V3" H 6415 6473 50  0000 C CNN
F 2 "" H 6400 6300 50  0001 C CNN
F 3 "" H 6400 6300 50  0001 C CNN
	1    6400 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 6300 6400 6400
Wire Wire Line
	5350 7550 5900 7550
Wire Wire Line
	5900 7550 6400 7550
Wire Wire Line
	6400 7550 6400 7250
Connection ~ 5900 7550
Text Label 6650 6800 0    50   ~ 0
RESET
Text Notes 6300 7800 0    50   ~ 0
VC0703 Reset\n
Wire Notes Line
	7050 7900 7050 6050
Wire Notes Line
	7050 6050 5250 6050
Wire Notes Line
	5250 7900 7050 7900
Wire Wire Line
	7250 1550 7250 1900
Wire Wire Line
	7150 1300 7150 1900
Wire Wire Line
	6850 1050 6850 1900
Wire Wire Line
	6750 950  6750 1900
Wire Wire Line
	6650 850  6650 1900
Wire Wire Line
	6550 750  6550 1900
Wire Wire Line
	7250 1550 7750 1550
Wire Wire Line
	7150 1300 7750 1300
Text Label 7750 1550 0    50   ~ 0
TEST_TX
Text Label 7750 1300 0    50   ~ 0
TEST_RX
Wire Wire Line
	6850 1050 8350 1050
Wire Wire Line
	6650 850  8350 850 
Wire Wire Line
	6550 750  8350 750 
Text Label 8350 750  0    50   ~ 0
MCU_MISO
Text Label 8350 850  0    50   ~ 0
MCU_MOSI
Text Label 8350 950  0    50   ~ 0
MCU_SCK
Text Label 8350 1050 0    50   ~ 0
MCU_CS_CS
Wire Wire Line
	6750 950  8350 950 
Text Label 6250 4350 0    50   ~ 0
RESET
Text Label 2050 4600 0    50   ~ 0
FRAME_VALID
$Comp
L Jumper:Jumper_2_Bridged JP1
U 1 1 5D8FB7CA
P 3200 4600
F 0 "JP1" H 3200 4795 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 3200 4704 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 3200 4600 50  0001 C CNN
F 3 "~" H 3200 4600 50  0001 C CNN
	1    3200 4600
	1    0    0    -1  
$EndComp
Text Label 3750 4600 0    50   ~ 0
CS_VSYNC
Wire Wire Line
	5150 2650 5600 2650
Wire Wire Line
	5150 2550 5600 2550
Text Label 5150 2650 0    50   ~ 0
CS_VSYNC
Text Label 5150 2550 0    50   ~ 0
CS_HSYNC
Text Label 3700 5250 0    50   ~ 0
CS_HSYNC
Text Label 2050 5250 0    50   ~ 0
LINE_VALID
Text Label 4450 1700 0    50   ~ 0
LED_OUT
Text Label 4450 1800 0    50   ~ 0
PICCLK
Text Label 4450 1900 0    50   ~ 0
FRAME_VALID
Text Label 4450 2000 0    50   ~ 0
LINE_VALID
Wire Wire Line
	3950 1700 4450 1700
Wire Wire Line
	3950 1900 4450 1900
Wire Wire Line
	3950 2000 4450 2000
$Comp
L Jumper:Jumper_2_Bridged JP2
U 1 1 5D8FB7FF
P 3200 5250
F 0 "JP2" H 3200 5445 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 3200 5354 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 3200 5250 50  0001 C CNN
F 3 "~" H 3200 5250 50  0001 C CNN
	1    3200 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2350 5600 2350
Wire Wire Line
	5150 2450 5600 2450
Text Label 5150 2450 0    50   ~ 0
CAM_SCK
Text Label 5150 2350 0    50   ~ 0
CAM_SDA
Text Label 2100 2850 0    50   ~ 0
CAM_SDA
Text Label 2100 2750 0    50   ~ 0
CAM_SCK
Wire Wire Line
	10350 8050 10350 8100
Wire Wire Line
	10200 8050 10350 8050
Connection ~ 9050 8200
Wire Wire Line
	10000 8200 9050 8200
Wire Wire Line
	9050 7900 10000 7900
$Comp
L Device:R R6
U 1 1 5D8FB816
P 9050 8050
F 0 "R6" H 9120 8096 50  0000 L CNN
F 1 "1M" H 9120 8005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8980 8050 50  0001 C CNN
F 3 "~" H 9050 8050 50  0001 C CNN
	1    9050 8050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5D8FB81C
P 10350 8100
F 0 "#PWR032" H 10350 7850 50  0001 C CNN
F 1 "GND" H 10355 7927 50  0000 C CNN
F 2 "" H 10350 8100 50  0001 C CNN
F 3 "" H 10350 8100 50  0001 C CNN
	1    10350 8100
	1    0    0    -1  
$EndComp
$Comp
L Device:Resonator Y1
U 1 1 5D8FB822
P 10000 8050
F 0 "Y1" V 10046 8161 50  0000 L CNN
F 1 "Resonator 6MHz" V 9955 8161 50  0000 L CNN
F 2 "Resonator:Ceramic_Res_6MHz_SMD_Custom" H 9975 8050 50  0001 C CNN
F 3 "~" H 9975 8050 50  0001 C CNN
	1    10000 8050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 4350 6250 4350
Wire Wire Line
	6650 3900 6650 4350
Wire Wire Line
	6100 1550 6100 1750
Wire Wire Line
	6350 1750 6350 1900
Wire Wire Line
	6100 1750 6350 1750
Wire Wire Line
	6000 1400 6000 1800
Wire Wire Line
	6250 1800 6250 1900
Wire Wire Line
	6000 1800 6250 1800
Wire Wire Line
	5900 1550 5900 1850
Wire Wire Line
	6150 1850 5900 1850
Wire Wire Line
	6150 1900 6150 1850
Wire Wire Line
	3950 3100 4450 3100
Wire Wire Line
	3950 3000 4450 3000
Wire Wire Line
	3950 2900 4450 2900
Wire Wire Line
	3950 2800 4450 2800
Wire Wire Line
	3950 2700 4450 2700
Wire Wire Line
	3950 2600 4450 2600
Wire Wire Line
	3950 2500 4450 2500
Wire Wire Line
	3950 2400 4450 2400
Wire Wire Line
	4450 2300 3950 2300
Wire Wire Line
	3950 2200 4450 2200
Text Label 4450 2200 2    50   ~ 0
IMG_D9
Text Label 4450 2300 2    50   ~ 0
IMG_D8
Text Label 4450 2400 2    50   ~ 0
IMG_D7
Text Label 4450 2500 2    50   ~ 0
IMG_D6
Text Label 4450 2600 2    50   ~ 0
IMG_D5
Text Label 4450 2700 2    50   ~ 0
IMG_D4
Text Label 4450 2800 2    50   ~ 0
IMG_D3
Text Label 4450 2900 2    50   ~ 0
IMG_D2
Text Label 4450 3000 2    50   ~ 0
IMG_D1
Text Label 4450 3100 2    50   ~ 0
IMG_D0
Wire Wire Line
	6350 4200 6350 3900
Wire Wire Line
	5300 4200 6350 4200
Wire Wire Line
	6250 4100 6250 3900
Wire Wire Line
	5300 4100 6250 4100
Wire Wire Line
	6150 4000 5300 4000
Wire Wire Line
	6150 3900 6150 4000
Text Label 5300 4200 0    50   ~ 0
IMG_D9
Text Label 5300 4100 0    50   ~ 0
IMG_D8
Text Label 5300 4000 0    50   ~ 0
IMG_D7
Wire Wire Line
	5300 3450 5600 3450
Wire Wire Line
	5300 3350 5600 3350
Wire Wire Line
	5300 3250 5600 3250
Wire Wire Line
	5600 3150 5300 3150
Wire Wire Line
	5300 3050 5600 3050
Text Label 5300 3450 0    50   ~ 0
IMG_D6
Text Label 5300 3350 0    50   ~ 0
IMG_D5
Text Label 5300 3250 0    50   ~ 0
IMG_D4
Text Label 5300 3150 0    50   ~ 0
IMG_D3
Wire Wire Line
	6850 4600 6850 4900
Connection ~ 5700 1400
Wire Wire Line
	5700 1100 5700 1400
Wire Wire Line
	6000 1100 6000 1400
Wire Wire Line
	6300 1100 6300 1400
Wire Wire Line
	9050 3300 9050 3050
Wire Wire Line
	7600 3050 9050 3050
Wire Wire Line
	9050 3700 9050 3600
$Comp
L power:GND #PWR030
U 1 1 5D8FB886
P 9050 3700
F 0 "#PWR030" H 9050 3450 50  0001 C CNN
F 1 "GND" H 9055 3527 50  0000 C CNN
F 2 "" H 9050 3700 50  0001 C CNN
F 3 "" H 9050 3700 50  0001 C CNN
	1    9050 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D8FB88C
P 9050 3450
F 0 "R5" H 9120 3496 50  0000 L CNN
F 1 "75" H 9120 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8980 3450 50  0001 C CNN
F 3 "~" H 9050 3450 50  0001 C CNN
	1    9050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3150 8600 3150
Connection ~ 8200 3150
Wire Wire Line
	8200 3300 8200 3150
Wire Wire Line
	8600 3150 8600 3300
Wire Wire Line
	7600 3150 8200 3150
Wire Wire Line
	8200 3650 8200 3600
Connection ~ 8200 3650
Wire Wire Line
	8600 3650 8200 3650
Wire Wire Line
	8600 3600 8600 3650
Wire Wire Line
	8200 3700 8200 3650
$Comp
L Device:C C18
U 1 1 5D8FB89C
P 8600 3450
F 0 "C18" H 8715 3496 50  0000 L CNN
F 1 "100nF" H 8715 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8638 3300 50  0001 C CNN
F 3 "~" H 8600 3450 50  0001 C CNN
	1    8600 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5D8FB8A2
P 8200 3450
F 0 "C16" H 8315 3496 50  0000 L CNN
F 1 "1uF" H 8315 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8238 3300 50  0001 C CNN
F 3 "~" H 8200 3450 50  0001 C CNN
	1    8200 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR026
U 1 1 5D8FB8A8
P 8200 3700
F 0 "#PWR026" H 8200 3550 50  0001 C CNN
F 1 "+3V3" H 8215 3873 50  0000 C CNN
F 2 "" H 8200 3700 50  0001 C CNN
F 3 "" H 8200 3700 50  0001 C CNN
	1    8200 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7850 3250 7850 3300
Wire Wire Line
	7600 3250 7850 3250
Wire Wire Line
	7700 3600 7700 3700
Connection ~ 7700 3600
Wire Wire Line
	7850 3600 7700 3600
Wire Wire Line
	7700 3350 7700 3600
$Comp
L Device:R R4
U 1 1 5D8FB8B4
P 7850 3450
F 0 "R4" H 7920 3496 50  0000 L CNN
F 1 "270" H 7920 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7780 3450 50  0001 C CNN
F 3 "~" H 7850 3450 50  0001 C CNN
	1    7850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3350 7700 3350
$Comp
L power:GND #PWR022
U 1 1 5D8FB8BB
P 7700 3700
F 0 "#PWR022" H 7700 3450 50  0001 C CNN
F 1 "GND" H 7705 3527 50  0000 C CNN
F 2 "" H 7700 3700 50  0001 C CNN
F 3 "" H 7700 3700 50  0001 C CNN
	1    7700 3700
	1    0    0    -1  
$EndComp
$Comp
L VC0703PLEB:VC0703PLEB U4
U 1 1 5D8FB8C1
P 6600 2900
F 0 "U4" H 7644 2946 50  0000 L CNN
F 1 "VC0703PLEB" H 7644 2855 50  0000 L CNN
F 2 "VC0703:VC0703PREB-QFP-48_7x7_Pitch0.5mm" H 7400 1900 50  0001 C CNN
F 3 "" H 7400 1900 50  0001 C CNN
	1    6600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1400 5700 1550
Wire Wire Line
	5700 1550 5900 1550
$Comp
L power:+3V3 #PWR013
U 1 1 5D8FB8C9
P 5700 1100
F 0 "#PWR013" H 5700 950 50  0001 C CNN
F 1 "+3V3" H 5715 1273 50  0000 C CNN
F 2 "" H 5700 1100 50  0001 C CNN
F 3 "" H 5700 1100 50  0001 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
Connection ~ 6300 1400
Wire Wire Line
	6300 1550 6100 1550
Wire Wire Line
	6300 1400 6300 1550
$Comp
L power:+1V2 #PWR016
U 1 1 5D8FB8D2
P 6300 1100
F 0 "#PWR016" H 6300 950 50  0001 C CNN
F 1 "+1V2" H 6315 1273 50  0000 C CNN
F 2 "" H 6300 1100 50  0001 C CNN
F 3 "" H 6300 1100 50  0001 C CNN
	1    6300 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5D8FB8D8
P 5850 1400
F 0 "C10" V 5598 1400 50  0000 C CNN
F 1 "100nF" V 5689 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5888 1250 50  0001 C CNN
F 3 "~" H 5850 1400 50  0001 C CNN
	1    5850 1400
	0    1    1    0   
$EndComp
Connection ~ 6000 1400
$Comp
L Device:C C11
U 1 1 5D8FB8DF
P 6150 1400
F 0 "C11" V 5898 1400 50  0000 C CNN
F 1 "100nF" V 5989 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6188 1250 50  0001 C CNN
F 3 "~" H 6150 1400 50  0001 C CNN
	1    6150 1400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5D8FB8E5
P 6000 1100
F 0 "#PWR015" H 6000 850 50  0001 C CNN
F 1 "GND" H 6005 927 50  0000 C CNN
F 2 "" H 6000 1100 50  0001 C CNN
F 3 "" H 6000 1100 50  0001 C CNN
	1    6000 1100
	-1   0    0    1   
$EndComp
Connection ~ 6550 4600
Wire Wire Line
	6550 4600 6550 4900
Connection ~ 7150 4600
Wire Wire Line
	7150 4900 7150 4600
$Comp
L power:+3V3 #PWR018
U 1 1 5D8FB8EF
P 6550 4900
F 0 "#PWR018" H 6550 4750 50  0001 C CNN
F 1 "+3V3" H 6565 5073 50  0000 C CNN
F 2 "" H 6550 4900 50  0001 C CNN
F 3 "" H 6550 4900 50  0001 C CNN
	1    6550 4900
	-1   0    0    1   
$EndComp
$Comp
L power:+1V2 #PWR020
U 1 1 5D8FB8F5
P 7150 4900
F 0 "#PWR020" H 7150 4750 50  0001 C CNN
F 1 "+1V2" H 7165 5073 50  0000 C CNN
F 2 "" H 7150 4900 50  0001 C CNN
F 3 "" H 7150 4900 50  0001 C CNN
	1    7150 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 4450 6750 3900
Wire Wire Line
	6550 4450 6750 4450
Wire Wire Line
	6550 4600 6550 4450
Wire Wire Line
	7150 4450 7150 4600
Wire Wire Line
	6950 4450 7150 4450
Wire Wire Line
	6950 3900 6950 4450
$Comp
L Device:C C13
U 1 1 5D8FB901
P 6700 4600
F 0 "C13" V 6448 4600 50  0000 C CNN
F 1 "100nF" V 6539 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6738 4450 50  0001 C CNN
F 3 "~" H 6700 4600 50  0001 C CNN
	1    6700 4600
	0    1    1    0   
$EndComp
Connection ~ 6850 4600
$Comp
L Device:C C14
U 1 1 5D8FB908
P 7000 4600
F 0 "C14" V 6748 4600 50  0000 C CNN
F 1 "100nF" V 6839 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7038 4450 50  0001 C CNN
F 3 "~" H 7000 4600 50  0001 C CNN
	1    7000 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 3900 6850 4600
$Comp
L power:GND #PWR019
U 1 1 5D8FB90F
P 6850 4900
F 0 "#PWR019" H 6850 4650 50  0001 C CNN
F 1 "GND" H 6855 4727 50  0000 C CNN
F 2 "" H 6850 4900 50  0001 C CNN
F 3 "" H 6850 4900 50  0001 C CNN
	1    6850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3950 7150 3950
Connection ~ 7400 3950
Wire Wire Line
	7400 4000 7400 3950
Wire Wire Line
	7150 3950 7150 3900
Wire Wire Line
	7750 3950 7400 3950
Connection ~ 7400 4350
Wire Wire Line
	7400 4350 7400 4300
Wire Wire Line
	7400 4350 7400 4400
Wire Wire Line
	7050 4350 7400 4350
Wire Wire Line
	7050 3900 7050 4350
$Comp
L power:+3V3 #PWR023
U 1 1 5D8FB91F
P 7750 3950
F 0 "#PWR023" H 7750 3800 50  0001 C CNN
F 1 "+3V3" H 7765 4123 50  0000 C CNN
F 2 "" H 7750 3950 50  0001 C CNN
F 3 "" H 7750 3950 50  0001 C CNN
	1    7750 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C15
U 1 1 5D8FB925
P 7400 4150
F 0 "C15" H 7515 4196 50  0000 L CNN
F 1 "100nF" H 7515 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7438 4000 50  0001 C CNN
F 3 "~" H 7400 4150 50  0001 C CNN
	1    7400 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5D8FB92B
P 7400 4400
F 0 "#PWR021" H 7400 4150 50  0001 C CNN
F 1 "GND" H 7405 4227 50  0000 C CNN
F 2 "" H 7400 4400 50  0001 C CNN
F 3 "" H 7400 4400 50  0001 C CNN
	1    7400 4400
	1    0    0    -1  
$EndComp
Text Notes 2850 8350 0    50   ~ 0
3.3V to 1.2V Regulator
$Comp
L power:+5V #PWR01
U 1 1 5D8FB9A7
P 1200 6550
F 0 "#PWR01" H 1200 6400 50  0001 C CNN
F 1 "+5V" V 1215 6678 50  0000 L CNN
F 2 "" H 1200 6550 50  0001 C CNN
F 3 "" H 1200 6550 50  0001 C CNN
	1    1200 6550
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR029
U 1 1 5D8FB9AD
P 8800 6350
F 0 "#PWR029" H 8800 6200 50  0001 C CNN
F 1 "+3.3V" H 8815 6523 50  0000 C CNN
F 2 "" H 8800 6350 50  0001 C CNN
F 3 "" H 8800 6350 50  0001 C CNN
	1    8800 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D8FB9BF
P 9900 6700
F 0 "R7" H 9970 6746 50  0000 L CNN
F 1 "10K" H 9970 6655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9830 6700 50  0001 C CNN
F 3 "~" H 9900 6700 50  0001 C CNN
	1    9900 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5D8FB9C5
P 10050 7150
F 0 "R8" H 10120 7196 50  0000 L CNN
F 1 "10K" H 10120 7105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9980 7150 50  0001 C CNN
F 3 "~" H 10050 7150 50  0001 C CNN
	1    10050 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 6350 8800 6550
$Comp
L power:GND #PWR031
U 1 1 5D8FB9D0
P 9900 7400
F 0 "#PWR031" H 9900 7150 50  0001 C CNN
F 1 "GND" H 9905 7227 50  0000 C CNN
F 2 "" H 9900 7400 50  0001 C CNN
F 3 "" H 9900 7400 50  0001 C CNN
	1    9900 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 6850 9900 7300
Wire Wire Line
	10050 7300 9900 7300
Connection ~ 9900 7300
Wire Wire Line
	9900 7300 9900 7400
Wire Wire Line
	8800 7000 8800 6550
Connection ~ 8800 6550
Wire Wire Line
	9900 6550 10250 6550
Wire Wire Line
	10050 7000 10250 7000
Text Label 10250 6550 0    50   ~ 0
CAM_ADDR_0
Text Label 10250 7000 0    50   ~ 0
CAM_ADDR_1
Text Label 2100 2550 0    50   ~ 0
CAM_ADDR_0
Text Label 2100 2650 0    50   ~ 0
CAM_ADDR_1
Wire Wire Line
	2100 2650 2700 2650
Wire Wire Line
	2100 2550 2700 2550
Text Notes 9550 7700 0    50   ~ 0
Image Sensor Address Selector
Wire Notes Line
	10850 7750 8600 7750
Wire Notes Line
	8600 7750 8600 6100
Wire Notes Line
	8600 6100 10850 6100
Wire Notes Line
	10850 6100 10850 7750
Wire Wire Line
	2700 2350 2000 2350
$Comp
L power:GND #PWR07
U 1 1 5D8FB9EC
P 3150 3700
F 0 "#PWR07" H 3150 3450 50  0001 C CNN
F 1 "GND" H 3155 3527 50  0000 C CNN
F 2 "" H 3150 3700 50  0001 C CNN
F 3 "" H 3150 3700 50  0001 C CNN
	1    3150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3500 2950 3650
Connection ~ 2950 3650
Wire Wire Line
	2950 3650 3050 3650
Wire Wire Line
	3050 3650 3050 3500
Connection ~ 3050 3650
Wire Wire Line
	3050 3650 3150 3650
Wire Wire Line
	3150 3500 3150 3650
Text Label 2100 2450 0    50   ~ 0
STANDBY
Wire Wire Line
	2100 2450 2700 2450
$Comp
L Device:C C2
U 1 1 5D8FBA02
P 1350 3450
F 0 "C2" H 1465 3496 50  0000 L CNN
F 1 "0.1uF" H 1465 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 3300 50  0001 C CNN
F 3 "~" H 1350 3450 50  0001 C CNN
	1    1350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3650 1350 3600
Wire Wire Line
	1350 3300 1350 2250
Wire Wire Line
	1350 2250 2700 2250
Text Label 2100 2350 0    50   ~ 0
Exposure
Wire Wire Line
	2100 2750 2700 2750
$Comp
L Device:R R2
U 1 1 5D8FBA0F
P 1350 2050
F 0 "R2" H 1420 2096 50  0000 L CNN
F 1 "10K" H 1420 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1280 2050 50  0001 C CNN
F 3 "~" H 1350 2050 50  0001 C CNN
	1    1350 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D8FBA15
P 1050 2050
F 0 "R1" H 1120 2096 50  0000 L CNN
F 1 "1.5K" H 1120 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 980 2050 50  0001 C CNN
F 3 "~" H 1050 2050 50  0001 C CNN
	1    1050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2850 1050 2200
$Comp
L power:+3V3 #PWR05
U 1 1 5D8FBA1C
P 2800 1100
F 0 "#PWR05" H 2800 950 50  0001 C CNN
F 1 "+3V3" H 2815 1273 50  0000 C CNN
F 2 "" H 2800 1100 50  0001 C CNN
F 3 "" H 2800 1100 50  0001 C CNN
	1    2800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1450 2950 1250
Connection ~ 2950 1250
Wire Wire Line
	2800 1250 2800 1100
Wire Wire Line
	3150 1450 3150 1250
Wire Wire Line
	8750 7900 9050 7900
Connection ~ 9050 7900
Text Label 5300 2950 0    50   ~ 0
IMG_D1
Text Label 5300 3050 0    50   ~ 0
IMG_D2
Wire Wire Line
	5300 2850 5600 2850
Wire Wire Line
	5600 2950 5300 2950
Text Label 5300 2850 0    50   ~ 0
PICCLK
Text Label 2100 2050 0    50   ~ 0
SYSCLK
Text Label 5300 2750 0    50   ~ 0
SYSCLK
Wire Wire Line
	2100 2050 2700 2050
Wire Wire Line
	1350 2250 1350 2200
Connection ~ 1350 2250
Wire Wire Line
	2700 2150 1650 2150
Wire Wire Line
	1650 2150 1650 1250
Wire Wire Line
	2950 1250 2800 1250
Wire Wire Line
	1050 1250 1050 1900
Connection ~ 1650 1250
Wire Wire Line
	1650 1250 1350 1250
Wire Wire Line
	1350 1900 1350 1250
Connection ~ 1350 1250
$Comp
L power:GNDA #PWR08
U 1 1 5D8FBA3A
P 3400 3700
F 0 "#PWR08" H 3400 3450 50  0001 C CNN
F 1 "GNDA" H 3405 3527 50  0000 C CNN
F 2 "" H 3400 3700 50  0001 C CNN
F 3 "" H 3400 3700 50  0001 C CNN
	1    3400 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3500 3250 3650
Wire Wire Line
	3250 3650 3400 3650
$Comp
L power:+3V3 #PWR010
U 1 1 5D8FBA48
P 3500 6450
F 0 "#PWR010" H 3500 6300 50  0001 C CNN
F 1 "+3V3" V 3515 6578 50  0000 L CNN
F 2 "" H 3500 6450 50  0001 C CNN
F 3 "" H 3500 6450 50  0001 C CNN
	1    3500 6450
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AP2127N-1.2 U2
U 1 1 5D8FBA4E
P 2150 7650
F 0 "U2" H 2150 7892 50  0000 C CNN
F 1 "AP2127N-1.2" H 2150 7801 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2150 7875 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AP2127.pdf" H 2150 7650 50  0001 C CNN
	1    2150 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5D8FBA54
P 1500 7900
F 0 "C3" H 1615 7946 50  0000 L CNN
F 1 "1uF" H 1615 7855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1538 7750 50  0001 C CNN
F 3 "~" H 1500 7900 50  0001 C CNN
	1    1500 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 7750 1500 7650
Wire Wire Line
	1500 7650 1850 7650
Wire Wire Line
	1500 8050 2150 8050
Wire Wire Line
	2150 8050 2150 7950
$Comp
L power:GND #PWR04
U 1 1 5D8FBA5E
P 2150 8150
F 0 "#PWR04" H 2150 7900 50  0001 C CNN
F 1 "GND" H 2155 7977 50  0000 C CNN
F 2 "" H 2150 8150 50  0001 C CNN
F 3 "" H 2150 8150 50  0001 C CNN
	1    2150 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 8150 2150 8050
Connection ~ 2150 8050
$Comp
L Device:C C6
U 1 1 5D8FBA66
P 2650 7900
F 0 "C6" H 2765 7946 50  0000 L CNN
F 1 "10uF" H 2765 7855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2688 7750 50  0001 C CNN
F 3 "~" H 2650 7900 50  0001 C CNN
	1    2650 7900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5D8FBA6C
P 3100 7900
F 0 "C7" H 3215 7946 50  0000 L CNN
F 1 "100nF" H 3215 7855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3138 7750 50  0001 C CNN
F 3 "~" H 3100 7900 50  0001 C CNN
	1    3100 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 8050 2650 8050
Connection ~ 2650 8050
Wire Wire Line
	2650 8050 3100 8050
Wire Wire Line
	2450 7650 2650 7650
Wire Wire Line
	2650 7650 2650 7750
Wire Wire Line
	3100 7650 3100 7750
Connection ~ 2650 7650
Wire Wire Line
	3100 7650 3400 7650
Connection ~ 3100 7650
$Comp
L power:+1V2 #PWR09
U 1 1 5D8FBA7B
P 3400 7650
F 0 "#PWR09" H 3400 7500 50  0001 C CNN
F 1 "+1V2" V 3415 7778 50  0000 L CNN
F 2 "" H 3400 7650 50  0001 C CNN
F 3 "" H 3400 7650 50  0001 C CNN
	1    3400 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 6550 1700 6550
$Comp
L Device:C C4
U 1 1 5D8FBA82
P 1700 6750
F 0 "C4" H 1815 6796 50  0000 L CNN
F 1 "100nF" H 1815 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1738 6600 50  0001 C CNN
F 3 "~" H 1700 6750 50  0001 C CNN
	1    1700 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5D8FBA88
P 2600 6750
F 0 "C5" H 2715 6796 50  0000 L CNN
F 1 "10uF" H 2715 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2638 6600 50  0001 C CNN
F 3 "~" H 2600 6750 50  0001 C CNN
	1    2600 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5D8FBA8E
P 3150 6750
F 0 "C8" H 3265 6796 50  0000 L CNN
F 1 "100nF" H 3265 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3188 6600 50  0001 C CNN
F 3 "~" H 3150 6750 50  0001 C CNN
	1    3150 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6600 1250 6550
Wire Wire Line
	3150 6550 3150 6600
Wire Wire Line
	2800 6550 3150 6550
Wire Wire Line
	2600 6600 2600 6550
Connection ~ 1700 6900
$Comp
L Device:C C1
U 1 1 5D8FBA99
P 1250 6750
F 0 "C1" H 1365 6796 50  0000 L CNN
F 1 "10uF" H 1365 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1288 6600 50  0001 C CNN
F 3 "~" H 1250 6750 50  0001 C CNN
	1    1250 6750
	1    0    0    -1  
$EndComp
Connection ~ 1250 6550
Wire Wire Line
	1250 6550 1450 6550
Wire Wire Line
	1700 6550 1700 6600
Connection ~ 1700 6550
Wire Wire Line
	1700 6550 1850 6550
Wire Wire Line
	1250 6900 1700 6900
Wire Wire Line
	1700 6900 2150 6900
Connection ~ 2600 6900
Wire Wire Line
	2600 6900 3150 6900
Wire Wire Line
	2450 6550 2600 6550
Connection ~ 2600 6550
Wire Wire Line
	2600 6550 2800 6550
Wire Wire Line
	3150 6550 3500 6550
Wire Wire Line
	3500 6550 3500 6450
Connection ~ 3150 6550
$Comp
L power:GND #PWR03
U 1 1 5D8FBAAE
P 2150 7000
F 0 "#PWR03" H 2150 6750 50  0001 C CNN
F 1 "GND" H 2155 6827 50  0000 C CNN
F 2 "" H 2150 7000 50  0001 C CNN
F 3 "" H 2150 7000 50  0001 C CNN
	1    2150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 6850 2150 6900
Connection ~ 2150 6900
Wire Wire Line
	2150 6900 2600 6900
Wire Wire Line
	2150 7000 2150 6900
$Comp
L power:+3V3 #PWR02
U 1 1 5D8FBAB8
P 1200 7650
F 0 "#PWR02" H 1200 7500 50  0001 C CNN
F 1 "+3V3" V 1215 7778 50  0000 L CNN
F 2 "" H 1200 7650 50  0001 C CNN
F 3 "" H 1200 7650 50  0001 C CNN
	1    1200 7650
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5D8FBABE
P 2900 7600
F 0 "TP3" H 2958 7718 50  0000 L CNN
F 1 "TestPoint" H 2958 7627 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 3100 7600 50  0001 C CNN
F 3 "~" H 3100 7600 50  0001 C CNN
	1    2900 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 7650 2900 7650
Wire Wire Line
	2900 7600 2900 7650
Connection ~ 2900 7650
Wire Wire Line
	2900 7650 3100 7650
Wire Notes Line
	3650 7250 3650 6050
Wire Notes Line
	850  6050 850  7250
Wire Wire Line
	1200 7650 1500 7650
Connection ~ 1500 7650
Wire Notes Line
	850  8400 3850 8400
Wire Notes Line
	3850 8400 3850 7350
Wire Notes Line
	3850 7350 850  7350
Wire Notes Line
	850  7350 850  8400
Wire Wire Line
	2950 1250 3150 1250
$Comp
L power:VAA #PWR06
U 1 1 5D8FBAD1
P 3050 1100
F 0 "#PWR06" H 3050 950 50  0001 C CNN
F 1 "VAA" H 3067 1273 50  0000 C CNN
F 2 "" H 3050 1100 50  0001 C CNN
F 3 "" H 3050 1100 50  0001 C CNN
	1    3050 1100
	1    0    0    -1  
$EndComp
Connection ~ 2800 1250
Wire Wire Line
	2800 1250 1650 1250
Wire Wire Line
	3050 1100 3050 1450
$Comp
L Device:L_Core_Ferrite L1
U 1 1 5D8FBADA
P 4050 6550
F 0 "L1" V 3869 6550 50  0000 C CNN
F 1 "330R-1000mA" V 3960 6550 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 6550 50  0001 C CNN
F 3 "~" H 4050 6550 50  0001 C CNN
	1    4050 6550
	0    1    1    0   
$EndComp
$Comp
L Device:L_Core_Ferrite L2
U 1 1 5D8FBAE0
P 4050 6900
F 0 "L2" V 3869 6900 50  0000 C CNN
F 1 "330R-1000mA" V 3960 6900 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 6900 50  0001 C CNN
F 3 "~" H 4050 6900 50  0001 C CNN
	1    4050 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 6900 3900 6900
Connection ~ 3150 6900
Wire Wire Line
	3500 6550 3900 6550
Connection ~ 3500 6550
Wire Wire Line
	4200 6550 4600 6550
Wire Wire Line
	4200 6900 4600 6900
$Comp
L Device:C C9
U 1 1 5D8FBAEC
P 4600 6750
F 0 "C9" H 4715 6796 50  0000 L CNN
F 1 "C" H 4715 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4638 6600 50  0001 C CNN
F 3 "~" H 4600 6750 50  0001 C CNN
	1    4600 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6600 4600 6550
Wire Wire Line
	4600 6550 4600 6450
Connection ~ 4600 6550
$Comp
L power:GNDA #PWR012
U 1 1 5D8FBAF5
P 4600 6950
F 0 "#PWR012" H 4600 6700 50  0001 C CNN
F 1 "GNDA" H 4605 6777 50  0000 C CNN
F 2 "" H 4600 6950 50  0001 C CNN
F 3 "" H 4600 6950 50  0001 C CNN
	1    4600 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6950 4600 6900
Connection ~ 4600 6900
$Comp
L power:VAA #PWR011
U 1 1 5D8FBAFD
P 4600 6450
F 0 "#PWR011" H 4600 6300 50  0001 C CNN
F 1 "VAA" H 4617 6623 50  0000 C CNN
F 2 "" H 4600 6450 50  0001 C CNN
F 3 "" H 4600 6450 50  0001 C CNN
	1    4600 6450
	1    0    0    -1  
$EndComp
Text Notes 4800 7200 0    50   ~ 0
Analog\nVoltage\nSupply
Wire Notes Line
	5150 7250 5150 6050
Wire Notes Line
	850  7250 5150 7250
Wire Notes Line
	850  6050 5150 6050
Text Label 8750 7900 0    50   ~ 0
CLK_OUT
Text Label 8750 8200 0    50   ~ 0
CLK_IN
Wire Wire Line
	8750 8200 9050 8200
Wire Wire Line
	7600 2550 7750 2550
Wire Wire Line
	7600 2450 7750 2450
Text Label 7750 2450 0    50   ~ 0
CLK_OUT
Text Label 7750 2550 0    50   ~ 0
CLK_IN
Wire Wire Line
	8350 2500 8350 2750
Wire Wire Line
	8150 2150 8150 2650
Connection ~ 8350 2150
Wire Wire Line
	8150 2150 8350 2150
Connection ~ 8700 2150
Wire Wire Line
	8350 2150 8350 2200
Wire Wire Line
	8700 2150 8350 2150
Wire Wire Line
	8700 2150 8700 2200
Wire Wire Line
	8800 2150 8700 2150
Wire Wire Line
	8350 2950 8700 2950
Wire Wire Line
	8700 2950 8800 2950
Connection ~ 8700 2950
Wire Wire Line
	8700 2500 8700 2950
$Comp
L power:GNDA #PWR028
U 1 1 5D8FBB1B
P 8800 2950
F 0 "#PWR028" H 8800 2700 50  0001 C CNN
F 1 "GNDA" V 8805 2822 50  0000 R CNN
F 2 "" H 8800 2950 50  0001 C CNN
F 3 "" H 8800 2950 50  0001 C CNN
	1    8800 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C17
U 1 1 5D8FBB21
P 8350 2350
F 0 "C17" H 8235 2396 50  0000 R CNN
F 1 "100nF" H 8235 2305 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8388 2200 50  0001 C CNN
F 3 "~" H 8350 2350 50  0001 C CNN
	1    8350 2350
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5D8FBB27
P 8700 2350
F 0 "C19" H 8585 2396 50  0000 R CNN
F 1 "1uF" H 8585 2305 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8738 2200 50  0001 C CNN
F 3 "~" H 8700 2350 50  0001 C CNN
	1    8700 2350
	-1   0    0    -1  
$EndComp
$Comp
L power:VAA #PWR027
U 1 1 5D8FBB2D
P 8800 2150
F 0 "#PWR027" H 8800 2000 50  0001 C CNN
F 1 "VAA" V 8817 2278 50  0000 L CNN
F 2 "" H 8800 2150 50  0001 C CNN
F 3 "" H 8800 2150 50  0001 C CNN
	1    8800 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 2650 8150 2650
Connection ~ 8150 2650
Wire Wire Line
	8150 2650 8150 2850
Wire Wire Line
	8150 2850 7600 2850
Wire Wire Line
	7600 2750 8350 2750
Connection ~ 8350 2750
Wire Wire Line
	7600 2950 8350 2950
Wire Wire Line
	9050 3050 9300 3050
Connection ~ 9050 3050
Text Label 9300 3050 0    50   ~ 0
V_OUT
Wire Wire Line
	3150 3650 3150 3700
Connection ~ 3150 3650
Wire Wire Line
	3400 3650 3400 3700
Text Notes 3650 3900 0    50   ~ 0
Image\nSensor
Text Notes 9800 8450 0    50   ~ 0
VC0703 Resonator
Wire Notes Line
	8600 7800 10550 7800
Wire Notes Line
	10550 7800 10550 8550
Wire Notes Line
	10550 8550 8600 8550
Wire Notes Line
	8600 8550 8600 7800
Text Notes 3350 5400 0    50   ~ 0
Sync Line Jumpers\n
Wire Notes Line
	2000 5450 2000 4050
Wire Notes Line
	2000 4050 4200 4050
Wire Notes Line
	4200 4050 4200 5450
Wire Notes Line
	2000 5450 4200 5450
Wire Wire Line
	1050 2850 2700 2850
Wire Wire Line
	1050 1250 1350 1250
Text Notes 10500 9050 0    50   ~ 0
Support Circuits
Text Notes 8800 5700 0    50   ~ 0
Core ICs
Wire Notes Line
	800  9100 800  6000
Wire Notes Line
	11250 6000 11250 9100
Connection ~ 8350 2950
Wire Wire Line
	8350 2750 8350 2950
Wire Notes Line
	800  6000 11250 6000
Wire Notes Line
	800  9100 11250 9100
Wire Wire Line
	2050 4600 3000 4600
Wire Wire Line
	3400 4600 3750 4600
Wire Wire Line
	3400 5250 3700 5250
Wire Wire Line
	2050 5250 3000 5250
Wire Wire Line
	5300 2750 5600 2750
$Comp
L MT9V034_Breakout:MT9V034 U3
U 1 1 5DBA462A
P 3200 2450
F 0 "U3" H 3325 3631 50  0000 C CNN
F 1 "MT9V034" H 3325 3540 50  0000 C CNN
F 2 "MT9V034_Breakout_Board:MT9V034-CLCC48" H 3800 1450 50  0001 C CNN
F 3 "" H 3800 1450 50  0001 C CNN
	1    3200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1800 4450 1800
Wire Wire Line
	1350 3650 2000 3650
Wire Wire Line
	2000 2350 2000 3650
Connection ~ 2000 3650
Wire Wire Line
	2000 3650 2950 3650
Text Notes 7550 7200 0    50   ~ 0
Link to PC/104 Board
Wire Notes Line
	5250 6050 5250 7900
Wire Wire Line
	5350 7150 5350 7550
Wire Wire Line
	5600 7150 5350 7150
$Comp
L Jumper:Jumper_2_Open JP3
U 1 1 5DD78B02
P 5800 7150
F 0 "JP3" H 5800 7385 50  0000 C CNN
F 1 "Jumper_2_Open" H 5800 7294 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 5800 7150 50  0001 C CNN
F 3 "~" H 5800 7150 50  0001 C CNN
	1    5800 7150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 5DD8E422
P 7600 6600
F 0 "J1" H 7708 6981 50  0000 C CNN
F 1 "Conn_01x05_Male" H 7708 6890 50  0000 C CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0571_1x05-1MP_P1.25mm_Vertical" H 7600 6600 50  0001 C CNN
F 3 "~" H 7600 6600 50  0001 C CNN
	1    7600 6600
	1    0    0    -1  
$EndComp
Text Label 8000 6700 0    50   ~ 0
V_OUT
$Comp
L power:+5V #PWR024
U 1 1 5DDE369B
P 8150 6350
F 0 "#PWR024" H 8150 6200 50  0001 C CNN
F 1 "+5V" H 8165 6523 50  0000 C CNN
F 2 "" H 8150 6350 50  0001 C CNN
F 3 "" H 8150 6350 50  0001 C CNN
	1    8150 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 6400 8150 6400
Wire Wire Line
	8150 6400 8150 6350
$Comp
L power:GND #PWR025
U 1 1 5DE19F46
P 8150 6850
F 0 "#PWR025" H 8150 6600 50  0001 C CNN
F 1 "GND" H 8155 6677 50  0000 C CNN
F 2 "" H 8150 6850 50  0001 C CNN
F 3 "" H 8150 6850 50  0001 C CNN
	1    8150 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 6800 8150 6850
Wire Wire Line
	7800 6800 8150 6800
Wire Wire Line
	7800 6700 8000 6700
Text Label 8000 6500 0    50   ~ 0
TEST_RX
Text Label 8000 6600 0    50   ~ 0
TEST_TX
Wire Wire Line
	7800 6500 8000 6500
Wire Wire Line
	7800 6600 8000 6600
Wire Notes Line
	7150 6100 7150 7300
Wire Notes Line
	7150 7300 8550 7300
Wire Notes Line
	8550 7300 8550 6100
Wire Notes Line
	8550 6100 7150 6100
Wire Notes Line
	9550 5750 9550 550 
Wire Notes Line
	9550 550  650  550 
Wire Notes Line
	650  550  650  5750
Wire Notes Line
	650  5750 9550 5750
$Comp
L Jumper:Jumper_2_Open JP4
U 1 1 5D8A590A
P 9300 6550
F 0 "JP4" H 9300 6785 50  0000 C CNN
F 1 "Jumper_2_Open" H 9300 6694 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 9300 6550 50  0001 C CNN
F 3 "~" H 9300 6550 50  0001 C CNN
	1    9300 6550
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Open JP5
U 1 1 5D8A61A2
P 9300 7000
F 0 "JP5" H 9300 7235 50  0000 C CNN
F 1 "Jumper_2_Open" H 9300 7144 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 9300 7000 50  0001 C CNN
F 3 "~" H 9300 7000 50  0001 C CNN
	1    9300 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 6550 9100 6550
Wire Wire Line
	8800 7000 9100 7000
Wire Wire Line
	9500 7000 10050 7000
Connection ~ 10050 7000
Wire Wire Line
	9500 6550 9900 6550
Connection ~ 9900 6550
$Comp
L Regulator_Linear:AP2127N-3.3 U1
U 1 1 5D8F3ED7
P 2150 6550
F 0 "U1" H 2150 6792 50  0000 C CNN
F 1 "AP2127N-3.3" H 2150 6701 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2150 6775 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AP2127.pdf" H 2150 6550 50  0001 C CNN
	1    2150 6550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
