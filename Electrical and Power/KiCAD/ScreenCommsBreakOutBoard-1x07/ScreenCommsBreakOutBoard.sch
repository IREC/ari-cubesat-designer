EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x07 J2
U 1 1 5FBD969E
P 3100 1350
F 0 "J2" H 3180 1392 50  0000 L CNN
F 1 "Conn_01x07" H 3180 1301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 3100 1350 50  0001 C CNN
F 3 "~" H 3100 1350 50  0001 C CNN
	1    3100 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J1
U 1 1 5FBDC0EF
P 2200 1350
F 0 "J1" H 2118 1867 50  0000 C CNN
F 1 "Conn_01x07" H 2118 1776 50  0000 C CNN
F 2 "Connector_Molex:Molex_PicoBlade_53261-0771_1x07-1MP_P1.25mm_Horizontal" H 2200 1350 50  0001 C CNN
F 3 "~" H 2200 1350 50  0001 C CNN
	1    2200 1350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2400 1050 2900 1050
Wire Wire Line
	2400 1150 2900 1150
Wire Wire Line
	2400 1250 2900 1250
Wire Wire Line
	2400 1350 2900 1350
Wire Wire Line
	2400 1450 2900 1450
Wire Wire Line
	2400 1550 2900 1550
Wire Wire Line
	2400 1650 2900 1650
$EndSCHEMATC
