EESchema Schematic File Version 4
LIBS:MT9V034_Breatout_Board-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT9V034_Breakout:MT9V034 U1
U 1 1 5D447C80
P 6650 3550
F 0 "U1" H 6775 4731 50  0000 C CNN
F 1 "MT9V034" H 6775 4640 50  0000 C CNN
F 2 "MT9V034_Breakout_Board:MT9V034-CLCC48" H 7250 2550 50  0001 C CNN
F 3 "" H 7250 2550 50  0001 C CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D45B680
P 4250 2800
F 0 "R1" H 4320 2846 50  0000 L CNN
F 1 "1.5K" H 4320 2755 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4180 2800 50  0001 C CNN
F 3 "~" H 4250 2800 50  0001 C CNN
	1    4250 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D45BBF0
P 4650 2800
F 0 "R2" H 4720 2846 50  0000 L CNN
F 1 "10K" H 4720 2755 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4580 2800 50  0001 C CNN
F 3 "~" H 4650 2800 50  0001 C CNN
	1    4650 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D45C29E
P 4650 4650
F 0 "C1" H 4765 4696 50  0000 L CNN
F 1 "0.1u" H 4765 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4688 4500 50  0001 C CNN
F 3 "~" H 4650 4650 50  0001 C CNN
	1    4650 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4600 6500 4850
Wire Wire Line
	6500 4850 6600 4850
Wire Wire Line
	6500 4850 6400 4850
Wire Wire Line
	4650 4850 4650 4800
Connection ~ 6500 4850
Wire Wire Line
	6400 4600 6400 4850
Connection ~ 6400 4850
Wire Wire Line
	6600 4850 6600 4600
Wire Wire Line
	4650 2950 4650 3350
$Comp
L Connector:Screw_Terminal_01x05 J1
U 1 1 5D45F840
P 2200 3000
F 0 "J1" H 2118 2575 50  0000 C CNN
F 1 "Screw_Terminal_01x05" H 2118 2666 50  0000 C CNN
F 2 "digikey-footprints:PinHeader_1x5_P2.5mm_Drill1.1mm" H 2200 3000 50  0001 C CNN
F 3 "~" H 2200 3000 50  0001 C CNN
	1    2200 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5D469A82
P 5300 6300
F 0 "R4" H 5370 6346 50  0000 L CNN
F 1 "10K" H 5370 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5230 6300 50  0001 C CNN
F 3 "~" H 5300 6300 50  0001 C CNN
	1    5300 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5D46A44F
P 5000 6300
F 0 "R3" H 5070 6346 50  0000 L CNN
F 1 "10K" H 5070 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4930 6300 50  0001 C CNN
F 3 "~" H 5000 6300 50  0001 C CNN
	1    5000 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5D46B3E5
P 5000 6600
F 0 "#PWR07" H 5000 6350 50  0001 C CNN
F 1 "GND" H 5005 6427 50  0000 C CNN
F 2 "" H 5000 6600 50  0001 C CNN
F 3 "" H 5000 6600 50  0001 C CNN
	1    5000 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6600 5000 6500
Wire Wire Line
	5300 6450 5300 6500
Wire Wire Line
	5300 6500 5000 6500
Connection ~ 5000 6500
Wire Wire Line
	5000 6500 5000 6450
Wire Wire Line
	4200 5350 4200 5550
Wire Wire Line
	4200 5550 4200 5900
Connection ~ 4200 5550
Wire Wire Line
	5000 5900 5000 6150
Wire Wire Line
	5300 5550 5300 6150
Wire Wire Line
	6150 3650 5300 3650
Wire Wire Line
	6150 3750 5300 3750
Text Label 5300 3650 0    50   ~ 0
ADR0
Text Label 5300 3750 0    50   ~ 0
ADR1
Wire Wire Line
	5000 5900 5600 5900
Connection ~ 5000 5900
Wire Wire Line
	5300 5550 5600 5550
Connection ~ 5300 5550
Text Label 5600 5550 0    50   ~ 0
ADR0
Text Label 5600 5900 0    50   ~ 0
ADR1
Wire Wire Line
	6150 3250 5850 3250
Wire Wire Line
	6150 3950 4250 3950
Wire Wire Line
	4250 3950 4250 2950
Wire Wire Line
	4650 4850 5150 4850
Connection ~ 5150 4850
Wire Wire Line
	5150 4850 6400 4850
Wire Wire Line
	5150 3450 6150 3450
Wire Wire Line
	5150 3450 4750 3450
Connection ~ 5150 3450
Text Label 4750 3450 0    50   ~ 0
EXP_OVR
Wire Wire Line
	2400 2800 2900 2800
Wire Wire Line
	2400 2900 2900 2900
Wire Wire Line
	2400 3000 2900 3000
Wire Wire Line
	2400 3100 2900 3100
Wire Wire Line
	2400 3200 2900 3200
Text Label 2900 2800 0    50   ~ 0
MASTER_CLK
Wire Wire Line
	4900 3150 6150 3150
Text Label 4900 3150 0    50   ~ 0
MASTER_CLK
Text Label 2900 2900 0    50   ~ 0
STANDBY
Wire Wire Line
	6150 3550 5300 3550
Text Label 5300 3550 0    50   ~ 0
STANDBY
$Comp
L Device:Jumper JP3
U 1 1 5D48B61B
P 7100 5450
F 0 "JP3" V 7054 5577 50  0000 L CNN
F 1 "Jumper" V 7145 5577 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 7100 5450 50  0001 C CNN
F 3 "~" H 7100 5450 50  0001 C CNN
	1    7100 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4600 6700 4700
Wire Wire Line
	6800 5450 6600 5450
Wire Wire Line
	6600 5450 6600 4850
Connection ~ 6600 4850
$Comp
L power:GNDA #PWR012
U 1 1 5D490069
P 7650 5550
F 0 "#PWR012" H 7650 5300 50  0001 C CNN
F 1 "GNDA" H 7655 5377 50  0000 C CNN
F 2 "" H 7650 5550 50  0001 C CNN
F 3 "" H 7650 5550 50  0001 C CNN
	1    7650 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4700 7650 5450
$Comp
L power:GNDD #PWR010
U 1 1 5D491BD7
P 6600 5550
F 0 "#PWR010" H 6600 5300 50  0001 C CNN
F 1 "GNDD" H 6604 5395 50  0000 C CNN
F 2 "" H 6600 5550 50  0001 C CNN
F 3 "" H 6600 5550 50  0001 C CNN
	1    6600 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5550 6600 5450
Connection ~ 6600 5450
$Comp
L power:VDD #PWR011
U 1 1 5D4938F6
P 6900 1350
F 0 "#PWR011" H 6900 1200 50  0001 C CNN
F 1 "VDD" H 6917 1523 50  0000 C CNN
F 2 "" H 6900 1350 50  0001 C CNN
F 3 "" H 6900 1350 50  0001 C CNN
	1    6900 1350
	1    0    0    -1  
$EndComp
$Comp
L power:VAA #PWR09
U 1 1 5D494082
P 6300 1350
F 0 "#PWR09" H 6300 1200 50  0001 C CNN
F 1 "VAA" H 6317 1523 50  0000 C CNN
F 2 "" H 6300 1350 50  0001 C CNN
F 3 "" H 6300 1350 50  0001 C CNN
	1    6300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP2
U 1 1 5D496608
P 6600 1600
F 0 "JP2" H 6600 1864 50  0000 C CNN
F 1 "Jumper" H 6600 1773 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 6600 1600 50  0001 C CNN
F 3 "~" H 6600 1600 50  0001 C CNN
	1    6600 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 1350 6300 1600
Wire Wire Line
	6300 1600 6300 1850
Wire Wire Line
	6300 1850 6500 1850
Connection ~ 6300 1600
Wire Wire Line
	6600 1850 6900 1850
Wire Wire Line
	6900 1850 6900 1600
Wire Wire Line
	6900 1600 6900 1350
Connection ~ 6900 1600
$Comp
L Device:Jumper JP1
U 1 1 5D4A0EEA
P 6000 1600
F 0 "JP1" H 6000 1864 50  0000 C CNN
F 1 "Jumper" H 6000 1773 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 6000 1600 50  0001 C CNN
F 3 "~" H 6000 1600 50  0001 C CNN
	1    6000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1600 5700 2000
Wire Wire Line
	5700 2000 6400 2000
Wire Wire Line
	6400 2000 6400 2550
Wire Wire Line
	6500 1850 6500 2550
Wire Wire Line
	6600 1850 6600 2350
$Comp
L power:VDDF #PWR08
U 1 1 5D4A66A1
P 5700 1350
F 0 "#PWR08" H 5700 1200 50  0001 C CNN
F 1 "VDDF" H 5717 1523 50  0000 C CNN
F 2 "" H 5700 1350 50  0001 C CNN
F 3 "" H 5700 1350 50  0001 C CNN
	1    5700 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1350 5700 1600
Connection ~ 5700 1600
Wire Wire Line
	7400 3500 7750 3500
Wire Wire Line
	7400 3600 7750 3600
Wire Wire Line
	7400 3700 7750 3700
Wire Wire Line
	7400 3800 7750 3800
Wire Wire Line
	7400 3900 7750 3900
Wire Wire Line
	7400 4000 7750 4000
Wire Wire Line
	7400 4100 7750 4100
Wire Wire Line
	7400 4200 7750 4200
Wire Notes Line
	7450 4550 8950 4550
Text Notes 8400 4500 0    50   ~ 0
Digital Output
Wire Notes Line
	3800 5100 3800 6850
Wire Notes Line
	3800 6850 6150 6850
Wire Notes Line
	6150 6850 6150 5100
Wire Notes Line
	6150 5100 3800 5100
Text Notes 5350 6800 0    50   ~ 0
I2C Address Select
Wire Notes Line
	5250 2100 7400 2100
Wire Notes Line
	7400 2100 7400 1050
Wire Notes Line
	7400 1050 5250 1050
Wire Notes Line
	5250 1050 5250 2100
Text Notes 6750 2050 0    50   ~ 0
Shared\nSupply Selector
Wire Wire Line
	6700 4700 7650 4700
Wire Wire Line
	7400 5450 7650 5450
Connection ~ 7650 5450
Wire Wire Line
	7650 5450 7650 5550
Wire Notes Line
	6300 5000 6300 5900
Wire Notes Line
	6300 5900 8450 5900
Wire Notes Line
	8450 5900 8450 5000
Wire Notes Line
	8450 5000 6300 5000
Text Notes 7500 5850 0    50   ~ 0
Shared Ground Selector
Wire Notes Line
	7450 2650 7450 4550
Wire Notes Line
	8950 2650 8950 4550
Text Notes 8350 3150 0    50   ~ 0
Status Output
Wire Wire Line
	6600 2350 5850 2350
Wire Wire Line
	4250 2350 4250 2650
Connection ~ 6600 2350
Wire Wire Line
	6600 2350 6600 2550
Wire Wire Line
	4650 2650 4650 2350
Wire Wire Line
	4650 2350 4250 2350
Wire Wire Line
	5850 2350 5850 3250
Wire Wire Line
	4650 2350 5850 2350
Connection ~ 4650 2350
Connection ~ 5850 2350
Wire Wire Line
	6150 3350 4650 3350
Connection ~ 4650 3350
Wire Wire Line
	4650 3350 4650 4500
Text Label 2900 3000 0    50   ~ 0
SDA
Text Label 2900 3100 0    50   ~ 0
SCL
Wire Wire Line
	6150 3850 5300 3850
Text Label 5300 3850 0    50   ~ 0
SCL
Text Label 5300 3950 0    50   ~ 0
SDA
Text Label 2900 3200 0    50   ~ 0
EXP_OVR
$Comp
L Connector:Screw_Terminal_01x05 J2
U 1 1 5D50B0F6
P 2200 4450
F 0 "J2" H 2118 4025 50  0000 C CNN
F 1 "Screw_Terminal_01x05" H 2118 4116 50  0000 C CNN
F 2 "digikey-footprints:PinHeader_1x5_P2.5mm_Drill1.1mm" H 2200 4450 50  0001 C CNN
F 3 "~" H 2200 4450 50  0001 C CNN
	1    2200 4450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 2550 6700 2350
Wire Wire Line
	6700 2350 6600 2350
Wire Notes Line
	1700 2400 1700 3500
Wire Notes Line
	1700 3500 4000 3500
Wire Notes Line
	4000 3500 4000 2400
Wire Notes Line
	4000 2400 1700 2400
Text Notes 3300 3450 0    50   ~ 0
Controller Inputs
Wire Wire Line
	2400 4650 2900 4650
Wire Wire Line
	2400 4250 2900 4250
$Comp
L power:GNDD #PWR02
U 1 1 5D5275EF
P 2900 4650
F 0 "#PWR02" H 2900 4400 50  0001 C CNN
F 1 "GNDD" H 2904 4495 50  0000 C CNN
F 2 "" H 2900 4650 50  0001 C CNN
F 3 "" H 2900 4650 50  0001 C CNN
	1    2900 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR04
U 1 1 5D528203
P 3150 4650
F 0 "#PWR04" H 3150 4400 50  0001 C CNN
F 1 "GNDA" H 3155 4477 50  0000 C CNN
F 2 "" H 3150 4650 50  0001 C CNN
F 3 "" H 3150 4650 50  0001 C CNN
	1    3150 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4550 3150 4650
Wire Wire Line
	2400 4550 3150 4550
$Comp
L power:VAA #PWR01
U 1 1 5D52BA21
P 2900 4250
F 0 "#PWR01" H 2900 4100 50  0001 C CNN
F 1 "VAA" H 2917 4423 50  0000 C CNN
F 2 "" H 2900 4250 50  0001 C CNN
F 3 "" H 2900 4250 50  0001 C CNN
	1    2900 4250
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR03
U 1 1 5D52BDB6
P 3150 4250
F 0 "#PWR03" H 3150 4100 50  0001 C CNN
F 1 "VDD" H 3167 4423 50  0000 C CNN
F 2 "" H 3150 4250 50  0001 C CNN
F 3 "" H 3150 4250 50  0001 C CNN
	1    3150 4250
	1    0    0    -1  
$EndComp
$Comp
L power:VDDF #PWR05
U 1 1 5D52C184
P 3400 4250
F 0 "#PWR05" H 3400 4100 50  0001 C CNN
F 1 "VDDF" H 3417 4423 50  0000 C CNN
F 2 "" H 3400 4250 50  0001 C CNN
F 3 "" H 3400 4250 50  0001 C CNN
	1    3400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4350 3150 4250
Wire Wire Line
	2400 4350 3150 4350
Wire Wire Line
	3400 4450 3400 4250
Wire Wire Line
	2400 4450 3400 4450
Wire Notes Line
	1700 3900 4000 3900
Wire Notes Line
	4000 3900 4000 5000
Wire Notes Line
	4000 5000 1700 5000
Wire Notes Line
	1700 3900 1700 5000
Text Notes 3200 4950 0    50   ~ 0
Power and Ground
$Comp
L power:VDD #PWR06
U 1 1 5D55FDDD
P 4200 5350
F 0 "#PWR06" H 4200 5200 50  0001 C CNN
F 1 "VDD" H 4217 5523 50  0000 C CNN
F 2 "" H 4200 5350 50  0001 C CNN
F 3 "" H 4200 5350 50  0001 C CNN
	1    4200 5350
	1    0    0    -1  
$EndComp
Text Notes 7400 7500 0    50   ~ 0
MT9V034 Testing Board
Text Notes 8150 7650 0    50   ~ 0
2019/08/02nd\n
Text Notes 10550 7650 0    50   ~ 0
0.1
Text Notes 7000 6650 0    50   ~ 0
CON-CAM-010-010
Wire Notes Line
	7450 2650 8950 2650
Wire Wire Line
	7400 3100 7750 3100
Wire Wire Line
	7400 3000 7750 3000
Wire Wire Line
	7400 2900 7750 2900
Wire Wire Line
	7400 2800 7750 2800
Wire Notes Line
	8950 3200 7450 3200
Wire Wire Line
	7400 3300 7750 3300
$Comp
L Connector:Screw_Terminal_01x04 J6
U 1 1 5D46172B
P 7950 2900
F 0 "J6" H 8030 2892 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 8030 2801 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x4_P2.54mm_Drill1.02mm" H 7950 2900 50  0001 C CNN
F 3 "~" H 7950 2900 50  0001 C CNN
	1    7950 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x10 J7
U 1 1 5D458EFD
P 7950 3700
F 0 "J7" H 8030 3692 50  0000 L CNN
F 1 "Screw_Terminal_01x10" H 8030 3601 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-10-2.54_1x10_P2.54mm_Horizontal" H 7950 3700 50  0001 C CNN
F 3 "~" H 7950 3700 50  0001 C CNN
	1    7950 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3400 7750 3400
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:22-23-2021 J4
U 1 1 5D583A9C
P 4550 6000
F 0 "J4" H 4728 6054 50  0000 L CNN
F 1 "22-23-2021" H 4728 5963 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm_Drill1.02mm" H 4750 6200 60  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 4750 6300 60  0001 L CNN
F 4 "WM4200-ND" H 4750 6400 60  0001 L CNN "Digi-Key_PN"
F 5 "22-23-2021" H 4750 6500 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 4750 6600 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 4750 6700 60  0001 L CNN "Family"
F 8 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 4750 6800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/molex/22-23-2021/WM4200-ND/26667" H 4750 6900 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 2POS 2.54MM" H 4750 7000 60  0001 L CNN "Description"
F 11 "Molex" H 4750 7100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4750 7200 60  0001 L CNN "Status"
	1    4550 6000
	1    0    0    -1  
$EndComp
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:22-23-2021 J3
U 1 1 5D58DBA0
P 4550 5650
F 0 "J3" H 4728 5704 50  0000 L CNN
F 1 "22-23-2021" H 4728 5613 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm_Drill1.02mm" H 4750 5850 60  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 4750 5950 60  0001 L CNN
F 4 "WM4200-ND" H 4750 6050 60  0001 L CNN "Digi-Key_PN"
F 5 "22-23-2021" H 4750 6150 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 4750 6250 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 4750 6350 60  0001 L CNN "Family"
F 8 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 4750 6450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/molex/22-23-2021/WM4200-ND/26667" H 4750 6550 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 2POS 2.54MM" H 4750 6650 60  0001 L CNN "Description"
F 11 "Molex" H 4750 6750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4750 6850 60  0001 L CNN "Status"
	1    4550 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5550 4550 5550
Wire Wire Line
	4650 5550 5300 5550
Wire Wire Line
	4200 5900 4550 5900
Wire Wire Line
	4650 5900 5000 5900
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:22-23-2021 J5
U 1 1 5D5A0E5A
P 5250 4700
F 0 "J5" H 5428 4754 50  0000 L CNN
F 1 "22-23-2021" H 5428 4663 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm_Drill1.02mm" H 5450 4900 60  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 5450 5000 60  0001 L CNN
F 4 "WM4200-ND" H 5450 5100 60  0001 L CNN "Digi-Key_PN"
F 5 "22-23-2021" H 5450 5200 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 5450 5300 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 5450 5400 60  0001 L CNN "Family"
F 8 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 5450 5500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/molex/22-23-2021/WM4200-ND/26667" H 5450 5600 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 2POS 2.54MM" H 5450 5700 60  0001 L CNN "Description"
F 11 "Molex" H 5450 5800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5450 5900 60  0001 L CNN "Status"
	1    5250 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 4700 5150 4850
Wire Wire Line
	5150 3450 5150 4600
$EndSCHEMATC
