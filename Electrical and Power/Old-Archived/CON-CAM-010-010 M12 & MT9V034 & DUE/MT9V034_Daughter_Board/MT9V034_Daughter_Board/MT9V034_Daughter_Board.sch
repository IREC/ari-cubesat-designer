EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT9V034_Breakout:MT9V034 U?
U 1 1 5D49F9DF
P 6050 3450
F 0 "U?" H 6175 4631 50  0000 C CNN
F 1 "MT9V034" H 6175 4540 50  0000 C CNN
F 2 "" H 6650 2450 50  0001 C CNN
F 3 "" H 6650 2450 50  0001 C CNN
	1    6050 3450
	1    0    0    -1  
$EndComp
$Comp
L dk_Interface-I-O-Expanders:MCP23017-E_SO U?
U 1 1 5D4A41A7
P 8000 3050
F 0 "U?" H 8100 4053 60  0000 C CNN
F 1 "MCP23017-E_SO" H 8100 3947 60  0000 C CNN
F 2 "digikey-footprints:SOIC-28_W7.5mm" H 8200 3250 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en023709" H 8200 3350 60  0001 L CNN
F 4 "MCP23017-E/SO-ND" H 8200 3450 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP23017-E/SO" H 8200 3550 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8200 3650 60  0001 L CNN "Category"
F 7 "Interface - I/O Expanders" H 8200 3750 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en023709" H 8200 3850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP23017-E-SO/MCP23017-E-SO-ND/894271" H 8200 3950 60  0001 L CNN "DK_Detail_Page"
F 10 "IC I/O EXPANDER I2C 16B 28SOIC" H 8200 4050 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 8200 4150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8200 4250 60  0001 L CNN "Status"
	1    8000 3050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
