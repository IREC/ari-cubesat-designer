EESchema Schematic File Version 4
LIBS:CON-CAM-020-010 - CameraCube-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OVM7690-R20A:OVM7690-R20A U?
U 1 1 5D2772D4
P 3650 3400
F 0 "U?" H 3600 4167 50  0000 C CNN
F 1 "OVM7690-R20A" H 3600 4076 50  0000 C CNN
F 2 "OVM7690-R20A" H 3650 3400 50  0001 L BNN
F 3 "" H 3650 3400 50  0001 L BNN
F 4 "Unavailable" H 3650 3400 50  0001 L BNN "Field4"
F 5 "OVM7690-R20A" H 3650 3400 50  0001 L BNN "Field5"
F 6 "None" H 3650 3400 50  0001 L BNN "Field6"
F 7 "- Omnivision Technologies" H 3650 3400 50  0001 L BNN "Field7"
F 8 "Omnivision Technologies" H 3650 3400 50  0001 L BNN "Field8"
	1    3650 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D279AB5
P 6350 2700
F 0 "#PWR?" H 6350 2550 50  0001 C CNN
F 1 "+3.3V" H 6365 2873 50  0000 C CNN
F 2 "" H 6350 2700 50  0001 C CNN
F 3 "" H 6350 2700 50  0001 C CNN
	1    6350 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D27B9EB
P 4700 3650
F 0 "C?" H 4585 3604 50  0000 R CNN
F 1 "100nF" H 4585 3695 50  0000 R CNN
F 2 "" H 4738 3500 50  0001 C CNN
F 3 "~" H 4700 3650 50  0001 C CNN
	1    4700 3650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D27C47C
P 5200 3650
F 0 "C?" H 5085 3604 50  0000 R CNN
F 1 "100nF" H 5085 3695 50  0000 R CNN
F 2 "" H 5238 3500 50  0001 C CNN
F 3 "~" H 5200 3650 50  0001 C CNN
	1    5200 3650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D27EEF1
P 7100 4650
F 0 "C?" H 6985 4604 50  0000 R CNN
F 1 "100nF" H 6985 4695 50  0000 R CNN
F 2 "" H 7138 4500 50  0001 C CNN
F 3 "~" H 7100 4650 50  0001 C CNN
	1    7100 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D27F0BF
P 5850 3650
F 0 "C?" H 5735 3604 50  0000 R CNN
F 1 "100nF" H 5735 3695 50  0000 R CNN
F 2 "" H 5888 3500 50  0001 C CNN
F 3 "~" H 5850 3650 50  0001 C CNN
	1    5850 3650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D27F27B
P 8000 4650
F 0 "C?" H 7885 4604 50  0000 R CNN
F 1 "100nF" H 7885 4695 50  0000 R CNN
F 2 "" H 8038 4500 50  0001 C CNN
F 3 "~" H 8000 4650 50  0001 C CNN
	1    8000 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5D27F4A2
P 8550 4650
F 0 "C?" H 8435 4604 50  0000 R CNN
F 1 "100nF" H 8435 4695 50  0000 R CNN
F 2 "" H 8588 4500 50  0001 C CNN
F 3 "~" H 8550 4650 50  0001 C CNN
	1    8550 4650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D2817DE
P 5200 4000
F 0 "#PWR?" H 5200 3750 50  0001 C CNN
F 1 "GND" H 5205 3827 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5200 3900
Wire Wire Line
	5200 3900 4700 3900
Wire Wire Line
	4700 3900 4700 3800
Wire Wire Line
	5200 3800 5200 3900
Connection ~ 5200 3900
Wire Wire Line
	5200 3300 4250 3300
Wire Wire Line
	5200 3500 5200 3300
Wire Wire Line
	4700 3400 4250 3400
Wire Wire Line
	4700 3500 4700 3400
$Comp
L Device:C C?
U 1 1 5D2774D9
P 6350 3650
F 0 "C?" H 6235 3604 50  0000 R CNN
F 1 "100nF" H 6235 3695 50  0000 R CNN
F 2 "" H 6388 3500 50  0001 C CNN
F 3 "~" H 6350 3650 50  0001 C CNN
	1    6350 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 3500 5850 3000
Wire Wire Line
	5200 3900 5650 3900
Wire Wire Line
	4250 3100 5650 3100
Wire Wire Line
	5650 3100 5650 3900
Connection ~ 5650 3900
Wire Wire Line
	5650 3900 5850 3900
Wire Wire Line
	4250 3000 5850 3000
Wire Wire Line
	4250 2900 6350 2900
Wire Wire Line
	6350 2900 6350 3500
Wire Wire Line
	6350 2900 6350 2700
Connection ~ 6350 2900
Wire Wire Line
	6350 3900 5850 3900
Connection ~ 5850 3900
Wire Wire Line
	5850 3800 5850 3900
Wire Wire Line
	6350 3800 6350 3900
$Comp
L power:GND #PWR?
U 1 1 5D29088C
P 5050 6750
F 0 "#PWR?" H 5050 6500 50  0001 C CNN
F 1 "GND" H 5055 6577 50  0000 C CNN
F 2 "" H 5050 6750 50  0001 C CNN
F 3 "" H 5050 6750 50  0001 C CNN
	1    5050 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D2910F7
P 5050 6100
F 0 "C?" H 5165 6146 50  0000 L CNN
F 1 "CP1" H 5165 6055 50  0000 L CNN
F 2 "" H 5050 6100 50  0001 C CNN
F 3 "~" H 5050 6100 50  0001 C CNN
	1    5050 6100
	1    0    0    -1  
$EndComp
Text Label 5100 5600 0    50   ~ 0
V28
Text Label 6500 3000 0    50   ~ 0
AVDD
Wire Wire Line
	5850 3000 6500 3000
Connection ~ 5850 3000
$Comp
L Connector:Screw_Terminal_01x10 J?
U 1 1 5D292879
P 9800 3150
F 0 "J?" H 9880 3142 50  0000 L CNN
F 1 "Screw_Terminal_01x10" H 9880 3051 50  0000 L CNN
F 2 "" H 9800 3150 50  0001 C CNN
F 3 "~" H 9800 3150 50  0001 C CNN
	1    9800 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x10 J?
U 1 1 5D29400B
P 9800 4300
F 0 "J?" H 9880 4292 50  0000 L CNN
F 1 "Screw_Terminal_01x10" H 9880 4201 50  0000 L CNN
F 2 "" H 9800 4300 50  0001 C CNN
F 3 "~" H 9800 4300 50  0001 C CNN
	1    9800 4300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
