# ARI CubeSat Designer

This repository contains the mechanical and electronic models of the AuroraSat-1, Northern Images Mission Payload. For more information please visit nwtresearch.com

# Mechanical Models

## Quick Start:
- All mechanical models for this project are generated using FreeCAD 0.19 (https://www.freecadweb.org/downloads.php).
- The current tested version is 22765 released 2020/10/17 at 8am. Mechanical assemblies are using Assembly4 (https://wiki.freecadweb.org/Assembly4_Workbench)
- Aside from the models inteded for AI&T, all models are grouped as either those that will be imported to KiCAD (FreeCAD Files) or exported from KiCAD (STEP files).

# Electronic Models

## Quick Start:
- All PCBs are generated using KiCAD (https://kicad.org/)
- The current tested version is KiCAD 5.1.2-2.
- There are two custom PCBs and one off the shelf PCB in the design of the payload.
